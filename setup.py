
from setuptools import setup


setup(name='simdy',
      version='0.6.1',
      description='Fast computing using simd instructions.',
      author='Mario Vidov',
      author_email='mvidov@yahoo.com',
      url='https://bitbucket.org/Tahir007/simdy',
      license='MIT',
      classifiers=['Intended Audience :: Developers',
                   'Topic :: Software Development :: Code Generators',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   ],
      keywords='simd SSE AVX FMA AVX-512',
      packages=['simdy'],
      package_dir={'simdy': 'simdy'},
      install_requires=['tdasm'],
      )
