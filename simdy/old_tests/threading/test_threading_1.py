
import unittest
from simdy import Kernel, int32


class TestThreading1(unittest.TestCase):

    def test_threading_1(self):
        source = """
p1 = p2 + p3
        """

        args = [('p1', int32()), ('p2', int32()), ('p3', int32())]
        kernel = Kernel(source, args=args, nthreads=2)
        kernel.set_value('p2', 2, thread_idx=0)
        kernel.set_value('p2', 3, thread_idx=1)
        kernel.set_value('p3', 5, thread_idx=0)
        kernel.set_value('p3', 10, thread_idx=1)
        kernel.run()
        self.assertEqual(kernel.get_value('p1', thread_idx=0), 7)
        self.assertEqual(kernel.get_value('p1', thread_idx=1), 13)

if __name__ == "__main__":
    unittest.main()
