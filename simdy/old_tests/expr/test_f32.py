
import unittest
from simdy import Kernel, float32, ISet


class ExpressionTestsF32(unittest.TestCase):

    def _test_expr_f32_1(self, iset, FMA=False):
        source = """
t1, t2, t3 = float32(2.3), float32(-6.9), float32(1.4)
t4, t5, t6 = float32(4.6), float32(8.0), float32(-6.6)

p1 = 2.0 * t1 + t2
p2 = t2 - t1 * t5
p3 = t1 / t4 + 1.0 - t1 * t6 - (t4 + t1)
p4 = t3 - (t1 * 2.0) - 4.4 - t4
        """

        args = [('p1', float32()), ('p2', float32()), ('p3', float32()), ('p4', float32())]
        kernel = Kernel(source, args=args, iset=iset, fma=FMA)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 2.0 * 2.3 - 6.9, places=6)
        self.assertAlmostEqual(kernel.get_value('p2'), -6.9 - 2.3 * 8.0, places=5)
        self.assertAlmostEqual(kernel.get_value('p3'), 2.3 / 4.6 + 1.0 - 2.3 * -6.6 - (4.6 + 2.3), places=5)
        self.assertAlmostEqual(kernel.get_value('p4'), 1.4 - (2.3 * 2.0) - 4.4 - 4.6, places=6)

    def test_expr_f32_1(self):
        self._test_expr_f32_1(ISet.AVX512, FMA=True)
        self._test_expr_f32_1(ISet.AVX512, FMA=False)
        self._test_expr_f32_1(ISet.AVX2, FMA=True)
        self._test_expr_f32_1(ISet.AVX2, FMA=False)
        self._test_expr_f32_1(ISet.AVX, FMA=False)
        self._test_expr_f32_1(ISet.SSE, FMA=False)

    def _test_expr_f32_4(self, iset):
        source = """
t1, t2, t3 = float32(8.0), float32(-4.0), float32(3.0)
t4, t5, t6 = float32(5.0), float32(-12.0), float32(9.0)

if t1 > t2 + 5:
    p1 = float32(13.0)

if t3:
    p2 = float32(5.0)

if t1 == float32(3.0):
    p3 = float32(55.0)
elif t2 == float32(-4.0):
    p3 = float32(22.0)
else:
    p3 = float32(11.0)

if t1 + t2 < t4 * t2 / t1:
    p4 = float32(12.0)
else:
    p4 = float32(9.0)
        """

        args = [('p1', float32()), ('p2', float32()), ('p3', float32()), ('p4', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 13.0)
        self.assertEqual(kernel.get_value('p2'), 5.0)
        self.assertEqual(kernel.get_value('p3'), 22.0)
        self.assertEqual(kernel.get_value('p4'), 9.0)

    def test_expr_f32_4(self):
        self._test_expr_f32_4(ISet.AVX512)
        self._test_expr_f32_4(ISet.AVX)
        self._test_expr_f32_4(ISet.SSE)

    def _test_expr_f32_6(self, iset):
        source = """
t1, t2, t3 = float32(8.0), float32(-4.0), float32(3.0)
t4, t5, t6 = float32(5.0), float32(-12.0), float32(9.0)

i = float32(0.0)
p1 = float32(0.0)
while i < t1:
    p1 += 1.0
    i += 1.0

i = float32(5.0)
p2 = float32(0.0)
while i > float32(0.0):
    j = float32(0.0)
    while j < t3:
        p2 += float32(1.0)
        j += float32(1.0)
    i -= 1.0
        """

        args = [('p1', float32()), ('p2', float32()), ('p3', float32()), ('p4', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 8.0)
        self.assertEqual(kernel.get_value('p2'), 15.0)

    def test_expr_f32_6(self):
        self._test_expr_f32_6(ISet.AVX512)
        self._test_expr_f32_6(ISet.AVX)
        self._test_expr_f32_6(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
