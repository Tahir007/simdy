import unittest
from simdy import Kernel, int32, int64, float32, float64, ISet, int32x2, int32x3, int32x4,\
    int32x8, int32x16, int64x2, int64x3, int64x4, int64x8, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16


class ExpressionTests1(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _check_almost(self, src, dst, n):
        for i in range(n):
            self.assertAlmostEqual(src[i], dst[i])

    def _test_unary1(self, iset):
        source = """
p2 = -p1
p3 = ~p1
p4 = -p1 + ~p1

p6 = -p5
p7 = ~p5
p8 = p5 + ~p5

p11 = -p9 + p10
p12 = p9 + -p10

p15 = -p13 + p14
p16 = p13 + -p14

p19 = -p17 + p18
p20 = p17 + -p18

p23 = -p21 + p22
p24 = p21 + -p22

p27 = -p25 + p26
p28 = p25 + -p26

p31 = -p29 + p30
p32 = p29 + -p30

p35 = -p33 + p34
p36 = p33 + -p34

p39 = -p37 + p38
p40 = p37 + -p38

p43 = -p41 + p42
p44 = p41 + -p42

p47 = -p45 + p46
p48 = p45 + -p46

p51 = -p49 + p50
p52 = p49 + -p50

p55 = -p53 + p54
p56 = p53 + -p54

p59 = -p57 + p58
p60 = p57 + -p58

p63 = -p61 + p62
p64 = p61 + -p62

p67 = -p65 + p66
p68 = p65 + -p66

p71 = -p69 + p70
p72 = p69 + -p70

p75 = -p73 + p74
p76 = p73 + -p74

p79 = -p77 + p78
p80 = p77 + -p78

p83 = -p81 + p82
p84 = p81 + -p82

p87 = -p85 + p86
p88 = p85 + -p86
        """

        args = [('p1', int32(3)), ('p2', int32()),
                ('p3', int32()), ('p4', int32()),
                ('p5', int64(5)), ('p6', int64()),
                ('p7', int64()), ('p8', int64()),
                ('p9', int32x2(7, 3)), ('p10', int32x2(1, 2)),
                ('p11', int32x2()), ('p12', int32x2()),
                ('p13', int32x3(7, 3, 5)), ('p14', int32x3(1, 2, 3)),
                ('p15', int32x3()), ('p16', int32x3()),
                ('p17', int32x4(7, 3, 5, -9)), ('p18', int32x4(1, 2, 3, 4)),
                ('p19', int32x4()), ('p20', int32x4()),
                ('p21', int32x8(7, 3, 5, -9, 4, 5, 2, 2)), ('p22', int32x8(1, 2, 3, 4, -5, 2, 7, 9)),
                ('p23', int32x8()), ('p24', int32x8()),
                ('p25', int32x16(7, 3, 5, -9, 4, 5, 2, 2, 3, 3, 3, 4, -9, 5, 5, 5)), ('p26', int32x16(1, 2, 3, 4, -5, 2, 7, 9, 1, 1, 1, 1, 1, 1, 1, 1)),
                ('p27', int32x16()), ('p28', int32x16()),

                ('p29', int64x2(7, 3)), ('p30', int64x2(1, 2)),
                ('p31', int64x2()), ('p32', int64x2()),
                ('p33', int64x3(7, 3, 5)), ('p34', int64x3(1, 2, 3)),
                ('p35', int64x3()), ('p36', int64x3()),
                ('p37', int64x4(7, 3, 5, -9)), ('p38', int64x4(1, 2, 3, 4)),
                ('p39', int64x4()), ('p40', int64x4()),
                ('p41', int64x8(7, 3, 5, -9, 4, 5, 2, 2)), ('p42', int64x8(1, 2, 3, 4, -5, 2, 7, 9)),
                ('p43', int64x8()), ('p44', int64x8()),

                ('p45', float64(5)), ('p46', float64(3)),
                ('p47', float64()), ('p48', float64()),

                ('p49', float32(5)), ('p50', float32(3)),
                ('p51', float32()), ('p52', float32()),

                ('p53', float64x2(7, 3)), ('p54', float64x2(1, 2)),
                ('p55', float64x2()), ('p56', float64x2()),
                ('p57', float64x3(7, 3, 5)), ('p58', float64x3(1, 2, 3)),
                ('p59', float64x3()), ('p60', float64x3()),
                ('p61', float64x4(7, 3, 5, -9)), ('p62', float64x4(1, 2, 3, 4)),
                ('p63', float64x4()), ('p64', float64x4()),
                ('p65', float64x8(7, 3, 5, -9, 4, 5, 2, 2)), ('p66', float64x8(1, 2, 3, 4, -5, 2, 7, 9)),
                ('p67', float64x8()), ('p68', float64x8()),

                ('p69', float32x2(7, 3)), ('p70', float32x2(1, 2)),
                ('p71', float32x2()), ('p72', float32x2()),
                ('p73', float32x3(7, 3, 5)), ('p74', float32x3(1, 2, 3)),
                ('p75', float32x3()), ('p76', float32x3()),
                ('p77', float32x4(7, 3, 5, -9)), ('p78', float32x4(1, 2, 3, 4)),
                ('p79', float32x4()), ('p80', float32x4()),
                ('p81', float32x8(7, 3, 5, -9, 4, 5, 2, 2)), ('p82', float32x8(1, 2, 3, 4, -5, 2, 7, 9)),
                ('p83', float32x8()), ('p84', float32x8()),
                ('p85', float32x16(7, 3, 5, -9, 4, 5, 2, 2, 3, 3, 3, 4, -9, 5, 5, 5)),
                ('p86', float32x16(1, 2, 3, 4, -5, 2, 7, 9, 1, 1, 1, 1, 1, 1, 1, 1)),
                ('p87', float32x16()), ('p88', float32x16()),

                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p = kernel.get_value('p1')
        self.assertEqual(kernel.get_value('p2'), -p)
        self.assertEqual(kernel.get_value('p3'), ~p)
        self.assertEqual(kernel.get_value('p4'), -p + ~p)

        p = kernel.get_value('p5')
        self.assertEqual(kernel.get_value('p6'), -p)
        self.assertEqual(kernel.get_value('p7'), ~p)
        self.assertEqual(kernel.get_value('p8'), p + ~p)

        self._check(kernel.get_value('p11'), int32x2(-6, -1), 2)
        self._check(kernel.get_value('p12'), int32x2(6, 1), 2)

        self._check(kernel.get_value('p15'), int32x3(-6, -1, -2), 3)
        self._check(kernel.get_value('p16'), int32x3(6, 1, 2), 3)

        self._check(kernel.get_value('p19'), int32x4(-6, -1, -2, 13), 4)
        self._check(kernel.get_value('p20'), int32x4(6, 1, 2, -13), 4)

        self._check(kernel.get_value('p23'), int32x8(-6, -1, -2, 13, -9, -3, 5, 7), 8)
        self._check(kernel.get_value('p24'), int32x8(6, 1, 2, -13, 9, 3, -5, -7), 8)

        self._check(kernel.get_value('p27'), int32x16(-6, -1, -2, 13, -9, -3, 5, 7, -2, -2, -2, -3, 10, -4, -4, -4), 16)
        self._check(kernel.get_value('p28'), int32x16(6, 1, 2, -13, 9, 3, -5, -7, 2, 2, 2, 3, -10, 4, 4, 4), 16)

        self._check(kernel.get_value('p31'), int64x2(-6, -1), 2)
        self._check(kernel.get_value('p32'), int64x2(6, 1), 2)

        self._check(kernel.get_value('p35'), int64x3(-6, -1, -2), 3)
        self._check(kernel.get_value('p36'), int64x3(6, 1, 2), 3)

        self._check(kernel.get_value('p39'), int64x4(-6, -1, -2, 13), 4)
        self._check(kernel.get_value('p40'), int64x4(6, 1, 2, -13), 4)

        self._check(kernel.get_value('p43'), int64x8(-6, -1, -2, 13, -9, -3, 5, 7), 8)
        self._check(kernel.get_value('p44'), int64x8(6, 1, 2, -13, 9, 3, -5, -7), 8)

        self.assertAlmostEqual(kernel.get_value('p47'), -2.0)
        self.assertAlmostEqual(kernel.get_value('p48'), 2.0)

        self.assertAlmostEqual(kernel.get_value('p51'), -2.0)
        self.assertAlmostEqual(kernel.get_value('p52'), 2.0)

        self._check_almost(kernel.get_value('p55'), float64x2(-6, -1), 2)
        self._check_almost(kernel.get_value('p56'), float64x2(6, 1), 2)

        self._check_almost(kernel.get_value('p59'), float64x3(-6, -1, -2), 3)
        self._check_almost(kernel.get_value('p60'), float64x3(6, 1, 2), 3)

        self._check_almost(kernel.get_value('p63'), float64x4(-6, -1, -2, 13), 4)
        self._check_almost(kernel.get_value('p64'), float64x4(6, 1, 2, -13), 4)

        self._check_almost(kernel.get_value('p67'), float64x8(-6, -1, -2, 13, -9, -3, 5, 7), 8)
        self._check_almost(kernel.get_value('p68'), float64x8(6, 1, 2, -13, 9, 3, -5, -7), 8)

        self._check_almost(kernel.get_value('p71'), float32x2(-6, -1), 2)
        self._check_almost(kernel.get_value('p72'), float32x2(6, 1), 2)

        self._check_almost(kernel.get_value('p75'), float32x3(-6, -1, -2), 3)
        self._check_almost(kernel.get_value('p76'), float32x3(6, 1, 2), 3)

        self._check_almost(kernel.get_value('p79'), float32x4(-6, -1, -2, 13), 4)
        self._check_almost(kernel.get_value('p80'), float32x4(6, 1, 2, -13), 4)

        self._check_almost(kernel.get_value('p83'), float32x8(-6, -1, -2, 13, -9, -3, 5, 7), 8)
        self._check_almost(kernel.get_value('p84'), float32x8(6, 1, 2, -13, 9, 3, -5, -7), 8)

        self._check_almost(kernel.get_value('p87'), float32x16(-6, -1, -2, 13, -9, -3, 5, 7, -2, -2, -2, -3, 10, -4, -4, -4), 16)
        self._check_almost(kernel.get_value('p88'), float32x16(6, 1, 2, -13, 9, 3, -5, -7, 2, 2, 2, 3, -10, 4, 4, 4), 16)

    def test_unary1(self):
        self._test_unary1(ISet.AVX512)
        self._test_unary1(ISet.AVX2)
        self._test_unary1(ISet.AVX)
        self._test_unary1(ISet.SSE)

if __name__ == "__main__":
    unittest.main()
