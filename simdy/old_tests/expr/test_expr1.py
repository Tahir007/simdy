
import unittest
from simdy import Kernel, int32, float32, float64, ISet


class ExpressionTests1(unittest.TestCase):

    def test_expr1(self):
        source = """
p1 = 2 + 9
p2 = 2.6 + 5
p3 = 2 << 3

        """

        args = [('p1', int32(3)), ('p2', float64(2.2)), ('p3', int32(1))]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 11)
        self.assertAlmostEqual(kernel.get_value('p2'), 7.6)
        self.assertEqual(kernel.get_value('p3'), 16)

    def cpu_test_expr2(self, iset):

        source = """
p3 = p1 + p2
p4 = p1 * p2
p5 = p1 - p2

p8 = p6 + p7
p9 = p6 * p6
p10 = p6 * p7
p11 = p7 - p6
p12 = p6 / p7

        """

        args = [('p1', int32(4)), ('p2', int32(2)), ('p3', int32()),
                ('p4', int32()), ('p5', int32()), ('p6', float32(4.0)),
                ('p7', float32(2.0)), ('p8', float32()), ('p9', float32()),
                ('p10', float32()), ('p11', float32()), ('p12', float32())]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p3'), 6)
        self.assertEqual(kernel.get_value('p4'), 8)
        self.assertEqual(kernel.get_value('p5'), 2)

        self.assertAlmostEqual(kernel.get_value('p8'), 6.0)
        self.assertAlmostEqual(kernel.get_value('p9'), 16.0)
        self.assertAlmostEqual(kernel.get_value('p10'), 8.0)
        self.assertAlmostEqual(kernel.get_value('p11'), -2.0)
        self.assertAlmostEqual(kernel.get_value('p12'), 2.0)

    def test_expr2(self):
        self.cpu_test_expr2(ISet.AVX512)
        self.cpu_test_expr2(ISet.AVX)
        self.cpu_test_expr2(ISet.SSE)

    def test_expr4(self):

        source = """
p3 = p1 + 7
p4 = p1 * 8
p5 = p1 - 1
p6 = 22 + p1
p7 = 9 - p2

p10 = p8 + float32(1.2)
p11 = float32(0.2) + p8
p12 = float32(1) + p8

        """

        args = [('p1', int32(4)), ('p2', int32(2)), ('p3', int32()), ('p4', int32()),
                ('p5', int32()), ('p6', int32(4)), ('p7', int32(2)),
                ('p8', float32(2.3)), ('p9', float32(1.9)), ('p10', float32()),
                ('p11', float32()), ('p12', float32())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p3'), 11)
        self.assertEqual(kernel.get_value('p4'), 32)
        self.assertEqual(kernel.get_value('p5'), 3)
        self.assertEqual(kernel.get_value('p6'), 26)
        self.assertEqual(kernel.get_value('p7'), 7)
        self.assertAlmostEqual(kernel.get_value('p10'), 3.5)
        self.assertAlmostEqual(kernel.get_value('p11'), 2.5)
        self.assertAlmostEqual(kernel.get_value('p12'), 3.3)

    def test_expr5(self):
        source = """
p4 = p1 + p2 - p3
p5 = p1 + p2 * p3
p6 = p2 - p1 * p3
p7 = p1 * p2 + p1 * p3
p8 = p1 - p2 + 5
p9 = 5 - p2 * p3
p10 = 3 + 8 - 10 + 7 - 3

        """

        args = [('p1', int32(4)), ('p2', int32(2)), ('p3', int32(5)),
                ('p4', int32()), ('p5', int32()), ('p6', int32()),
                ('p7', int32()), ('p8', int32()), ('p9', int32()), ('p10', int32())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), 1)
        self.assertEqual(kernel.get_value('p5'), 14)
        self.assertEqual(kernel.get_value('p6'), -18)
        self.assertEqual(kernel.get_value('p7'), 28)
        self.assertEqual(kernel.get_value('p8'), 7)
        self.assertEqual(kernel.get_value('p9'), -5)
        self.assertEqual(kernel.get_value('p10'), 5)

    def cpu_test_expr6(self, iset):

        source = """
p4 = p1 + p2 - p3
p5 = p1 + p2 * p3
p6 = p2 - p1 * p3
p7 = p1 * p2 + p1 * p3
p8 = p1 - p2 + float32(5)
p9 = float32(5) - p2 * p3

        """

        args = [('p1', float32(4)), ('p2', float32(2)), ('p3', float32(5)),
                ('p4', float32()), ('p5', float32()), ('p6', float32()),
                ('p7', float32()), ('p8', float32()), ('p9', float32())]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self.assertAlmostEqual(kernel.get_value('p4'), 1.0)
        self.assertAlmostEqual(kernel.get_value('p5'), 14.0)
        self.assertAlmostEqual(kernel.get_value('p6'), -18.0)
        self.assertAlmostEqual(kernel.get_value('p7'), 28.0)
        self.assertAlmostEqual(kernel.get_value('p8'), 7.0)
        self.assertAlmostEqual(kernel.get_value('p9'), -5.0)

    def test_expr6(self):
        self.cpu_test_expr6(ISet.AVX512)
        self.cpu_test_expr6(ISet.AVX)
        self.cpu_test_expr6(ISet.SSE)

    def test_expr8(self):

        source = """
p4 = p1 * p2 + p3
p5 = p1 - p2 * p3
p6 = float32(4) * p1 + p2
p7 = p1 * p2 + p2 * p3
p8 = p1 - p2 * p1 + p3 * float32(2)
p9 = p1 * p2 * p3 + p1
p10 = p1 * p2 * float32(4) + p2

        """

        args = [('p1', float32(4)), ('p2', float32(2)), ('p3', float32(5)),
                ('p4', float32()), ('p5', float32()), ('p6', float32()),
                ('p7', float32()), ('p8', float32()), ('p9', float32()),
                ('p10', float32())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p4'), 13.0)
        self.assertAlmostEqual(kernel.get_value('p5'), -6.0)
        self.assertAlmostEqual(kernel.get_value('p6'), 18.0)
        self.assertAlmostEqual(kernel.get_value('p7'), 18.0)
        self.assertAlmostEqual(kernel.get_value('p8'), 6.0)
        self.assertAlmostEqual(kernel.get_value('p9'), 44.0)
        self.assertAlmostEqual(kernel.get_value('p10'), 34.0)


if __name__ == "__main__":
    unittest.main()
