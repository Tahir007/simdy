
import unittest
from simdy import Kernel, int32


class ExpressionTestsInt32(unittest.TestCase):

    def test_expr_int32_1(self):
        source = """
t1, t2, t3 = 8, -4, 3
t4, t5, t6 = 5, -12, 9

p1 = 2 + 9 - 7 * 9 + t1
p2 = t1 * t2 / t3
p3 = ((t1 << 3) % 43) >> t3
p4 = (t4 & t6) | 19 ^ t3
        """

        args = [('p1', int32()), ('p2', int32()), ('p3', int32()), ('p4', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), -44)
        self.assertEqual(kernel.get_value('p2'), -10)
        self.assertEqual(kernel.get_value('p3'), 2)
        self.assertEqual(kernel.get_value('p4'), 17)

    def test_expr_int32_2(self):
        source = """
t1, t2, t3 = 8, -4, 3
t4, t5, t6 = 5, -12, 9

if t1 > t2 + 5:
    p1 = 13

if t3:
    p2 = 5

if t1 == 3:
    p3 = 55
elif t2 == -4:
    p3 = 22
else:
    p3 = 11

if t1 + t2 == t4 * t2 / t1:
    p4 = 12
else:
    p4 = 9
        """

        args = [('p1', int32()), ('p2', int32()), ('p3', int32()), ('p4', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 13)
        self.assertEqual(kernel.get_value('p2'), 5)
        self.assertEqual(kernel.get_value('p3'), 22)
        self.assertEqual(kernel.get_value('p4'), 9)

    def test_expr_int32_3(self):
        source = """
t1, t2, t3 = 8, -4, 3
t4, t5, t6 = 5, -12, 9

i = 0
p1 = 0
while i < t1:
    p1 += 1
    i += 1

i = 5
p2 = 0
while i > 0:
    j = 0
    while j < t3:
        p2 += 1
        j += 1
    i -= 1
        """

        args = [('p1', int32()), ('p2', int32()), ('p3', int32()), ('p4', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 8)
        self.assertEqual(kernel.get_value('p2'), 15)

if __name__ == "__main__":
    unittest.main()
