
import unittest
from simdy import Kernel, int64


class ExpressionTestsInt64(unittest.TestCase):

    def test_expr_int64_1(self):
        source = """
t1, t2, t3 = int64(8), int64(-4), int64(3)
t4, t5, t6 = int64(5), int64(-12), int64(9)

p1 = t1 + 459
p2 = t3 + 12345678912345
p3 = t1 * t2 / t3
p4 = ((t1 << 3) % int64(43)) >> t3
p5 = (t4 & t6) | 19 ^ t3
p6 = t6 / 4
        """

        args = [('p1', int64()), ('p2', int64()), ('p3', int64()),
                ('p4', int64()), ('p5', int64()), ('p6', int64())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 467)
        self.assertEqual(kernel.get_value('p2'), 12345678912348)
        self.assertEqual(kernel.get_value('p3'), -10)
        self.assertEqual(kernel.get_value('p4'), 2)
        self.assertEqual(kernel.get_value('p5'), 17)
        self.assertEqual(kernel.get_value('p6'), 2)

    def test_expr_int64_2(self):
        source = """
t1, t2, t3 = int64(8), int64(-4), int64(3)
t4, t5, t6 = int64(5), int64(-12), int64(9)

if t1 > t2 + 5:
    p1 = int64(13)

if t3:
    p2 = int64(5)

if t1 == 3:
    p3 = int64(55)
elif t2 == -4:
    p3 = int64(22)
else:
    p3 = int64(11)

if t1 + t2 == t4 * t2 / t1:
    p4 = int64(12)
else:
    p4 = int64(9)
        """

        args = [('p1', int64()), ('p2', int64()), ('p3', int64()), ('p4', int64())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 13)
        self.assertEqual(kernel.get_value('p2'), 5)
        self.assertEqual(kernel.get_value('p3'), 22)
        self.assertEqual(kernel.get_value('p4'), 9)

    def test_expr_int64_3(self):
        source = """
t1, t2, t3 = int64(8), int64(-4), int64(3)
t4, t5, t6 = int64(5), int64(-12), int64(9)

i = int64(0)
p1 = int64(0)
while i < t1:
    p1 += 1
    i += 1

i = int64(5)
p2 = int64(0)
while i > 0:
    j = int64(0)
    while j < t3:
        p2 += 1
        j += 1
    i -= 1
        """

        args = [('p1', int64()), ('p2', int64()), ('p3', int64()), ('p4', int64())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 8)
        self.assertEqual(kernel.get_value('p2'), 15)

if __name__ == "__main__":
    unittest.main()
