
import unittest
from simdy import Kernel, int32, int64, float32, float64


class ExpressionIntTests1(unittest.TestCase):

    def test_expr_int1(self):
        source = """
p4 = p3 / p1
p5 = p3 % p1
p6 = p1 | 1
p7 = p3 & p1
p8 = p1 ^ p2
p9 = p2 / 3
        """

        args = [('p1', int32(4)), ('p2', int32(2)), ('p3', int32(14)),
                ('p4', int32()), ('p5', int32()), ('p6', int32()),
                ('p7', int32()), ('p8', int32()), ('p9', int32())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), 3)
        self.assertEqual(kernel.get_value('p5'), 2)
        self.assertEqual(kernel.get_value('p6'), 5)
        self.assertEqual(kernel.get_value('p7'), 4)
        self.assertEqual(kernel.get_value('p8'), 6)
        self.assertEqual(kernel.get_value('p9'), 0)

    def test_expr_int2(self):
        source = """
p7 = p1 * 2 - p2 + int32(p4)
p8 = int32(p5) + int32(p6) + p1
p9 = int32(p6 * 2.0) + 4
tt = 11
p10 = 2 * p1 + p3 - int32(p5) - int32(p6) + p1 + p1 + p2 + tt - 4
        """

        args = [('p1', int32(4)), ('p2', int32(2)), ('p3', int32(14)),
                ('p4', int64(5)), ('p5', float32(6.8)), ('p6', float64(8.8)),
                ('p7', int32()), ('p8', int32()), ('p9', int32()),
                ('p10', int32())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p7'), 11)
        self.assertEqual(kernel.get_value('p8'), 18)
        self.assertEqual(kernel.get_value('p9'), 21)
        self.assertEqual(kernel.get_value('p10'), 25)

    def test_expr_int3(self):
        source = """
p4 = p3 / p1
p5 = p3 % p1
p6 = p1 | int64(1)
p7 = p3 & p1
p8 = p1 ^ p2
p9 = p2 / int64(3)
        """

        args = [('p1', int64(4)), ('p2', int64(2)), ('p3', int64(14)),
                ('p4', int64()), ('p5', int64()), ('p6', int64()),
                ('p7', int64()), ('p8', int64()), ('p9', int64())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), 3)
        self.assertEqual(kernel.get_value('p5'), 2)
        self.assertEqual(kernel.get_value('p6'), 5)
        self.assertEqual(kernel.get_value('p7'), 4)
        self.assertEqual(kernel.get_value('p8'), 6)
        self.assertEqual(kernel.get_value('p9'), 0)

    def test_expr_int4(self):
        source = """
aa = -3
p4 = int64(aa)
p5 = int32(p4)
p6 = int64(3 * p1 + p2 + int64(aa))
        """

        args = [('p1', int64(4)), ('p2', int64(2)), ('p3', int64(14)),
                ('p4', int64()), ('p5', int32()), ('p6', int64()),
                ('p7', int64()), ('p8', int64()), ('p9', int64())]

        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), -3)
        self.assertEqual(kernel.get_value('p5'), -3)
        self.assertEqual(kernel.get_value('p6'), 11)


if __name__ == "__main__":
    unittest.main()
