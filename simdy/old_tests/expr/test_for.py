
import unittest
from simdy import Kernel, int32, int64


class ExpressionTestsFoorLoop(unittest.TestCase):

    def test_for_1(self):
        source = """
p1 = 0
for i in range(4):
    p1 += i

p2 = 0
n1 = 2
for i in range(n1 * 2):
    p2 += i

p3 = int64(0)
for j in range(int64(4)):
    p3 += j

p4 = 0
for i in range(2, 5):
    p4 += i

p5 = int64(0)
for j in range(int64(2), int64(5)):
    p5 += j

p6 = 0
for i in range(2, 9, 3):
    p6 += i

p7 = int64(0)
for j in range(int64(2), int64(9), int64(3)):
    p7 += j

n1 = 3
n2 = 4
p8 = 0
for i in range(n1, 3 * n1 + 1, n2):
    p8 += i


p9 = 0
for i in range(10):
    if i == 3:
        continue
    if i == 5:
        break
    p9 += i

p10 = 0
for i in range(7):
    if i % 2:
        continue
    p10 += i
        """

        args = [('p1', int32()), ('p2', int32()), ('p3', int64()), ('p4', int32()),
                ('p5', int64()), ('p6', int32()), ('p7', int64()), ('p8', int32()),
                ('p9', int32()), ('p10', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 6)
        self.assertEqual(kernel.get_value('p2'), 6)
        self.assertEqual(kernel.get_value('p3'), 6)
        self.assertEqual(kernel.get_value('p4'), 9)
        self.assertEqual(kernel.get_value('p5'), 9)
        self.assertEqual(kernel.get_value('p6'), 15)
        self.assertEqual(kernel.get_value('p7'), 15)
        self.assertEqual(kernel.get_value('p8'), 10)
        self.assertEqual(kernel.get_value('p9'), 7)
        self.assertEqual(kernel.get_value('p10'), 12)


if __name__ == "__main__":
    unittest.main()
