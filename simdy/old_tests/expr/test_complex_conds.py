
import unittest
from simdy import Kernel, float64, ISet


class ComplexCondsTestsF64(unittest.TestCase):

    def _test_complex_conds(self, iset):
        source = """
t1, t2, t3 = 2.3, -6.9, 1.4
t4, t5, t6 = 4.6, 8.0, -6.6

if t1 > t2 and t3 > t2 and t1 > t3 and t4 > t1:
    p1 = 2.2

if t4 > t1 and t5 > t1 or t2 > t3:
    p2 = 1.4

if t4 > t1 and t5 > t1 or t2 < t3:
    p3 = 1.4

if t4 > t1 or t2 < t1 and t2 < t3 or t1 == t2 and t1 < t2:
    p4 = 1.8

if t1 > t2 and t2 > t3 and t1 > t2 and t1 > t2 or 1:
    p5 = 1.25

if t1 > t2 and t2 > t3 and t1 > t2 and t1 > t2 or 0:
    p6 = 1.25

if t1 > t2 or t1 == t2 and t1 > t3:
    p7 = 1.4

if t1 < t2 or t1 == t2 and t1 > t3:
    p8 = 1.4

if t1 > t2 and t2 > t3 or t1 > t2 and t1 == t2 and t2 > t3:
    p9 = 1.4

if t1 > t2 and t2 > t3 or t1 < t2 and t1 == t2 and t2 > t3:
    p10 = 1.4

if t1 > t2 and t1 == t3 or t2 > t3 or t1 > t2 and t2 < t3:
    p11 = 1.4

if t1 > t2 and t1 == t3 or t2 > t3 or t1 > t2 and t2 == t3:
    p12 = 1.4
        """

        args = [('p1', float64()), ('p2', float64()), ('p3', float64()),
                ('p4', float64()), ('p5', float64()), ('p6', float64()),
                ('p7', float64()), ('p8', float64()), ('p9', float64()),
                ('p10', float64()), ('p11', float64()), ('p12', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 2.2)
        self.assertAlmostEqual(kernel.get_value('p2'), 1.4)
        self.assertAlmostEqual(kernel.get_value('p3'), 1.4)
        self.assertAlmostEqual(kernel.get_value('p4'), 1.8)
        self.assertAlmostEqual(kernel.get_value('p5'), 1.25)
        self.assertAlmostEqual(kernel.get_value('p6'), 0.0)
        self.assertAlmostEqual(kernel.get_value('p7'), 1.4)
        self.assertAlmostEqual(kernel.get_value('p8'), 0.0)
        self.assertAlmostEqual(kernel.get_value('p9'), 1.4)
        self.assertAlmostEqual(kernel.get_value('p10'), 0.0)
        self.assertAlmostEqual(kernel.get_value('p11'), 1.4)
        self.assertAlmostEqual(kernel.get_value('p12'), 0.0)

    def test_complexs_conds_1(self):
        self._test_complex_conds(ISet.AVX512)
        self._test_complex_conds(ISet.AVX)
        self._test_complex_conds(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
