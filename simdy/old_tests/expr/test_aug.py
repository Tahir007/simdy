
import unittest
from simdy import Kernel, ISet, int32, float64, float64x4


class AugAssignTests(unittest.TestCase):

    def _test_aug_1(self, iset):
        source = """
p1 += 1
t = 1.1
p2 -= t
m1 = float64x4(1, 2, 3, 4)
m2 = float64x4(2)
p3 *= m1 + m2
p4 += t * t
        """

        args = [('p1', int32(3)), ('p2', float64(2.0)), ('p3', float64x4(4)), ('p4', float64(2.0))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 4)
        self.assertAlmostEqual(kernel.get_value('p2'), 0.9)
        self.assertAlmostEqual(kernel.get_value('p3')[0], 12.0)
        self.assertAlmostEqual(kernel.get_value('p3')[1], 16.0)
        self.assertAlmostEqual(kernel.get_value('p3')[2], 20.0)
        self.assertAlmostEqual(kernel.get_value('p3')[3], 24.0)
        self.assertAlmostEqual(kernel.get_value('p4'), 1.1 * 1.1 + 2.0)

    def test_aug_1(self):
        self._test_aug_1(ISet.AVX512)
        self._test_aug_1(ISet.AVX2)
        self._test_aug_1(ISet.AVX)
        self._test_aug_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
