
import unittest
from math import sin
from simdy import Kernel, float64, ISet, float64x2, float64x3, float64x4, float64x8,\
    float32, float32x2, float32x3, float32x4, float32x8, float32x16


class SinTests(unittest.TestCase):

    def _test_sin_f64(self, iset, fma):
        source = """
result = sin(p1)
result2 = sin(p2)
result3 = sin(p3)
result4 = sin(p4)
result5 = sin(p5)
        """
        args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()),
                ('p5', float64x8()), ('result', float64()), ('result2', float64x2()),
                ('result3', float64x3()), ('result4', float64x4()), ('result5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-6.2, -5.3, -4.2, -3.45, -2.9, -2.1, -1.2, -0.7, 6.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float64x2(val, val + 1.3))
            kernel.set_value('p3', float64x3(val, val + 1.3, val + 2.1))
            kernel.set_value('p4', float64x4(val, val + 1.3, val + 2.1, val + 3.1))
            v = float64x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            kernel.set_value('p5', v)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), sin(val))
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], sin(val))
            self.assertAlmostEqual(res[1], sin(val + 1.3))
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], sin(val))
            self.assertAlmostEqual(res[1], sin(val + 1.3))
            self.assertAlmostEqual(res[2], sin(val + 2.1))
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], sin(val))
            self.assertAlmostEqual(res[1], sin(val + 1.3))
            self.assertAlmostEqual(res[2], sin(val + 2.1))
            self.assertAlmostEqual(res[3], sin(val + 3.1))
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], sin(val))
            self.assertAlmostEqual(res[1], sin(val + 1.3))
            self.assertAlmostEqual(res[2], sin(val + 2.1))
            self.assertAlmostEqual(res[3], sin(val + 3.1))
            self.assertAlmostEqual(res[4], sin(val + 1.2))
            self.assertAlmostEqual(res[5], sin(val + 1.7))
            self.assertAlmostEqual(res[6], sin(val + 1.3))
            self.assertAlmostEqual(res[7], sin(val + 4.8))

    def test_sin_f64_1(self):
        self._test_sin_f64(ISet.AVX512, fma=True)
        self._test_sin_f64(ISet.AVX512, fma=False)
        self._test_sin_f64(ISet.AVX2, fma=True)
        self._test_sin_f64(ISet.AVX2, fma=False)
        self._test_sin_f64(ISet.AVX, fma=False)
        self._test_sin_f64(ISet.SSE, fma=False)

    def _test_sin_f32(self, iset, fma):
        source = """
result = sin(p1)
result2 = sin(p2)
result3 = sin(p3)
result4 = sin(p4)
result5 = sin(p5)
result6 = sin(p6)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()), ('p4', float32x4()),
                ('p5', float32x8()), ('p6', float32x16()), ('result', float32()), ('result2', float32x2()),
                ('result3', float32x3()), ('result4', float32x4()), ('result5', float32x8()), ('result6', float32x16())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-6.2, -5.3, -4.2, -3.45, -2.9, -2.1, -1.2, -0.7, 6.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float32x2(val, val + 1.3))
            kernel.set_value('p3', float32x3(val, val + 1.3, val + 2.1))
            kernel.set_value('p4', float32x4(val, val + 1.3, val + 2.1, val + 3.1))
            v = float32x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            kernel.set_value('p5', v)
            v16 = float32x16(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7])
            kernel.set_value('p6', v16)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), sin(val), places=6)
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], sin(val), places=6)
            self.assertAlmostEqual(res[1], sin(val + 1.3), places=6)
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], sin(val), places=6)
            self.assertAlmostEqual(res[1], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[2], sin(val + 2.1), places=6)
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], sin(val), places=6)
            self.assertAlmostEqual(res[1], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[2], sin(val + 2.1), places=6)
            self.assertAlmostEqual(res[3], sin(val + 3.1), places=6)
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], sin(val), places=6)
            self.assertAlmostEqual(res[1], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[2], sin(val + 2.1), places=6)
            self.assertAlmostEqual(res[3], sin(val + 3.1), places=6)
            self.assertAlmostEqual(res[4], sin(val + 1.2), places=6)
            self.assertAlmostEqual(res[5], sin(val + 1.7), places=6)
            self.assertAlmostEqual(res[6], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[7], sin(val + 4.8), places=6)
            res = kernel.get_value('result6')
            self.assertAlmostEqual(res[0], sin(val), places=6)
            self.assertAlmostEqual(res[1], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[2], sin(val + 2.1), places=6)
            self.assertAlmostEqual(res[3], sin(val + 3.1), places=6)
            self.assertAlmostEqual(res[4], sin(val + 1.2), places=6)
            self.assertAlmostEqual(res[5], sin(val + 1.7), places=6)
            self.assertAlmostEqual(res[6], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[7], sin(val + 4.8), places=6)
            self.assertAlmostEqual(res[8], sin(val), places=6)
            self.assertAlmostEqual(res[9], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[10], sin(val + 2.1), places=6)
            self.assertAlmostEqual(res[11], sin(val + 3.1), places=6)
            self.assertAlmostEqual(res[12], sin(val + 1.2), places=6)
            self.assertAlmostEqual(res[13], sin(val + 1.7), places=6)
            self.assertAlmostEqual(res[14], sin(val + 1.3), places=6)
            self.assertAlmostEqual(res[15], sin(val + 4.8), places=6)

    def test_sin_f32_1(self):
        self._test_sin_f32(ISet.AVX512, fma=True)
        self._test_sin_f32(ISet.AVX512, fma=False)
        self._test_sin_f32(ISet.AVX2, fma=True)
        self._test_sin_f32(ISet.AVX2, fma=False)
        self._test_sin_f32(ISet.AVX, fma=False)
        self._test_sin_f32(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
