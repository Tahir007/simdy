
import unittest
from simdy import Kernel, float64, ISet, float64x2, float64x3, float64x4, float64x8,\
    float32, float32x2, float32x3, float32x4, float32x8, float32x16


class PownTests(unittest.TestCase):

    def _test_pown_f64(self, iset):
        source = """
result = pown(p1, 4)
result2 = pown(p2, 7)
result3 = pown(p3, -2)
result4 = pown(p4, 3)
result5 = pown(p5, 4)
        """
        args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()),
                ('p5', float64x8()), ('result', float64()), ('result2', float64x2()),
                ('result3', float64x3()), ('result4', float64x4()), ('result5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset)
        vals = [-6.2, -5.3, -4.2, -3.45, -2.9, -2.1, -1.2, -0.7, 6.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float64x2(val, val + 1.3))
            kernel.set_value('p3', float64x3(val, val + 1.3, val + 2.2))
            kernel.set_value('p4', float64x4(val, val + 1.3, val + 2.1, val + 3.1))
            v = float64x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            kernel.set_value('p5', v)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), pow(val, 4))
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], pow(val, 7))
            self.assertAlmostEqual(res[1], pow(val + 1.3, 7))
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], pow(val, -2))
            self.assertAlmostEqual(res[1], pow(val + 1.3, -2))
            self.assertAlmostEqual(res[2], pow(val + 2.2, -2))
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], pow(val, 3))
            self.assertAlmostEqual(res[1], pow(val + 1.3, 3))
            self.assertAlmostEqual(res[2], pow(val + 2.1, 3))
            self.assertAlmostEqual(res[3], pow(val + 3.1, 3))
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], pow(val, 4))
            self.assertAlmostEqual(res[1], pow(val + 1.3, 4))
            self.assertAlmostEqual(res[2], pow(val + 2.1, 4))
            self.assertAlmostEqual(res[3], pow(val + 3.1, 4))
            self.assertAlmostEqual(res[4], pow(val + 1.2, 4))
            self.assertAlmostEqual(res[5], pow(val + 1.7, 4))
            self.assertAlmostEqual(res[6], pow(val + 1.3, 4))
            self.assertAlmostEqual(res[7], pow(val + 4.8, 4))

    def test_pown_f64_1(self):
        self._test_pown_f64(ISet.AVX512)
        self._test_pown_f64(ISet.AVX2)
        self._test_pown_f64(ISet.AVX)
        self._test_pown_f64(ISet.SSE)

    def _test_pown_f32(self, iset):
        source = """
result = pown(p1, 3)
result2 = pown(p2, 2)
result3 = pown(p3, -2)
result4 = pown(p4, 3)
result5 = pown(p5, 4)
result6 = pown(p6, 3)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()), ('p4', float32x4()),
                ('p5', float32x8()), ('p6', float32x16()), ('result', float32()), ('result2', float32x2()),
                ('result3', float32x3()), ('result4', float32x4()), ('result5', float32x8()), ('result6', float32x16())]
        kernel = Kernel(source, args=args, iset=iset)
        vals = [-6.2, -5.3, -4.2, -3.45, -2.9, -2.1, -1.2, -0.7, 6.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float32x2(val, val + 1.3))
            kernel.set_value('p3', float32x3(val, val + 1.3, val + 2.2))
            kernel.set_value('p4', float32x4(val, val + 1.3, val + 2.1, val + 3.1))
            v = float32x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            kernel.set_value('p5', v)
            v = float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
            kernel.set_value('p6', v)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), pow(val, 3), places=3)
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], pow(val, 2), places=5)
            self.assertAlmostEqual(res[1], pow(val + 1.3, 2), places=5)
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], pow(val, -2))
            self.assertAlmostEqual(res[1], pow(val + 1.3, -2), places=4)
            self.assertAlmostEqual(res[2], pow(val + 2.2, -2), places=4)
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], pow(val, 3), places=3)
            self.assertAlmostEqual(res[1], pow(val + 1.3, 3), places=4)
            self.assertAlmostEqual(res[2], pow(val + 2.1, 3), places=4)
            self.assertAlmostEqual(res[3], pow(val + 3.1, 3), places=3)
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], pow(val, 4), places=3)
            self.assertAlmostEqual(res[1], pow(val + 1.3, 4), places=2)
            self.assertAlmostEqual(res[2], pow(val + 2.1, 4), places=3)
            self.assertAlmostEqual(res[3], pow(val + 3.1, 4), places=2)
            self.assertAlmostEqual(res[4], pow(val + 1.2, 4), places=3)
            self.assertAlmostEqual(res[5], pow(val + 1.7, 4), places=3)
            self.assertAlmostEqual(res[6], pow(val + 1.3, 4), places=3)
            self.assertAlmostEqual(res[7], pow(val + 4.8, 4), places=2)
            res = kernel.get_value('result6')
            self.assertAlmostEqual(res[0], pow(1, 3))
            self.assertAlmostEqual(res[1], pow(2, 3))
            self.assertAlmostEqual(res[2], pow(3, 3))
            self.assertAlmostEqual(res[3], pow(4, 3))
            self.assertAlmostEqual(res[4], pow(5, 3))
            self.assertAlmostEqual(res[5], pow(6, 3))
            self.assertAlmostEqual(res[6], pow(7, 3))
            self.assertAlmostEqual(res[7], pow(8, 3))
            self.assertAlmostEqual(res[8], pow(9, 3))
            self.assertAlmostEqual(res[9], pow(10, 3))
            self.assertAlmostEqual(res[10], pow(11, 3))
            self.assertAlmostEqual(res[11], pow(12, 3))
            self.assertAlmostEqual(res[12], pow(13, 3))
            self.assertAlmostEqual(res[13], pow(14, 3))
            self.assertAlmostEqual(res[14], pow(15, 3))
            self.assertAlmostEqual(res[15], pow(16, 3))

    def test_pown_f32_1(self):
        self._test_pown_f32(ISet.AVX512)
        self._test_pown_f32(ISet.AVX2)
        self._test_pown_f32(ISet.AVX)
        self._test_pown_f32(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
