import unittest
from math import acos
from simdy import Kernel, float64, ISet, float64x2, float64x3, float64x4, float64x8,\
    float32, float32x2, float32x3, float32x4, float32x8, float32x16


class ACosTests(unittest.TestCase):

    def _test_acos_f64(self, iset, fma):
        source = """
result = acos(p1)
result2 = acos(p2)
result3 = acos(p3)
result4 = acos(p4)
result5 = acos(p5)
        """
        args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()),
                ('p5', float64x8()), ('result', float64()), ('result2', float64x2()),
                ('result3', float64x3()), ('result4', float64x4()), ('result5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-0.95, -0.72, -0.53, -0.38, -0.22, -0.11, -0.05, 0.07, 0.18, 0.41, 0.63, 0.88, 0.91]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float64x2(val, val + 0.01))
            kernel.set_value('p3', float64x3(val, val + 0.01, val + 0.02))
            kernel.set_value('p4', float64x4(val, val + 0.01, val + 0.02, val + 0.03))
            v = float64x8(val, val + 0.01, val + 0.02, val + 0.03, val + 0.04, val + 0.05, val + 0.06, val + 0.07)
            kernel.set_value('p5', v)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), acos(val))
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], acos(val))
            self.assertAlmostEqual(res[1], acos(val + 0.01))
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], acos(val))
            self.assertAlmostEqual(res[1], acos(val + 0.01))
            self.assertAlmostEqual(res[2], acos(val + 0.02))
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], acos(val))
            self.assertAlmostEqual(res[1], acos(val + 0.01))
            self.assertAlmostEqual(res[2], acos(val + 0.02))
            self.assertAlmostEqual(res[3], acos(val + 0.03))
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], acos(val))
            self.assertAlmostEqual(res[1], acos(val + 0.01))
            self.assertAlmostEqual(res[2], acos(val + 0.02))
            self.assertAlmostEqual(res[3], acos(val + 0.03))
            self.assertAlmostEqual(res[4], acos(val + 0.04))
            self.assertAlmostEqual(res[5], acos(val + 0.05))
            self.assertAlmostEqual(res[6], acos(val + 0.06))
            self.assertAlmostEqual(res[7], acos(val + 0.07))

    def test_acos_f64_1(self):
        self._test_acos_f64(ISet.AVX512, fma=True)
        self._test_acos_f64(ISet.AVX512, fma=False)
        self._test_acos_f64(ISet.AVX2, fma=True)
        self._test_acos_f64(ISet.AVX2, fma=False)
        self._test_acos_f64(ISet.AVX, fma=False)
        self._test_acos_f64(ISet.SSE, fma=False)

    def _test_acos_f32(self, iset, fma):
        source = """
result = acos(p1)
result2 = acos(p2)
result3 = acos(p3)
result4 = acos(p4)
result5 = acos(p5)
result6 = acos(p6)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()), ('p4', float32x4()),
                ('p5', float32x8()), ('p6', float32x16()), ('result', float32()), ('result2', float32x2()),
                ('result3', float32x3()), ('result4', float32x4()), ('result5', float32x8()), ('result6', float32x16())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-0.95, -0.72, -0.53, -0.38, -0.22, -0.11, -0.05, 0.07, 0.18, 0.41, 0.63, 0.88, 0.91]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float32x2(val, val + 0.01))
            kernel.set_value('p3', float32x3(val, val + 0.01, val + 0.02))
            kernel.set_value('p4', float32x4(val, val + 0.01, val + 0.02, val + 0.03))
            v = float32x8(val, val + 0.01, val + 0.02, val + 0.03, val + 0.04, val + 0.05, val + 0.06, val + 0.07)
            kernel.set_value('p5', v)
            v16 = float32x16(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7])
            kernel.set_value('p6', v16)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), acos(val), places=6)
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], acos(val), places=6)
            self.assertAlmostEqual(res[1], acos(val + 0.01), places=6)
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], acos(val), places=6)
            self.assertAlmostEqual(res[1], acos(val + 0.01), places=6)
            self.assertAlmostEqual(res[2], acos(val + 0.02), places=6)
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], acos(val), places=6)
            self.assertAlmostEqual(res[1], acos(val + 0.01), places=6)
            self.assertAlmostEqual(res[2], acos(val + 0.02), places=6)
            self.assertAlmostEqual(res[3], acos(val + 0.03), places=6)
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], acos(val), places=6)
            self.assertAlmostEqual(res[1], acos(val + 0.01), places=6)
            self.assertAlmostEqual(res[2], acos(val + 0.02), places=6)
            self.assertAlmostEqual(res[3], acos(val + 0.03), places=6)
            self.assertAlmostEqual(res[4], acos(val + 0.04), places=6)
            self.assertAlmostEqual(res[5], acos(val + 0.05), places=6)
            self.assertAlmostEqual(res[6], acos(val + 0.06), places=6)
            self.assertAlmostEqual(res[7], acos(val + 0.07), places=6)
            res = kernel.get_value('result6')
            self.assertAlmostEqual(res[0], acos(val), places=6)
            self.assertAlmostEqual(res[1], acos(val + 0.01), places=6)
            self.assertAlmostEqual(res[2], acos(val + 0.02), places=6)
            self.assertAlmostEqual(res[3], acos(val + 0.03), places=6)
            self.assertAlmostEqual(res[4], acos(val + 0.04), places=6)
            self.assertAlmostEqual(res[5], acos(val + 0.05), places=6)
            self.assertAlmostEqual(res[6], acos(val + 0.06), places=6)
            self.assertAlmostEqual(res[7], acos(val + 0.07), places=6)
            self.assertAlmostEqual(res[8], acos(val), places=6)
            self.assertAlmostEqual(res[9], acos(val + 0.01), places=6)
            self.assertAlmostEqual(res[10], acos(val + 0.02), places=6)
            self.assertAlmostEqual(res[11], acos(val + 0.03), places=6)
            self.assertAlmostEqual(res[12], acos(val + 0.04), places=6)
            self.assertAlmostEqual(res[13], acos(val + 0.05), places=6)
            self.assertAlmostEqual(res[14], acos(val + 0.06), places=6)
            self.assertAlmostEqual(res[15], acos(val + 0.07), places=6)

    def test_acos_f32_1(self):
        self._test_acos_f32(ISet.AVX512, fma=True)
        self._test_acos_f32(ISet.AVX512, fma=False)
        self._test_acos_f32(ISet.AVX2, fma=True)
        self._test_acos_f32(ISet.AVX2, fma=False)
        self._test_acos_f32(ISet.AVX, fma=False)
        self._test_acos_f32(ISet.SSE, fma=False)

if __name__ == "__main__":
    unittest.main()