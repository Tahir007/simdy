import unittest
from math import atan2
from simdy import Kernel, float64, ISet, float64x2, float64x3, float64x4, float64x8,\
    float32, float32x2, float32x3, float32x4, float32x8, float32x16


class Atan2Tests(unittest.TestCase):

    def _test_atan2_f64(self, iset, fma):
        source = """
result = atan2(p1, q1)
result2 = atan2(p2, q2)
result3 = atan2(p3, q3)
result4 = atan2(p4, q4)
result5 = atan2(p5, q5)
        """
        args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()),
                ('p5', float64x8()), ('result', float64()), ('result2', float64x2()),
                ('result3', float64x3()), ('result4', float64x4()), ('result5', float64x8()),
                ('q1', float64()), ('q2', float64x2()), ('q3', float64x3()), ('q4', float64x4()), ('q5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-6.2, -5.3, -4.2, -3.45, 0, -2.1, -1.2, -0.7, 0, 5.3, 4.2, 3.45, 2.9, 0, 1.2, 0.7]
        qvals = [2.2, 3.3, -3.2, -1.45, 0, 2.1, 0, 0.75, -6.2, -2.3, 1.2, -2.45, -3.9, 4.1, -1.3, -0.7]
        for val, q in zip(vals, qvals):
            kernel.set_value('p1', val)
            kernel.set_value('q1', q)
            kernel.set_value('p2', float64x2(val, val + 1.3))
            kernel.set_value('q2', float64x2(q, q + 0.3))
            kernel.set_value('p3', float64x3(val, val + 1.3, val + 2.1))
            kernel.set_value('q3', float64x3(q, q + 0.3, q + 1.1))
            kernel.set_value('p4', float64x4(val, val + 1.3, val + 2.1, val + 3.1))
            kernel.set_value('q4', float64x4(q, q + 0.3, q + 1.1, q + 0.2))
            v = float64x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            qq = float64x8(q, q + 0.3, q + 1.1, q + 0.2, q - 1.3, q + 0.1, q -1, q + 2.1)
            kernel.set_value('p5', v)
            kernel.set_value('q5', qq)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), atan2(val, q))
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], atan2(val, q))
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3))
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], atan2(val, q))
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3))
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1))
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], atan2(val, q))
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3))
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1))
            self.assertAlmostEqual(res[3], atan2(val + 3.1, q + 0.2))
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], atan2(val, q))
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3))
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1))
            self.assertAlmostEqual(res[3], atan2(val + 3.1, q + 0.2))
            self.assertAlmostEqual(res[4], atan2(val + 1.2, q - 1.3))
            self.assertAlmostEqual(res[5], atan2(val + 1.7, q + 0.1))
            self.assertAlmostEqual(res[6], atan2(val + 1.3, q - 1))
            self.assertAlmostEqual(res[7], atan2(val + 4.8, q + 2.1))

    def test_atan2_f64_1(self):
        self._test_atan2_f64(ISet.AVX512, fma=True)
        self._test_atan2_f64(ISet.AVX512, fma=False)
        self._test_atan2_f64(ISet.AVX2, fma=True)
        self._test_atan2_f64(ISet.AVX2, fma=False)
        self._test_atan2_f64(ISet.AVX, fma=False)
        self._test_atan2_f64(ISet.SSE, fma=False)

    def _test_atan_f32(self, iset, fma):
        source = """
result = atan2(p1, q1)
result2 = atan2(p2, q2)
result3 = atan2(p3, q3)
result4 = atan2(p4, q4)
result5 = atan2(p5, q5)
result6 = atan2(p6, q6)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()), ('p4', float32x4()),
                ('p5', float32x8()), ('p6', float32x16()), ('result', float32()), ('result2', float32x2()),
                ('result3', float32x3()), ('result4', float32x4()), ('result5', float32x8()), ('result6', float32x16()),
                ('q1', float32()), ('q2', float32x2()), ('q3', float32x3()), ('q4', float32x4()), ('q5', float32x8()), ('q6', float32x16())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-6.2, -5.3, -4.2, -3.45, 0, -2.1, -1.2, -0.7, 0, 5.3, 4.2, 3.45, 2.9, 0, 1.2, 0.7]
        qvals = [2.2, 3.3, -3.2, -1.45, 0, 2.1, 0, 0.75, -6.2, -2.3, 1.2, -2.45, -3.9, 4.1, -1.3, -0.7]
        for val, q in zip(vals, qvals):
            kernel.set_value('p1', val)
            kernel.set_value('q1', q)
            kernel.set_value('p2', float32x2(val, val + 1.3))
            kernel.set_value('q2', float32x2(q, q + 0.3))
            kernel.set_value('p3', float32x3(val, val + 1.3, val + 2.1))
            kernel.set_value('q3', float32x3(q, q + 0.3, q + 1.1))
            kernel.set_value('p4', float32x4(val, val + 1.3, val + 2.1, val + 3.1))
            kernel.set_value('q4', float32x4(q, q + 0.3, q + 1.1, q + 0.2))
            v = float32x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val)
            qq = float32x8(q, q + 0.3, q + 1.1, q + 0.2, q - 1.3, q + 0.1, q - 1, q + 2.1)
            kernel.set_value('p5', v)
            kernel.set_value('q5', qq)
            v16 = float32x16(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7])
            kernel.set_value('p6', v16)
            q16 = float32x16(qq[0], qq[1], qq[2], qq[3], qq[4], qq[5], qq[6], qq[7], qq[0], qq[1], qq[2], qq[3], qq[4], qq[5], qq[6], qq[7])
            kernel.set_value('q6', q16)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), atan2(val, q), places=5)
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], atan2(val, q), places=5)
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3), places=5)
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], atan2(val, q), places=4)
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3), places=4)
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1), places=4)
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], atan2(val, q), places=4)
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3), places=4)
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1), places=4)
            self.assertAlmostEqual(res[3], atan2(val + 3.1, q + 0.2), places=4)
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], atan2(val, q), places=4)
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3), places=4)
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1), places=4)
            self.assertAlmostEqual(res[3], atan2(val + 3.1, q + 0.2), places=4)
            self.assertAlmostEqual(res[4], atan2(val + 1.2, q - 1.3), places=4)
            self.assertAlmostEqual(res[5], atan2(val + 1.7, q + 0.1), places=4)
            self.assertAlmostEqual(res[6], atan2(val + 1.3, q - 1), places=4)
            self.assertAlmostEqual(res[7], atan2(val, q + 2.1), places=4)
            res = kernel.get_value('result6')
            self.assertAlmostEqual(res[0], atan2(val, q), places=4)
            self.assertAlmostEqual(res[1], atan2(val + 1.3, q + 0.3), places=4)
            self.assertAlmostEqual(res[2], atan2(val + 2.1, q + 1.1), places=4)
            self.assertAlmostEqual(res[3], atan2(val + 3.1, q + 0.2), places=4)
            self.assertAlmostEqual(res[4], atan2(val + 1.2, q - 1.3), places=4)
            self.assertAlmostEqual(res[5], atan2(val + 1.7, q + 0.1), places=4)
            self.assertAlmostEqual(res[6], atan2(val + 1.3, q - 1), places=4)
            self.assertAlmostEqual(res[7], atan2(val, q + 2.1), places=4)
            self.assertAlmostEqual(res[8], atan2(val, q), places=4)
            self.assertAlmostEqual(res[9], atan2(val + 1.3, q + 0.3), places=4)
            self.assertAlmostEqual(res[10], atan2(val + 2.1, q + 1.1), places=4)
            self.assertAlmostEqual(res[11], atan2(val + 3.1, q + 0.2), places=4)
            self.assertAlmostEqual(res[12], atan2(val + 1.2, q - 1.3), places=4)
            self.assertAlmostEqual(res[13], atan2(val + 1.7, q + 0.1), places=4)
            self.assertAlmostEqual(res[14], atan2(val + 1.3, q - 1), places=4)
            self.assertAlmostEqual(res[15], atan2(val, q + 2.1), places=4)

    def test_atan_f32_1(self):
        self._test_atan_f32(ISet.AVX512, fma=True)
        self._test_atan_f32(ISet.AVX512, fma=False)
        self._test_atan_f32(ISet.AVX2, fma=True)
        self._test_atan_f32(ISet.AVX2, fma=False)
        self._test_atan_f32(ISet.AVX, fma=False)
        self._test_atan_f32(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
