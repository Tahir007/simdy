import unittest
from simdy import Kernel, float64, ISet, float64x2, float64x3, float64x4, float64x8,\
    float32, float32x2, float32x3, float32x4, float32x8, float32x16


class PowTests(unittest.TestCase):

    def _test_pow_f64(self, iset, fma):
        source = """
result = pow(p1, e1)
result2 = pow(p2, e2)
result3 = pow(p3, e3)
result4 = pow(p4, e4)
result5 = pow(p5, e5)
        """
        args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()),
                ('p5', float64x8()), ('result', float64()), ('result2', float64x2()),
                ('result3', float64x3()), ('result4', float64x4()), ('result5', float64x8()),
                ('e1', float64()), ('e2', float64x2()), ('e3', float64x3()), ('e4', float64x4()), ('e5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [6.0, 5.0, 4.5, 3.65, 2.7, 2.2, 1.1, 0.4, 6.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        exps = [-1.2, -0.6, -0.3, 0.1, 0.2, 0.4, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6]
        for val, e in zip(vals, exps):
            kernel.set_value('p1', val)
            kernel.set_value('e1', e)
            kernel.set_value('p2', float64x2(val, val + 1.3))
            kernel.set_value('e2', float64x2(e, e + 0.2))
            kernel.set_value('p3', float64x3(val, val + 1.3, val + 2.1))
            kernel.set_value('e3', float64x3(e, e + 0.3, e + 0.15))
            kernel.set_value('p4', float64x4(val, val + 1.3, val + 2.1, val + 3.1))
            kernel.set_value('e4', float64x4(e, e + 0.3, e + 0.1, e + 1.1))
            v = float64x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            kernel.set_value('p5', v)
            e5 = float64x8(e, e + 1.3, e + 2.1, e + 3.1, e + 1.2, e + 1.7, e + 1.3, e + 4.8)
            kernel.set_value('e5', e5)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), pow(val, e))
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], pow(val, e))
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 0.2))
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], pow(val, e))
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 0.3))
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 0.15))
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], pow(val, e))
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 0.3))
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 0.1))
            self.assertAlmostEqual(res[3], pow(val + 3.1, e + 1.1))
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], pow(val, e))
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 1.3))
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 2.1))
            self.assertAlmostEqual(res[3], pow(val + 3.1, e + 3.1))
            self.assertAlmostEqual(res[4], pow(val + 1.2, e + 1.2))
            self.assertAlmostEqual(res[5], pow(val + 1.7, e + 1.7))
            self.assertAlmostEqual(res[6], pow(val + 1.3, e + 1.3))
            self.assertAlmostEqual(res[7], pow(val + 4.8, e + 4.8))

    def test_pow_f64_1(self):
        self._test_pow_f64(ISet.AVX512, fma=True)
        self._test_pow_f64(ISet.AVX512, fma=False)
        self._test_pow_f64(ISet.AVX2, fma=True)
        self._test_pow_f64(ISet.AVX2, fma=False)
        self._test_pow_f64(ISet.AVX, fma=False)
        self._test_pow_f64(ISet.SSE, fma=False)

    def _test_pow_f32(self, iset, fma):
        source = """
result = pow(p1, e1)
result2 = pow(p2, e2)
result3 = pow(p3, e3)
result4 =  pow(p4, e4)
result5 = pow(p5, e5)
result6 = pow(p6, e6)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()), ('p4', float32x4()),
                ('p5', float32x8()), ('p6', float32x16()),('result', float32()), ('result2', float32x2()),
                ('result3', float32x3()), ('result4', float32x4()), ('result5', float32x8()),
                ('result6', float32x16()), ('e1', float32()), ('e2', float32x2()), ('e3', float32x3()),
                ('e4', float32x4()), ('e5', float32x8()), ('e6', float32x16())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [1.0, 1.5, 0.6, 2.2, 2.7, 2.2, 1.1, 0.4, 2.2, 2.3, 0.2, 1.45, 1.9, 1.1, 1.2, 0.7]
        exps = [-1.2, -0.6, -0.3, 0.1, 0.2, 0.4, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 0.3, 1.4, 1.5, 1.6]
        for val, e in zip(vals, exps):
            kernel.set_value('p1', val)
            kernel.set_value('e1', e)
            kernel.set_value('p2', float32x2(val, val + 1.3))
            kernel.set_value('e2', float32x2(e, e + 1.3))
            kernel.set_value('p3', float32x3(val, val + 1.3, val + 2.1))
            kernel.set_value('e3', float32x3(e, e + 1.3, e + 2.1))
            kernel.set_value('p4', float32x4(val, val + 1.3, val + 2.1, val + 1.1))
            kernel.set_value('e4', float32x4(e, e + 1.3, e + 2.1, e + 1.1))
            v = float32x8(val, val + 1.3, val + 2.1, val + 1.1, val + 1.2, val + 1.7, val + 1.3, val + 1.8)
            et = float32x8(e, e + 1.3, e + 2.1, e + 1.1, e + 1.2, e + 1.7, e + 1.3, e + 1.8)
            kernel.set_value('p5', v)
            kernel.set_value('e5', et)
            v = float32x16(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8,
                           val, val + 1.1, val + 2.2, val + 3.3, val + 4.4, val + 2.5, val + 2.0, val + 2.7)
            et = float32x16(e, e + 1.3, e + 2.1, e + 3.1, e + 1.2, e + 1.7, e + 1.3, e + 4.8,
                           e, e + 1.1, e + 2.2, e + 3.3, e + 4.4, e + 2.5, e + 2.0, e + 2.7)
            kernel.set_value('p6', v)
            kernel.set_value('e6', et)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), pow(val, e), places=6)
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], pow(val, e), places=6)
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 1.3), places=5)
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], pow(val, e), places=6)
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 1.3), places=5)
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 2.1), places=4)
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], pow(val, e), places=6)
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 1.3), places=5)
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 2.1), places=4)
            self.assertAlmostEqual(res[3], pow(val + 1.1, e + 1.1), places=4)
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], pow(val, e), places=6)
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 1.3), places=5)
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 2.1), places=4)
            self.assertAlmostEqual(res[3], pow(val + 1.1, e + 1.1), places=4)
            self.assertAlmostEqual(res[4], pow(val + 1.2, e + 1.2), places=5)
            self.assertAlmostEqual(res[5], pow(val + 1.7, e + 1.7), places=4)
            self.assertAlmostEqual(res[6], pow(val + 1.3, e + 1.3), places=5)
            self.assertAlmostEqual(res[7], pow(val + 1.8, e + 1.8), places=4)
            res = kernel.get_value('result6')
            self.assertAlmostEqual(res[0], pow(val, e), places=4)
            self.assertAlmostEqual(res[1], pow(val + 1.3, e + 1.3), places=3)
            self.assertAlmostEqual(res[2], pow(val + 2.1, e + 2.1), places=3)
            self.assertAlmostEqual(res[3], pow(val + 3.1, e + 3.1), places=3)
            self.assertAlmostEqual(res[4], pow(val + 1.2, e + 1.2), places=3)
            self.assertAlmostEqual(res[5], pow(val + 1.7, e + 1.7), places=3)
            self.assertAlmostEqual(res[6], pow(val + 1.3, e + 1.3), places=3)
            self.assertAlmostEqual(res[7], pow(val + 4.8, e + 4.8), places=1)
            self.assertAlmostEqual(res[8], pow(val, e), places=4)
            self.assertAlmostEqual(res[9], pow(val + 1.1, e + 1.1), places=3)
            self.assertAlmostEqual(res[10], pow(val + 2.2, e + 2.2), places=3)
            self.assertAlmostEqual(res[11], pow(val + 3.3, e + 3.3), places=2)
            self.assertAlmostEqual(res[12], pow(val + 4.4, e + 4.4), places=1)
            self.assertAlmostEqual(res[13], pow(val + 2.5, e + 2.5), places=1)
            self.assertAlmostEqual(res[14], pow(val + 2.0, e + 2.0), places=1)
            self.assertAlmostEqual(res[15], pow(val + 2.7, e + 2.7), places=1)

    def test_pow_f32_1(self):
        self._test_pow_f32(ISet.AVX512, fma=True)
        self._test_pow_f32(ISet.AVX512, fma=False)
        self._test_pow_f32(ISet.AVX2, fma=True)
        self._test_pow_f32(ISet.AVX2, fma=False)
        self._test_pow_f32(ISet.AVX, fma=False)
        self._test_pow_f32(ISet.SSE, fma=False)

if __name__ == "__main__":
    unittest.main()
