
import unittest
from math import exp
from simdy import Kernel, float64, ISet, float64x2, float64x3, float64x4, float64x8, float32, float32x2,\
    float32x3, float32x4, float32x8, float32x16


class ExpTests(unittest.TestCase):

    def _test_exp_f64(self, iset, fma):
        source = """
result = exp(p1)
result2 = exp(p2)
result3 = exp(p3)
result4 = exp(p4)
result5 = exp(p5)
        """
        args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()),
                ('p5', float64x8()), ('result', float64()), ('result2', float64x2()),
                ('result3', float64x3()), ('result4', float64x4()), ('result5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-6.2, -5.3, -4.2, -3.45, -2.9, -2.1, -1.2, -0.7, 6.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float64x2(val, val + 1.3))
            kernel.set_value('p3', float64x3(val, val + 1.3, val + 2.1))
            kernel.set_value('p4', float64x4(val, val + 1.3, val + 2.1, val + 3.1))
            v = float64x8(val, val + 1.3, val + 2.1, val + 3.1, val + 1.2, val + 1.7, val + 1.3, val + 4.8)
            kernel.set_value('p5', v)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), exp(val))
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], exp(val))
            self.assertAlmostEqual(res[1], exp(val + 1.3))
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], exp(val))
            self.assertAlmostEqual(res[1], exp(val + 1.3))
            self.assertAlmostEqual(res[2], exp(val + 2.1))
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], exp(val))
            self.assertAlmostEqual(res[1], exp(val + 1.3))
            self.assertAlmostEqual(res[2], exp(val + 2.1))
            self.assertAlmostEqual(res[3], exp(val + 3.1))
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], exp(val))
            self.assertAlmostEqual(res[1], exp(val + 1.3))
            self.assertAlmostEqual(res[2], exp(val + 2.1))
            self.assertAlmostEqual(res[3], exp(val + 3.1))
            self.assertAlmostEqual(res[4], exp(val + 1.2))
            self.assertAlmostEqual(res[5], exp(val + 1.7))
            self.assertAlmostEqual(res[6], exp(val + 1.3))
            self.assertAlmostEqual(res[7], exp(val + 4.8))

    def test_exp_f64_1(self):
        self._test_exp_f64(ISet.AVX512, fma=True)
        self._test_exp_f64(ISet.AVX512, fma=False)
        self._test_exp_f64(ISet.AVX2, fma=True)
        self._test_exp_f64(ISet.AVX2, fma=False)
        self._test_exp_f64(ISet.AVX, fma=False)
        self._test_exp_f64(ISet.SSE, fma=False)

    def _test_exp_f32(self, iset, fma):
        source = """
result = exp(p1)
result2 = exp(p2)
result3 = exp(p3)
result4 = exp(p4)
result5 = exp(p5)
result6 = exp(p6)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()), ('p4', float32x4()),
                ('p5', float32x8()), ('p6', float32x16()),('result', float32()), ('result2', float32x2()),
                ('result3', float32x3()), ('result4', float32x4()), ('result5', float32x8()), ('result6', float32x16())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        vals = [-1.2, -5.3, -4.2, -3.45, -2.9, -2.1, -1.2, -0.7, 2.2, 5.3, 4.2, 3.45, 2.9, 2.1, 1.2, 0.7]
        for val in vals:
            kernel.set_value('p1', val)
            kernel.set_value('p2', float32x2(val, val + 0.3))
            kernel.set_value('p3', float32x3(val, val + 0.2, val + 0.1))
            kernel.set_value('p4', float32x4(val, val + 0.3, val + 0.1, val + 0.15))
            v = float32x8(val, val + 0.3, val + 0.1, val + 0.1, val + 0.2, val + 0.7, val + 0.3, val + 0.8)
            kernel.set_value('p5', v)
            v = float32x16(val, val + 0.3, val + 0.1, val + 0.1, val + 0.2, val + 0.7, val + 0.3, val + 0.8,
                           val, val + 0.11, val + 0.14, val + 0.15, val + 0.17, val + 0.13, val + 0.18, val + 0.07)
            kernel.set_value('p6', v)
            kernel.run()
            self.assertAlmostEqual(kernel.get_value('result'), exp(val), places=4)
            res = kernel.get_value('result2')
            self.assertAlmostEqual(res[0], exp(val), places=4)
            self.assertAlmostEqual(res[1], exp(val + 0.3), places=4)
            res = kernel.get_value('result3')
            self.assertAlmostEqual(res[0], exp(val), places=4)
            self.assertAlmostEqual(res[1], exp(val + 0.2), places=4)
            self.assertAlmostEqual(res[2], exp(val + 0.1), places=4)
            res = kernel.get_value('result4')
            self.assertAlmostEqual(res[0], exp(val), places=4)
            self.assertAlmostEqual(res[1], exp(val + 0.3), places=4)
            self.assertAlmostEqual(res[2], exp(val + 0.1), places=4)
            self.assertAlmostEqual(res[3], exp(val + 0.15), places=4)
            res = kernel.get_value('result5')
            self.assertAlmostEqual(res[0], exp(val), places=4)
            self.assertAlmostEqual(res[1], exp(val + 0.3), places=4)
            self.assertAlmostEqual(res[2], exp(val + 0.1), places=4)
            self.assertAlmostEqual(res[3], exp(val + 0.1), places=4)
            self.assertAlmostEqual(res[4], exp(val + 0.2), places=4)
            self.assertAlmostEqual(res[5], exp(val + 0.7), places=4)
            self.assertAlmostEqual(res[6], exp(val + 0.3), places=4)
            self.assertAlmostEqual(res[7], exp(val + 0.8), places=4)
            res = kernel.get_value('result6')
            self.assertAlmostEqual(res[0], exp(val), places=4)
            self.assertAlmostEqual(res[1], exp(val + 0.3), places=4)
            self.assertAlmostEqual(res[2], exp(val + 0.1), places=4)
            self.assertAlmostEqual(res[3], exp(val + 0.1), places=4)
            self.assertAlmostEqual(res[4], exp(val + 0.2), places=4)
            self.assertAlmostEqual(res[5], exp(val + 0.7), places=4)
            self.assertAlmostEqual(res[6], exp(val + 0.3), places=4)
            self.assertAlmostEqual(res[7], exp(val + 0.8), places=4)

            self.assertAlmostEqual(res[8], exp(val), places=4)
            self.assertAlmostEqual(res[9], exp(val + 0.11), places=4)
            self.assertAlmostEqual(res[10], exp(val + 0.14), places=4)
            self.assertAlmostEqual(res[11], exp(val + 0.15), places=4)
            self.assertAlmostEqual(res[12], exp(val + 0.17), places=4)
            self.assertAlmostEqual(res[13], exp(val + 0.13), places=4)
            self.assertAlmostEqual(res[14], exp(val + 0.18), places=4)
            self.assertAlmostEqual(res[15], exp(val + 0.07), places=4)

    def test_exp_f32_1(self):
        self._test_exp_f32(ISet.AVX512, fma=True)
        self._test_exp_f32(ISet.AVX512, fma=False)
        self._test_exp_f32(ISet.AVX2, fma=True)
        self._test_exp_f32(ISet.AVX2, fma=False)
        self._test_exp_f32(ISet.AVX, fma=False)
        self._test_exp_f32(ISet.SSE, fma=False)



if __name__ == "__main__":
    unittest.main()
