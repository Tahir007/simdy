
import unittest
from simdy import Kernel, float64x2, float64x3, float64x4, float64x8, int64x2, int64x3, int64x4, int64x8, ISet


class TestsSimpleFloat64Vec(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _test_simple_float64_vec_1(self, iset):
        source = """
p3 = p1
p4 = p2
tmp1 = p1
p5 = tmp1

p8 = p6
p9 = p7
tmp2 = p6
p10 = tmp2

p13 = p11
p14 = p12
tmp3 = p11
p15 = tmp3

p18 = p16
p19 = p17
tmp4 = p16
p20 = tmp4
                """

        args = [
            ('p1', float64x2(1, 5)), ('p2', float64x2(2)), ('p3', float64x2()),
            ('p4', float64x2()), ('p5', float64x2()),
            ('p6', float64x3(1, 5, 9)), ('p7', float64x3(2)), ('p8', float64x3()),
            ('p9', float64x3()), ('p10', float64x3()),
            ('p11', float64x4(1, 5, 9, 18)), ('p12', float64x4(2)), ('p13', float64x4()),
            ('p14', float64x4()), ('p15', float64x4()),
            ('p16', float64x8(1, 5, 9, 18, 6, 15, 78, 125)), ('p17', float64x8(2)), ('p18', float64x8()),
            ('p19', float64x8()), ('p20', float64x8())
        ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.set_value('p2', float64x2(3, 8))
        kernel.set_value('p7', float64x3(3, 8, 11))
        kernel.set_value('p12', float64x4(3, 8, 11, 18))
        kernel.set_value('p17', float64x8(3, 8, 11, 18, 9, 9, 8, 6))
        kernel.run()

        self._check(kernel.get_value('p3'), float64x2(1, 5), 2)
        self._check(kernel.get_value('p4'), float64x2(3, 8), 2)
        self._check(kernel.get_value('p5'), float64x2(1, 5), 2)
        self._check(kernel.get_value('p8'), float64x3(1, 5, 9), 3)
        self._check(kernel.get_value('p9'), float64x3(3, 8, 11), 3)
        self._check(kernel.get_value('p10'), float64x3(1, 5, 9), 3)
        self._check(kernel.get_value('p13'), float64x4(1, 5, 9, 18), 4)
        self._check(kernel.get_value('p14'), float64x4(3, 8, 11, 18), 4)
        self._check(kernel.get_value('p15'), float64x4(1, 5, 9, 18), 4)
        self._check(kernel.get_value('p18'), float64x8(1, 5, 9, 18, 6, 15, 78, 125), 8)
        self._check(kernel.get_value('p19'), float64x8(3, 8, 11, 18, 9, 9, 8, 6), 8)
        self._check(kernel.get_value('p20'), float64x8(1, 5, 9, 18, 6, 15, 78, 125), 8)

    def test_simple_float64_vec_1(self):
        self._test_simple_float64_vec_1(ISet.AVX512)
        self._test_simple_float64_vec_1(ISet.AVX)
        self._test_simple_float64_vec_1(ISet.SSE)

    def _test_simple_float64_vec_2(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1
p8 = p1 * 5
p9 = 6.0 * p1

mask1 = p1 > p2
mask2 = p1 < p2
mask3 = p1 != p2
mask4 = p1 == p2
mask6 = p1 >= p2
mask7 = p1 <= p2
mask8 = mask1 | mask2 & mask3 ^ mask4

p19 = p16 + p17
p20 = p16 - p17
tmp2 = p16 * p17
p21 = tmp2
p22 = p18 / p16
p23 = p16 * 5
p24 = 6.0 * p16

mask11 = p16 > p17
mask12 = p16 < p17
mask13 = p16 != p17
mask14 = p16 == p17
mask15 = p16 >= p17
mask16 = p16 <= p17
mask17 = mask11 | mask12 ^ mask13 & mask14

        """
        args = [('p1', float64x2(2)), ('p2', float64x2(-16)), ('p3', float64x2(87)),
                ('p4', float64x2()), ('p5', float64x2()), ('p6', float64x2()), ('p7', float64x2()),
                ('p8', float64x2()), ('p9', float64x2()), ('p10', int64x2(55)), ('p11', int64x2(55)),
                ('p12', int64x2(55)), ('p13', int64x2(55)), ('p14', int64x2(55)), ('p15', int64x2(55)),
                ('p16', float64x3(2)), ('p17', float64x3(-16)), ('p18', float64x3(87)),
                ('p19', float64x3()), ('p20', float64x3()), ('p21', float64x3()), ('p22', float64x3()),
                ('p23', float64x3()), ('p24', float64x3()), ('p25', int64x3(55)), ('p26', int64x3(55)),
                ('p27', int64x3(55)), ('p28', int64x3(55)), ('p29', int64x3(55)), ('p30', int64x3(55)),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), float64x2(-14), 2)
        self._check(kernel.get_value('p5'), float64x2(18), 2)
        self._check(kernel.get_value('p6'), float64x2(-32), 2)
        self._check(kernel.get_value('p7'), float64x2(43.5), 2)
        self._check(kernel.get_value('p8'), float64x2(10), 2)
        self._check(kernel.get_value('p9'), float64x2(12), 2)

        self._check(kernel.get_value('p19'), float64x3(-14), 3)
        self._check(kernel.get_value('p20'), float64x3(18), 3)
        self._check(kernel.get_value('p21'), float64x3(-32), 3)
        self._check(kernel.get_value('p22'), float64x3(43.5), 3)
        self._check(kernel.get_value('p23'), float64x3(10), 3)
        self._check(kernel.get_value('p24'), float64x3(12), 3)

    def test_simple_float64_vec_2(self):
        self._test_simple_float64_vec_2(ISet.AVX512)
        self._test_simple_float64_vec_2(ISet.AVX)
        self._test_simple_float64_vec_2(ISet.SSE)

    def _test_simple_float64_vec_3(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1
p8 = p1 * 5
p9 = 6.0 * p1

mask1 = p1 > p2
mask2 = p1 < p2
mask3 = p1 != p2
mask4 = p1 == p2
mask5 = p1 >= p2
mask6 = p1 <= p2
mask7 = mask1 | mask2 & mask3 ^ mask4

p19 = p16 + p17
p20 = p16 - p17
tmp2 = p16 * p17
p21 = tmp2
p22 = p18 / p16
p23 = p16 * 5
p24 = 6.0 * p16

mask11 = p16 > p17
mask12 = p16 < p17
mask13 = p16 != p17
mask14 = p16 == p17
mask15 = p16 >= p17
mask16 = p16 <= p17
mask17 = mask11 | mask12 & mask13 ^ mask14

        """
        args = [('p1', float64x4(2)), ('p2', float64x4(-16)), ('p3', float64x4(87)),
                ('p4', float64x4()), ('p5', float64x4()), ('p6', float64x4()), ('p7', float64x4()),
                ('p8', float64x4()), ('p9', float64x4()), ('p10', int64x4(55)), ('p11', int64x4(55)),
                ('p12', int64x4(55)), ('p13', int64x4(55)), ('p14', int64x4(55)), ('p15', int64x4(55)),
                ('p16', float64x8(2)), ('p17', float64x8(-16)), ('p18', float64x8(87)),
                ('p19', float64x8()), ('p20', float64x8()), ('p21', float64x8()), ('p22', float64x8()),
                ('p23', float64x8()), ('p24', float64x8()), ('p25', int64x8(55)), ('p26', int64x8(55)),
                ('p27', int64x8(55)), ('p28', int64x8(55)), ('p29', int64x8(55)), ('p30', int64x8(55)),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), float64x4(-14), 4)
        self._check(kernel.get_value('p5'), float64x4(18), 4)
        self._check(kernel.get_value('p6'), float64x4(-32), 4)
        self._check(kernel.get_value('p7'), float64x4(43.5), 4)
        self._check(kernel.get_value('p8'), float64x4(10), 4)
        self._check(kernel.get_value('p9'), float64x4(12), 4)

        self._check(kernel.get_value('p19'), float64x8(-14), 8)
        self._check(kernel.get_value('p20'), float64x8(18), 8)
        self._check(kernel.get_value('p21'), float64x8(-32), 8)
        self._check(kernel.get_value('p22'), float64x8(43.5), 8)
        self._check(kernel.get_value('p23'), float64x8(10), 8)
        self._check(kernel.get_value('p24'), float64x8(12), 8)

    def test_simple_float64_vec_3(self):
        self._test_simple_float64_vec_3(ISet.AVX512)
        self._test_simple_float64_vec_3(ISet.AVX)
        self._test_simple_float64_vec_3(ISet.SSE)

    def _test_simple_float64_vec_4(self, iset, fma):
        source = """
p4 = p1 * p2 + p3
p5 = p1 * p2 - p3
p6 = p3 + p1 * p2
p7 = p3 - p1 * p2

p14 = p11 * p12 + p13
p15 = p11 * p12 - p13
p16 = p13 + p11 * p12
p17 = p13 - p11 * p12

p24 = p21 * p22 + p23
p25 = p21 * p22 - p23
p26 = p23 + p21 * p22
p27 = p23 - p21 * p22

p34 = p31 * p32 + p33
p35 = p31 * p32 - p33
p36 = p33 + p31 * p32
p37 = p33 - p31 * p32
        """
        args = [('p1', float64x2(2)), ('p2', float64x2(3)), ('p3', float64x2(4)),
                ('p4', float64x2()), ('p5', float64x2()), ('p6', float64x2()), ('p7', float64x2()),
                ('p8', float64x2()), ('p9', float64x2()), ('p10', float64x2()),
                ('p11', float64x3(2)), ('p12', float64x3(3)), ('p13', float64x3(4)),
                ('p14', float64x3()), ('p15', float64x3()), ('p16', float64x3()), ('p17', float64x3()),
                ('p21', float64x4(2)), ('p22', float64x4(3)), ('p23', float64x4(4)),
                ('p24', float64x4()), ('p25', float64x4()), ('p26', float64x4()), ('p27', float64x4()),
                ('p31', float64x8(2)), ('p32', float64x8(3)), ('p33', float64x8(4)),
                ('p34', float64x8()), ('p35', float64x8()), ('p36', float64x8()), ('p37', float64x8())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()

        self._check(kernel.get_value('p4'), float64x2(10), 2)
        self._check(kernel.get_value('p5'), float64x2(2), 2)
        self._check(kernel.get_value('p6'), float64x2(10), 2)
        self._check(kernel.get_value('p7'), float64x2(-2), 2)

        self._check(kernel.get_value('p14'), float64x3(10), 3)
        self._check(kernel.get_value('p15'), float64x3(2), 3)
        self._check(kernel.get_value('p16'), float64x3(10), 3)
        self._check(kernel.get_value('p17'), float64x3(-2), 3)

        self._check(kernel.get_value('p24'), float64x4(10), 4)
        self._check(kernel.get_value('p25'), float64x4(2), 4)
        self._check(kernel.get_value('p26'), float64x4(10), 4)
        self._check(kernel.get_value('p27'), float64x4(-2), 4)

        self._check(kernel.get_value('p34'), float64x8(10), 8)
        self._check(kernel.get_value('p35'), float64x8(2), 8)
        self._check(kernel.get_value('p36'), float64x8(10), 8)
        self._check(kernel.get_value('p37'), float64x8(-2), 8)

    def test_simple_float64_vec_4(self):
        self._test_simple_float64_vec_4(ISet.AVX512, fma=True)
        self._test_simple_float64_vec_4(ISet.AVX512, fma=False)
        self._test_simple_float64_vec_4(ISet.AVX2, fma=True)
        self._test_simple_float64_vec_4(ISet.AVX2, fma=False)
        self._test_simple_float64_vec_4(ISet.AVX, fma=False)
        self._test_simple_float64_vec_4(ISet.SSE, fma=False)

    def _test_simple_float64_vec_5(self, iset, fma):
        source = """
p4 = p1 * 3 + p3
p5 = 2.0 * p2 - p3
p6 = p3 + 2 * p2
p7 = p3 - 2 * p2

p14 = 2 * p12 + p13
p15 = p11 * 3.0 - p13
p16 = p13 + 2.0 * p12
p17 = p13 - p11 * 3

p24 = 2 * p22 + p23
p25 = p21 * 3 - p23
p26 = p23 + p21 * 3.0
p27 = p23 - 2 * p22

p34 = p31 * 3 + p33
p35 = 2 * p32 - p33
p36 = p33 + 2 * p32
p37 = p33 - 2.0 * p32
        """
        args = [('p1', float64x2(2)), ('p2', float64x2(3)), ('p3', float64x2(4)),
                ('p4', float64x2()), ('p5', float64x2()), ('p6', float64x2()), ('p7', float64x2()),
                ('p8', float64x2()), ('p9', float64x2()), ('p10', float64x2()),
                ('p11', float64x3(2)), ('p12', float64x3(3)), ('p13', float64x3(4)),
                ('p14', float64x3()), ('p15', float64x3()), ('p16', float64x3()), ('p17', float64x3()),
                ('p21', float64x4(2)), ('p22', float64x4(3)), ('p23', float64x4(4)),
                ('p24', float64x4()), ('p25', float64x4()), ('p26', float64x4()), ('p27', float64x4()),
                ('p31', float64x8(2)), ('p32', float64x8(3)), ('p33', float64x8(4)),
                ('p34', float64x8()), ('p35', float64x8()), ('p36', float64x8()), ('p37', float64x8())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        self._check(kernel.get_value('p4'), float64x2(10), 2)
        self._check(kernel.get_value('p5'), float64x2(2), 2)
        self._check(kernel.get_value('p6'), float64x2(10), 2)
        self._check(kernel.get_value('p7'), float64x2(-2), 2)

        self._check(kernel.get_value('p14'), float64x3(10), 3)
        self._check(kernel.get_value('p15'), float64x3(2), 3)
        self._check(kernel.get_value('p16'), float64x3(10), 3)
        self._check(kernel.get_value('p17'), float64x3(-2), 3)

        self._check(kernel.get_value('p24'), float64x4(10), 4)
        self._check(kernel.get_value('p25'), float64x4(2), 4)
        self._check(kernel.get_value('p26'), float64x4(10), 4)
        self._check(kernel.get_value('p27'), float64x4(-2), 4)

        self._check(kernel.get_value('p34'), float64x8(10), 8)
        self._check(kernel.get_value('p35'), float64x8(2), 8)
        self._check(kernel.get_value('p36'), float64x8(10), 8)
        self._check(kernel.get_value('p37'), float64x8(-2), 8)

    def test_simple_float64_vec_5(self):
        self._test_simple_float64_vec_5(ISet.AVX512, fma=True)
        self._test_simple_float64_vec_5(ISet.AVX512, fma=False)
        self._test_simple_float64_vec_5(ISet.AVX2, fma=True)
        self._test_simple_float64_vec_5(ISet.AVX2, fma=False)
        self._test_simple_float64_vec_5(ISet.AVX, fma=False)
        self._test_simple_float64_vec_5(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
