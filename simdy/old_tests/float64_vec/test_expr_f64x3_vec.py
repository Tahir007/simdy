
import unittest
from simdy import float64x3, Kernel, ISet


class TestsExprF64x3(unittest.TestCase):

    def _tmps(self, p1, p2, p3, p4):
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 / p2)
        tmp4 = 5 * p1 / p2
        return (tmp1, tmp2, tmp3, tmp4)

    def _expr_p5(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
        return p5

    def _expr_p6(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p6 = tmp4 + tmp1 / tmp2 * tmp1 - p1 - p2 - (tmp1 * tmp3 - p4)
        return p6

    def _expr_p7(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p7 = p1 * tmp1 / tmp2 - tmp1 + tmp1 * p3 + p1 + p1 * p2 - tmp1 * tmp2 - tmp4
        return p7

    def _expr_p8(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p8 = p3 * tmp4 * 2 - tmp1 * p3 / p1 * 3 / p4 + tmp4 + tmp1 * (2 * tmp4 - p1 - p2)
        return p8

    def _test_expr_float64x3_1(self, iset, fma):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 / p2)
tmp4 = 5 * p1 / p2
p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
p6 = tmp4 + tmp1 / tmp2 * tmp1 - p1 - p2 - (tmp1 * tmp3 - p4)
p7 = p1 * tmp1 / tmp2 - tmp1 + tmp1 * p3 + p1 + p1 * p2 - tmp1 * tmp2 - tmp4
p8 = p3 * tmp4 * 2 - tmp1 * p3 / p1 * 3 / p4 + tmp4 + tmp1 * (2 * tmp4 - p1 - p2)

scale = 2.0
p9 = p1 * scale
p10 = p1 * (scale + scale)
p11 = scale * p1 + p2
p12 = p1 * (scale + scale) * p1 + p2
        """
        args = [('p1', float64x3(10, 3, -5)), ('p2', float64x3(-6, 7, 1)), ('p3', float64x3(2, -5, 8)),
                ('p4', float64x3(-20, 3, 10)),
                ('p5', float64x3()), ('p6', float64x3()), ('p7', float64x3(111)), ('p8', float64x3()),
                ('p9', float64x3()), ('p10', float64x3()), ('p11', float64x3()), ('p12', float64x3())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        p5_0 = self._expr_p5(p1[0], p2[0], p3[0], p4[0])
        p5_1 = self._expr_p5(p1[1], p2[1], p3[1], p4[1])
        p5_2 = self._expr_p5(p1[2], p2[2], p3[2], p4[2])
        self.assertEqual(kernel.get_value('p5')[0], p5_0)
        self.assertEqual(kernel.get_value('p5')[1], p5_1)
        self.assertEqual(kernel.get_value('p5')[2], p5_2)
        p6_0 = self._expr_p6(p1[0], p2[0], p3[0], p4[0])
        p6_1 = self._expr_p6(p1[1], p2[1], p3[1], p4[1])
        p6_2 = self._expr_p6(p1[2], p2[2], p3[2], p4[2])
        self.assertEqual(kernel.get_value('p6')[0], p6_0)
        self.assertEqual(kernel.get_value('p6')[1], p6_1)
        self.assertEqual(kernel.get_value('p6')[2], p6_2)
        p7_0 = self._expr_p7(p1[0], p2[0], p3[0], p4[0])
        p7_1 = self._expr_p7(p1[1], p2[1], p3[1], p4[1])
        p7_2 = self._expr_p7(p1[2], p2[2], p3[2], p4[2])
        self.assertEqual(kernel.get_value('p7')[0], p7_0)
        self.assertEqual(kernel.get_value('p7')[1], p7_1)
        self.assertEqual(kernel.get_value('p7')[2], p7_2)
        p8_0 = self._expr_p8(p1[0], p2[0], p3[0], p4[0])
        p8_1 = self._expr_p8(p1[1], p2[1], p3[1], p4[1])
        p8_2 = self._expr_p8(p1[2], p2[2], p3[2], p4[2])
        self.assertEqual(kernel.get_value('p8')[0], p8_0)
        self.assertEqual(kernel.get_value('p8')[1], p8_1)
        self.assertEqual(kernel.get_value('p8')[2], p8_2)

        # scalar x vector multiplication
        self.assertEqual(kernel.get_value('p9')[0], 2.0 * 10)
        self.assertEqual(kernel.get_value('p9')[1], 2.0 * 3)
        self.assertEqual(kernel.get_value('p9')[2], 2.0 * -5)
        self.assertEqual(kernel.get_value('p10')[0], 4.0 * 10)
        self.assertEqual(kernel.get_value('p10')[1], 4.0 * 3)
        self.assertEqual(kernel.get_value('p10')[2], 4.0 * -5)
        self.assertEqual(kernel.get_value('p11')[0], 2.0 * 10 + -6)
        self.assertEqual(kernel.get_value('p11')[1], 2.0 * 3 + 7)
        self.assertEqual(kernel.get_value('p11')[2], 2.0 * -5 + 1)
        self.assertEqual(kernel.get_value('p12')[0], 4.0 * 10 * 10 + -6)
        self.assertEqual(kernel.get_value('p12')[1], 4.0 * 3 * 3 + 7)
        self.assertEqual(kernel.get_value('p12')[2], 4.0 * -5 * -5 + 1)

    def test_expr_float64x3_1(self):
        self._test_expr_float64x3_1(ISet.AVX512, fma=True)
        self._test_expr_float64x3_1(ISet.AVX512, fma=False)
        self._test_expr_float64x3_1(ISet.AVX2, fma=True)
        self._test_expr_float64x3_1(ISet.AVX2, fma=False)
        self._test_expr_float64x3_1(ISet.AVX, fma=False)
        self._test_expr_float64x3_1(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
