
import unittest
from simdy import int32x2, Kernel, ISet


class TestsExprInt32x2(unittest.TestCase):

    def _tmps(self, p1, p2, p3, p4):
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 - p2)
        return (tmp1, tmp2, tmp3)

    def _expr_p5(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3 = self._tmps(p1, p2, p3, p4)
        p5 = (tmp1 << 3) | (tmp2 >> 1)
        return p5

    def _expr_p6(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3 = self._tmps(p1, p2, p3, p4)
        p6 = tmp3 & tmp1 ^ p1 + tmp2 + p1 + p2 - p4
        return p6

    def _expr_p7(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3 = self._tmps(p1, p2, p3, p4)
        p7 = (p1 + tmp3) * p1 - tmp1 | p2
        return p7

    def _test_expr_int32x2_1(self, iset):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 - p2)

p5 = (tmp1 << 3) | (tmp2 >> 1)
p6 = tmp3 & tmp1 ^ p1 + tmp2 + p1 + p2 - p4
p7 = (p1 + tmp3) * p1 - tmp1 | p2
        """

        args = [('p1', int32x2(10)), ('p2', int32x2(-6)),
                ('p3', int32x2(2)), ('p4', int32x2(-20)),
                ('p5', int32x2()), ('p6', int32x2()), ('p7', int32x2())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        p5_0 = self._expr_p5(p1[0], p2[0], p3[0], p4[0])
        p5_1 = self._expr_p5(p1[1], p2[1], p3[1], p4[1])
        self.assertEqual(kernel.get_value('p5')[0], p5_0)
        self.assertEqual(kernel.get_value('p5')[1], p5_1)
        p6_0 = self._expr_p6(p1[0], p2[0], p3[0], p4[0])
        p6_1 = self._expr_p6(p1[1], p2[1], p3[1], p4[1])
        self.assertEqual(kernel.get_value('p6')[0], p6_0)
        self.assertEqual(kernel.get_value('p6')[1], p6_1)
        p7_0 = self._expr_p7(p1[0], p2[0], p3[0], p4[0])
        p7_1 = self._expr_p7(p1[1], p2[1], p3[1], p4[1])
        self.assertEqual(kernel.get_value('p7')[0], p7_0)
        self.assertEqual(kernel.get_value('p7')[1], p7_1)

    def test_expr_int32x2_1(self):
        self._test_expr_int32x2_1(ISet.AVX512)
        self._test_expr_int32x2_1(ISet.AVX2)
        self._test_expr_int32x2_1(ISet.AVX)
        self._test_expr_int32x2_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
