
import unittest
from simdy import int32x8, Kernel, ISet


class TestsExprInt32x8(unittest.TestCase):

    def _tmps(self, p1, p2, p3, p4):
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 - p2)
        return (tmp1, tmp2, tmp3)

    def _expr_p5(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3 = self._tmps(p1, p2, p3, p4)
        p5 = (tmp1 << 3) | (tmp2 >> 1)
        return p5

    def _expr_p6(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3 = self._tmps(p1, p2, p3, p4)
        p6 = tmp3 & tmp1 ^ p1 + tmp2 + p1 + p2 - p4
        return p6

    def _expr_p7(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3 = self._tmps(p1, p2, p3, p4)
        p7 = (p1 + tmp3) * p1 - tmp1 | p2
        return p7

    def _test_expr_int32x8_1(self, iset):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 - p2)

p5 = (tmp1 << 3) | (tmp2 >> 1)
p6 = tmp3 & tmp1 ^ p1 + tmp2 + p1 + p2 - p4
p7 = (p1 + tmp3) * p1 - tmp1 | p2
        """

        args = [('p1', int32x8(10, 3, -1, 6, 3, 4, 5, 6)),
                ('p2', int32x8(-6, 4, 5, 1, 1, 7, 7, 4)),
                ('p3', int32x8(2, 7, 8, 2, 3, 2, 1, 8)),
                ('p4', int32x8(-20, 2, 1, 9, 1, 99, 3, -2)),
                ('p5', int32x8()), ('p6', int32x8()), ('p7', int32x8())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        p5_0 = self._expr_p5(p1[0], p2[0], p3[0], p4[0])
        p5_1 = self._expr_p5(p1[1], p2[1], p3[1], p4[1])
        p5_2 = self._expr_p5(p1[2], p2[2], p3[2], p4[2])
        p5_3 = self._expr_p5(p1[3], p2[3], p3[3], p4[3])
        p5_4 = self._expr_p5(p1[4], p2[4], p3[4], p4[4])
        p5_5 = self._expr_p5(p1[5], p2[5], p3[5], p4[5])
        p5_6 = self._expr_p5(p1[6], p2[6], p3[6], p4[6])
        p5_7 = self._expr_p5(p1[7], p2[7], p3[7], p4[7])
        self.assertEqual(kernel.get_value('p5')[0], p5_0)
        self.assertEqual(kernel.get_value('p5')[1], p5_1)
        self.assertEqual(kernel.get_value('p5')[2], p5_2)
        self.assertEqual(kernel.get_value('p5')[3], p5_3)
        self.assertEqual(kernel.get_value('p5')[4], p5_4)
        self.assertEqual(kernel.get_value('p5')[5], p5_5)
        self.assertEqual(kernel.get_value('p5')[6], p5_6)
        self.assertEqual(kernel.get_value('p5')[7], p5_7)
        p6_0 = self._expr_p6(p1[0], p2[0], p3[0], p4[0])
        p6_1 = self._expr_p6(p1[1], p2[1], p3[1], p4[1])
        p6_2 = self._expr_p6(p1[2], p2[2], p3[2], p4[2])
        p6_3 = self._expr_p6(p1[3], p2[3], p3[3], p4[3])
        p6_4 = self._expr_p6(p1[4], p2[4], p3[4], p4[4])
        p6_5 = self._expr_p6(p1[5], p2[5], p3[5], p4[5])
        p6_6 = self._expr_p6(p1[6], p2[6], p3[6], p4[6])
        p6_7 = self._expr_p6(p1[7], p2[7], p3[7], p4[7])
        self.assertEqual(kernel.get_value('p6')[0], p6_0)
        self.assertEqual(kernel.get_value('p6')[1], p6_1)
        self.assertEqual(kernel.get_value('p6')[2], p6_2)
        self.assertEqual(kernel.get_value('p6')[3], p6_3)
        self.assertEqual(kernel.get_value('p6')[4], p6_4)
        self.assertEqual(kernel.get_value('p6')[5], p6_5)
        self.assertEqual(kernel.get_value('p6')[6], p6_6)
        self.assertEqual(kernel.get_value('p6')[7], p6_7)
        p7_0 = self._expr_p7(p1[0], p2[0], p3[0], p4[0])
        p7_1 = self._expr_p7(p1[1], p2[1], p3[1], p4[1])
        p7_2 = self._expr_p7(p1[2], p2[2], p3[2], p4[2])
        p7_3 = self._expr_p7(p1[3], p2[3], p3[3], p4[3])
        p7_4 = self._expr_p7(p1[4], p2[4], p3[4], p4[4])
        p7_5 = self._expr_p7(p1[5], p2[5], p3[5], p4[5])
        p7_6 = self._expr_p7(p1[6], p2[6], p3[6], p4[6])
        p7_7 = self._expr_p7(p1[7], p2[7], p3[7], p4[7])
        self.assertEqual(kernel.get_value('p7')[0], p7_0)
        self.assertEqual(kernel.get_value('p7')[1], p7_1)
        self.assertEqual(kernel.get_value('p7')[2], p7_2)
        self.assertEqual(kernel.get_value('p7')[3], p7_3)
        self.assertEqual(kernel.get_value('p7')[4], p7_4)
        self.assertEqual(kernel.get_value('p7')[5], p7_5)
        self.assertEqual(kernel.get_value('p7')[6], p7_6)
        self.assertEqual(kernel.get_value('p7')[7], p7_7)

    def test_expr_int32x8_1(self):
        self._test_expr_int32x8_1(ISet.AVX512)
        self._test_expr_int32x8_1(ISet.AVX2)
        self._test_expr_int32x8_1(ISet.AVX)
        self._test_expr_int32x8_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
