
import unittest
from simdy import Kernel, int32x2, int32x3, int32x4, int32x8, int32x16, ISet


class TestsSimpleInt32Vec(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _test_simple_int32_vec_1(self, iset):
        source = """
p3 = p1
p4 = p2
tmp1 = p1
p5 = tmp1

p8 = p6
p9 = p7
tmp2 = p6
p10 = tmp2

p13 = p11
p14 = p12
tmp3 = p11
p15 = tmp3

p18 = p16
p19 = p17
tmp4 = p16
p20 = tmp4

p23 = p21
p24 = p22
tmp5 = p21
p25 = tmp5
                """

        args = [
            ('p1', int32x2(1, 5)), ('p2', int32x2(2)), ('p3', int32x2()),
            ('p4', int32x2()), ('p5', int32x2()),
            ('p6', int32x3(1, 5, 9)), ('p7', int32x3(2)), ('p8', int32x3()),
            ('p9', int32x3()), ('p10', int32x3()),
            ('p11', int32x4(1, 5, 9, 18)), ('p12', int32x4(2)), ('p13', int32x4()),
            ('p14', int32x4()), ('p15', int32x4()),
            ('p16', int32x8(1, 5, 9, 18, 6, 15, 78, 125)), ('p17', int32x8(2)), ('p18', int32x8()),
            ('p19', int32x8()), ('p20', int32x8()),
            ('p21', int32x16(1, 5, 9, 18, 6, 15, 78, 125, 7, 7, 7, 3, 4, 4, 4, 14)), ('p22', int32x16(2)),
            ('p23', int32x16()), ('p24', int32x16()), ('p25', int32x16())
        ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.set_value('p2', int32x2(3, 8))
        kernel.set_value('p7', int32x3(3, 8, 11))
        kernel.set_value('p12', int32x4(3, 8, 11, 18))
        kernel.set_value('p17', int32x8(3, 8, 11, 18, 9, 9, 8, 6))
        kernel.set_value('p22', int32x16(3, 8, 11, 18, 9, 9, 8, 6, 2, 2, 2, 3, 7, 7, 7, 2))
        kernel.run()

        self._check(kernel.get_value('p3'), int32x2(1, 5), 2)
        self._check(kernel.get_value('p4'), int32x2(3, 8), 2)
        self._check(kernel.get_value('p5'), int32x2(1, 5), 2)

        self._check(kernel.get_value('p8'), int32x3(1, 5, 9), 3)
        self._check(kernel.get_value('p9'), int32x3(3, 8, 11), 3)
        self._check(kernel.get_value('p10'), int32x3(1, 5, 9), 3)

        self._check(kernel.get_value('p13'), int32x4(1, 5, 9, 18), 4)
        self._check(kernel.get_value('p14'), int32x4(3, 8, 11, 18), 4)
        self._check(kernel.get_value('p15'), int32x4(1, 5, 9, 18), 4)

        self._check(kernel.get_value('p18'), int32x8(1, 5, 9, 18, 6, 15, 78, 125), 8)
        self._check(kernel.get_value('p19'), int32x8(3, 8, 11, 18, 9, 9, 8, 6), 8)
        self._check(kernel.get_value('p20'), int32x8(1, 5, 9, 18, 6, 15, 78, 125), 8)

        self._check(kernel.get_value('p23'), int32x16(1, 5, 9, 18, 6, 15, 78, 125, 7, 7, 7, 3, 4, 4, 4, 14), 16)
        self._check(kernel.get_value('p24'), int32x16(3, 8, 11, 18, 9, 9, 8, 6, 2, 2, 2, 3, 7, 7, 7, 2), 16)
        self._check(kernel.get_value('p25'), int32x16(1, 5, 9, 18, 6, 15, 78, 125, 7, 7, 7, 3, 4, 4, 4, 14), 16)

    def test_simple_int32_vec_1(self):
        self._test_simple_int32_vec_1(ISet.AVX512)
        self._test_simple_int32_vec_1(ISet.AVX2)
        self._test_simple_int32_vec_1(ISet.AVX)
        self._test_simple_int32_vec_1(ISet.SSE)

    def _test_simple_int32_vec_2(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
p6 = p1 * p2
mask1 = p1 > p2
mask2 = p1 == p2
p9 = p1 & p3
p10 = p1 | p3
p11 = p1 ^ p3
mask3 = mask1 & mask2

p15 = p12 + p13
p16 = p12 - p13
p17 = p12 * p13
mask11 = p12 > p13
mask12 = p12 == p13
p20 = p12 & p14
p21 = p12 | p14
p22 = p12 ^ p14
mask13 = mask11 | mask12

p26 = p23 + p24
p27 = p23 - p24
p28 = p23 * p24
mask21 = p23 > p24
mask22 = p23 == p24
p31 = p23 & p25
p32 = p23 | p25
p33 = p23 ^ p25
mask23 = mask21 ^ mask22
        """
        args = [('p1', int32x2(2)), ('p2', int32x2(-16)), ('p3', int32x2(8)),
                ('p4', int32x2()), ('p5', int32x2()), ('p6', int32x2()), ('p7', int32x2(88)),
                ('p8', int32x2(88)), ('p9', int32x2(88)), ('p10', int32x2(88)), ('p11', int32x2(88)),
                ('p12', int32x3(2)), ('p13', int32x3(-16)), ('p14', int32x3(8)),
                ('p15', int32x3()), ('p16', int32x3()), ('p17', int32x3()), ('p18', int32x3(88)),
                ('p19', int32x3(88)), ('p20', int32x3(88)), ('p21', int32x3(88)), ('p22', int32x3(88)),
                ('p23', int32x4(2)), ('p24', int32x4(-16)), ('p25', int32x4(8)),
                ('p26', int32x4()), ('p27', int32x4()), ('p28', int32x4()), ('p29', int32x4(88)),
                ('p30', int32x4(88)), ('p31', int32x4(88)), ('p32', int32x4(88)), ('p33', int32x4(88))]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), int32x2(-14), 2)
        self._check(kernel.get_value('p5'), int32x2(18), 2)
        self._check(kernel.get_value('p6'), int32x2(-32), 2)
        self._check(kernel.get_value('p9'), int32x2(0), 2)
        self._check(kernel.get_value('p10'), int32x2(10), 2)
        self._check(kernel.get_value('p11'), int32x2(10), 2)

        self._check(kernel.get_value('p15'), int32x3(-14), 3)
        self._check(kernel.get_value('p16'), int32x3(18), 3)
        self._check(kernel.get_value('p17'), int32x3(-32), 3)
        self._check(kernel.get_value('p20'), int32x3(0), 3)
        self._check(kernel.get_value('p21'), int32x3(10), 3)
        self._check(kernel.get_value('p22'), int32x3(10), 3)

        self._check(kernel.get_value('p26'), int32x4(-14), 4)
        self._check(kernel.get_value('p27'), int32x4(18), 4)
        self._check(kernel.get_value('p28'), int32x4(-32), 4)
        self._check(kernel.get_value('p31'), int32x4(0), 4)
        self._check(kernel.get_value('p32'), int32x4(10), 4)
        self._check(kernel.get_value('p33'), int32x4(10), 4)

    def test_simple_int32_vec_2(self):
        self._test_simple_int32_vec_2(ISet.AVX512)
        self._test_simple_int32_vec_2(ISet.AVX)
        self._test_simple_int32_vec_2(ISet.SSE)

    def _test_simple_int32_vec_3(self, iset):
        source = """
p0_r = p0 + p1
p4 = p1 + p2
p5 = p1 - p2
p6 = p1 * p2
mask1 = p1 > p2
mask2 = p1 == p2
p9 = p1 & p3
p10 = p1 | p3
p11 = p1 ^ p3
mask3 = mask1 | mask2

p15 = p12 + p13
p16 = p12 - p13
p17 = p12 * p13
mask11 = p12 > p13
mask12 = p12 == p13
p20 = p12 & p14
p21 = p12 | p14
p22 = p12 ^ p14
mask13 = mask11 & mask12
        """
        args = [('p0', int32x8(1, 2, 3, 4, 5, 6, 7, 8)), ('p0_r', int32x8()),
                ('p1', int32x8(2)), ('p2', int32x8(-16)), ('p3', int32x8(8)),
                ('p4', int32x8()), ('p5', int32x8()), ('p6', int32x8()), ('p7', int32x8(88)),
                ('p8', int32x8(88)), ('p9', int32x8(88)), ('p10', int32x8(88)), ('p11', int32x8(88)),
                ('p12', int32x16(2)), ('p13', int32x16(-16)), ('p14', int32x16(8)),
                ('p15', int32x16()), ('p16', int32x16()), ('p17', int32x16()), ('p18', int32x16(88)),
                ('p19', int32x16(88)), ('p20', int32x16(88)), ('p21', int32x16(88)), ('p22', int32x16(88))]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check(kernel.get_value('p0_r'), int32x8(3, 4, 5, 6, 7, 8, 9, 10), 8)
        self._check(kernel.get_value('p4'), int32x8(-14), 8)
        self._check(kernel.get_value('p5'), int32x8(18), 8)
        self._check(kernel.get_value('p6'), int32x8(-32), 8)
        self._check(kernel.get_value('p9'), int32x8(0), 8)
        self._check(kernel.get_value('p10'), int32x8(10), 8)
        self._check(kernel.get_value('p11'), int32x8(10), 8)

        self._check(kernel.get_value('p15'), int32x16(-14), 16)
        self._check(kernel.get_value('p16'), int32x16(18), 16)
        self._check(kernel.get_value('p17'), int32x16(-32), 16)
        self._check(kernel.get_value('p20'), int32x16(0), 16)
        self._check(kernel.get_value('p21'), int32x16(10), 16)
        self._check(kernel.get_value('p22'), int32x16(10), 16)

    def test_simple_int32_vec_3(self):
        self._test_simple_int32_vec_3(ISet.AVX512)
        self._test_simple_int32_vec_3(ISet.AVX2)
        self._test_simple_int32_vec_3(ISet.AVX)
        self._test_simple_int32_vec_3(ISet.SSE)

    def _test_simple_int32_vec4(self, iset):
        source = """
p2 = p1 << 2
p3 = p1 >> 3

p12 = p11 << 2
p13 = p11 >> 3

p22 = p21 << 2
p23 = p21 >> 3

p32 = p31 << 2
p33 = p31 >> 3

p42 = p41 << 2
p43 = p41 >> 3
        """
        args = [('p1', int32x2(20)), ('p2', int32x2()), ('p3', int32x2()),
                ('p11', int32x3(20)), ('p12', int32x3()), ('p13', int32x3()),
                ('p21', int32x4(20)), ('p22', int32x4()), ('p23', int32x4()),
                ('p31', int32x8(20)), ('p32', int32x8()), ('p33', int32x8()),
                ('p41', int32x16(20, 20, 20, 20, 40, 40, 40, 80, 80, 80, 80, 10, 10, 10, 10, 10)),
                ('p42', int32x16()), ('p43', int32x16())]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p2'), int32x2(80), 2)
        self._check(kernel.get_value('p3'), int32x2(2), 2)

        self._check(kernel.get_value('p12'), int32x3(80), 3)
        self._check(kernel.get_value('p13'), int32x3(2), 3)

        self._check(kernel.get_value('p22'), int32x4(80), 4)
        self._check(kernel.get_value('p23'), int32x4(2), 4)

        self._check(kernel.get_value('p32'), int32x8(80), 8)
        self._check(kernel.get_value('p33'), int32x8(2), 8)

        self._check(kernel.get_value('p42'), int32x16(80, 80, 80, 80, 160, 160, 160, 320, 320, 320, 320, 40, 40, 40, 40, 40), 16)
        self._check(kernel.get_value('p43'), int32x16(2, 2, 2, 2, 5, 5, 5, 10, 10, 10, 10, 1, 1, 1, 1, 1), 16)

    def test_simple_int32_vec4(self):
        self._test_simple_int32_vec4(ISet.AVX512)
        self._test_simple_int32_vec4(ISet.AVX2)
        self._test_simple_int32_vec4(ISet.AVX)
        self._test_simple_int32_vec4(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
