
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, float32, float64, int64x2, int64x3, int64x4, int64x8, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, struct, register_user_type, ISet


class TestStruct3(struct):
    __slots__ = ['p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10']

    def __init__(self):
        self.p1 = int32(1)
        self.p2 = float32(1)
        self.p3 = float64(1)
        self.p4 = int64(1)

        self.p5 = float64x2(1, 2)
        self.p6 = float32x3(1, 2, 3)
        self.p7 = int32x4(1, 2, 3, 4)
        self.p8 = float64x8(1, 2, 3, 4, 5, 6, 7, 8)
        self.p9 = float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
        self.p10 = int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)


register_user_type(TestStruct3, factory=lambda: TestStruct3())


class TestStruct4(struct):
    __slots__ = ['p1', 'p2', 'p3', 'p4']

    def __init__(self, p1, p2):
        self.p1 = int32(p1)
        self.p2 = int32(p2)
        self.p3 = float64(1)
        self.p4 = int64(1)


register_user_type(TestStruct4, factory=lambda: TestStruct4(1, 1))


class StructTest2(unittest.TestCase):

    def _check_x2(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])

    def _check_x3(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])

    def _check_x4(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])

    def _check_x8(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])

    def _check_x16(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])
        self.assertAlmostEqual(src1[8], src2[8])
        self.assertAlmostEqual(src1[9], src2[9])
        self.assertAlmostEqual(src1[10], src2[10])
        self.assertAlmostEqual(src1[11], src2[11])
        self.assertAlmostEqual(src1[12], src2[12])
        self.assertAlmostEqual(src1[13], src2[13])
        self.assertAlmostEqual(src1[14], src2[14])
        self.assertAlmostEqual(src1[15], src2[15])

    def _test_struct_1(self, iset):
        source = """

def add_params(strc1, strc2):
    tmp = strc1.p1 + strc2.p2
    return tmp

def add_params2(strc1: TestStruct4, strc2: TestStruct4):
    return strc1.p3 + strc1.p3

p1 = s1.p1 + 4
p2 = 6 + s1.p4
p3 = s1.p10 + s2.p10
p4 = s1.p5 + n1
p5 = n1 + s1.p5
p6 = float32x16(4) + s1.p9
p7 = s1.p8 + float64x8(5)
p8 = (s1.p7 + s2.p7) + s1.p7
p9 = s1.p6 + (s1.p6 + s2.p6)

loc_s1 = TestStruct4(2, 3)
p10 = loc_s1.p2
loc_s1.p3 = 4.5
p11 = add_params(loc_s1, loc_s1)
p12 = add_params2(loc_s1, loc_s1)
        """

        args = [('s1', TestStruct3()), ('s2', TestStruct3()), ('p1', int32()), ('p2', int64()),
                ('p3', int32x16()), ('n1', float64x2(2)), ('p4', float64x2()), ('p5', float64x2()),
                ('p6', float32x16()), ('p7', float64x8()), ('p8', int32x4()), ('p9', float32x3()),
                ('p10', int32()), ('p11', int32()), ('p12', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 5)
        self.assertEqual(kernel.get_value('p2'), 7)
        self._check_x16(kernel.get_value('p3'), int32x16(2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32))
        self._check_x2(kernel.get_value('p4'), float64x2(3, 4))
        self._check_x2(kernel.get_value('p5'), float64x2(3, 4))
        self._check_x16(kernel.get_value('p6'), float32x16(5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))
        self._check_x8(kernel.get_value('p7'), float64x8(6, 7, 8, 9, 10, 11, 12, 13))
        self._check_x4(kernel.get_value('p8'), int32x4(3, 6, 9, 12))
        self._check_x3(kernel.get_value('p9'), float32x3(3, 6, 9))
        self.assertEqual(kernel.get_value('p10'), 3)
        self.assertEqual(kernel.get_value('p11'), 5)
        self.assertAlmostEqual(kernel.get_value('p12'), 9.0)

    def test_struct_1(self):
        self._test_struct_1(ISet.AVX512)
        self._test_struct_1(ISet.AVX2)
        self._test_struct_1(ISet.AVX)
        self._test_struct_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
