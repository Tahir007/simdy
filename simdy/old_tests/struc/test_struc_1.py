
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, float32, float64, int64x2, int64x3, int64x4, int64x8, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, struct, register_user_type, ISet


class TestStruct1(struct):
    __slots__ = ['p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10',
                 'p11', 'p12', 'p13', 'p14', 'p15', 'p16', 'p17', 'p18', 'p19', 'p20', 'p21', 'p22']

    def __init__(self):
        self.p1 = int32(1)
        self.p2 = float32(1)
        self.p3 = float64(1)
        self.p4 = int64(1)

        self.p5 = int32x2(1, 2)
        self.p6 = int32x3(1, 2, 3)
        self.p7 = int32x4(1, 2, 3, 4)
        self.p8 = int32x8(1, 2, 3, 4, 5, 6, 7, 8)
        self.p9 = int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)

        self.p10 = float32x2(1, 2)
        self.p11 = float32x3(1, 2, 3)
        self.p12 = float32x4(1, 2, 3, 4)
        self.p13 = float32x8(1, 2, 3, 4, 5, 6, 7, 8)
        self.p14 = float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)

        self.p15 = int64x2(1, 2)
        self.p16 = int64x3(1, 2, 3)
        self.p17 = int64x4(1, 2, 3, 4)
        self.p18 = int64x8(1, 2, 3, 4, 5, 6, 7, 8)

        self.p19 = float64x2(1, 2)
        self.p20 = float64x3(1, 2, 3)
        self.p21 = float64x4(1, 2, 3, 4)
        self.p22 = float64x8(1, 2, 3, 4, 5, 6, 7, 8)


register_user_type(TestStruct1, factory=lambda: TestStruct1())


class TestStruct2(struct):
    __slots__ = ['p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10',
                 'p11', 'p12', 'p13', 'p14', 'p15', 'p16', 'p17', 'p18', 'p19', 'p20', 'p21', 'p22']

    def __init__(self):
        self.p1 = int32()
        self.p2 = float32()
        self.p3 = float64()
        self.p4 = int64()

        self.p5 = int32x2()
        self.p6 = int32x3()
        self.p7 = int32x4()
        self.p8 = int32x8()
        self.p9 = int32x16()

        self.p10 = float32x2()
        self.p11 = float32x3()
        self.p12 = float32x4()
        self.p13 = float32x8()
        self.p14 = float32x16()

        self.p15 = int64x2()
        self.p16 = int64x3()
        self.p17 = int64x4()
        self.p18 = int64x8()

        self.p19 = float64x2()
        self.p20 = float64x3()
        self.p21 = float64x4()
        self.p22 = float64x8()


register_user_type(TestStruct2, factory=lambda: TestStruct2())


class StructTest1(unittest.TestCase):

    def _check_x2(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])

    def _check_x3(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])

    def _check_x4(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])

    def _check_x8(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])

    def _check_x16(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])
        self.assertAlmostEqual(src1[8], src2[8])
        self.assertAlmostEqual(src1[9], src2[9])
        self.assertAlmostEqual(src1[10], src2[10])
        self.assertAlmostEqual(src1[11], src2[11])
        self.assertAlmostEqual(src1[12], src2[12])
        self.assertAlmostEqual(src1[13], src2[13])
        self.assertAlmostEqual(src1[14], src2[14])
        self.assertAlmostEqual(src1[15], src2[15])

    def _test_struct_1(self, iset):
        source = """
s2.p1 = s1.p1
s2.p2 = s1.p2
s2.p3 = s1.p3
s2.p4 = s1.p4

s2.p5 = s1.p5
s2.p6 = s1.p6
s2.p7 = s1.p7
s2.p8 = s1.p8
s2.p9 = s1.p9

s2.p10 = s1.p10
s2.p11 = s1.p11
s2.p12 = s1.p12
s2.p13 = s1.p13
s2.p14 = s1.p14

s2.p15 = s1.p15
s2.p16 = s1.p16
s2.p17 = s1.p17
s2.p18 = s1.p18

s2.p19 = s1.p19
s2.p20 = s1.p20
s2.p21 = s1.p21
s2.p22 = s1.p22
        """

        args = [('s1', TestStruct1()), ('s2', TestStruct2())]
        kernel = Kernel(source, args=args, iset=iset)
        val = TestStruct1()
        kernel.set_value('s1', val)
        kernel.run()
        s1 = kernel.get_value('s1')
        s2 = kernel.get_value('s2')
        self.assertAlmostEqual(s1.p1, 1)
        self.assertAlmostEqual(s2.p1, 1)
        self.assertAlmostEqual(s1.p2, 1)
        self.assertAlmostEqual(s2.p2, 1)
        self.assertAlmostEqual(s1.p3, 1)
        self.assertAlmostEqual(s2.p3, 1)
        self.assertAlmostEqual(s1.p4, 1)
        self.assertAlmostEqual(s2.p4, 1)

        self._check_x2(s1.p5, int32x2(1, 2))
        self._check_x2(s2.p5, int32x2(1, 2))
        self._check_x3(s1.p6, int32x3(1, 2, 3))
        self._check_x3(s2.p6, int32x3(1, 2, 3))
        self._check_x4(s1.p7, int32x4(1, 2, 3, 4))
        self._check_x4(s2.p7, int32x4(1, 2, 3, 4))
        self._check_x8(s1.p8, int32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p8, int32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x16(s1.p9, int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        self._check_x16(s2.p9, int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))

        self._check_x2(s1.p10, float32x2(1, 2))
        self._check_x2(s2.p10, float32x2(1, 2))
        self._check_x3(s1.p11, float32x3(1, 2, 3))
        self._check_x3(s2.p11, float32x3(1, 2, 3))
        self._check_x4(s1.p12, float32x4(1, 2, 3, 4))
        self._check_x4(s2.p12, float32x4(1, 2, 3, 4))
        self._check_x8(s1.p13, float32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p13, float32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x16(s1.p14, float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        self._check_x16(s2.p14, float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))

        self._check_x2(s1.p15, int64x2(1, 2))
        self._check_x2(s2.p15, int64x2(1, 2))
        self._check_x3(s1.p16, int64x3(1, 2, 3))
        self._check_x3(s2.p16, int64x3(1, 2, 3))
        self._check_x4(s1.p17, int64x4(1, 2, 3, 4))
        self._check_x4(s2.p17, int64x4(1, 2, 3, 4))
        self._check_x8(s1.p18, int64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p18, int64x8(1, 2, 3, 4, 5, 6, 7, 8))

        self._check_x2(s1.p19, float64x2(1, 2))
        self._check_x2(s2.p19, float64x2(1, 2))
        self._check_x3(s1.p20, float64x3(1, 2, 3))
        self._check_x3(s2.p20, float64x3(1, 2, 3))
        self._check_x4(s1.p21, float64x4(1, 2, 3, 4))
        self._check_x4(s2.p21, float64x4(1, 2, 3, 4))
        self._check_x8(s1.p22, float64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p22, float64x8(1, 2, 3, 4, 5, 6, 7, 8))

    def test_struct_1(self):
        self._test_struct_1(ISet.AVX512)
        self._test_struct_1(ISet.AVX2)
        self._test_struct_1(ISet.AVX)
        self._test_struct_1(ISet.SSE)

    def _test_struct_4(self, iset):
        source = """
tmp_s1 = s1
p_s1 = tmp_s1
p_s2 = s2

p_s2.p1 = p_s1.p1
p_s2.p2 = p_s1.p2
p_s2.p3 = p_s1.p3
p_s2.p4 = p_s1.p4

p_s2.p5 = p_s1.p5
p_s2.p6 = p_s1.p6
p_s2.p7 = p_s1.p7
p_s2.p8 = p_s1.p8
p_s2.p9 = p_s1.p9

p_s2.p10 = p_s1.p10
p_s2.p11 = p_s1.p11
p_s2.p12 = p_s1.p12
p_s2.p13 = p_s1.p13
p_s2.p14 = p_s1.p14

p_s2.p15 = p_s1.p15
p_s2.p16 = p_s1.p16
p_s2.p17 = p_s1.p17
p_s2.p18 = p_s1.p18

p_s2.p19 = p_s1.p19
p_s2.p20 = p_s1.p20
p_s2.p21 = p_s1.p21
p_s2.p22 = p_s1.p22
        """

        args = [('s1', TestStruct1()), ('s2', TestStruct2())]
        kernel = Kernel(source, args=args, iset=iset)
        val = TestStruct1()
        kernel.set_value('s1', val)
        kernel.run()
        s1 = kernel.get_value('s1')
        s2 = kernel.get_value('s2')
        self.assertAlmostEqual(s1.p1, 1)
        self.assertAlmostEqual(s2.p1, 1)
        self.assertAlmostEqual(s1.p2, 1)
        self.assertAlmostEqual(s2.p2, 1)
        self.assertAlmostEqual(s1.p3, 1)
        self.assertAlmostEqual(s2.p3, 1)
        self.assertAlmostEqual(s1.p4, 1)
        self.assertAlmostEqual(s2.p4, 1)

        self._check_x2(s1.p5, int32x2(1, 2))
        self._check_x2(s2.p5, int32x2(1, 2))
        self._check_x3(s1.p6, int32x3(1, 2, 3))
        self._check_x3(s2.p6, int32x3(1, 2, 3))
        self._check_x4(s1.p7, int32x4(1, 2, 3, 4))
        self._check_x4(s2.p7, int32x4(1, 2, 3, 4))
        self._check_x8(s1.p8, int32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p8, int32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x16(s1.p9, int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        self._check_x16(s2.p9, int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))

        self._check_x2(s1.p10, float32x2(1, 2))
        self._check_x2(s2.p10, float32x2(1, 2))
        self._check_x3(s1.p11, float32x3(1, 2, 3))
        self._check_x3(s2.p11, float32x3(1, 2, 3))
        self._check_x4(s1.p12, float32x4(1, 2, 3, 4))
        self._check_x4(s2.p12, float32x4(1, 2, 3, 4))
        self._check_x8(s1.p13, float32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p13, float32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x16(s1.p14, float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        self._check_x16(s2.p14, float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))

        self._check_x2(s1.p15, int64x2(1, 2))
        self._check_x2(s2.p15, int64x2(1, 2))
        self._check_x3(s1.p16, int64x3(1, 2, 3))
        self._check_x3(s2.p16, int64x3(1, 2, 3))
        self._check_x4(s1.p17, int64x4(1, 2, 3, 4))
        self._check_x4(s2.p17, int64x4(1, 2, 3, 4))
        self._check_x8(s1.p18, int64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p18, int64x8(1, 2, 3, 4, 5, 6, 7, 8))

        self._check_x2(s1.p19, float64x2(1, 2))
        self._check_x2(s2.p19, float64x2(1, 2))
        self._check_x3(s1.p20, float64x3(1, 2, 3))
        self._check_x3(s2.p20, float64x3(1, 2, 3))
        self._check_x4(s1.p21, float64x4(1, 2, 3, 4))
        self._check_x4(s2.p21, float64x4(1, 2, 3, 4))
        self._check_x8(s1.p22, float64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(s2.p22, float64x8(1, 2, 3, 4, 5, 6, 7, 8))

    def test_struct_4(self):
        self._test_struct_4(ISet.AVX512)
        self._test_struct_4(ISet.AVX2)
        self._test_struct_4(ISet.AVX)
        self._test_struct_4(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
