import unittest
from simdy import int32, Kernel


class TestsSimpleInt32(unittest.TestCase):

    def test_simple_int32_1(self):
        source = """
p3 = p1
p4 = p2
p5 = -26
tmp = 88
p6 = tmp
tmp2 = p1
p7 = tmp2
        """

        args = [('p1', int32(10)), ('p2', int32()), ('p3', int32()), ('p4', int32()),
                ('p5', int32()), ('p6', int32()), ('p7', int32())]
        kernel = Kernel(source, args=args)
        kernel.set_value('p2', -69)
        kernel.run()
        self.assertEqual(kernel.get_value('p3'), 10)
        self.assertEqual(kernel.get_value('p4'), -69)
        self.assertEqual(kernel.get_value('p5'), -26)
        self.assertEqual(kernel.get_value('p6'), 88)
        self.assertEqual(kernel.get_value('p7'), 10)

    def test_simple_int32_2(self):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1
p8 = p3 % p1
p9 = p1 & p3
p10 = p1 ^ p3
p11 = p1 | p3
p12 = p3 << p1
p13 = p3 >> p1

p14 = p1 + 69
p15 = 25 - p3
p16 = tmp / -5
p17 = 11 * p1
p18 = p3 % 13
p19 = 5 & p1
p20 = p3 | 23
p21 = 14 ^ p2
p22 = p3 << 3
p23 = 56 >> p1
        """
        args = [('p1', int32(2)), ('p2', int32(-16)), ('p3', int32(87)),
                ('p4', int32()), ('p5', int32()), ('p6', int32()), ('p7', int32()),
                ('p8', int32()), ('p9', int32()), ('p10', int32()), ('p11', int32()),
                ('p12', int32()), ('p13', int32()), ('p14', int32()), ('p15', int32()),
                ('p16', int32()), ('p17', int32()), ('p18', int32()), ('p19', int32()),
                ('p20', int32()), ('p21', int32()), ('p22', int32()), ('p23', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), -14)
        self.assertEqual(kernel.get_value('p5'), 18)
        self.assertEqual(kernel.get_value('p6'), -32)
        self.assertEqual(kernel.get_value('p7'), 43)
        self.assertEqual(kernel.get_value('p8'), 1)
        self.assertEqual(kernel.get_value('p9'), 2 & 87)
        self.assertEqual(kernel.get_value('p10'), 2 ^ 87)
        self.assertEqual(kernel.get_value('p11'), 2 | 87)
        self.assertEqual(kernel.get_value('p12'), 87 << 2)
        self.assertEqual(kernel.get_value('p13'), 87 >> 2)

        self.assertEqual(kernel.get_value('p14'), 71)
        self.assertEqual(kernel.get_value('p15'), 25 - 87)
        self.assertEqual(kernel.get_value('p16'), 6)
        self.assertEqual(kernel.get_value('p17'), 22)
        self.assertEqual(kernel.get_value('p18'), 87 % 13)
        self.assertEqual(kernel.get_value('p19'), 5 & 2)
        self.assertEqual(kernel.get_value('p20'), 87 | 23)
        self.assertEqual(kernel.get_value('p21'), 14 ^ -16)
        self.assertEqual(kernel.get_value('p22'), 87 << 3)
        self.assertEqual(kernel.get_value('p23'), 56 >> 2)

if __name__ == "__main__":
    unittest.main()
