
import unittest
from simdy import int32, Kernel


class TestsExprInt32(unittest.TestCase):

    def test_expr_int32_1(self):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 / p2)
tmp4 = 5 * p1 % 7
tmp5 = 15 + p1 % p3
p5 = (tmp1 << 3) | (tmp2 << 1)
p6 = tmp3 & tmp1 - tmp4 * 6 ^ p1 + tmp5
p7 = 4 * p1 % p3 + 45 - (p1 | 16) * 8 / 3
p8 = ((p1 << p3) * p5 * tmp1) / (p1 % (p3 + 1))
        """

        args = [('p1', int32(10)), ('p2', int32(-6)), ('p3', int32(2)), ('p4', int32(-20)),
                ('p5', int32()), ('p6', int32()), ('p7', int32(111)), ('p8', int32()),
                ('p9', int32())
                ]
        kernel = Kernel(source, args=args)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 // p2)
        tmp4 = 5 * p1 % 7
        tmp5 = 15 + p1 % p3
        p5 = (tmp1 << 3) | (tmp2 << 1)
        p6 = tmp3 & tmp1 - tmp4 * 6 ^ p1 + tmp5
        p7 = 4 * p1 % p3 + 45 - (p1 | 16) * 8 // 3
        p8 = ((p1 << p3) * p5 * tmp1) // (p1 % (p3 + 1))
        self.assertEqual(kernel.get_value('p5'), p5)
        self.assertEqual(kernel.get_value('p6'), p6)
        self.assertEqual(kernel.get_value('p7'), p7)
        self.assertEqual(kernel.get_value('p8'), p8)

if __name__ == "__main__":
    unittest.main()
