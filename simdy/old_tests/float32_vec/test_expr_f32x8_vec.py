
import unittest
from simdy import float32x8, Kernel, ISet


class TestsExprF32x8(unittest.TestCase):

    def _tmps(self, p1, p2, p3, p4):
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 / p2)
        tmp4 = 5 * p1 / p2
        return (tmp1, tmp2, tmp3, tmp4)

    def _expr_p5(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
        return p5

    def _expr_p6(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p6 = tmp4 + tmp1 / tmp2 * tmp1 - p1 - p2 - (tmp1 * tmp3 - p4)
        return p6

    def _expr_p7(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p7 = p1 * tmp1 / tmp2 - tmp1 + tmp1 * p3 + p1 + p1 * p2 - tmp1 * tmp2 - tmp4
        return p7

    def _expr_p8(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p8 = p3 / tmp4 * 2 - tmp1 * p3 + p1 * 3 + p4 + tmp4 + tmp1 * (2 * tmp4 - p1 - p2)
        return p8

    def _test_expr_float32x8_1(self, iset, fma):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 / p2)
tmp4 = 5 * p1 / p2
p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
p6 = tmp4 + tmp1 / tmp2 * tmp1 - p1 - p2 - (tmp1 * tmp3 - p4)
p7 = p1 * tmp1 / tmp2 - tmp1 + tmp1 * p3 + p1 + p1 * p2 - tmp1 * tmp2 - tmp4
p8 = p3 / tmp4 * 2 - tmp1 * p3 + p1 * 3 + p4 + tmp4 + tmp1 * (2 * tmp4 - p1 - p2)

scale = float32(2.0)
p9 = p1 * scale
p10 = p1 * (scale + scale)
p11 = scale * p1 + p2
p12 = p1 * (scale + scale) * p1 + p2
        """
        args = [('p1', float32x8(10, 3, 1, -7, 2, 9, 6, 1)), ('p2', float32x8(-6, 7, 2, 1, 4, 4, 7, 8)),
                ('p3', float32x8(2, -5, -4, 7, 1, 9, -6, 4)), ('p4', float32x8(-20, 3, 5, 2, 2, 3, 4, 4)),
                ('p5', float32x8()), ('p6', float32x8()), ('p7', float32x8(111)), ('p8', float32x8()),
                ('p9', float32x8()), ('p10', float32x8()), ('p11', float32x8()), ('p12', float32x8())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        p5_0 = self._expr_p5(p1[0], p2[0], p3[0], p4[0])
        p5_1 = self._expr_p5(p1[1], p2[1], p3[1], p4[1])
        p5_2 = self._expr_p5(p1[2], p2[2], p3[2], p4[2])
        p5_3 = self._expr_p5(p1[3], p2[3], p3[3], p4[3])
        p5_4 = self._expr_p5(p1[4], p2[4], p3[4], p4[4])
        p5_5 = self._expr_p5(p1[5], p2[5], p3[5], p4[5])
        p5_6 = self._expr_p5(p1[6], p2[6], p3[6], p4[6])
        p5_7 = self._expr_p5(p1[7], p2[7], p3[7], p4[7])
        self.assertAlmostEqual(kernel.get_value('p5')[0], p5_0)
        self.assertAlmostEqual(kernel.get_value('p5')[1], p5_1)
        self.assertAlmostEqual(kernel.get_value('p5')[2], p5_2, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[3], p5_3, places=4)
        self.assertAlmostEqual(kernel.get_value('p5')[4], p5_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[5], p5_5, places=3)
        self.assertAlmostEqual(kernel.get_value('p5')[6], p5_6, places=3)
        self.assertAlmostEqual(kernel.get_value('p5')[7], p5_7, places=5)
        p6_0 = self._expr_p6(p1[0], p2[0], p3[0], p4[0])
        p6_1 = self._expr_p6(p1[1], p2[1], p3[1], p4[1])
        p6_2 = self._expr_p6(p1[2], p2[2], p3[2], p4[2])
        p6_3 = self._expr_p6(p1[3], p2[3], p3[3], p4[3])
        p6_4 = self._expr_p6(p1[4], p2[4], p3[4], p4[4])
        p6_5 = self._expr_p6(p1[5], p2[5], p3[5], p4[5])
        p6_6 = self._expr_p6(p1[6], p2[6], p3[6], p4[6])
        p6_7 = self._expr_p6(p1[7], p2[7], p3[7], p4[7])
        self.assertAlmostEqual(kernel.get_value('p6')[0], p6_0, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[1], p6_1, places=4)
        self.assertAlmostEqual(kernel.get_value('p6')[2], p6_2, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[3], p6_3)
        self.assertAlmostEqual(kernel.get_value('p6')[4], p6_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[5], p6_5, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[6], p6_6, places=4)
        self.assertAlmostEqual(kernel.get_value('p6')[7], p6_7, places=5)
        p7_0 = self._expr_p7(p1[0], p2[0], p3[0], p4[0])
        p7_1 = self._expr_p7(p1[1], p2[1], p3[1], p4[1])
        p7_2 = self._expr_p7(p1[2], p2[2], p3[2], p4[2])
        p7_3 = self._expr_p7(p1[3], p2[3], p3[3], p4[3])
        p7_4 = self._expr_p7(p1[4], p2[4], p3[4], p4[4])
        p7_5 = self._expr_p7(p1[5], p2[5], p3[5], p4[5])
        p7_6 = self._expr_p7(p1[6], p2[6], p3[6], p4[6])
        p7_7 = self._expr_p7(p1[7], p2[7], p3[7], p4[7])
        self.assertAlmostEqual(kernel.get_value('p7')[0], p7_0, places=4)
        self.assertAlmostEqual(kernel.get_value('p7')[1], p7_1, places=3)
        self.assertAlmostEqual(kernel.get_value('p7')[2], p7_2, places=4)
        self.assertAlmostEqual(kernel.get_value('p7')[3], p7_3)
        self.assertAlmostEqual(kernel.get_value('p7')[4], p7_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[5], p7_5, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[6], p7_6, places=3)
        self.assertAlmostEqual(kernel.get_value('p7')[7], p7_7, places=5)
        p8_0 = self._expr_p8(p1[0], p2[0], p3[0], p4[0])
        p8_1 = self._expr_p8(p1[1], p2[1], p3[1], p4[1])
        p8_2 = self._expr_p8(p1[2], p2[2], p3[2], p4[2])
        p8_3 = self._expr_p8(p1[3], p2[3], p3[3], p4[3])
        p8_4 = self._expr_p8(p1[4], p2[4], p3[4], p4[4])
        p8_5 = self._expr_p8(p1[5], p2[5], p3[5], p4[5])
        p8_6 = self._expr_p8(p1[6], p2[6], p3[6], p4[6])
        p8_7 = self._expr_p8(p1[7], p2[7], p3[7], p4[7])
        self.assertAlmostEqual(kernel.get_value('p8')[0], p8_0, places=6)
        self.assertAlmostEqual(kernel.get_value('p8')[1], p8_1, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[2], p8_2, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[3], p8_3, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[4], p8_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[5], p8_5, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[6], p8_6, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[7], p8_7, places=4)

        # scalar x vector multiplication
        self.assertAlmostEqual(kernel.get_value('p9')[0], 2.0 * 10)
        self.assertAlmostEqual(kernel.get_value('p9')[1], 2.0 * 3)
        self.assertAlmostEqual(kernel.get_value('p9')[2], 2.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p9')[3], 2.0 * -7)
        self.assertAlmostEqual(kernel.get_value('p9')[4], 2.0 * 2)
        self.assertAlmostEqual(kernel.get_value('p9')[5], 2.0 * 9)
        self.assertAlmostEqual(kernel.get_value('p9')[6], 2.0 * 6)
        self.assertAlmostEqual(kernel.get_value('p9')[7], 2.0 * 1)

        self.assertAlmostEqual(kernel.get_value('p10')[0], 4.0 * 10)
        self.assertAlmostEqual(kernel.get_value('p10')[1], 4.0 * 3)
        self.assertAlmostEqual(kernel.get_value('p10')[2], 4.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p10')[3], 4.0 * -7)
        self.assertAlmostEqual(kernel.get_value('p10')[4], 4.0 * 2)
        self.assertAlmostEqual(kernel.get_value('p10')[5], 4.0 * 9)
        self.assertAlmostEqual(kernel.get_value('p10')[6], 4.0 * 6)
        self.assertAlmostEqual(kernel.get_value('p10')[7], 4.0 * 1)

        self.assertAlmostEqual(kernel.get_value('p11')[0], 2.0 * 10 + -6)
        self.assertAlmostEqual(kernel.get_value('p11')[1], 2.0 * 3 + 7)
        self.assertAlmostEqual(kernel.get_value('p11')[2], 2.0 * 1 + 2)
        self.assertAlmostEqual(kernel.get_value('p11')[3], 2.0 * -7 + 1)
        self.assertAlmostEqual(kernel.get_value('p11')[4], 2.0 * 2 + 4)
        self.assertAlmostEqual(kernel.get_value('p11')[5], 2.0 * 9 + 4)
        self.assertAlmostEqual(kernel.get_value('p11')[6], 2.0 * 6 + 7)
        self.assertAlmostEqual(kernel.get_value('p11')[7], 2.0 * 1 + 8)

        self.assertAlmostEqual(kernel.get_value('p12')[0], 4.0 * 10 * 10 + -6)
        self.assertAlmostEqual(kernel.get_value('p12')[1], 4.0 * 3 * 3 + 7)
        self.assertAlmostEqual(kernel.get_value('p12')[2], 4.0 * 1 * 1 + 2)
        self.assertAlmostEqual(kernel.get_value('p12')[3], 4.0 * -7 * -7 + 1)
        self.assertAlmostEqual(kernel.get_value('p12')[4], 4.0 * 2 * 2 + 4)
        self.assertAlmostEqual(kernel.get_value('p12')[5], 4.0 * 9 * 9 + 4)
        self.assertAlmostEqual(kernel.get_value('p12')[6], 4.0 * 6 * 6 + 7)
        self.assertAlmostEqual(kernel.get_value('p12')[7], 4.0 * 1 * 1 + 8)

    def test_expr_float32x8_1(self):
        self._test_expr_float32x8_1(ISet.AVX512, fma=True)
        self._test_expr_float32x8_1(ISet.AVX512, fma=False)
        self._test_expr_float32x8_1(ISet.AVX2, fma=True)
        self._test_expr_float32x8_1(ISet.AVX2, fma=False)
        self._test_expr_float32x8_1(ISet.AVX, fma=False)
        self._test_expr_float32x8_1(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
