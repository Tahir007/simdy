
import unittest
from simdy import float32x16, Kernel, ISet


class TestsExprF32x16(unittest.TestCase):

    def _tmps(self, p1, p2, p3, p4):
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 / p2)
        tmp4 = 5 * p1 / p2
        return (tmp1, tmp2, tmp3, tmp4)

    def _expr_p5(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
        return p5

    def _expr_p6(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p6 = tmp4 + tmp1 / tmp2 * tmp1 - p1 - p2 - (tmp1 * tmp3 - p4)
        return p6

    def _expr_p7(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p7 = p1 * tmp1 / tmp2 - tmp1 + tmp1 * p3 + p1 + p1 * p2 - tmp1 * tmp2 - tmp4
        return p7

    def _expr_p8(self, p1, p2, p3, p4):
        tmp1, tmp2, tmp3, tmp4 = self._tmps(p1, p2, p3, p4)
        p8 = p3 / tmp4 * 2 - tmp1 * p3 + p1 * 3 + p4 + tmp4 + tmp1 * (2 * tmp4 - p1 - p2)
        return p8

    def _test_expr_float32x16_1(self, iset, fma):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 / p2)
tmp4 = 5 * p1 / p2
p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
p6 = tmp4 + tmp1 / tmp2 * tmp1 - p1 - p2 - (tmp1 * tmp3 - p4)
p7 = p1 * tmp1 / tmp2 - tmp1 + tmp1 * p3 + p1 + p1 * p2 - tmp1 * tmp2 - tmp4
p8 = p3 / tmp4 * 2 - tmp1 * p3 + p1 * 3 + p4 + tmp4 + tmp1 * (2 * tmp4 - p1 - p2)

scale = float32(2.0)
p9 = p1 * scale
p10 = p1 * (scale + scale)
p11 = scale * p1 + p2
p12 = p1 * (scale + scale) * p1 + p2
        """
        args = [('p1', float32x16(10, 3, 1, -7, 2, 9, 6, 1, 2, 3, 4, 5, 1, 8, 9, 3)),
                ('p2', float32x16(-6, 7, 2, 1, 4, 4, 7, 8, 7, 6, 4, 3, 2, 1, 3, 4)),
                ('p3', float32x16(2, -5, -4, 7, 1, 9, -6, 4, 4, 5, 1, 3, 9, 7, 5, 4)),
                ('p4', float32x16(-20, 3, 5, 2, 2, 3, 4, 4, 1, 2, 4, 6, 3, 2, 1, 3)),
                ('p5', float32x16()), ('p6', float32x16()), ('p7', float32x16(111)), ('p8', float32x16()),
                ('p9', float32x16()), ('p10', float32x16()), ('p11', float32x16()), ('p12', float32x16())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        p5_0 = self._expr_p5(p1[0], p2[0], p3[0], p4[0])
        p5_1 = self._expr_p5(p1[1], p2[1], p3[1], p4[1])
        p5_2 = self._expr_p5(p1[2], p2[2], p3[2], p4[2])
        p5_3 = self._expr_p5(p1[3], p2[3], p3[3], p4[3])
        p5_4 = self._expr_p5(p1[4], p2[4], p3[4], p4[4])
        p5_5 = self._expr_p5(p1[5], p2[5], p3[5], p4[5])
        p5_6 = self._expr_p5(p1[6], p2[6], p3[6], p4[6])
        p5_7 = self._expr_p5(p1[7], p2[7], p3[7], p4[7])
        p5_8 = self._expr_p5(p1[8], p2[8], p3[8], p4[8])
        p5_9 = self._expr_p5(p1[9], p2[9], p3[9], p4[9])
        p5_10 = self._expr_p5(p1[10], p2[10], p3[10], p4[10])
        p5_11 = self._expr_p5(p1[11], p2[11], p3[11], p4[11])
        p5_12 = self._expr_p5(p1[12], p2[12], p3[12], p4[12])
        p5_13 = self._expr_p5(p1[13], p2[13], p3[13], p4[13])
        p5_14 = self._expr_p5(p1[14], p2[14], p3[14], p4[14])
        p5_15 = self._expr_p5(p1[15], p2[15], p3[15], p4[15])
        self.assertAlmostEqual(kernel.get_value('p5')[0], p5_0)
        self.assertAlmostEqual(kernel.get_value('p5')[1], p5_1)
        self.assertAlmostEqual(kernel.get_value('p5')[2], p5_2, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[3], p5_3, places=4)
        self.assertAlmostEqual(kernel.get_value('p5')[4], p5_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[5], p5_5, places=3)
        self.assertAlmostEqual(kernel.get_value('p5')[6], p5_6, places=3)
        self.assertAlmostEqual(kernel.get_value('p5')[7], p5_7, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[8], p5_8, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[9], p5_9, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[10], p5_10, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[11], p5_11, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[12], p5_12, places=4)
        self.assertAlmostEqual(kernel.get_value('p5')[13], p5_13, places=5)
        self.assertAlmostEqual(kernel.get_value('p5')[14], p5_14, places=3)
        self.assertAlmostEqual(kernel.get_value('p5')[15], p5_15, places=4)
        p6_0 = self._expr_p6(p1[0], p2[0], p3[0], p4[0])
        p6_1 = self._expr_p6(p1[1], p2[1], p3[1], p4[1])
        p6_2 = self._expr_p6(p1[2], p2[2], p3[2], p4[2])
        p6_3 = self._expr_p6(p1[3], p2[3], p3[3], p4[3])
        p6_4 = self._expr_p6(p1[4], p2[4], p3[4], p4[4])
        p6_5 = self._expr_p6(p1[5], p2[5], p3[5], p4[5])
        p6_6 = self._expr_p6(p1[6], p2[6], p3[6], p4[6])
        p6_7 = self._expr_p6(p1[7], p2[7], p3[7], p4[7])
        p6_8 = self._expr_p6(p1[8], p2[8], p3[8], p4[8])
        p6_9 = self._expr_p6(p1[9], p2[9], p3[9], p4[9])
        p6_10 = self._expr_p6(p1[10], p2[10], p3[10], p4[10])
        p6_11 = self._expr_p6(p1[11], p2[11], p3[11], p4[11])
        p6_12 = self._expr_p6(p1[12], p2[12], p3[12], p4[12])
        p6_13 = self._expr_p6(p1[13], p2[13], p3[13], p4[13])
        p6_14 = self._expr_p6(p1[14], p2[14], p3[14], p4[14])
        p6_15 = self._expr_p6(p1[15], p2[15], p3[15], p4[15])
        self.assertAlmostEqual(kernel.get_value('p6')[0], p6_0, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[1], p6_1, places=4)
        self.assertAlmostEqual(kernel.get_value('p6')[2], p6_2, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[3], p6_3)
        self.assertAlmostEqual(kernel.get_value('p6')[4], p6_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[5], p6_5, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[6], p6_6, places=4)
        self.assertAlmostEqual(kernel.get_value('p6')[7], p6_7, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[8], p6_8, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[9], p6_9, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[10], p6_10, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[11], p6_11, places=4)
        self.assertAlmostEqual(kernel.get_value('p6')[12], p6_12, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[13], p6_13, places=5)
        self.assertAlmostEqual(kernel.get_value('p6')[14], p6_14, places=4)
        self.assertAlmostEqual(kernel.get_value('p6')[15], p6_15, places=5)
        p7_0 = self._expr_p7(p1[0], p2[0], p3[0], p4[0])
        p7_1 = self._expr_p7(p1[1], p2[1], p3[1], p4[1])
        p7_2 = self._expr_p7(p1[2], p2[2], p3[2], p4[2])
        p7_3 = self._expr_p7(p1[3], p2[3], p3[3], p4[3])
        p7_4 = self._expr_p7(p1[4], p2[4], p3[4], p4[4])
        p7_5 = self._expr_p7(p1[5], p2[5], p3[5], p4[5])
        p7_6 = self._expr_p7(p1[6], p2[6], p3[6], p4[6])
        p7_7 = self._expr_p7(p1[7], p2[7], p3[7], p4[7])
        p7_8 = self._expr_p7(p1[8], p2[8], p3[8], p4[8])
        p7_9 = self._expr_p7(p1[9], p2[9], p3[9], p4[9])
        p7_10 = self._expr_p7(p1[10], p2[10], p3[10], p4[10])
        p7_11 = self._expr_p7(p1[11], p2[11], p3[11], p4[11])
        p7_12 = self._expr_p7(p1[12], p2[12], p3[12], p4[12])
        p7_13 = self._expr_p7(p1[13], p2[13], p3[13], p4[13])
        p7_14 = self._expr_p7(p1[14], p2[14], p3[14], p4[14])
        p7_15 = self._expr_p7(p1[15], p2[15], p3[15], p4[15])
        self.assertAlmostEqual(kernel.get_value('p7')[0], p7_0, places=4)
        self.assertAlmostEqual(kernel.get_value('p7')[1], p7_1, places=3)
        self.assertAlmostEqual(kernel.get_value('p7')[2], p7_2, places=4)
        self.assertAlmostEqual(kernel.get_value('p7')[3], p7_3)
        self.assertAlmostEqual(kernel.get_value('p7')[4], p7_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[5], p7_5, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[6], p7_6, places=3)
        self.assertAlmostEqual(kernel.get_value('p7')[7], p7_7, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[8], p7_8, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[9], p7_9, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[10], p7_10, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[11], p7_11, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[12], p7_12, places=4)
        self.assertAlmostEqual(kernel.get_value('p7')[13], p7_13, places=5)
        self.assertAlmostEqual(kernel.get_value('p7')[14], p7_14, places=4)
        self.assertAlmostEqual(kernel.get_value('p7')[15], p7_15, places=5)
        p8_0 = self._expr_p8(p1[0], p2[0], p3[0], p4[0])
        p8_1 = self._expr_p8(p1[1], p2[1], p3[1], p4[1])
        p8_2 = self._expr_p8(p1[2], p2[2], p3[2], p4[2])
        p8_3 = self._expr_p8(p1[3], p2[3], p3[3], p4[3])
        p8_4 = self._expr_p8(p1[4], p2[4], p3[4], p4[4])
        p8_5 = self._expr_p8(p1[5], p2[5], p3[5], p4[5])
        p8_6 = self._expr_p8(p1[6], p2[6], p3[6], p4[6])
        p8_7 = self._expr_p8(p1[7], p2[7], p3[7], p4[7])
        p8_8 = self._expr_p8(p1[8], p2[8], p3[8], p4[8])
        p8_9 = self._expr_p8(p1[9], p2[9], p3[9], p4[9])
        p8_10 = self._expr_p8(p1[10], p2[10], p3[10], p4[10])
        p8_11 = self._expr_p8(p1[11], p2[11], p3[11], p4[11])
        p8_12 = self._expr_p8(p1[12], p2[12], p3[12], p4[12])
        p8_13 = self._expr_p8(p1[13], p2[13], p3[13], p4[13])
        p8_14 = self._expr_p8(p1[14], p2[14], p3[14], p4[14])
        p8_15 = self._expr_p8(p1[15], p2[15], p3[15], p4[15])
        self.assertAlmostEqual(kernel.get_value('p8')[0], p8_0, places=6)
        self.assertAlmostEqual(kernel.get_value('p8')[1], p8_1, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[2], p8_2, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[3], p8_3, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[4], p8_4, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[5], p8_5, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[6], p8_6, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[7], p8_7, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[8], p8_8, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[9], p8_9, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[10], p8_10, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[11], p8_11, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[12], p8_12, places=5)
        self.assertAlmostEqual(kernel.get_value('p8')[13], p8_13, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[14], p8_14, places=4)
        self.assertAlmostEqual(kernel.get_value('p8')[15], p8_15, places=5)

        # scalar x vector multiplication
        self.assertAlmostEqual(kernel.get_value('p9')[0], 2.0 * 10)
        self.assertAlmostEqual(kernel.get_value('p9')[1], 2.0 * 3)
        self.assertAlmostEqual(kernel.get_value('p9')[2], 2.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p9')[3], 2.0 * -7)
        self.assertAlmostEqual(kernel.get_value('p9')[4], 2.0 * 2)
        self.assertAlmostEqual(kernel.get_value('p9')[5], 2.0 * 9)
        self.assertAlmostEqual(kernel.get_value('p9')[6], 2.0 * 6)
        self.assertAlmostEqual(kernel.get_value('p9')[7], 2.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p9')[8], 2.0 * 2)
        self.assertAlmostEqual(kernel.get_value('p9')[9], 2.0 * 3)
        self.assertAlmostEqual(kernel.get_value('p9')[10], 2.0 * 4)
        self.assertAlmostEqual(kernel.get_value('p9')[11], 2.0 * 5)
        self.assertAlmostEqual(kernel.get_value('p9')[12], 2.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p9')[13], 2.0 * 8)
        self.assertAlmostEqual(kernel.get_value('p9')[14], 2.0 * 9)
        self.assertAlmostEqual(kernel.get_value('p9')[15], 2.0 * 3)

        self.assertAlmostEqual(kernel.get_value('p10')[0], 4.0 * 10)
        self.assertAlmostEqual(kernel.get_value('p10')[1], 4.0 * 3)
        self.assertAlmostEqual(kernel.get_value('p10')[2], 4.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p10')[3], 4.0 * -7)
        self.assertAlmostEqual(kernel.get_value('p10')[4], 4.0 * 2)
        self.assertAlmostEqual(kernel.get_value('p10')[5], 4.0 * 9)
        self.assertAlmostEqual(kernel.get_value('p10')[6], 4.0 * 6)
        self.assertAlmostEqual(kernel.get_value('p10')[7], 4.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p10')[8], 4.0 * 2)
        self.assertAlmostEqual(kernel.get_value('p10')[9], 4.0 * 3)
        self.assertAlmostEqual(kernel.get_value('p10')[10], 4.0 * 4)
        self.assertAlmostEqual(kernel.get_value('p10')[11], 4.0 * 5)
        self.assertAlmostEqual(kernel.get_value('p10')[12], 4.0 * 1)
        self.assertAlmostEqual(kernel.get_value('p10')[13], 4.0 * 8)
        self.assertAlmostEqual(kernel.get_value('p10')[14], 4.0 * 9)
        self.assertAlmostEqual(kernel.get_value('p10')[15], 4.0 * 3)

        self.assertAlmostEqual(kernel.get_value('p11')[0], 2.0 * 10 + -6)
        self.assertAlmostEqual(kernel.get_value('p11')[1], 2.0 * 3 + 7)
        self.assertAlmostEqual(kernel.get_value('p11')[2], 2.0 * 1 + 2)
        self.assertAlmostEqual(kernel.get_value('p11')[3], 2.0 * -7 + 1)
        self.assertAlmostEqual(kernel.get_value('p11')[4], 2.0 * 2 + 4)
        self.assertAlmostEqual(kernel.get_value('p11')[5], 2.0 * 9 + 4)
        self.assertAlmostEqual(kernel.get_value('p11')[6], 2.0 * 6 + 7)
        self.assertAlmostEqual(kernel.get_value('p11')[7], 2.0 * 1 + 8)
        self.assertAlmostEqual(kernel.get_value('p11')[8], 2.0 * 2 + 7)
        self.assertAlmostEqual(kernel.get_value('p11')[9], 2.0 * 3 + 6)
        self.assertAlmostEqual(kernel.get_value('p11')[10], 2.0 * 4 + 4)
        self.assertAlmostEqual(kernel.get_value('p11')[11], 2.0 * 5 + 3)
        self.assertAlmostEqual(kernel.get_value('p11')[12], 2.0 * 1 + 2)
        self.assertAlmostEqual(kernel.get_value('p11')[13], 2.0 * 8 + 1)
        self.assertAlmostEqual(kernel.get_value('p11')[14], 2.0 * 9 + 3)
        self.assertAlmostEqual(kernel.get_value('p11')[15], 2.0 * 3 + 4)

        self.assertAlmostEqual(kernel.get_value('p12')[0], 4.0 * 10 * 10 + -6)
        self.assertAlmostEqual(kernel.get_value('p12')[1], 4.0 * 3 * 3 + 7)
        self.assertAlmostEqual(kernel.get_value('p12')[2], 4.0 * 1 * 1 + 2)
        self.assertAlmostEqual(kernel.get_value('p12')[3], 4.0 * -7 * -7 + 1)
        self.assertAlmostEqual(kernel.get_value('p12')[4], 4.0 * 2 * 2 + 4)
        self.assertAlmostEqual(kernel.get_value('p12')[5], 4.0 * 9 * 9 + 4)
        self.assertAlmostEqual(kernel.get_value('p12')[6], 4.0 * 6 * 6 + 7)
        self.assertAlmostEqual(kernel.get_value('p12')[7], 4.0 * 1 * 1 + 8)
        self.assertAlmostEqual(kernel.get_value('p12')[8], 4.0 * 2 * 2 + 7)
        self.assertAlmostEqual(kernel.get_value('p12')[9], 4.0 * 3 * 3 + 6)
        self.assertAlmostEqual(kernel.get_value('p12')[10], 4.0 * 4 * 4 + 4)
        self.assertAlmostEqual(kernel.get_value('p12')[11], 4.0 * 5 * 5 + 3)
        self.assertAlmostEqual(kernel.get_value('p12')[12], 4.0 * 1 * 1 + 2)
        self.assertAlmostEqual(kernel.get_value('p12')[13], 4.0 * 8 * 8 + 1)
        self.assertAlmostEqual(kernel.get_value('p12')[14], 4.0 * 9 * 9 + 3)
        self.assertAlmostEqual(kernel.get_value('p12')[15], 4.0 * 3 * 3 + 4)

    def test_expr_float32x16_1(self):
        self._test_expr_float32x16_1(ISet.AVX512, fma=True)
        self._test_expr_float32x16_1(ISet.AVX512, fma=False)
        self._test_expr_float32x16_1(ISet.AVX2, fma=True)
        self._test_expr_float32x16_1(ISet.AVX2, fma=False)
        self._test_expr_float32x16_1(ISet.AVX, fma=False)
        self._test_expr_float32x16_1(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
