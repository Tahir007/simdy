
import unittest
from simdy import Kernel, float32x2, float32x3, float32x4, float32x8, float32x16
from simdy import int32x2, int32x3, int32x4, int32x8, int32x16, ISet


class TestsSimpleFloat32Vec(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _test_simple_float32_vec_1(self, iset):
        source = """
p3 = p1
p4 = p2
tmp1 = p1
p5 = tmp1

p8 = p6
p9 = p7
tmp2 = p6
p10 = tmp2

p13 = p11
p14 = p12
tmp3 = p11
p15 = tmp3
 
p18 = p16
p19 = p17
tmp4 = p16
p20 = tmp4

p23 = p21
p24 = p22
tmp5 = p21
p25 = tmp5
                """

        args = [
            ('p1', float32x2(1, 5)), ('p2', float32x2(2)), ('p3', float32x2()),
            ('p4', float32x2()), ('p5', float32x2()),
            ('p6', float32x3(1, 5, 9)), ('p7', float32x3(2)), ('p8', float32x3()),
            ('p9', float32x3()), ('p10', float32x3()),
            ('p11', float32x4(1, 5, 9, 18)), ('p12', float32x4(2)), ('p13', float32x4()),
            ('p14', float32x4()), ('p15', float32x4()),
            ('p16', float32x8(1, 5, 9, 18, 6, 15, 78, 125)), ('p17', float32x8(2)), ('p18', float32x8()),
            ('p19', float32x8()), ('p20', float32x8()),
            ('p21', float32x16(1, 5, 9, 18, 6, 15, 78, 125, 7, 7, 7, 3, 4, 4, 4, 14)), ('p22', float32x16(2)),
            ('p23', float32x16()), ('p24', float32x16()), ('p25', float32x16())
        ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.set_value('p2', float32x2(3, 8))
        kernel.set_value('p7', float32x3(3, 8, 11))
        kernel.set_value('p12', float32x4(3, 8, 11, 18))
        kernel.set_value('p17', float32x8(3, 8, 11, 18, 9, 9, 8, 6))
        kernel.set_value('p22', float32x16(3, 8, 11, 18, 9, 9, 8, 6, 2, 2, 2, 3, 7, 7, 7, 2))
        kernel.run()

        self._check(kernel.get_value('p3'), float32x2(1, 5), 2)
        self._check(kernel.get_value('p4'), float32x2(3, 8), 2)
        self._check(kernel.get_value('p5'), float32x2(1, 5), 2)

        self._check(kernel.get_value('p8'), float32x3(1, 5, 9), 3)
        self._check(kernel.get_value('p9'), float32x3(3, 8, 11), 3)
        self._check(kernel.get_value('p10'), float32x3(1, 5, 9), 3)

        self._check(kernel.get_value('p13'), float32x4(1, 5, 9, 18), 4)
        self._check(kernel.get_value('p14'), float32x4(3, 8, 11, 18), 4)
        self._check(kernel.get_value('p15'), float32x4(1, 5, 9, 18), 4)

        self._check(kernel.get_value('p18'), float32x8(1, 5, 9, 18, 6, 15, 78, 125), 8)
        self._check(kernel.get_value('p19'), float32x8(3, 8, 11, 18, 9, 9, 8, 6), 8)
        self._check(kernel.get_value('p20'), float32x8(1, 5, 9, 18, 6, 15, 78, 125), 8)

        self._check(kernel.get_value('p23'), float32x16(1, 5, 9, 18, 6, 15, 78, 125, 7, 7, 7, 3, 4, 4, 4, 14), 16)
        self._check(kernel.get_value('p24'), float32x16(3, 8, 11, 18, 9, 9, 8, 6, 2, 2, 2, 3, 7, 7, 7, 2), 16)
        self._check(kernel.get_value('p25'), float32x16(1, 5, 9, 18, 6, 15, 78, 125, 7, 7, 7, 3, 4, 4, 4, 14), 16)

    def test_simple_float32_vec_1(self):
        self._test_simple_float32_vec_1(ISet.AVX512)
        self._test_simple_float32_vec_1(ISet.AVX)
        self._test_simple_float32_vec_1(ISet.SSE)

    def _test_simple_float32_vec_2(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1
p8 = p1 * 5
p9 = 6.0 * p1

mask1 = p1 > p2
mask2 = p1 < p2
mask3 = p1 != p2
mask4 = p1 == p2
mask5 = p1 >= p2
mask6 = p1 <= p2
mask7 = mask1 | mask2 ^ mask3 & mask4

p19 = p16 + p17
p20 = p16 - p17
tmp2 = p16 * p17
p21 = tmp2
p22 = p18 / p16
p23 = p16 * 5
p24 = 6.0 * p16

mask11 = p16 > p17
mask12 = p16 < p17
mask13 = p16 != p17
mask14 = p16 == p17
mask15 = p16 >= p17
mask16 = p16 <= p17
mask17 = mask11 | mask12 ^ mask13 & mask14

        """
        args = [('p1', float32x2(2)), ('p2', float32x2(-16)), ('p3', float32x2(87)),
                ('p4', float32x2()), ('p5', float32x2()), ('p6', float32x2()), ('p7', float32x2()),
                ('p8', float32x2()), ('p9', float32x2()), ('p10', int32x2(55)), ('p11', int32x2(55)),
                ('p12', int32x2(55)), ('p13', int32x2(55)), ('p14', int32x2(55)), ('p15', int32x2(55)),
                ('p16', float32x3(2)), ('p17', float32x3(-16)), ('p18', float32x3(87)),
                ('p19', float32x3()), ('p20', float32x3()), ('p21', float32x3()), ('p22', float32x3()),
                ('p23', float32x3()), ('p24', float32x3()), ('p25', int32x3(55)), ('p26', int32x3(55)),
                ('p27', int32x3(55)), ('p28', int32x3(55)), ('p29', int32x3(55)), ('p30', int32x3(55)),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), float32x2(-14), 2)
        self._check(kernel.get_value('p5'), float32x2(18), 2)
        self._check(kernel.get_value('p6'), float32x2(-32), 2)
        self._check(kernel.get_value('p7'), float32x2(43.5), 2)
        self._check(kernel.get_value('p8'), float32x2(10), 2)
        self._check(kernel.get_value('p9'), float32x2(12), 2)

        self._check(kernel.get_value('p19'), float32x3(-14), 3)
        self._check(kernel.get_value('p20'), float32x3(18), 3)
        self._check(kernel.get_value('p21'), float32x3(-32), 3)
        self._check(kernel.get_value('p22'), float32x3(43.5), 3)
        self._check(kernel.get_value('p23'), float32x3(10), 3)
        self._check(kernel.get_value('p24'), float32x3(12), 3)

    def test_simple_float32_vec_2(self):
        self._test_simple_float32_vec_2(ISet.AVX512)
        self._test_simple_float32_vec_2(ISet.AVX)
        self._test_simple_float32_vec_2(ISet.SSE)


    def _test_simple_float32_vec_3(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1
p8 = p1 * 5
p9 = 6.0 * p1

mask1 = p1 > p2
mask2 = p1 < p2
mask3 = p1 != p2
mask4 = p1 == p2
mask5 = p1 >= p2
mask6 = p1 <= p2
mask7 = mask1 | mask2 ^ mask3 & mask4

p19 = p16 + p17
p20 = p16 - p17
tmp2 = p16 * p17
p21 = tmp2
p22 = p18 / p16
p23 = p16 * 5
p24 = 6.0 * p16

mask11 = p16 > p17
mask12 = p16 < p17
mask13 = p16 != p17
mask14 = p16 == p17
mask15 = p16 >= p17
mask16 = p16 <= p17
mask17 = mask11 | mask12 ^ mask13 & mask14
        """
        args = [('p1', float32x4(2)), ('p2', float32x4(-16)), ('p3', float32x4(87)),
                ('p4', float32x4()), ('p5', float32x4()), ('p6', float32x4()), ('p7', float32x4()),
                ('p8', float32x4()), ('p9', float32x4()), ('p10', int32x4(55)), ('p11', int32x4(55)),
                ('p12', int32x4(55)), ('p13', int32x4(55)), ('p14', int32x4(55)), ('p15', int32x4(55)),
                ('p16', float32x8(2)), ('p17', float32x8(-16)), ('p18', float32x8(87)),
                ('p19', float32x8()), ('p20', float32x8()), ('p21', float32x8()), ('p22', float32x8()),
                ('p23', float32x8()), ('p24', float32x8()), ('p25', int32x8(55)), ('p26', int32x8(55)),
                ('p27', int32x8(55)), ('p28', int32x8(55)), ('p29', int32x8(55)), ('p30', int32x8(55)),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), float32x4(-14), 4)
        self._check(kernel.get_value('p5'), float32x4(18), 4)
        self._check(kernel.get_value('p6'), float32x4(-32), 4)
        self._check(kernel.get_value('p7'), float32x4(43.5), 4)
        self._check(kernel.get_value('p8'), float32x4(10), 4)
        self._check(kernel.get_value('p9'), float32x4(12), 4)

        self._check(kernel.get_value('p19'), float32x8(-14), 8)
        self._check(kernel.get_value('p20'), float32x8(18), 8)
        self._check(kernel.get_value('p21'), float32x8(-32), 8)
        self._check(kernel.get_value('p22'), float32x8(43.5), 8)
        self._check(kernel.get_value('p23'), float32x8(10), 8)
        self._check(kernel.get_value('p24'), float32x8(12), 8)

    def test_simple_float32_vec_3(self):
        self._test_simple_float32_vec_3(ISet.AVX512)
        self._test_simple_float32_vec_3(ISet.AVX)
        self._test_simple_float32_vec_3(ISet.SSE)

    def _test_simple_float32_vec_4(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1
p8 = p1 * 5
p9 = 6.0 * p1

mask1 = p1 > p2
mask2 = p1 < p2
mask3 = p1 != p2
mask4 = p1 == p2
mask5 = p1 >= p2
mask6 = p1 <= p2
mask7 = mask1 | mask2 ^ mask3 & mask4

        """
        args = [('p1', float32x16(2)), ('p2', float32x16(-16)), ('p3', float32x16(87)),
                ('p4', float32x16()), ('p5', float32x16()), ('p6', float32x16()), ('p7', float32x16()),
                ('p8', float32x16()), ('p9', float32x16()), ('p10', int32x16(55)), ('p11', int32x16(55)),
                ('p12', int32x16(55)), ('p13', int32x16(55)), ('p14', int32x16(55)), ('p15', int32x16(55)),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), float32x16(-14), 16)
        self._check(kernel.get_value('p5'), float32x16(18), 16)
        self._check(kernel.get_value('p6'), float32x16(-32), 16)
        self._check(kernel.get_value('p7'), float32x16(43.5), 16)
        self._check(kernel.get_value('p8'), float32x16(10), 16)
        self._check(kernel.get_value('p9'), float32x16(12), 16)

    def test_simple_float32_vec_4(self):
        self._test_simple_float32_vec_4(ISet.AVX512)
        self._test_simple_float32_vec_4(ISet.AVX)
        self._test_simple_float32_vec_4(ISet.SSE)

    def _test_simple_float32_vec_5(self, iset, fma):
        source = """
p4 = p1 * p2 + p3
p5 = p1 * p2 - p3
p6 = p3 + p1 * p2
p7 = p3 - p1 * p2

p14 = p11 * p12 + p13
p15 = p11 * p12 - p13
p16 = p13 + p11 * p12
p17 = p13 - p11 * p12

p24 = p21 * p22 + p23
p25 = p21 * p22 - p23
p26 = p23 + p21 * p22
p27 = p23 - p21 * p22

p34 = p31 * p32 + p33
p35 = p31 * p32 - p33
p36 = p33 + p31 * p32
p37 = p33 - p31 * p32
# 
p44 = p41 * p42 + p43
p45 = p41 * p42 - p43
p46 = p43 + p41 * p42
p47 = p43 - p41 * p42
        """
        args = [('p1', float32x2(2)), ('p2', float32x2(3)), ('p3', float32x2(4)),
                ('p4', float32x2()), ('p5', float32x2()), ('p6', float32x2()), ('p7', float32x2()),
                ('p8', float32x2()), ('p9', float32x2()), ('p10', float32x2()),
                ('p11', float32x3(2)), ('p12', float32x3(3)), ('p13', float32x3(4)),
                ('p14', float32x3()), ('p15', float32x3()), ('p16', float32x3()), ('p17', float32x3()),
                ('p21', float32x4(2)), ('p22', float32x4(3)), ('p23', float32x4(4)),
                ('p24', float32x4()), ('p25', float32x4()), ('p26', float32x4()), ('p27', float32x4()),
                ('p31', float32x8(2)), ('p32', float32x8(3)), ('p33', float32x8(4)),
                ('p34', float32x8()), ('p35', float32x8()), ('p36', float32x8()), ('p37', float32x8()),
                ('p41', float32x16(2)), ('p42', float32x16(3)), ('p43', float32x16(4)),
                ('p44', float32x16()), ('p45', float32x16()), ('p46', float32x16()), ('p47', float32x16())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        self._check(kernel.get_value('p4'), float32x2(10), 2)
        self._check(kernel.get_value('p5'), float32x2(2), 2)
        self._check(kernel.get_value('p6'), float32x2(10), 2)
        self._check(kernel.get_value('p7'), float32x2(-2), 2)

        self._check(kernel.get_value('p14'), float32x3(10), 3)
        self._check(kernel.get_value('p15'), float32x3(2), 3)
        self._check(kernel.get_value('p16'), float32x3(10), 3)
        self._check(kernel.get_value('p17'), float32x3(-2), 3)

        self._check(kernel.get_value('p24'), float32x4(10), 4)
        self._check(kernel.get_value('p25'), float32x4(2), 4)
        self._check(kernel.get_value('p26'), float32x4(10), 4)
        self._check(kernel.get_value('p27'), float32x4(-2), 4)

        self._check(kernel.get_value('p34'), float32x8(10), 8)
        self._check(kernel.get_value('p35'), float32x8(2), 8)
        self._check(kernel.get_value('p36'), float32x8(10), 8)
        self._check(kernel.get_value('p37'), float32x8(-2), 8)

        self._check(kernel.get_value('p44'), float32x16(10), 16)
        self._check(kernel.get_value('p45'), float32x16(2), 16)
        self._check(kernel.get_value('p46'), float32x16(10), 16)
        self._check(kernel.get_value('p47'), float32x16(-2), 16)

    def test_simple_float32_vec_5(self):
        self._test_simple_float32_vec_5(ISet.AVX512, fma=False)
        self._test_simple_float32_vec_5(ISet.AVX512, fma=True)
        self._test_simple_float32_vec_5(ISet.AVX2, fma=True)
        self._test_simple_float32_vec_5(ISet.AVX2, fma=False)
        self._test_simple_float32_vec_5(ISet.AVX, fma=False)
        self._test_simple_float32_vec_5(ISet.SSE, fma=False)

    def _test_simple_float32_vec_6(self, iset, fma):
        source = """
p4 = 2 * p2 + p3
p5 = p1 * 3 - p3
p6 = p3 + 2.0 * p2
p7 = p3 - p1 * 3.0

p14 = 2 * p12 + p13
p15 = p11 * 3.0 - p13
p16 = p13 + p11 * 3
p17 = p13 - 2.0 * p12

p24 = 2 * p22 + p23
p25 = 2 * p22 - p23
p26 = p23 + p21 * 3.0
p27 = p23 - p21 * 3.0

p34 = 2.0 * p32 + p33
p35 = 2 * p32 - p33
p36 = p33 + p31 * 3
p37 = p33 - 2 * p32

p44 = 2 * p42 + p43
p45 = 2.0 * p42 - p43
p46 = p43 + 2 * p42
p47 = p43 - p41 * 3
        """
        args = [('p1', float32x2(2)), ('p2', float32x2(3)), ('p3', float32x2(4)),
                ('p4', float32x2()), ('p5', float32x2()), ('p6', float32x2()), ('p7', float32x2()),
                ('p8', float32x2()), ('p9', float32x2()), ('p10', float32x2()),
                ('p11', float32x3(2)), ('p12', float32x3(3)), ('p13', float32x3(4)),
                ('p14', float32x3()), ('p15', float32x3()), ('p16', float32x3()), ('p17', float32x3()),
                ('p21', float32x4(2)), ('p22', float32x4(3)), ('p23', float32x4(4)),
                ('p24', float32x4()), ('p25', float32x4()), ('p26', float32x4()), ('p27', float32x4()),
                ('p31', float32x8(2)), ('p32', float32x8(3)), ('p33', float32x8(4)),
                ('p34', float32x8()), ('p35', float32x8()), ('p36', float32x8()), ('p37', float32x8()),
                ('p41', float32x16(2)), ('p42', float32x16(3)), ('p43', float32x16(4)),
                ('p44', float32x16()), ('p45', float32x16()), ('p46', float32x16()), ('p47', float32x16())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        self._check(kernel.get_value('p4'), float32x2(10), 2)
        self._check(kernel.get_value('p5'), float32x2(2), 2)
        self._check(kernel.get_value('p6'), float32x2(10), 2)
        self._check(kernel.get_value('p7'), float32x2(-2), 2)

        self._check(kernel.get_value('p14'), float32x3(10), 3)
        self._check(kernel.get_value('p15'), float32x3(2), 3)
        self._check(kernel.get_value('p16'), float32x3(10), 3)
        self._check(kernel.get_value('p17'), float32x3(-2), 3)

        self._check(kernel.get_value('p24'), float32x4(10), 4)
        self._check(kernel.get_value('p25'), float32x4(2), 4)
        self._check(kernel.get_value('p26'), float32x4(10), 4)
        self._check(kernel.get_value('p27'), float32x4(-2), 4)

        self._check(kernel.get_value('p34'), float32x8(10), 8)
        self._check(kernel.get_value('p35'), float32x8(2), 8)
        self._check(kernel.get_value('p36'), float32x8(10), 8)
        self._check(kernel.get_value('p37'), float32x8(-2), 8)

        self._check(kernel.get_value('p44'), float32x16(10), 16)
        self._check(kernel.get_value('p45'), float32x16(2), 16)
        self._check(kernel.get_value('p46'), float32x16(10), 16)
        self._check(kernel.get_value('p47'), float32x16(-2), 16)

    def test_simple_float32_vec_6(self):
        self._test_simple_float32_vec_6(ISet.AVX512, fma=True)
        self._test_simple_float32_vec_6(ISet.AVX512, fma=False)
        self._test_simple_float32_vec_6(ISet.AVX2, fma=True)
        self._test_simple_float32_vec_6(ISet.AVX2, fma=False)
        self._test_simple_float32_vec_6(ISet.AVX, fma=False)
        self._test_simple_float32_vec_6(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
