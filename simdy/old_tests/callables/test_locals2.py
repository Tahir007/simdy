

import unittest
from simdy import Kernel, float64x2, float64x3, float64x4, float64x8, ISet


class LocalsFuncsTestsfloat64_v_1(unittest.TestCase):

    def _test_locals_float64x2_1(self, iset):
        source = """
def f1():
    return float64x2(10, -3)

def f2(n):
    return n * n

def f3(n: float64x2):
    return n + n

def f4(x1: float64x2, x2: float64x2, x3: float64x2):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2

tmp1 = float64x2(8, -4)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, float64x2(4, -1), float64x2(-6, 2))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', float64x2(3)), ('p2', float64x2(5)), ('p3', float64x2()), ('p4', float64x2()), ('p5', float64x2())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1')[0], 10)
        self.assertAlmostEqual(kernel.get_value('p1')[1], -3)
        self.assertAlmostEqual(kernel.get_value('p2')[0], 64)
        self.assertAlmostEqual(kernel.get_value('p2')[1], 16)
        self.assertAlmostEqual(kernel.get_value('p3')[0], 16)
        self.assertAlmostEqual(kernel.get_value('p3')[1], -8)
        self.assertAlmostEqual(kernel.get_value('p4')[0], 12)
        self.assertAlmostEqual(kernel.get_value('p4')[1], -6)
        self.assertAlmostEqual(kernel.get_value('p5')[0], 16)
        self.assertAlmostEqual(kernel.get_value('p5')[1], -8)

    def test_locals_float64x2_1(self):
        self._test_locals_float64x2_1(ISet.AVX512)
        self._test_locals_float64x2_1(ISet.AVX)
        self._test_locals_float64x2_1(ISet.SSE)

    def _test_locals_float64x3_1(self, iset):
        source = """
def f1():
    return float64x3(10, -3, 5)

def f2(n):
    return n * n

def f3(n: float64x3):
    return n + n

def f4(x1: float64x3, x2: float64x3, x3: float64x3):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2


tmp1 = float64x3(8, -4, 7)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, float64x3(4, -1, 1), float64x3(-6, 2, 3))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', float64x3(3)), ('p2', float64x3(5)), ('p3', float64x3()), ('p4', float64x3()), ('p5', float64x3())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1')[0], 10)
        self.assertAlmostEqual(kernel.get_value('p1')[1], -3)
        self.assertAlmostEqual(kernel.get_value('p1')[2], 5)
        self.assertAlmostEqual(kernel.get_value('p2')[0], 64)
        self.assertAlmostEqual(kernel.get_value('p2')[1], 16)
        self.assertAlmostEqual(kernel.get_value('p2')[2], 49)
        self.assertAlmostEqual(kernel.get_value('p3')[0], 16)
        self.assertAlmostEqual(kernel.get_value('p3')[1], -8)
        self.assertAlmostEqual(kernel.get_value('p3')[2], 14)
        self.assertAlmostEqual(kernel.get_value('p4')[0], 12)
        self.assertAlmostEqual(kernel.get_value('p4')[1], -6)
        self.assertAlmostEqual(kernel.get_value('p4')[2], 22)
        self.assertAlmostEqual(kernel.get_value('p5')[0], 16)
        self.assertAlmostEqual(kernel.get_value('p5')[1], -8)
        self.assertAlmostEqual(kernel.get_value('p5')[2], 14)

    def test_locals_float64x3_1(self):
        self._test_locals_float64x3_1(ISet.AVX512)
        self._test_locals_float64x3_1(ISet.AVX)
        self._test_locals_float64x3_1(ISet.SSE)

    def _test_locals_float64x4_1(self, iset):
        source = """
def f1():
    return float64x4(10, -3, 5, -4)

def f2(n):
    return n * n

def f3(n: float64x4):
    return n + n

def f4(x1: float64x4, x2: float64x4, x3: float64x4):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2


tmp1 = float64x4(8, -4, 7, -2)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, float64x4(4, -1, 1, -2), float64x4(-6, 2, 3, 1))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', float64x4(3)), ('p2', float64x4(5)), ('p3', float64x4()), ('p4', float64x4()), ('p5', float64x4())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1')[0], 10)
        self.assertAlmostEqual(kernel.get_value('p1')[1], -3)
        self.assertAlmostEqual(kernel.get_value('p1')[2], 5)
        self.assertAlmostEqual(kernel.get_value('p1')[3], -4)
        self.assertAlmostEqual(kernel.get_value('p2')[0], 64)
        self.assertAlmostEqual(kernel.get_value('p2')[1], 16)
        self.assertAlmostEqual(kernel.get_value('p2')[2], 49)
        self.assertAlmostEqual(kernel.get_value('p2')[3], 4)
        self.assertAlmostEqual(kernel.get_value('p3')[0], 16)
        self.assertAlmostEqual(kernel.get_value('p3')[1], -8)
        self.assertAlmostEqual(kernel.get_value('p3')[2], 14)
        self.assertAlmostEqual(kernel.get_value('p3')[3], -4)
        self.assertAlmostEqual(kernel.get_value('p4')[0], 12)
        self.assertAlmostEqual(kernel.get_value('p4')[1], -6)
        self.assertAlmostEqual(kernel.get_value('p4')[2], 22)
        self.assertAlmostEqual(kernel.get_value('p4')[3], -6)
        self.assertAlmostEqual(kernel.get_value('p5')[0], 16)
        self.assertAlmostEqual(kernel.get_value('p5')[1], -8)
        self.assertAlmostEqual(kernel.get_value('p5')[2], 14)
        self.assertAlmostEqual(kernel.get_value('p5')[3], -4)

    def test_locals_float64x4_1(self):
        self._test_locals_float64x4_1(ISet.AVX512)
        self._test_locals_float64x4_1(ISet.AVX)
        self._test_locals_float64x4_1(ISet.SSE)

    def _check_x8(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])

    def _test_locals_float64x8_1(self, iset):
        source = """
def f1():
    return float64x8(10, -3, 5, -4, 2, 1, -3, 1)

def f2(n):
    return n * n

def f3(n: float64x8):
    return n + n

def f4(x1: float64x8, x2: float64x8, x3: float64x8):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2


tmp1 = float64x8(8, -4, 7, -2, 1, 2, 3, -4)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, float64x8(4, -1, 1, -2, 1, 2, 3, 4), float64x8(-6, 2, 3, 1, -3, -4, 1, 2))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', float64x8(3)), ('p2', float64x8(5)), ('p3', float64x8()), ('p4', float64x8()), ('p5', float64x8())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_x8(kernel.get_value('p1'), float64x8(10, -3, 5, -4, 2, 1, -3, 1))
        self._check_x8(kernel.get_value('p2'), float64x8(64, 16, 49, 4, 1, 4, 9, 16))
        self._check_x8(kernel.get_value('p3'), float64x8(16, -8, 14, -4, 2, 4, 6, -8))
        self._check_x8(kernel.get_value('p4'), float64x8(12, -6, 22, -6, -2, 0, 14, 4))
        self._check_x8(kernel.get_value('p5'), float64x8(16, -8, 14, -4, 2, 4, 6, -8))

    def test_locals_float64x8_1(self):
        self._test_locals_float64x8_1(ISet.AVX512)
        self._test_locals_float64x8_1(ISet.AVX)
        self._test_locals_float64x8_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
