
import unittest
from simdy import Kernel, int32, float64, int64, float32, ISet, float32x2, float32x3,\
    float32x4, float32x8, float32x16, float64x2, float64x3, float64x4, float64x8,\
    int32x2, int32x3, int32x4, int32x8, int32x16, int64x2, int64x3, int64x4, int64x8


class ExteranlFuncsTests1(unittest.TestCase):

    def test_external_1(self):
        source = """
def generate_num():
    return int32(6)
        """
        ext_kernel = Kernel(source, args=[], name='generate_num', standalone=False)

        source = """
p1 = generate_num()
        """
        kernel = Kernel(source, args=[('p1', int32())], kernels=[ext_kernel])
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 6)

    def test_external_2(self):
        source = """
def calc_eq(a, b):
    return a + b
        """
        ext1 = Kernel(source, func_args=[('a', int32()), ('b', int32())], name='calc_eq', standalone=False)

        source = """
def calc_eq(a, b):
    return a * b
        """
        ext2 = Kernel(source, func_args=[('a', float64()), ('b', float64())], name='calc_eq', standalone=False)

        source = """
p1 = calc_eq(3, 3)
p2 = calc_eq(3.0, 3.0)
        """
        kernel = Kernel(source, args=[('p1', int32()), ('p2', float64())], kernels=[ext1, ext2])
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 6)
        self.assertEqual(kernel.get_value('p2'), 9)

    def _test_external_3(self, iset):
        source = """
def sum_numbers(p1, p2, p3, p4, p5):
    s = p5[0] + p5[1] + p5[2] + p5[3] + p5[4] + p5[5] + p5[6] + p5[7]
    return p1 + p2[0] + p2[1] + p3[0] + p3[1] + p3[2] + p4[0] + p4[1] + p4[2] + p4[3] + s
        """
        func_args = [('p1', float64()), ('p2', float64x2()), ('p3', float64x3()), ('p4', float64x4()), ('p5', float64x8())]
        ext1 = Kernel(source, func_args=func_args, name='sum_numbers', standalone=False, iset=iset)

        source = """
v3 = float64x3(1.0, 2.0, 3.0)
v4 = float64x4(1.0, 2.0, 3.0, 4.0)
v5 = float64x8(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0)
p1 = sum_numbers(1.0, float64x2(1.0, 2.0), v3, v4, v5)
        """
        kernel = Kernel(source, args=[('p1', float64())], kernels=[ext1], iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 56)

    def test_external_3(self):
        self._test_external_3(ISet.AVX512)
        self._test_external_3(ISet.AVX2)
        self._test_external_3(ISet.AVX)
        self._test_external_3(ISet.SSE)

    def _test_external_6(self, iset):
        source = """
def sum_numbers(p1, p2, p3, p4, p5, p6):
    s1 = p5[0] + p5[1] + p5[2] + p5[3] + p5[4] + p5[5] + p5[6] + p5[7]
    s2a = p6[0] + p6[1] + p6[2] + p6[3] + p6[4] + p6[5] + p6[6] + p6[7]
    s2b = p6[8] + p6[9] + p6[10] + p6[11] + p6[12] + p6[13] + p6[14] + p6[15]
    s = s1 + s2a + s2b
    return p1 + p2[0] + p2[1] + p3[0] + p3[1] + p3[2] + p4[0] + p4[1] + p4[2] + p4[3] + s
        """
        func_args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()),
                     ('p4', float32x4()), ('p5', float32x8()), ('p6', float32x16())]
        ext1 = Kernel(source, func_args=func_args, name='sum_numbers', standalone=False, iset=iset)

        source = """
v3 = float32x3(1.0, 2.0, 3.0)
v4 = float32x4(1.0, 2.0, 3.0, 4.0)
v5 = float32x8(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0)
v6 = float32x16(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0)
p1 = sum_numbers(float32(1.0), float32x2(1.0, 2.0), v3, v4, v5, v6)
        """
        kernel = Kernel(source, args=[('p1', float32())], kernels=[ext1], iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 192)

    def test_external_6(self):
        self._test_external_6(ISet.AVX512)
        self._test_external_6(ISet.AVX2)
        self._test_external_6(ISet.AVX)
        self._test_external_6(ISet.SSE)

    def _test_external_9(self, iset):
        source = """
def sum_numbers(p1, p2, p3, p4, p5, p6):
    s1 = p5[0] + p5[1] + p5[2] + p5[3] + p5[4] + p5[5] + p5[6] + p5[7]
    s2a = p6[0] + p6[1] + p6[2] + p6[3] + p6[4] + p6[5] + p6[6] + p6[7]
    s2b = p6[8] + p6[9] + p6[10] + p6[11] + p6[12] + p6[13] + p6[14] + p6[15]
    s = s1 + s2a + s2b
    return p1 + p2[0] + p2[1] + p3[0] + p3[1] + p3[2] + p4[0] + p4[1] + p4[2] + p4[3] + s
        """
        func_args = [('p1', int32()), ('p2', int32x2()), ('p3', int32x3()),
                     ('p4', int32x4()), ('p5', int32x8()), ('p6', int32x16())]
        ext1 = Kernel(source, func_args=func_args, name='sum_numbers', standalone=False, iset=iset)

        source = """
v3 = int32x3(1, 2, 3)
v4 = int32x4(1, 2, 3, 4)
v5 = int32x8(1, 2, 3, 4, 5, 6, 7, 8)
v6 = int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
p1 = sum_numbers(int32(1.0), int32x2(1.0, 2.0), v3, v4, v5, v6)
        """
        kernel = Kernel(source, args=[('p1', int32())], kernels=[ext1], iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 192)

    def test_external_9(self):
        self._test_external_9(ISet.AVX512)
        self._test_external_9(ISet.AVX2)
        self._test_external_9(ISet.AVX)
        self._test_external_9(ISet.SSE)

    def _test_external_12(self, iset):
        source = """
def sum_numbers(p1, p2, p3, p4, p5):
    s = p5[0] + p5[1] + p5[2] + p5[3] + p5[4] + p5[5] + p5[6] + p5[7]
    return p1 + p2[0] + p2[1] + p3[0] + p3[1] + p3[2] + p4[0] + p4[1] + p4[2] + p4[3] + s
        """
        func_args = [('p1', int64()), ('p2', int64x2()), ('p3', int64x3()), ('p4', int64x4()), ('p5', int64x8())]
        ext1 = Kernel(source, func_args=func_args, name='sum_numbers', standalone=False, iset=iset)

        source = """
v3 = int64x3(1, 2, 3)
v4 = int64x4(1, 2, 3, 4)
v5 = int64x8(1, 2, 3, 4, 5, 6, 7, 8)
p1 = sum_numbers(int64(1), int64x2(1, 2), v3, v4, v5)
        """
        kernel = Kernel(source, args=[('p1', int64())], kernels=[ext1], iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 56)

    def test_external_12(self):
        self._test_external_12(ISet.AVX512)
        self._test_external_12(ISet.AVX2)
        self._test_external_12(ISet.AVX)
        self._test_external_12(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
