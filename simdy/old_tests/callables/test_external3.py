
import unittest
from simdy import Kernel, int32, register_kernel


k_source = """
def global_kernel_1(x):
    return x + x
"""

register_kernel('global_kernel_1', (('x', int32()),), k_source)


class ExteranlFuncsTests3(unittest.TestCase):

    def test_external_1(self):
        source = """
p1 = global_kernel_1(5)
        """
        args = [('p1', int32())]
        ext1 = Kernel(source, args=args)
        ext1.run()
        self.assertEqual(ext1.get_value('p1'), 10)


if __name__ == "__main__":
    unittest.main()
