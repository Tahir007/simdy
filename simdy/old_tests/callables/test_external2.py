
import unittest
from simdy import Kernel, int32, struct, register_user_type, array, int64, float32


class ExtStruct(struct):
    __slots__ = ['x1', 'x2']

    def __init__(self, x1, x2):
        self.x1 = int32(x1)
        self.x2 = int32(x2)


register_user_type(ExtStruct, lambda: ExtStruct(0, 0))


class ExteranlFuncsTests2(unittest.TestCase):

    def test_external_1(self):
        source = """
def sum_n(s):
    return s.x1 + s.x2
        """
        func_args = [('s', ExtStruct(1, 1))]
        ext1 = Kernel(source, func_args=func_args, name='sum_n', standalone=False)

        source = """
e = ExtStruct(2, 3)
p1 = sum_n(e)
        """
        kernel = Kernel(source, args=[('p1', int32())], kernels=[ext1])
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 5)

    def test_external_2(self):
        source = """
def sum_n(arr):
    return arr[0] + arr[1] + arr[2]
        """
        func_args = [('arr', array(int32))]
        ext1 = Kernel(source, func_args=func_args, name='sum_n', standalone=False)

        source = """
p1 = sum_n(arr)
        """
        arr = array(int32)
        arr.append(1)
        arr.append(2)
        arr.append(3)
        kernel = Kernel(source, args=[('p1', int32()), ('arr', arr)], kernels=[ext1])
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 6)

    def test_external_3(self):
        source = """
def sum_n(arr):
    return arr[0] + arr[1] + arr[2]
        """
        func_args = [('arr', array(int32))]
        ext1 = Kernel(source, func_args=func_args, name='sum_n', standalone=False)

        source = """
arr = array(int32, 5)
arr[0] = 4
arr[1] = 6
arr[2] = 9
p1 = sum_n(arr)
        """
        kernel = Kernel(source, args=[('p1', int32())], kernels=[ext1])
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 19)

if __name__ == "__main__":
    unittest.main()
