
import unittest
import math
from simdy import Kernel, int32, float64, int64, float32, ISet


class LocalsFuncsTests1(unittest.TestCase):

    def test_locals_int32_1(self):
        source = """
def f1():
    return 10

def f2(n):
    return n * n

def f3(n: int32):
    return n + n

def f4(x1: int32, x2: int32, x3: int32):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2

tmp1 = 8

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, 4, -6)
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', int32(3)), ('p2', int32(5)), ('p3', int32()), ('p4', int32()), ('p5', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 10)
        self.assertEqual(kernel.get_value('p2'), 64)
        self.assertEqual(kernel.get_value('p3'), 16)
        self.assertEqual(kernel.get_value('p4'), 12)
        self.assertEqual(kernel.get_value('p5'), 16)

    def test_locals_int64_1(self):
        source = """
def f1():
    return int64(10)

def f2(n):
    return n * n

def f3(n: int64):
    return n + n

def f4(x1: int64, x2: int64, x3: int64):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2

tmp1 = int64(8)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, int64(4), int64(-6))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', int64(3)), ('p2', int64(5)), ('p3', int64()), ('p4', int64()), ('p5', int64())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 10)
        self.assertEqual(kernel.get_value('p2'), 64)
        self.assertEqual(kernel.get_value('p3'), 16)
        self.assertEqual(kernel.get_value('p4'), 12)
        self.assertEqual(kernel.get_value('p5'), 16)

    def _test_locals_float32_1(self, iset):
        source = """
def f1():
    return float32(10)

def f2(n):
    return n * n

def f3(n: float32):
    return n + n

def f4(x1: float32, x2: float32, x3: float32):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2

tmp1 = float32(8)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, float32(4), float32(-6))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', float32(3)), ('p2', float32(5)), ('p3', float32()), ('p4', float32()), ('p5', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 10)
        self.assertAlmostEqual(kernel.get_value('p2'), 64)
        self.assertAlmostEqual(kernel.get_value('p3'), 16)
        self.assertAlmostEqual(kernel.get_value('p4'), 12)
        self.assertAlmostEqual(kernel.get_value('p5'), 16)

    def test_locals_float32_1(self):
        self._test_locals_float32_1(ISet.AVX512)
        self._test_locals_float32_1(ISet.AVX)
        self._test_locals_float32_1(ISet.SSE)

    def _test_locals_float64_1(self, iset):
        source = """
def f1():
    return float64(10)

def f2(n):
    return n * n

def f3(n: float64):
    return n + n

def f4(x1: float64, x2: float64, x3: float64):
    tmp = x1 + x2 + x3
    tmp = tmp + tmp
    return tmp

def f5(x1, x2):
    tmp2 = x1 + x2
    return tmp2

tmp1 = float64(8)

p1 = f1()
p2 = f2(tmp1)
p3 = f3(tmp1)
p4 = f4(tmp1, float64(4), float64(-6))
p5 = f5(tmp1, tmp1)
        """

        args = [('p1', float64(3)), ('p2', float64(5)), ('p3', float64()), ('p4', float64()), ('p5', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 10)
        self.assertAlmostEqual(kernel.get_value('p2'), 64)
        self.assertAlmostEqual(kernel.get_value('p3'), 16)
        self.assertAlmostEqual(kernel.get_value('p4'), 12)
        self.assertAlmostEqual(kernel.get_value('p5'), 16)

    def test_locals_float64_1(self):
        self._test_locals_float64_1(ISet.AVX512)
        self._test_locals_float64_1(ISet.AVX)
        self._test_locals_float64_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
