
import unittest
from simdy import Kernel, float32, float64, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, ISet


class BuiltinsRoundTests(unittest.TestCase):

    def _check_x2(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])

    def _check_x3(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])

    def _check_x4(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])

    def _check_x8(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])

    def _check_x16(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])
        self.assertAlmostEqual(src[8], dst[8])
        self.assertAlmostEqual(src[9], dst[9])
        self.assertAlmostEqual(src[10], dst[10])
        self.assertAlmostEqual(src[11], dst[11])
        self.assertAlmostEqual(src[12], dst[12])
        self.assertAlmostEqual(src[13], dst[13])
        self.assertAlmostEqual(src[14], dst[14])
        self.assertAlmostEqual(src[15], dst[15])

    def _test_round_1(self, iset):
        source = """
p2 = round(p1)
p3 = round(p1 * 2)

p12 = round(p11)
p13 = round(2 * p11)

p22 = round(p21)
p23 = round(p21 * 2)

p32 = round(p31)
p33 = round(p31 * 2)

p42 = round(p41)
p43 = round(p41 * 2)

p52 = round(p51)
p53 = round(p51 * 2)

p62 = round(p61)
p63 = round(p61 * 2)

p72 = round(p71)
p73 = round(p71 * 2)

p82 = round(p81)
p83 = round(p81 * 2)

p92 = round(p91)
p93 = round(p91 * 2)

p102 = round(p101)
p103 = round(p101 * 2)
        """

        args = [('p1', float32(0.3)), ('p2', float32()), ('p3', float32()),
                ('p11', float64(0.8)), ('p12', float64()), ('p13', float64()),
                ('p21', float64x2(0.3, -0.7)), ('p22', float64x2()), ('p23', float64x2()),
                ('p31', float64x3(-0.7, 0.3, 0.8)), ('p32', float64x3()), ('p33', float64x3()),
                ('p41', float64x4(0.3, -1.6, 0.7, 0.3)), ('p42', float64x4()), ('p43', float64x4()),
                ('p51', float64x8(1.2, -0.6, 1.8, 0.6, 1.5, 2.1, -1.3, 2.8)), ('p52', float64x8()), ('p53', float64x8()),
                ('p61', float32x2(0.7, 1.3)), ('p62', float32x2()), ('p63', float32x2()),
                ('p71', float32x3(0.5, -1.3, 0.8)), ('p72', float32x3()), ('p73', float32x3()),
                ('p81', float32x4(0.4, -0.7, 2.6, -0.6)), ('p82', float32x4()), ('p83', float32x4()),
                ('p91', float32x8(0.3, -0.7, 0.8, 1.2, 1.8, -2.6, 4.4, -1.2)), ('p92', float32x8()), ('p93', float32x8()),
                ('p101', float32x16(0.7, -0.8, 0.3, 1.6, -1.6, 2.2, 2.6, 4.2, 0.9, -1.1, -1.6, 1.6, 0.1, 1.4, -1.8, 2.6)),
                ('p102', float32x16()), ('p103', float32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self.assertAlmostEqual(kernel.get_value('p2'), round(0.3))
        self.assertAlmostEqual(kernel.get_value('p3'), round(0.6))
        self.assertAlmostEqual(kernel.get_value('p12'), round(0.8))
        self.assertAlmostEqual(kernel.get_value('p13'), round(1.6))
        pp = (round(0.3), round(-0.7))
        self._check_x2(kernel.get_value('p22'), pp)
        pp = (round(0.6), round(-1.4))
        self._check_x2(kernel.get_value('p23'), pp)
        pp = (round(-0.7), round(0.3), round(0.8))
        self._check_x3(kernel.get_value('p32'), pp)
        pp = (round(-1.4), round(0.6), round(1.6))
        self._check_x3(kernel.get_value('p33'), pp)
        pp = (round(0.3), round(-1.6), round(0.7), round(0.3))
        self._check_x4(kernel.get_value('p42'), pp)
        pp = (round(0.6), round(-3.2), round(1.4), round(0.6))
        self._check_x4(kernel.get_value('p43'), pp)
        pp = (round(1.2), round(-0.6), round(1.8), round(0.6), round(1.5), round(2.1), round(-1.3), round(2.8))
        self._check_x8(kernel.get_value('p52'), pp)
        pp = (round(2.4), round(-1.2), round(3.6), round(1.2), round(3.0), round(4.2), round(-2.6), round(5.6))
        self._check_x8(kernel.get_value('p53'), pp)

        pp = (round(0.7), round(1.3))
        self._check_x2(kernel.get_value('p62'), pp)
        pp = (round(1.4), round(2.6))
        self._check_x2(kernel.get_value('p63'), pp)
        pp = (round(0.5), round(-1.3), round(0.8))
        self._check_x3(kernel.get_value('p72'), pp)
        pp = (round(1.0), round(-2.6), round(1.6))
        self._check_x3(kernel.get_value('p73'), pp)
        pp = (round(0.4), round(-0.7), round(2.6), round(-0.6))
        self._check_x4(kernel.get_value('p82'), pp)
        pp = (round(0.8), round(-1.4), round(5.2), round(-1.2))
        self._check_x4(kernel.get_value('p83'), pp)
        pp = (round(0.3), round(-0.7), round(0.8), round(1.2), round(1.8), round(-2.6), round(4.4), round(-1.2))
        self._check_x8(kernel.get_value('p92'), pp)
        pp = (round(0.6), round(-1.4), round(1.6), round(2.4), round(3.6), round(-5.2), round(8.8), round(-2.4))
        self._check_x8(kernel.get_value('p93'), pp)
        pp = (round(0.7), round(-0.8), round(0.3), round(1.6), round(-1.6), round(2.2), round(2.6), round(4.2),
              round(0.9), round(-1.1), round(-1.6), round(1.6), round(0.1), round(1.4), round(-1.8), round(2.6))
        self._check_x16(kernel.get_value('p102'), pp)
        pp = (round(1.4), round(-1.6), round(0.6), round(3.2), round(-3.2), round(4.4), round(5.2), round(8.4),
              round(1.8), round(-2.2), round(-3.2), round(3.2), round(0.2), round(2.8), round(-3.6), round(5.2))
        self._check_x16(kernel.get_value('p103'), pp)

    def test_round_1(self):
        self._test_round_1(ISet.AVX512)
        self._test_round_1(ISet.AVX)
        self._test_round_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
