
import unittest
from simdy import int64, Kernel, array, int32


class TestsLen(unittest.TestCase):

    def test_len_1(self):
        arr = array(int64)
        arr.append(33)
        arr.append(11)
        source = """
p1 = len(arr)
arr2 = array(int32, 5)
p2 = len(arr2)
        """
        args = [('p1', int64()), ('p2', int64()), ('arr', arr)]
        k = Kernel(source, args=args)
        k.run()
        self.assertEqual(k.get_value('p1'), 2)
        self.assertEqual(k.get_value('p2'), 5)

    def test_len_2(self):
        arr = array(int64)
        arr.extend([33, 11])
        source = """
def length_array(a):
    return len(a)

p1 = length_array(arr)
arr2 = array(int32, 5)
p2 = length_array(arr2)
        """
        args = [('p1', int64()), ('p2', int64()), ('arr', arr)]
        k = Kernel(source, args=args)
        k.run()
        self.assertEqual(k.get_value('p1'), 2)
        self.assertEqual(k.get_value('p2'), 5)

    def test_len_3(self):
        source = """
def length_array(arr):
    return len(arr)
        """
        func_args = [('arr', array(int32))]
        fk = Kernel(source, func_args=func_args, name='length_array', standalone=False)

        arr = array(int32)
        arr.extend([33, 11])
        source2 = """
p1 = length_array(arr)
arr2 = array(int32, 5)
p2 = length_array(arr2)
        """
        args = [('p1', int64()), ('p2', int64()), ('arr', arr)]
        k = Kernel(source2, args=args, kernels=[fk])
        k.run()
        self.assertEqual(k.get_value('p1'), 2)
        self.assertEqual(k.get_value('p2'), 5)

if __name__ == "__main__":
    unittest.main()
