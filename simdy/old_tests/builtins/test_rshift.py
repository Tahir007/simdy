
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, int64x2, int64x3, int64x4, int64x8, ISet


class BuiltinsRshiftTests(unittest.TestCase):

    def _test_rshift(self, iset):
        source = """
n = 4
p1 = rshift(45, 3)
p2 = rshift(85, n)
p3 = rshift(int64(125), 7)
p4 = rshift(int64(125), n)

p5 = rshift(int32x2(345, 111), 3)
p6 = rshift(int32x3(345, 111, 214), 4)
p7 = rshift(int32x4(345, 111, 214, 998), 5)
p8 = rshift(int32x8(345, 111, 214, 998, 334, 111, 756, 922), 5)
p9 = rshift(int32x16(345, 111, 214, 998, 334, 111, 756, 922, 222, 119, 497, 565, 444, 771, 223, 685), 5)

p10 = rshift(int64x2(345, 111), 3)
p11 = rshift(int64x3(345, 111, 214), 4)
p12 = rshift(int64x4(345, 111, 214, 998), 5)
p13 = rshift(int64x8(345, 111, 214, 998, 334, 111, 756, 922), 5)
        """
        args = [('p1', int32()), ('p2', int32()), ('p3', int64()), ('p4', int64()),
                ('p5', int32x2()), ('p6', int32x3()), ('p7', int32x4()), ('p8', int32x8()),
                ('p9', int32x16()), ('p10', int64x2()), ('p11', int64x3()), ('p12', int64x4()), ('p13', int64x8())]
        k = Kernel(source, args=args, iset=iset)
        k.run()
        self.assertEqual(k.get_value('p1'), 45 >> 3)
        self.assertEqual(k.get_value('p2'), 85 >> 4)
        self.assertEqual(k.get_value('p3'), 125 >> 7)
        self.assertEqual(k.get_value('p4'), 125 >> 4)
        p = k.get_value('p5')
        self.assertEqual(p[0], 345 >> 3)
        self.assertEqual(p[1], 111 >> 3)
        p = k.get_value('p6')
        self.assertEqual(p[0], 345 >> 4)
        self.assertEqual(p[1], 111 >> 4)
        self.assertEqual(p[2], 214 >> 4)
        p = k.get_value('p7')
        self.assertEqual(p[0], 345 >> 5)
        self.assertEqual(p[1], 111 >> 5)
        self.assertEqual(p[2], 214 >> 5)
        self.assertEqual(p[3], 998 >> 5)
        p = k.get_value('p8')
        self.assertEqual(p[0], 345 >> 5)
        self.assertEqual(p[1], 111 >> 5)
        self.assertEqual(p[2], 214 >> 5)
        self.assertEqual(p[3], 998 >> 5)
        self.assertEqual(p[4], 334 >> 5)
        self.assertEqual(p[5], 111 >> 5)
        self.assertEqual(p[6], 756 >> 5)
        self.assertEqual(p[7], 922 >> 5)
        p = k.get_value('p9')
        self.assertEqual(p[0], 345 >> 5)
        self.assertEqual(p[1], 111 >> 5)
        self.assertEqual(p[2], 214 >> 5)
        self.assertEqual(p[3], 998 >> 5)
        self.assertEqual(p[4], 334 >> 5)
        self.assertEqual(p[5], 111 >> 5)
        self.assertEqual(p[6], 756 >> 5)
        self.assertEqual(p[7], 922 >> 5)
        self.assertEqual(p[8], 222 >> 5)
        self.assertEqual(p[9], 119 >> 5)
        self.assertEqual(p[10], 497 >> 5)
        self.assertEqual(p[11], 565 >> 5)
        self.assertEqual(p[12], 444 >> 5)
        self.assertEqual(p[13], 771 >> 5)
        self.assertEqual(p[14], 223 >> 5)
        self.assertEqual(p[15], 685 >> 5)

        p = k.get_value('p10')
        self.assertEqual(p[0], 345 >> 3)
        self.assertEqual(p[1], 111 >> 3)
        p = k.get_value('p11')
        self.assertEqual(p[0], 345 >> 4)
        self.assertEqual(p[1], 111 >> 4)
        self.assertEqual(p[2], 214 >> 4)
        p = k.get_value('p12')
        self.assertEqual(p[0], 345 >> 5)
        self.assertEqual(p[1], 111 >> 5)
        self.assertEqual(p[2], 214 >> 5)
        self.assertEqual(p[3], 998 >> 5)
        p = k.get_value('p13')
        self.assertEqual(p[0], 345 >> 5)
        self.assertEqual(p[1], 111 >> 5)
        self.assertEqual(p[2], 214 >> 5)
        self.assertEqual(p[3], 998 >> 5)
        self.assertEqual(p[4], 334 >> 5)
        self.assertEqual(p[5], 111 >> 5)
        self.assertEqual(p[6], 756 >> 5)
        self.assertEqual(p[7], 922 >> 5)

    def test_rshift_1(self):
        self._test_rshift(ISet.AVX512)
        self._test_rshift(ISet.AVX2)
        self._test_rshift(ISet.AVX)
        self._test_rshift(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
