
import unittest
from math import floor
from simdy import Kernel, float32, float64, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, ISet


class BuiltinsFloorTests(unittest.TestCase):

    def _check_x2(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])

    def _check_x3(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])

    def _check_x4(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])

    def _check_x8(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])

    def _check_x16(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])
        self.assertAlmostEqual(src[8], dst[8])
        self.assertAlmostEqual(src[9], dst[9])
        self.assertAlmostEqual(src[10], dst[10])
        self.assertAlmostEqual(src[11], dst[11])
        self.assertAlmostEqual(src[12], dst[12])
        self.assertAlmostEqual(src[13], dst[13])
        self.assertAlmostEqual(src[14], dst[14])
        self.assertAlmostEqual(src[15], dst[15])

    def _test_floor_1(self, iset):
        source = """
p2 = floor(p1)
p3 = floor(p1 * 2)

p12 = floor(p11)
p13 = floor(2 * p11)

p22 = floor(p21)
p23 = floor(p21 * 2)

p32 = floor(p31)
p33 = floor(p31 * 2)

p42 = floor(p41)
p43 = floor(p41 * 2)

p52 = floor(p51)
p53 = floor(p51 * 2)

p62 = floor(p61)
p63 = floor(p61 * 2)

p72 = floor(p71)
p73 = floor(p71 * 2)

p82 = floor(p81)
p83 = floor(p81 * 2)

p92 = floor(p91)
p93 = floor(p91 * 2)

p102 = floor(p101)
p103 = floor(p101 * 2)
        """

        args = [('p1', float32(0.3)), ('p2', float32()), ('p3', float32()),
                ('p11', float64(0.8)), ('p12', float64()), ('p13', float64()),
                ('p21', float64x2(0.3, -0.7)), ('p22', float64x2()), ('p23', float64x2()),
                ('p31', float64x3(-0.7, 0.3, 0.8)), ('p32', float64x3()), ('p33', float64x3()),
                ('p41', float64x4(0.3, -1.6, 0.7, 0.3)), ('p42', float64x4()), ('p43', float64x4()),
                ('p51', float64x8(1.2, -0.6, 1.8, 0.6, 1.5, 2.1, -1.3, 2.8)), ('p52', float64x8()), ('p53', float64x8()),
                ('p61', float32x2(0.7, 1.3)), ('p62', float32x2()), ('p63', float32x2()),
                ('p71', float32x3(0.5, -1.3, 0.8)), ('p72', float32x3()), ('p73', float32x3()),
                ('p81', float32x4(0.4, -0.7, 2.6, -0.6)), ('p82', float32x4()), ('p83', float32x4()),
                ('p91', float32x8(0.3, -0.7, 0.8, 1.2, 1.8, -2.6, 4.4, -1.2)), ('p92', float32x8()), ('p93', float32x8()),
                ('p101', float32x16(0.7, -0.8, 0.3, 1.6, -1.6, 2.2, 2.6, 4.2, 0.9, -1.1, -1.6, 1.6, 0.1, 1.4, -1.8, 2.6)),
                ('p102', float32x16()), ('p103', float32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self.assertAlmostEqual(kernel.get_value('p2'), floor(0.3))
        self.assertAlmostEqual(kernel.get_value('p3'), floor(0.6))
        self.assertAlmostEqual(kernel.get_value('p12'), floor(0.8))
        self.assertAlmostEqual(kernel.get_value('p13'), floor(1.6))
        pp = (floor(0.3), floor(-0.7))
        self._check_x2(kernel.get_value('p22'), pp)
        pp = (floor(0.6), floor(-1.4))
        self._check_x2(kernel.get_value('p23'), pp)
        pp = (floor(-0.7), floor(0.3), floor(0.8))
        self._check_x3(kernel.get_value('p32'), pp)
        pp = (floor(-1.4), floor(0.6), floor(1.6))
        self._check_x3(kernel.get_value('p33'), pp)
        pp = (floor(0.3), floor(-1.6), floor(0.7), floor(0.3))
        self._check_x4(kernel.get_value('p42'), pp)
        pp = (floor(0.6), floor(-3.2), floor(1.4), floor(0.6))
        self._check_x4(kernel.get_value('p43'), pp)
        pp = (floor(1.2), floor(-0.6), floor(1.8), floor(0.6), floor(1.5), floor(2.1), floor(-1.3), floor(2.8))
        self._check_x8(kernel.get_value('p52'), pp)
        pp = (floor(2.4), floor(-1.2), floor(3.6), floor(1.2), floor(3.0), floor(4.2), floor(-2.6), floor(5.6))
        self._check_x8(kernel.get_value('p53'), pp)

        pp = (floor(0.7), floor(1.3))
        self._check_x2(kernel.get_value('p62'), pp)
        pp = (floor(1.4), floor(2.6))
        self._check_x2(kernel.get_value('p63'), pp)
        pp = (floor(0.5), floor(-1.3), floor(0.8))
        self._check_x3(kernel.get_value('p72'), pp)
        pp = (floor(1.0), floor(-2.6), floor(1.6))
        self._check_x3(kernel.get_value('p73'), pp)
        pp = (floor(0.4), floor(-0.7), floor(2.6), floor(-0.6))
        self._check_x4(kernel.get_value('p82'), pp)
        pp = (floor(0.8), floor(-1.4), floor(5.2), floor(-1.2))
        self._check_x4(kernel.get_value('p83'), pp)
        pp = (floor(0.3), floor(-0.7), floor(0.8), floor(1.2), floor(1.8), floor(-2.6), floor(4.4), floor(-1.2))
        self._check_x8(kernel.get_value('p92'), pp)
        pp = (floor(0.6), floor(-1.4), floor(1.6), floor(2.4), floor(3.6), floor(-5.2), floor(8.8), floor(-2.4))
        self._check_x8(kernel.get_value('p93'), pp)
        pp = (floor(0.7), floor(-0.8), floor(0.3), floor(1.6), floor(-1.6), floor(2.2), floor(2.6), floor(4.2),
              floor(0.9), floor(-1.1), floor(-1.6), floor(1.6), floor(0.1), floor(1.4), floor(-1.8), floor(2.6))
        self._check_x16(kernel.get_value('p102'), pp)
        pp = (floor(1.4), floor(-1.6), floor(0.6), floor(3.2), floor(-3.2), floor(4.4), floor(5.2), floor(8.4),
              floor(1.8), floor(-2.2), floor(-3.2), floor(3.2), floor(0.2), floor(2.8), floor(-3.6), floor(5.2))
        self._check_x16(kernel.get_value('p103'), pp)

    def test_floor_1(self):
        self._test_floor_1(ISet.AVX512)
        self._test_floor_1(ISet.AVX)
        self._test_floor_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
