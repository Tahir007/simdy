
import unittest
from simdy import Kernel, int32, int64, float32, float64, ISet, float32x2,\
    float32x3, float32x4, float32x8, float32x16, float64x2, float64x3, float64x4,\
    float64x8, int32x2, int32x3, int32x4, int32x8, int32x16


class BuiltinsMaxTests(unittest.TestCase):

    def _check_x2(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])

    def _check_x3(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])

    def _check_x4(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])

    def _check_x8(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])

    def _check_x16(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])
        self.assertAlmostEqual(src[8], dst[8])
        self.assertAlmostEqual(src[9], dst[9])
        self.assertAlmostEqual(src[10], dst[10])
        self.assertAlmostEqual(src[11], dst[11])
        self.assertAlmostEqual(src[12], dst[12])
        self.assertAlmostEqual(src[13], dst[13])
        self.assertAlmostEqual(src[14], dst[14])
        self.assertAlmostEqual(src[15], dst[15])

    def test_max_i32_1(self):
        source = """
a = -4
b = 6
c = 45
p1 = max(b, a)
p2 = max(a, b)
p3 = max(c * 2, 6)
        """

        args = [('p1', int32()), ('p2', int32()), ('p3', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 6)
        self.assertEqual(kernel.get_value('p2'), 6)
        self.assertEqual(kernel.get_value('p3'), 90)

    def test_max_i64_1(self):
        source = """
a = int64(-4)
b = int64(6)
c = int64(45)
p1 = max(b, a)
p2 = max(a, b)
p3 = max(c * 2, int64(6))
        """

        args = [('p1', int64()), ('p2', int64()), ('p3', int64())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 6)
        self.assertEqual(kernel.get_value('p2'), 6)
        self.assertEqual(kernel.get_value('p3'), 90)

    def _test_max_f32_1(self, iset):
        source = """
a = float32(-2.2)
b = float32(3.3)
p1 = max(a, b)
p2 = max(b, a)
c = float32(11.9)
p3 = max(b * 2, c)
        """

        args = [('p1', float32()), ('p2', float32()), ('p3', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 3.3, places=6)
        self.assertAlmostEqual(kernel.get_value('p2'), 3.3, places=6)
        self.assertAlmostEqual(kernel.get_value('p3'), 11.9, places=6)

    def test_max_f32_1(self):
        self._test_max_f32_1(ISet.AVX512)
        self._test_max_f32_1(ISet.AVX)
        self._test_max_f32_1(ISet.SSE)

    def _test_max_f64_1(self, iset):
        source = """
a = -2.2
b = 3.3
p1 = max(a, b)
p2 = max(b, a)
c = 11.9
p3 = max(b * 2, c)
        """
        args = [('p1', float64()), ('p2', float64()), ('p3', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 3.3)
        self.assertEqual(kernel.get_value('p2'), 3.3)
        self.assertEqual(kernel.get_value('p3'), 11.9)

    def test_max_f64_1(self):
        self._test_max_f64_1(ISet.AVX512)
        self._test_max_f64_1(ISet.AVX)
        self._test_max_f64_1(ISet.SSE)

    def _test_max_f32_f64_vec_1(self, iset):
        source = """
p1 = max(float32x2(2.2, 1.1), float32x2(1.1, 3.3))
p2 = max(float32x3(2.2, 1.1, -2.5), float32x3(1.1, 3.3, 1.5))
p3 = max(float32x4(2.2, 1.1, -2.5, 2.2), float32x4(1.1, 3.3, 1.5, 1.1))
p4 = max(float32x8(2.2, 1.1, -2.5, 2.2, -2.1, 1.1, 2.2, 0.8), float32x8(1.1, 3.3, 1.5, 1.1, 1.1, 2.0, 1.0, 2.0))
p5 = max(float32x16(2.2, 1.1, 0.5, 2.2, 0.1, 1.1, 2.2, 0.8, 0.4, 1.2, 1.8, 0.6, 1.2, 1.7, 2.2, 1.0),
         float32x16(1.1, 3.3, 1.5, 1.1, 1.1, 2.0, 1.0, 2.0, 0.8, 1.0, 0.6, 0.8, 2.2, 1.4, 0.8, 1.8))

p6 = max(float64x2(2.2, 1.1), float64x2(1.1, 3.3))
p7 = max(float64x3(2.2, 1.1, -2.5), float64x3(1.1, 3.3, 1.5))
p8 = max(float64x4(2.2, 1.1, -2.5, 2.2), float64x4(1.1, 3.3, 1.5, 1.1))
p9 = max(float64x8(2.2, 1.1, -2.5, 2.2, -2.1, 1.1, 2.2, 0.8), float64x8(1.1, 3.3, 1.5, 1.1, 1.1, 2.0, 1.0, 2.0))
        """
        args = [('p1', float32x2()), ('p2', float32x3()), ('p3', float32x4()), ('p4', float32x8()), ('p5', float32x16()),
                ('p6', float64x2()), ('p7', float64x3()), ('p8', float64x4()), ('p9', float64x8())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_x2(kernel.get_value('p1'), float32x2(2.2, 3.3))
        self._check_x3(kernel.get_value('p2'), float32x3(2.2, 3.3, 1.5))
        self._check_x4(kernel.get_value('p3'), float32x4(2.2, 3.3, 1.5, 2.2))
        self._check_x8(kernel.get_value('p4'), float32x8(2.2, 3.3, 1.5, 2.2, 1.1, 2.0, 2.2, 2.0))
        self._check_x16(kernel.get_value('p5'), float32x16(2.2, 3.3, 1.5, 2.2, 1.1, 2.0, 2.2, 2.0, 0.8, 1.2, 1.8, 0.8, 2.2, 1.7, 2.2, 1.8))
        self._check_x2(kernel.get_value('p6'), float64x2(2.2, 3.3))
        self._check_x3(kernel.get_value('p7'), float64x3(2.2, 3.3, 1.5))
        self._check_x4(kernel.get_value('p8'), float64x4(2.2, 3.3, 1.5, 2.2))
        self._check_x8(kernel.get_value('p9'), float64x8(2.2, 3.3, 1.5, 2.2, 1.1, 2.0, 2.2, 2.0))

    def test_max_f32_f64_vec_1(self):
        self._test_max_f32_f64_vec_1(ISet.AVX512)
        self._test_max_f32_f64_vec_1(ISet.AVX)
        self._test_max_f32_f64_vec_1(ISet.SSE)

    def _test_max_i32_vec_1(self, iset):
        source = """
p1 = max(int32x2(2, 1), int32x2(1, 3))
p2 = max(int32x3(2, 1, 3), int32x3(1, 3, 5))
p3 = max(int32x4(2, 1, 3, 5), int32x4(1, 3, 5, 2))
p4 = max(int32x8(2, 1, 3, 5, 1, 7, 4, 6), int32x8(1, 3, 5, 2, 2, 4, 7, 5))
p5 = max(int32x16(2, 1, 3, 5, 1, 7, 4, 6, 9, 2, 4, 1, 7, 9, 3, 4),
         int32x16(1, 3, 5, 2, 2, 4, 7, 5, 3, 4, 2, 2, 2, 7, 5, 6))
        """
        args = [('p1', int32x2()), ('p2', int32x3()), ('p3', int32x4()), ('p4', int32x8()), ('p5', int32x16())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_x2(kernel.get_value('p1'), int32x2(2, 3))
        self._check_x3(kernel.get_value('p2'), int32x3(2, 3, 5))
        self._check_x4(kernel.get_value('p3'), int32x4(2, 3, 5, 5))
        self._check_x8(kernel.get_value('p4'), int32x8(2, 3, 5, 5, 2, 7, 7, 6))
        self._check_x16(kernel.get_value('p5'), int32x16(2, 3, 5, 5, 2, 7, 7, 6, 9, 4, 4, 2, 7, 9, 5, 6))

    def test_max_i32_vec_1(self):
        self._test_max_i32_vec_1(ISet.AVX512)
        self._test_max_i32_vec_1(ISet.AVX2)
        self._test_max_i32_vec_1(ISet.AVX)
        self._test_max_i32_vec_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
