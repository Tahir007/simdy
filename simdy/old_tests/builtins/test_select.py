
import unittest
from simdy import Kernel, float64x2, float64x3,\
    float64x4, float64x8, int32x2, int32x3, int32x4, int32x8, int32x16, ISet,\
    float32x2, float32x3, float32x4, float32x8, float32x16, int64x2, int64x3,\
    int64x4, int64x8


class BuiltinsSelectTests(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _check_almost(self, src, dst, n):
        for i in range(n):
            self.assertAlmostEqual(src[i], dst[i])

    def _test_select_1(self, iset):
        source = """
p1 = select(int32x2(1), int32x2(2), int32x2(1, 6) > int32x2(2, 3))
p2 = select(int32x2(1), int32x2(2), float32x2(2, -3) < float32x2(3, 6))
p3 = select(int32x2(1), int32x2(2), int64x2(3, 4) > int64x2(2, 5))
p4 = select(int32x2(1), int32x2(2), float64x2(3, 4) > float64x2(2, 5))

p5 = select(int32x3(1), int32x3(2), int32x3(1, 6, 3) == int32x3(2, 6, 2))
p6 = select(int32x3(1), int32x3(2), float32x3(1, 6, 3) > float32x3(0, 2, 4))
p7 = select(int32x3(1), int32x3(2), int64x3(1, 6, 3) > int64x3(0, 2, 4))
p8 = select(int32x3(1), int32x3(2), float64x3(1, 1, 5) > float64x3(0, 2, 4))

p9 = select(int32x4(1), int32x4(2), int32x4(1, 6, 3, 4) > int32x4(0, 2, 4, 10))
p10 = select(int32x4(1), int32x4(2), float32x4(1, 2, 3, 2) <= float32x4(-4, 2, 4, 8))
p11 = select(int32x4(1), int32x4(2), int64x4(1, 6, 3, 4) > int64x4(0, 2, 4, 10))
p12 = select(int32x4(1), int32x4(2), float64x4(1, 2, 3, 2) <= float64x4(-4, 2, 4, 8))

p13 = select(int32x8(1), int32x8(2), int32x8(1, 6, 3, 4, 3, 8, -4, 2) > int32x8(0, 2, 4, 10, 4, 6, 1, 3))
p14 = select(int32x8(1), int32x8(2), float32x8(1, 2, 3, 2, -4, 4, 5, 2) > float32x8(-4, 2, 4, 8, 2, 2, 9, 2))
p15 = select(int32x8(1), int32x8(2), int64x8(1, 6, 3, 4, 3, 8, -4, 2) > int64x8(0, 2, 4, 10, 4, 6, 1, 3))
p16 = select(int32x8(1), int32x8(2), float64x8(1, 2, 3, 2, -4, 4, 5, 2) > float64x8(-4, 2, 4, 8, 2, 2, 9, 2))


p17 = select(int32x16(1), int32x16(2), int32x16(1, 6, 3, 4, 3, 8, -4, 2, 3, 4, 5, 6, 3, 2, -4, -7) > int32x16(0, 2, 4, 10, 4, 6, 1, 3, -4, -3, 1, 1, 8, 9, 1, 1))
p18 = select(int32x16(1), int32x16(2), float32x16(1, 2, 3, 2, -4, 4, 5, 2, -4, -2, 1, 1, 7, 7, 2, 1) > float32x16(-4, 2, 4, 8, 2, 2, 9, 2, 2, 2, 3, 3, 8, 8, 1, 1))

        """
        args = [('p1', int32x2()), ('p2', int32x2()), ('p3', int32x2()), ('p4', int32x2()),
                ('p5', int32x3()), ('p6', int32x3()), ('p7', int32x3()), ('p8', int32x3()),
                ('p9', int32x4()), ('p10', int32x4()), ('p11', int32x4()), ('p12', int32x4()),
                ('p13', int32x8()), ('p14', int32x8()), ('p15', int32x8()), ('p16', int32x8()),
                ('p17', int32x16()), ('p18', int32x16()), ('p19', int32x16()), ('p20', int32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check(kernel.get_value('p1'), int32x2(2, 1), 2)
        self._check(kernel.get_value('p2'), int32x2(1, 1), 2)
        self._check(kernel.get_value('p3'), int32x2(1, 2), 2)
        self._check(kernel.get_value('p4'), int32x2(1, 2), 2)

        self._check(kernel.get_value('p5'), int32x3(2, 1, 2), 3)
        self._check(kernel.get_value('p6'), int32x3(1, 1, 2), 3)
        self._check(kernel.get_value('p7'), int32x3(1, 1, 2), 3)
        self._check(kernel.get_value('p8'), int32x3(1, 2, 1), 3)

        self._check(kernel.get_value('p9'), int32x4(1, 1, 2, 2), 4)
        self._check(kernel.get_value('p10'), int32x4(2, 1, 1, 1), 4)
        self._check(kernel.get_value('p11'), int32x4(1, 1, 2, 2), 4)
        self._check(kernel.get_value('p12'), int32x4(2, 1, 1, 1), 4)

        self._check(kernel.get_value('p13'), int32x8(1, 1, 2, 2, 2, 1, 2, 2), 8)
        self._check(kernel.get_value('p14'), int32x8(1, 2, 2, 2, 2, 1, 2, 2), 8)
        self._check(kernel.get_value('p15'), int32x8(1, 1, 2, 2, 2, 1, 2, 2), 8)
        self._check(kernel.get_value('p16'), int32x8(1, 2, 2, 2, 2, 1, 2, 2), 8)

        self._check(kernel.get_value('p17'), int32x16(1, 1, 2, 2, 2, 1, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2), 16)
        self._check(kernel.get_value('p18'), int32x16(1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2), 16)

    def test_select_1(self):
        self._test_select_1(ISet.AVX512)
        self._test_select_1(ISet.AVX2)
        self._test_select_1(ISet.AVX)
        self._test_select_1(ISet.SSE)

    def _test_select_2(self, iset):
        source = """
p1 = select(float64x2(1.232), float64x2(2.157), float64x2(1, 6) > float64x2(2, 3))
p2 = select(float64x2(1.232), float64x2(2.157), int64x2(2, 9) > int64x2(3, 6))
p3 = select(float64x2(1.232), float64x2(2.157), int32x2(10, 2) > int32x2(3, 6))
p4 = select(float64x2(1.232), float64x2(2.157), float32x2(10, 2) > float32x2(3, 6))

p5 = select(float64x3(1.232), float64x3(2.157), float64x3(1, 6, 3) >= float64x3(2, 6, 2))
p6 = select(float64x3(1.232), float64x3(2.157), int64x3(1, 6, 3) > int64x3(0, 2, 4))
p7 = select(float64x3(1.232), float64x3(2.157), int32x3(10, 2, 5) > int32x3(3, 6, 3))
p8 = select(float64x3(1.232), float64x3(2.157), float32x3(1, 6, 3) > float32x3(0, 2, 4))

p9 = select(float64x4(1.232), float64x4(2.157), float64x4(1, 6, 3, 4) < float64x4(0, 2, 4, 10))
p10 = select(float64x4(1.232), float64x4(2.157), int64x4(1, 2, 3, 2) > int64x4(-4, 2, 4, 8))
p11 = select(float64x4(1.232), float64x4(2.157), float32x4(1, 6, 3, 4) < float32x4(0, 2, 4, 10))
p12 = select(float64x4(1.232), float64x4(2.157), int32x4(1, 2, 3, 2) > int32x4(-4, 2, 4, 8))

p13 = select(float64x8(1.232), float64x8(2.157), float64x8(1, 6, 3, 4, 3, 8, -4, 2) > float64x8(0, 2, 4, 10, 4, 6, 1, 3))
p14 = select(float64x8(1.232), float64x8(2.157), int64x8(1, 2, 3, 2, -4, 4, 5, 2) > int64x8(-4, 2, 4, 8, 2, 2, 9, 2))
p15 = select(float64x8(1.232), float64x8(2.157), float32x8(1, 6, 3, 4, 3, 8, -4, 2) > float32x8(0, 2, 4, 10, 4, 6, 1, 3))
p16 = select(float64x8(1.232), float64x8(2.157), int32x8(1, 2, 3, 2, -4, 4, 5, 2) > int32x8(-4, 2, 4, 8, 2, 2, 9, 2))

        """
        args = [('p1', float64x2()), ('p2', float64x2()), ('p3', float64x2()), ('p4', float64x2()),
                ('p5', float64x3()), ('p6', float64x3()), ('p7', float64x3()), ('p8', float64x3()),
                ('p9', float64x4()), ('p10', float64x4()), ('p11', float64x4()), ('p12', float64x4()),
                ('p13', float64x8()), ('p14', float64x8()), ('p15', float64x8()), ('p16', float64x8()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p1'), float64x2(2.157, 1.232), 2)
        self._check(kernel.get_value('p2'), float64x2(2.157, 1.232), 2)
        self._check(kernel.get_value('p3'), float64x2(1.232, 2.157), 2)
        self._check(kernel.get_value('p4'), float64x2(1.232, 2.157), 2)

        self._check(kernel.get_value('p5'), float64x3(2.157, 1.232, 1.232), 3)
        self._check(kernel.get_value('p6'), float64x3(1.232, 1.232, 2.157), 3)
        self._check(kernel.get_value('p7'), float64x3(1.232, 2.157, 1.232), 3)
        self._check(kernel.get_value('p8'), float64x3(1.232, 1.232, 2.157), 3)

        self._check(kernel.get_value('p9'), float64x4(2.157, 2.157, 1.232, 1.232), 4)
        self._check(kernel.get_value('p10'), float64x4(1.232, 2.157, 2.157, 2.157), 4)
        self._check(kernel.get_value('p11'), float64x4(2.157, 2.157, 1.232, 1.232), 4)
        self._check(kernel.get_value('p12'), float64x4(1.232, 2.157, 2.157, 2.157), 4)

        self._check(kernel.get_value('p13'), float64x8(1.232, 1.232, 2.157, 2.157, 2.157, 1.232, 2.157, 2.157), 8)
        self._check(kernel.get_value('p14'), float64x8(1.232, 2.157, 2.157, 2.157, 2.157, 1.232, 2.157, 2.157), 8)
        self._check(kernel.get_value('p15'), float64x8(1.232, 1.232, 2.157, 2.157, 2.157, 1.232, 2.157, 2.157), 8)
        self._check(kernel.get_value('p16'), float64x8(1.232, 2.157, 2.157, 2.157, 2.157, 1.232, 2.157, 2.157), 8)

    def test_select_2(self):
        self._test_select_2(ISet.AVX512)
        self._test_select_2(ISet.AVX2)
        self._test_select_2(ISet.AVX)
        self._test_select_2(ISet.SSE)

    def _test_select_3(self, iset):
        source = """
p1 = select(float32x2(0.112), float32x2(0.254), float32x2(1, 6) > float32x2(2, 3))
p2 = select(float32x2(0.112), float32x2(0.254), int64x2(2, 9) > int64x2(3, 6))
p3 = select(float32x2(0.112), float32x2(0.254), int32x2(10, 2) > int32x2(3, 6))
p4 = select(float32x2(0.112), float32x2(0.254), float64x2(10, 2) > float64x2(3, 6))

p5 = select(float32x3(0.112), float32x3(0.254), float32x3(1, 6, 3) >= float32x3(2, 6, 2))
p6 = select(float32x3(0.112), float32x3(0.254), int64x3(1, 6, 3) > int64x3(0, 2, 4))
p7 = select(float32x3(0.112), float32x3(0.254), int32x3(1, 6, 3) > int32x3(0, 2, 4))
p8 = select(float32x3(0.112), float32x3(0.254), float64x3(10, 2, 5) > float64x3(3, 6, 3))

p9 = select(float32x4(0.112), float32x4(0.254), float32x4(1, 6, 3, 4) < float32x4(0, 2, 4, 10))
p10 = select(float32x4(0.112), float32x4(0.254), int64x4(1, 2, 10, 2) > int64x4(-4, 2, 4, 8))
p11 = select(float32x4(0.112), float32x4(0.254), int32x4(1, 2, 3, 2) > int32x4(-4, 2, 4, 8))
p12 = select(float32x4(0.112), float32x4(0.254), float64x4(1, 1, 2, 12) < float64x4(0, 2, 4, 10))

p13 = select(float32x8(0.112), float32x8(0.254), float32x8(1, 6, 3, 4, 3, 8, -4, 2) > float32x8(0, 2, 4, 10, 4, 6, 1, 3))
p14 = select(float32x8(0.112), float32x8(0.254), int64x8(1, 2, 3, 2, -4, 4, 5, 3) > int64x8(-4, 2, 4, 8, 2, 2, 9, 2))
p15 = select(float32x8(0.112), float32x8(0.254), int32x8(1, 2, 3, 2, -4, 4, 5, 2) > int32x8(-4, 2, 4, 8, 2, 2, 9, 2))
p16 = select(float32x8(0.112), float32x8(0.254), float64x8(1, 6, 3, 4, 3, 8, -4, 2) > float64x8(0, 2, 4, 10, 4, 6, 1, 3))

p17 = select(float32x16(0.112), float32x16(0.254), float32x16(1, 6, 3, 4, 3, 8, -4, 2, 2, 4, 7, 8, -5, 0, 4, 6) >
                                                   float32x16(0, 2, 4, 10, 4, 6, 1, 3, 1, 0, 9, 6, -7, -1, 8, 3))
p18 = select(float32x16(0.112), float32x16(0.254), int32x16(1, 6, 3, 4, 3, 8, -4, 2, 2, 4, 7, 8, -5, 0, 4, 6) >
                                                   int32x16(0, 2, 4, 10, 4, 6, 1, 3, 1, 0, 9, 6, -7, -1, 8, 3))
        """
        args = [('p1', float32x2()), ('p2', float32x2()), ('p3', float32x2()), ('p4', float32x2()),
                ('p5', float32x3()), ('p6', float32x3()), ('p7', float32x3()), ('p8', float32x3()),
                ('p9', float32x4()), ('p10', float32x4()), ('p11', float32x4()), ('p12', float32x4()),
                ('p13', float32x8()), ('p14', float32x8()), ('p15', float32x8()), ('p16', float32x8()),
                ('p17', float32x16()), ('p18', float32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_almost(kernel.get_value('p1'), float32x2(0.254, 0.112), 2)
        self._check_almost(kernel.get_value('p2'), float32x2(0.254, 0.112), 2)
        self._check_almost(kernel.get_value('p3'), float32x2(0.112, 0.254), 2)
        self._check_almost(kernel.get_value('p4'), float32x2(0.112, 0.254), 2)

        self._check_almost(kernel.get_value('p5'), float32x3(0.254, 0.112, 0.112), 3)
        self._check_almost(kernel.get_value('p6'), float32x3(0.112, 0.112, 0.254), 3)
        self._check_almost(kernel.get_value('p7'), float32x3(0.112, 0.112, 0.254), 3)
        self._check_almost(kernel.get_value('p8'), float32x3(0.112, 0.254, 0.112), 3)

        self._check_almost(kernel.get_value('p9'), float32x4(0.254, 0.254, 0.112, 0.112), 4)
        self._check_almost(kernel.get_value('p10'), float32x4(0.112, 0.254, 0.112, 0.254), 4)
        self._check_almost(kernel.get_value('p11'), float32x4(0.112, 0.254, 0.254, 0.254), 4)
        self._check_almost(kernel.get_value('p12'), float32x4(0.254, 0.112, 0.112, 0.254), 4)

        self._check_almost(kernel.get_value('p13'), float32x8(0.112, 0.112, 0.254, 0.254, 0.254, 0.112, 0.254, 0.254), 8)
        self._check_almost(kernel.get_value('p14'), float32x8(0.112, 0.254, 0.254, 0.254, 0.254, 0.112, 0.254, 0.112), 8)
        self._check_almost(kernel.get_value('p15'), float32x8(0.112, 0.254, 0.254, 0.254, 0.254, 0.112, 0.254, 0.254), 8)
        self._check_almost(kernel.get_value('p16'), float32x8(0.112, 0.112, 0.254, 0.254, 0.254, 0.112, 0.254, 0.254), 8)

        self._check_almost(kernel.get_value('p17'), float32x16(0.112, 0.112, 0.254, 0.254, 0.254, 0.112, 0.254, 0.254, 0.112, 0.112, 0.254, 0.112, 0.112, 0.112, 0.254, 0.112), 16)
        self._check_almost(kernel.get_value('p18'), float32x16(0.112, 0.112, 0.254, 0.254, 0.254, 0.112, 0.254, 0.254, 0.112, 0.112, 0.254, 0.112, 0.112, 0.112, 0.254, 0.112), 16)

    def test_select_3(self):
        self._test_select_3(ISet.AVX512)
        self._test_select_3(ISet.AVX2)
        self._test_select_3(ISet.AVX)
        self._test_select_3(ISet.SSE)

    def _test_select_4(self, iset):
        source = """
p1 = select(int64x2(1), int64x2(2), int32x2(1, 6) > int32x2(2, 3))
p2 = select(int64x2(1), int64x2(2), float32x2(2, -3) < float32x2(3, 6))
p3 = select(int64x2(1), int64x2(2), int64x2(3, 4) > int64x2(2, 5))
p4 = select(int64x2(1), int64x2(2), float64x2(3, 4) > float64x2(2, 5))

p5 = select(int64x3(1), int64x3(2), int32x3(1, 6, 3) == int32x3(2, 6, 2))
p6 = select(int64x3(1), int64x3(2), float32x3(1, 6, 3) > float32x3(0, 2, 4))
p7 = select(int64x3(1), int64x3(2), int64x3(1, 6, 3) > int64x3(0, 2, 4))
p8 = select(int64x3(1), int64x3(2), float64x3(1, 1, 5) > float64x3(0, 2, 4))

p9 = select(int64x4(1), int64x4(2), int32x4(1, 6, 3, 4) > int32x4(0, 2, 4, 10))
p10 = select(int64x4(1), int64x4(2), float32x4(1, 2, 3, 2) <= float32x4(-4, 2, 4, 8))
p11 = select(int64x4(1), int64x4(2), int64x4(1, 6, 3, 4) > int64x4(0, 2, 4, 10))
p12 = select(int64x4(1), int64x4(2), float64x4(1, 2, 3, 2) <= float64x4(-4, 2, 4, 8))

p13 = select(int64x8(1), int64x8(2), int32x8(1, 6, 3, 4, 3, 8, -4, 2) > int32x8(0, 2, 4, 10, 4, 6, 1, 3))
p14 = select(int64x8(1), int64x8(2), float32x8(1, 2, 3, 2, -4, 4, 5, 2) > float32x8(-4, 2, 4, 8, 2, 2, 9, 2))
p15 = select(int64x8(1), int64x8(2), int64x8(1, 6, 3, 4, 3, 8, -4, 2) > int64x8(0, 2, 4, 10, 4, 6, 1, 3))
p16 = select(int64x8(1), int64x8(2), float64x8(1, 2, 3, 2, -4, 4, 5, 2) > float64x8(-4, 2, 4, 8, 2, 2, 9, 2))

        """
        args = [('p1', int64x2()), ('p2', int64x2()), ('p3', int64x2()), ('p4', int64x2()),
                ('p5', int64x3()), ('p6', int64x3()), ('p7', int64x3()), ('p8', int64x3()),
                ('p9', int64x4()), ('p10', int64x4()), ('p11', int64x4()), ('p12', int64x4()),
                ('p13', int64x8()), ('p14', int64x8()), ('p15', int64x8()), ('p16', int64x8()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p1'), int64x2(2, 1), 2)
        self._check(kernel.get_value('p2'), int64x2(1, 1), 2)
        self._check(kernel.get_value('p3'), int64x2(1, 2), 2)
        self._check(kernel.get_value('p4'), int64x2(1, 2), 2)

        self._check(kernel.get_value('p5'), int64x3(2, 1, 2), 3)
        self._check(kernel.get_value('p6'), int64x3(1, 1, 2), 3)
        self._check(kernel.get_value('p7'), int64x3(1, 1, 2), 3)
        self._check(kernel.get_value('p8'), int64x3(1, 2, 1), 3)

        self._check(kernel.get_value('p9'), int64x4(1, 1, 2, 2), 4)
        self._check(kernel.get_value('p10'), int64x4(2, 1, 1, 1), 4)
        self._check(kernel.get_value('p11'), int64x4(1, 1, 2, 2), 4)
        self._check(kernel.get_value('p12'), int64x4(2, 1, 1, 1), 4)

        self._check(kernel.get_value('p13'), int64x8(1, 1, 2, 2, 2, 1, 2, 2), 8)
        self._check(kernel.get_value('p14'), int64x8(1, 2, 2, 2, 2, 1, 2, 2), 8)
        self._check(kernel.get_value('p15'), int64x8(1, 1, 2, 2, 2, 1, 2, 2), 8)
        self._check(kernel.get_value('p16'), int64x8(1, 2, 2, 2, 2, 1, 2, 2), 8)

    def test_select_4(self):
        self._test_select_4(ISet.AVX512)
        self._test_select_4(ISet.AVX2)
        self._test_select_4(ISet.AVX)
        self._test_select_4(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
