
import unittest
from math import ldexp
from simdy import Kernel, float32, float64, ISet, float32x2, float32x3, float32x4,\
    float32x8, float32x16, float64x2, float64x3, float64x4, float64x8


class BuiltinsLdexpTests(unittest.TestCase):

    def _test_ldexp_f32_1(self, iset):
        source = """
a = float32(2.2)
p1 = ldexp(a, 3)
p2 = ldexp(float32(0.0), 6)
p3 = ldexp(float32(-0.034), -2)
        """

        args = [('p1', float32()), ('p2', float32()), ('p3', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), ldexp(2.2, 3), places=6)
        self.assertAlmostEqual(kernel.get_value('p2'), ldexp(0.0, 6), places=6)
        self.assertAlmostEqual(kernel.get_value('p3'), ldexp(-0.034, -2), places=6)

    def test_ldexp_f32_1(self):
        self._test_ldexp_f32_1(ISet.AVX512)
        self._test_ldexp_f32_1(ISet.AVX)
        self._test_ldexp_f32_1(ISet.SSE)

    def _test_ldexp_f64_1(self, iset):
        source = """
a = 2.2
p1 = ldexp(a, 3)
p2 = ldexp(0.0, 6)
p3 = ldexp(-0.034, -2)
        """

        args = [('p1', float64()), ('p2', float64()), ('p3', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), ldexp(2.2, 3))
        self.assertEqual(kernel.get_value('p2'), ldexp(0.0, 6))
        self.assertEqual(kernel.get_value('p3'), ldexp(-0.034, -2))

    def test_ldexp_f64_1(self):
        self._test_ldexp_f64_1(ISet.AVX512)
        self._test_ldexp_f64_1(ISet.AVX)
        self._test_ldexp_f64_1(ISet.SSE)

    def _test_ldexp_f32_vec(self, iset):
        source = """
p1 = ldexp(float32x2(0.5, 0.75), int32x2(3, -3))
p2 = ldexp(float32x3(0.5, 0.75, 0.55), int32x3(3, -3, 4))
p3 = ldexp(float32x4(0.5, 0.75, 0.55, 0.25), int32x4(3, -3, 4, -2))
p4 = ldexp(float32x8(0.5, 0.75, 0.55, 0.25, -0.20, 0.4, 0.65, 0.15), int32x8(3, -3, 4, -2, -3, 2, 0, -1))
p5 = ldexp(float32x16(0.5, 0.75, 0.55, 0.25, -0.20, 0.4, 0.65, 0.15, 0.11, -0.25, 0.15, 0.5, 0.25, 0.75, 0.4, 0.1),
           int32x16(3, -3, 4, -2, -3, 2, 0, -1, 4, -2, 0, 3, -4, 2, 3, 2))
        """

        args = [('p1', float32x2()), ('p2', float32x3()), ('p3', float32x4()), ('p4', float32x8()), ('p5', float32x16())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p1 = kernel.get_value('p1')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        p1 = kernel.get_value('p2')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4), places=6)
        p1 = kernel.get_value('p3')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4), places=6)
        self.assertAlmostEqual(p1[3], ldexp(0.25, -2))
        p1 = kernel.get_value('p4')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4), places=6)
        self.assertAlmostEqual(p1[3], ldexp(0.25, -2))
        self.assertAlmostEqual(p1[4], ldexp(-0.2, -3))
        self.assertAlmostEqual(p1[5], ldexp(0.4, 2))
        self.assertAlmostEqual(p1[6], ldexp(0.65, 0))
        self.assertAlmostEqual(p1[7], ldexp(0.15, -1))
        p1 = kernel.get_value('p5')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4), places=6)
        self.assertAlmostEqual(p1[3], ldexp(0.25, -2))
        self.assertAlmostEqual(p1[4], ldexp(-0.2, -3))
        self.assertAlmostEqual(p1[5], ldexp(0.4, 2))
        self.assertAlmostEqual(p1[6], ldexp(0.65, 0))
        self.assertAlmostEqual(p1[7], ldexp(0.15, -1))
        self.assertAlmostEqual(p1[8], ldexp(0.11, 4))
        self.assertAlmostEqual(p1[9], ldexp(-0.25, -2))
        self.assertAlmostEqual(p1[10], ldexp(0.15, 0))
        self.assertAlmostEqual(p1[11], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[12], ldexp(0.25, -4))
        self.assertAlmostEqual(p1[13], ldexp(0.75, 2))
        self.assertAlmostEqual(p1[14], ldexp(0.4, 3))
        self.assertAlmostEqual(p1[15], ldexp(0.1, 2))

    def test_ldexp_f32_vec_1(self):
        self._test_ldexp_f32_vec(ISet.AVX512)
        self._test_ldexp_f32_vec(ISet.AVX2)
        self._test_ldexp_f32_vec(ISet.AVX)
        self._test_ldexp_f32_vec(ISet.SSE)

    def _test_ldexp_f64_vec(self, iset):
        source = """
p1 = ldexp(float64x2(0.5, 0.75), int32x2(3, -3))
p2 = ldexp(float64x3(0.5, 0.75, 0.55), int32x3(3, -3, 4))
p3 = ldexp(float64x4(0.5, 0.75, 0.55, 0.25), int32x4(3, -3, 4, -2))
p4 = ldexp(float64x8(0.5, 0.75, 0.55, 0.25, -0.20, 0.4, 0.65, 0.15), int32x8(3, -3, 4, -2, -3, 2, 0, -1))
        """
        args = [('p1', float64x2()), ('p2', float64x3()), ('p3', float64x4()), ('p4', float64x8())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p1 = kernel.get_value('p1')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        p1 = kernel.get_value('p2')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4))
        p1 = kernel.get_value('p3')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4))
        self.assertAlmostEqual(p1[3], ldexp(0.25, -2))
        p1 = kernel.get_value('p4')
        self.assertAlmostEqual(p1[0], ldexp(0.5, 3))
        self.assertAlmostEqual(p1[1], ldexp(0.75, -3))
        self.assertAlmostEqual(p1[2], ldexp(0.55, 4))
        self.assertAlmostEqual(p1[3], ldexp(0.25, -2))
        self.assertAlmostEqual(p1[4], ldexp(-0.2, -3))
        self.assertAlmostEqual(p1[5], ldexp(0.4, 2))
        self.assertAlmostEqual(p1[6], ldexp(0.65, 0))
        self.assertAlmostEqual(p1[7], ldexp(0.15, -1))

    def test_ldexp_f64_vec_1(self):
        self._test_ldexp_f64_vec(ISet.AVX512)
        self._test_ldexp_f64_vec(ISet.AVX2)
        self._test_ldexp_f64_vec(ISet.AVX)
        self._test_ldexp_f64_vec(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
