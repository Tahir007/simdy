
import unittest
from simdy import Kernel, int32, ISet, float32, int64, float64, float64x2, float64x3, float64x4, float64x8,\
    float32x2, float32x3, float32x4, float32x8, float32x16, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64x2, int64x3, int64x4, int64x8


class RngBuiltInTests(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertAlmostEqual(src[i], dst[i])

    def _check_e(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def test_rng_1(self):
        source = """
seed(int64(10))
p1 = random_int32()
p2 = random_int32()
        """
        k = Kernel(source, args=[('p1', int32()), ('p2', int32())])
        k.run()
        self.assertEqual(k.get_value('p1'), 78309535)
        self.assertEqual(k.get_value('p2'), -518197635)

    def _test_rng_2(self, iset):
        source = """
seed(int64(20))
p1 = random_float32()
p2 = random_float32()
        """
        k = Kernel(source, args=[('p1', float32()), ('p2', float32())], iset=iset)
        k.run()
        self.assertAlmostEqual(k.get_value('p1'), 0.3459414)
        self.assertAlmostEqual(k.get_value('p2'), 0.5728327)

    def test_rng_2(self):
        self._test_rng_2(iset=ISet.AVX512)
        self._test_rng_2(iset=ISet.AVX)
        self._test_rng_2(iset=ISet.SSE)

    def test_rng_4(self):
        source = """
seed(int64(40))
p1 = random_int64()
p2 = random_int64()
        """
        k = Kernel(source, args=[('p1', int64()), ('p2', int64())])
        k.run()
        self.assertEqual(k.get_value('p1'), -9176455055171378852)
        self.assertEqual(k.get_value('p2'), 686906800220320631)

    def _test_rng_5(self, iset):
        source = """
seed(int64(60))
p1 = random_float64()
p2 = random_float64()
        """
        k = Kernel(source, args=[('p1', float64()), ('p2', float64())], iset=iset)
        k.run()
        self.assertAlmostEqual(k.get_value('p1'), 0.430850308)
        self.assertAlmostEqual(k.get_value('p2'), 0.829202393)

    def test_rng_5(self):
        self._test_rng_5(iset=ISet.AVX512)
        self._test_rng_5(iset=ISet.AVX)
        self._test_rng_5(iset=ISet.SSE)

    def _test_rng_6(self, iset):
        source = """
seed(int64(60))
p1 = random_float64x2()
p2 = random_float64x2()
        """
        k = Kernel(source, args=[('p1', float64x2()), ('p2', float64x2())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float64x2(0.430850308, 0.829202393), 2)
        self._check(k.get_value('p2'), float64x2(0.057862944903946, 0.7912344262862), 2)

    def test_rng_6(self):
        self._test_rng_6(iset=ISet.AVX512)
        self._test_rng_6(iset=ISet.AVX)
        self._test_rng_6(iset=ISet.SSE)


    def _test_rng_7(self, iset):
        source = """
seed(int64(60))
p1 = random_float64x3()
p2 = random_float64x3()
        """
        k = Kernel(source, args=[('p1', float64x3()), ('p2', float64x3())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float64x3(0.430850308, 0.829202393, 0.057862944903946), 3)
        self._check(k.get_value('p2'), float64x3(0.7912344262862, 0.901632678774505, 0.79916769621763), 3)

    def test_rng_7(self):
        self._test_rng_7(iset=ISet.AVX512)
        self._test_rng_7(iset=ISet.AVX)
        self._test_rng_7(iset=ISet.SSE)

    def _test_rng_8(self, iset):
        source = """
seed(int64(60))
p1 = random_float64x4()
p2 = random_float64x4()
        """
        k = Kernel(source, args=[('p1', float64x4()), ('p2', float64x4())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float64x4(0.430850308, 0.829202393, 0.057862944903946, 0.7912344262862), 4)
        self._check(k.get_value('p2'), float64x4(0.901632678774505, 0.79916769621763, 0.564377687932347, 0.8817004377376076), 4)

    def test_rng_8(self):
        self._test_rng_8(iset=ISet.AVX512)
        self._test_rng_8(iset=ISet.AVX)
        self._test_rng_8(iset=ISet.SSE)

    def _test_rng_9(self, iset):
        source = """
seed(int64(60))
p1 = random_float64x8()
p2 = random_float64x8()
        """
        k = Kernel(source, args=[('p1', float64x8()), ('p2', float64x8())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float64x8(0.430850308, 0.829202393, 0.057862944903946, 0.7912344262862,
                                                 0.901632678774505, 0.79916769621763, 0.564377687932347, 0.8817004377376076), 8)
        self._check(k.get_value('p2'), float64x8(0.5644011791724581, 0.376134372662833, 0.4821483585059745, 0.29871571727566404,
                                                 0.6205069681371618, 0.5373722462848998, 0.6758631199478435, 0.044787386845335675), 8)

    def test_rng_9(self):
        self._test_rng_9(iset=ISet.AVX512)
        self._test_rng_9(iset=ISet.AVX)
        self._test_rng_9(iset=ISet.SSE)

    def _test_rng_10(self, iset):
        source = """
seed(int64(20))
p1 = random_float32x2()
p2 = random_float32x2()
        """
        k = Kernel(source, args=[('p1', float32x2()), ('p2', float32x2())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float32x2(0.3459414, 0.5728327), 2)
        self._check(k.get_value('p2'), float32x2(0.702602505, 0.083703875), 2)

    def test_rng_10(self):
        self._test_rng_10(iset=ISet.AVX512)
        self._test_rng_10(iset=ISet.AVX)
        self._test_rng_10(iset=ISet.SSE)

    def _test_rng_11(self, iset):
        source = """
seed(int64(20))
p1 = random_float32x3()
p2 = random_float32x3()
        """
        k = Kernel(source, args=[('p1', float32x3()), ('p2', float32x3())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float32x3(0.3459414, 0.5728327, 0.702602505), 3)
        self._check(k.get_value('p2'), float32x3( 0.083703875, 0.8642009496688843, 0.44513261318206787), 3)

    def test_rng_11(self):
        self._test_rng_11(iset=ISet.AVX512)
        self._test_rng_11(iset=ISet.AVX)
        self._test_rng_11(iset=ISet.SSE)

    def _test_rng_12(self, iset):
        source = """
seed(int64(20))
p1 = random_float32x4()
p2 = random_float32x4()
        """
        k = Kernel(source, args=[('p1', float32x4()), ('p2', float32x4())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float32x4(0.3459414, 0.5728327, 0.702602505, 0.083703875), 4)
        self._check(k.get_value('p2'), float32x4(0.8642009496688843, 0.44513261318206787, 0.175731539, 0.08595621585), 4)

    def test_rng_12(self):
        self._test_rng_12(iset=ISet.AVX512)
        self._test_rng_12(iset=ISet.AVX)
        self._test_rng_12(iset=ISet.SSE)

    def _test_rng_13(self, iset):
        source = """
seed(int64(20))
p1 = random_float32x8()
p2 = random_float32x8()
        """
        k = Kernel(source, args=[('p1', float32x8()), ('p2', float32x8())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float32x8(0.3459414, 0.5728327, 0.702602505, 0.083703875,
                                                 0.8642009496688843, 0.44513261318206787, 0.175731539, 0.08595621585), 8)
        self._check(k.get_value('p2'), float32x8(0.93453609943, 0.5299322605, 0.77486002445, 0.99406695365,
                                                 0.03522026538, 0.1642799377, 0.316853404, 0.07016468), 8)

    def test_rng_13(self):
        self._test_rng_13(iset=ISet.AVX512)
        self._test_rng_13(iset=ISet.AVX)
        self._test_rng_13(iset=ISet.SSE)

    def _test_rng_14(self, iset):
        source = """
seed(int64(20))
p1 = random_float32x16()
p2 = random_float32x16()
        """
        k = Kernel(source, args=[('p1', float32x16()), ('p2', float32x16())], iset=iset)
        k.run()
        self._check(k.get_value('p1'), float32x16(0.3459414, 0.5728327, 0.702602505, 0.083703875,
                                                  0.8642009496688843, 0.44513261318206787, 0.175731539, 0.08595621585,
                                                  0.93453609943, 0.5299322605, 0.77486002445, 0.99406695365,
                                                  0.03522026538, 0.1642799377, 0.316853404, 0.07016468), 16)
        self._check(k.get_value('p2'), float32x16(0.6985812187, 0.38952231, 0.52323162, 0.75302469,
                                                  0.8184930086, 0.02574384212, 0.413204193, 0.823151469,
                                                  0.04759097099, 0.20681047439, 0.9961555004, 0.6578763723,
                                                  0.2011001110, 0.5265001058, 0.62555909156, 0.7193123102), 16)

    def test_rng_14(self):
        self._test_rng_14(iset=ISet.AVX512)
        self._test_rng_14(iset=ISet.AVX)
        self._test_rng_14(iset=ISet.SSE)

    def _test_rng_15(self, iset):
        source = """
seed(int64(10))
p1 = random_int32x2()
p2 = random_int32x2()
        """
        k = Kernel(source, args=[('p1', int32x2()), ('p2', int32x2())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int32x2(78309535, -518197635), 2)
        self._check_e(k.get_value('p2'), int32x2(-1556819108, 1166444115), 2)

    def test_rng_15(self):
        self._test_rng_15(iset=ISet.AVX512)
        self._test_rng_15(iset=ISet.AVX)
        self._test_rng_15(iset=ISet.SSE)

    def _test_rng_16(self, iset):
        source = """
seed(int64(10))
p1 = random_int32x3()
p2 = random_int32x3()
        """
        k = Kernel(source, args=[('p1', int32x3()), ('p2', int32x3())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int32x3(78309535, -518197635, -1556819108), 3)
        self._check_e(k.get_value('p2'), int32x3(1166444115, -1262349069, -402999515), 3)

    def test_rng_16(self):
        self._test_rng_16(iset=ISet.AVX512)
        self._test_rng_16(iset=ISet.AVX)
        self._test_rng_16(iset=ISet.SSE)

    def _test_rng_17(self, iset):
        source = """
seed(int64(10))
p1 = random_int32x4()
p2 = random_int32x4()
        """
        k = Kernel(source, args=[('p1', int32x4()), ('p2', int32x4())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int32x4(78309535, -518197635, -1556819108, 1166444115), 4)
        self._check_e(k.get_value('p2'), int32x4(-1262349069, -402999515, 473092013, -1598205890), 4)

    def test_rng_17(self):
        self._test_rng_17(iset=ISet.AVX512)
        self._test_rng_17(iset=ISet.AVX)
        self._test_rng_17(iset=ISet.SSE)

    def _test_rng_18(self, iset):
        source = """
seed(int64(10))
p1 = random_int32x8()
p2 = random_int32x8()
        """
        k = Kernel(source, args=[('p1', int32x8()), ('p2', int32x8())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int32x8(78309535, -518197635, -1556819108, 1166444115,
                                                 -1262349069, -402999515, 473092013, -1598205890), 8)
        self._check_e(k.get_value('p2'), int32x8(-858987986, 1500338577, 103613005, 349612142,
                                                 1392407146, 2016090044, -77200999, -1352918831), 8)

    def test_rng_18(self):
        self._test_rng_18(iset=ISet.AVX512)
        self._test_rng_18(iset=ISet.AVX)
        self._test_rng_18(iset=ISet.SSE)

    def _test_rng_19(self, iset):
        source = """
seed(int64(10))
p1 = random_int32x16()
p2 = random_int32x16()
        """
        k = Kernel(source, args=[('p1', int32x16()), ('p2', int32x16())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int32x16(78309535, -518197635, -1556819108, 1166444115,
                                                 -1262349069, -402999515, 473092013, -1598205890,
                                                  -858987986, 1500338577, 103613005, 349612142,
                                                  1392407146, 2016090044, -77200999, -1352918831), 16)
        self._check_e(k.get_value('p2'), int32x16(-1063894349, -107394749, 1505603816, 1413724775,
                                                  1270215197, 277243280, 1679164433, 510792490,
                                                  -725786323, -474815911, -2100346282, 1083782387,
                                                  326934133, 301659751, -2000685468, -240949390), 16)

    def test_rng_19(self):
        self._test_rng_19(iset=ISet.AVX512)
        self._test_rng_19(iset=ISet.AVX)
        self._test_rng_19(iset=ISet.SSE)

    def _test_rng_20(self, iset):
        source = """
seed(int64(40))
p1 = random_int64x2()
p2 = random_int64x2()
        """
        k = Kernel(source, args=[('p1', int64x2()), ('p2', int64x2())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int64x2(-9176455055171378852, 686906800220320631), 2)
        self._check_e(k.get_value('p2'), int64x2(-591056874080640695, 1252066329183052711), 2)

    def test_rng_20(self):
        self._test_rng_20(iset=ISet.AVX512)
        self._test_rng_20(iset=ISet.AVX)
        self._test_rng_20(iset=ISet.SSE)

    def _test_rng_21(self, iset):
        source = """
seed(int64(40))
p1 = random_int64x3()
p2 = random_int64x3()
        """
        k = Kernel(source, args=[('p1', int64x3()), ('p2', int64x3())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int64x3(-9176455055171378852, 686906800220320631, -591056874080640695), 3)
        self._check_e(k.get_value('p2'), int64x3(1252066329183052711, -5683759657157768996, -6481045671433947287), 3)

    def test_rng_21(self):
        self._test_rng_21(iset=ISet.AVX512)
        self._test_rng_21(iset=ISet.AVX)
        self._test_rng_21(iset=ISet.SSE)

    def _test_rng_22(self, iset):
        source = """
seed(int64(40))
p1 = random_int64x4()
p2 = random_int64x4()
        """
        k = Kernel(source, args=[('p1', int64x4()), ('p2', int64x4())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int64x4(-9176455055171378852, 686906800220320631, -591056874080640695, 1252066329183052711), 4)
        self._check_e(k.get_value('p2'), int64x4(-5683759657157768996, -6481045671433947287, -8305516479879773023, 7294170912388826235), 4)

    def test_rng_22(self):
        self._test_rng_22(iset=ISet.AVX512)
        self._test_rng_22(iset=ISet.AVX)
        self._test_rng_22(iset=ISet.SSE)

    def _test_rng_23(self, iset):
        source = """
seed(int64(40))
p1 = random_int64x8()
p2 = random_int64x8()
        """
        k = Kernel(source, args=[('p1', int64x8()), ('p2', int64x8())], iset=iset)
        k.run()
        self._check_e(k.get_value('p1'), int64x8(-9176455055171378852, 686906800220320631, -591056874080640695, 1252066329183052711,
                                                 -5683759657157768996, -6481045671433947287, -8305516479879773023, 7294170912388826235), 8)
        self._check_e(k.get_value('p2'), int64x8(-2072358583670343394, 5948652653797279383, 5088698448076657022, 9124364036538744689,
                                                 3200030109685425950, -7722630800792525588, -2847929825680948461, -3697221713968066650), 8)

    def test_rng_23(self):
        self._test_rng_23(iset=ISet.AVX512)
        self._test_rng_23(iset=ISet.AVX)
        self._test_rng_23(iset=ISet.SSE)



if __name__ == "__main__":
    unittest.main()
