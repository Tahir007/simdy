
import unittest
from simdy import Kernel, float64x2, float64x3,\
    float64x4, float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, ISet


class BuiltinsCopysignTests(unittest.TestCase):

    def _check_x2(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])

    def _check_x3(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])

    def _check_x4(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])

    def _check_x8(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])

    def _check_x16(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])
        self.assertAlmostEqual(src[8], dst[8])
        self.assertAlmostEqual(src[9], dst[9])
        self.assertAlmostEqual(src[10], dst[10])
        self.assertAlmostEqual(src[11], dst[11])
        self.assertAlmostEqual(src[12], dst[12])
        self.assertAlmostEqual(src[13], dst[13])
        self.assertAlmostEqual(src[14], dst[14])
        self.assertAlmostEqual(src[15], dst[15])

    def _test_copysign_1(self, iset):
        source = """
p3 = copysign(p1, float64x2(1.0))
p4 = copysign(p2, float64x2(-1.0))

p13 = copysign(p11, float64x3(1.0))
p14 = copysign(p12, float64x3(-1.0))

p23 = copysign(p21, float64x4(1.0))
p24 = copysign(p22, float64x4(-1.0))

p33 = copysign(p31, float64x8(1.0))
p34 = copysign(p32, float64x8(-1.0))
        """
        args = [('p1', float64x2(-25, 10)), ('p2', float64x2(10, -5)),
                ('p3', float64x2()), ('p4', float64x2()), ('p5', float64x2()),
                ('p11', float64x3(-25, 10, -14)), ('p12', float64x3(10, -4, -7)),
                ('p13', float64x3()), ('p14', float64x3()), ('p15', float64x3()),
                ('p21', float64x4(-25, 10, -14, 6)), ('p22', float64x4(10, -4, -7, 4)),
                ('p23', float64x4()), ('p24', float64x4()), ('p25', float64x4()),
                ('p31', float64x8(-25, 10, -14, 6, -6, 8, -7, 8)),
                ('p32', float64x8(10, -4, -7, 4, 88, -4, -7, 3)),
                ('p33', float64x8()), ('p34', float64x8()), ('p35', float64x8()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_x2(kernel.get_value('p3'), float64x2(25, 10))
        self._check_x2(kernel.get_value('p4'), float64x2(-10, -5))
        self._check_x3(kernel.get_value('p13'), float64x3(25, 10, 14))
        self._check_x3(kernel.get_value('p14'), float64x3(-10, -4, -7))
        self._check_x4(kernel.get_value('p23'), float64x4(25, 10, 14, 6))
        self._check_x4(kernel.get_value('p24'), float64x4(-10, -4, -7, -4))
        self._check_x8(kernel.get_value('p33'), float64x8(25, 10, 14, 6, 6, 8, 7, 8))
        self._check_x8(kernel.get_value('p34'), float64x8(-10, -4, -7, -4, -88, -4, -7, -3))

    def test_copysign_1(self):
        self._test_copysign_1(ISet.AVX512)
        self._test_copysign_1(ISet.AVX2)
        self._test_copysign_1(ISet.AVX)
        self._test_copysign_1(ISet.SSE)

    def _test_copysign_2(self, iset):
        source = """
p3 = copysign(p1, float32x2(1.0))
p4 = copysign(p2, float32x2(-1.0))

p13 = copysign(p11, float32x3(1.0))
p14 = copysign(p12, float32x3(-1.0))

p23 = copysign(p21, float32x4(1.0))
p24 = copysign(p22, float32x4(-1.0))

p33 = copysign(p31, float32x8(1.0))
p34 = copysign(p32, float32x8(-1.0))

p43 = copysign(p41, float32x16(1.0))
p44 = copysign(p42, float32x16(-1.0))
        """
        args = [('p1', float32x2(-25, 10)), ('p2', float32x2(10, -5)),
                ('p3', float32x2()), ('p4', float32x2()), ('p5', float32x2()),
                ('p11', float32x3(-25, 10, -14)), ('p12', float32x3(10, -4, -7)),
                ('p13', float32x3()), ('p14', float32x3()), ('p15', float32x3()),
                ('p21', float32x4(-25, 10, -14, 6)), ('p22', float32x4(10, -4, -7, 4)),
                ('p23', float32x4()), ('p24', float32x4()), ('p25', float32x4()),
                ('p31', float32x8(-25, 10, -14, 6, -6, 8, -7, 8)),
                ('p32', float32x8(10, -4, -7, 4, 88, -4, -7, 3)),
                ('p33', float32x8()), ('p34', float32x8()), ('p35', float32x8()),
                ('p41', float32x16(-25, 10, -14, 6, -6, 8, -7, 8, 1, 4, -4, 5, -6, 8, -9, 2)),
                ('p42', float32x16(10, -4, -7, 4, 88, -4, -7, 3, 2, 3, -4, 1, 4, 5, -8, 3)),
                ('p43', float32x16()), ('p44', float32x16()), ('p45', float32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_x2(kernel.get_value('p3'), float32x2(25, 10))
        self._check_x2(kernel.get_value('p4'), float32x2(-10, -5))
        self._check_x3(kernel.get_value('p13'), float32x3(25, 10, 14))
        self._check_x3(kernel.get_value('p14'), float32x3(-10, -4, -7))
        self._check_x4(kernel.get_value('p23'), float32x4(25, 10, 14, 6))
        self._check_x4(kernel.get_value('p24'), float32x4(-10, -4, -7, -4))
        self._check_x8(kernel.get_value('p33'), float32x8(25, 10, 14, 6, 6, 8, 7, 8))
        self._check_x8(kernel.get_value('p34'), float32x8(-10, -4, -7, -4, -88, -4, -7, -3))
        self._check_x16(kernel.get_value('p43'), float32x16(25, 10, 14, 6, 6, 8, 7, 8, 1, 4, 4, 5, 6, 8, 9, 2))
        self._check_x16(kernel.get_value('p44'), float32x16(-10, -4, -7, -4, -88, -4, -7, -3, -2, -3, -4, -1, -4, -5, -8, -3))

    def test_copysign_2(self):
        self._test_copysign_2(ISet.AVX512)
        self._test_copysign_2(ISet.AVX2)
        self._test_copysign_2(ISet.AVX)
        self._test_copysign_2(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
