
import unittest
from math import sqrt
from simdy import Kernel, float32, float64, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, ISet


class BuiltinsSqrtTests(unittest.TestCase):

    def _test_sqrt_1(self, iset):
        source = """
p2 = sqrt(p1)
p3 = sqrt(p1 * 2)

p12 = sqrt(p11)
p13 = sqrt(p11 * 2)

p22 = sqrt(p21)
p23 = sqrt(p21 * 2)

p32 = sqrt(p31)
p33 = sqrt(p31 * 2)

p42 = sqrt(p41)
p43 = sqrt(p41 * 2)

p52 = sqrt(p51)
p53 = sqrt(p51 * 2)

p62 = sqrt(p61)
p63 = sqrt(p61 * 2)

p72 = sqrt(p71)
p73 = sqrt(p71 * 2)

p82 = sqrt(p81)
p83 = sqrt(p81 * 2)

p92 = sqrt(p91)
p93 = sqrt(p91 * 2)

p102 = sqrt(p101)
p103 = sqrt(p101 * 2)
        """

        args = [('p1', float32(4.0)), ('p2', float32()), ('p3', float32()),
                ('p11', float64(4.0)), ('p12', float64()), ('p13', float64()),
                ('p21', float64x2(4.0, 2)), ('p22', float64x2()), ('p23', float64x2()),
                ('p31', float64x3(4.0, 2, 7)), ('p32', float64x3()), ('p33', float64x3()),
                ('p41', float64x4(4.0, 2, 7, 5)), ('p42', float64x4()), ('p43', float64x4()),
                ('p51', float64x8(4.0, 2, 7, 5, 3, 4, 2, 4)), ('p52', float64x8()), ('p53', float64x8()),
                ('p61', float32x2(4.0, 2)), ('p62', float32x2()), ('p63', float32x2()),
                ('p71', float32x3(4.0, 2, 7)), ('p72', float32x3()), ('p73', float32x3()),
                ('p81', float32x4(4.0, 2, 7, 5)), ('p82', float32x4()), ('p83', float32x4()),
                ('p91', float32x8(4.0, 2, 7, 5, 3, 4, 2, 4)), ('p92', float32x8()), ('p93', float32x8()),
                ('p101', float32x16(4.0, 2, 7, 5, 3, 4, 2, 4, 6, 5, 4, 9, 7, 6, 5, 4)),
                ('p102', float32x16()), ('p103', float32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p2'), sqrt(4.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p3'), sqrt(8.0), places=6)

        self.assertAlmostEqual(kernel.get_value('p12'), sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p13'), sqrt(8.0))

        self.assertAlmostEqual(kernel.get_value('p22')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p22')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p23')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p23')[1], sqrt(4.0))

        self.assertAlmostEqual(kernel.get_value('p32')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p32')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p32')[2], sqrt(7.0))
        self.assertAlmostEqual(kernel.get_value('p33')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p33')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p33')[2], sqrt(14.0))

        self.assertAlmostEqual(kernel.get_value('p42')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p42')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p42')[2], sqrt(7.0))
        self.assertAlmostEqual(kernel.get_value('p42')[3], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p43')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p43')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p43')[2], sqrt(14.0))
        self.assertAlmostEqual(kernel.get_value('p43')[3], sqrt(10.0))

        self.assertAlmostEqual(kernel.get_value('p52')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p52')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p52')[2], sqrt(7.0))
        self.assertAlmostEqual(kernel.get_value('p52')[3], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p52')[4], sqrt(3.0))
        self.assertAlmostEqual(kernel.get_value('p52')[5], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p52')[6], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p52')[7], sqrt(4.0))

        self.assertAlmostEqual(kernel.get_value('p53')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p53')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p53')[2], sqrt(14.0))
        self.assertAlmostEqual(kernel.get_value('p53')[3], sqrt(10.0))
        self.assertAlmostEqual(kernel.get_value('p53')[4], sqrt(6.0))
        self.assertAlmostEqual(kernel.get_value('p53')[5], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p53')[6], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p53')[7], sqrt(8.0))

        self.assertAlmostEqual(kernel.get_value('p62')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p62')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p63')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p63')[1], sqrt(4.0))

        self.assertAlmostEqual(kernel.get_value('p72')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p72')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p72')[2], sqrt(7.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p73')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p73')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p73')[2], sqrt(14.0), places=6)

        self.assertAlmostEqual(kernel.get_value('p82')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p82')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p82')[2], sqrt(7.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p82')[3], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p83')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p83')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p83')[2], sqrt(14.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p83')[3], sqrt(10.0))

        self.assertAlmostEqual(kernel.get_value('p92')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p92')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p92')[2], sqrt(7.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p92')[3], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p92')[4], sqrt(3.0))
        self.assertAlmostEqual(kernel.get_value('p92')[5], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p92')[6], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p92')[7], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p93')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p93')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p93')[2], sqrt(14.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p93')[3], sqrt(10.0))
        self.assertAlmostEqual(kernel.get_value('p93')[4], sqrt(6.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p93')[5], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p93')[6], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p93')[7], sqrt(8.0))

        self.assertAlmostEqual(kernel.get_value('p102')[0], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p102')[1], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p102')[2], sqrt(7.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p102')[3], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p102')[4], sqrt(3.0))
        self.assertAlmostEqual(kernel.get_value('p102')[5], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p102')[6], sqrt(2.0))
        self.assertAlmostEqual(kernel.get_value('p102')[7], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p102')[8], sqrt(6.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p102')[9], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p102')[10], sqrt(4.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p102')[11], sqrt(9.0))
        self.assertAlmostEqual(kernel.get_value('p102')[12], sqrt(7.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p102')[13], sqrt(6.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p102')[14], sqrt(5.0))
        self.assertAlmostEqual(kernel.get_value('p102')[15], sqrt(4.0))

        self.assertAlmostEqual(kernel.get_value('p103')[0], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p103')[1], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p103')[2], sqrt(14.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[3], sqrt(10.0))
        self.assertAlmostEqual(kernel.get_value('p103')[4], sqrt(6.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[5], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p103')[6], sqrt(4.0))
        self.assertAlmostEqual(kernel.get_value('p103')[7], sqrt(8.0))
        self.assertAlmostEqual(kernel.get_value('p103')[8], sqrt(12.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[9], sqrt(10.0))
        self.assertAlmostEqual(kernel.get_value('p103')[10], sqrt(8.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[11], sqrt(18.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[12], sqrt(14.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[13], sqrt(12.0), places=6)
        self.assertAlmostEqual(kernel.get_value('p103')[14], sqrt(10.0))
        self.assertAlmostEqual(kernel.get_value('p103')[15], sqrt(8.0))

    def test_sqrt_1(self):
        self._test_sqrt_1(ISet.AVX512)
        self._test_sqrt_1(ISet.AVX)
        self._test_sqrt_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
