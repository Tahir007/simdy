
import unittest
from simdy import Kernel, float32, float64, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, ISet


class BuiltinsDotTests(unittest.TestCase):

    def _check_x8(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])

    def _check_x16(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])
        self.assertAlmostEqual(src1[8], src2[8])
        self.assertAlmostEqual(src1[9], src2[9])
        self.assertAlmostEqual(src1[10], src2[10])
        self.assertAlmostEqual(src1[11], src2[11])
        self.assertAlmostEqual(src1[12], src2[12])
        self.assertAlmostEqual(src1[13], src2[13])
        self.assertAlmostEqual(src1[14], src2[14])
        self.assertAlmostEqual(src1[15], src2[15])

    def _test_dot_1(self, iset):
        source = """
p1 = dot(float64x2(1, 2), float64x2(3, 1))
p2 = dot(float64x3(1, 2, 3), float64x3(2, 2, 2))
p3 = dot(float64x4(1, 2, 3, 4), float64x4(2, 2, 2, 2))
p4 = dot(float64x8(1, 2, 3, 4, 5, 6, 7, 8), float64x8(2, 2, 2, 2, 2, 2, 2, 2))

p5 = dot(float32x2(1, 2), float32x2(3, 1))
p6 = dot(float32x3(1, 2, 3), float32x3(2, 2, 2))
p7 = dot(float32x4(1, 2, 3, 4), float32x4(2, 2, 2, 2))
p8 = dot(float32x8(1, 2, 3, 4, 5, 6, 7, 8), float32x8(2, 2, 2, 2, 2, 2, 2, 2))
p9 = dot(float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16),
         float32x16(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2))

# p10 = ddot(float32x8(1, 2, 3, 4, 5, 6, 7, 8), float32x8(2, 2, 2, 2, 2, 2, 2, 2))
# p11 = qdot(float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16),
#            float32x16(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2))

        """
        args = [('p1', float64()), ('p2', float64()), ('p3', float64()), ('p4', float64()),
                ('p5', float32()), ('p6', float32()), ('p7', float32()), ('p8', float32()),
                ('p9', float32()), ('p10', float32x8()), ('p11', float32x16())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 5)
        self.assertAlmostEqual(kernel.get_value('p2'), 12)
        self.assertAlmostEqual(kernel.get_value('p3'), 20)
        self.assertAlmostEqual(kernel.get_value('p4'), 72)
        self.assertAlmostEqual(kernel.get_value('p5'), 5)
        self.assertAlmostEqual(kernel.get_value('p6'), 12)
        self.assertAlmostEqual(kernel.get_value('p7'), 20)
        self.assertAlmostEqual(kernel.get_value('p8'), 72)
        self.assertAlmostEqual(kernel.get_value('p9'), 272)

        # self._check_x8(kernel.get_value('p10'), float32x8(20, 20, 20, 20, 52, 52, 52, 52))
        # self._check_x16(kernel.get_value('p11'), float32x16(20, 20, 20, 20, 52, 52, 52, 52, 84, 84, 84, 84, 116, 116, 116, 116))

    def test_dot_1(self):
        self._test_dot_1(ISet.AVX512)
        self._test_dot_1(ISet.AVX)
        self._test_dot_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
