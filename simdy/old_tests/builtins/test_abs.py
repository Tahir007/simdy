
import unittest
from simdy import Kernel, int32, int64, float32, float64, float64x2, float64x3,\
    float64x4, float64x8, ISet, float32x2, float32x3, float32x4, float32x8, float32x16,\
    int32x2, int32x3, int32x4, int32x8, int32x16


class BuiltinsAbsTests(unittest.TestCase):

    def _check_x2(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])

    def _check_x3(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])

    def _check_x4(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])

    def _check_x8(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])

    def _check_x16(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0])
        self.assertAlmostEqual(src[1], dst[1])
        self.assertAlmostEqual(src[2], dst[2])
        self.assertAlmostEqual(src[3], dst[3])
        self.assertAlmostEqual(src[4], dst[4])
        self.assertAlmostEqual(src[5], dst[5])
        self.assertAlmostEqual(src[6], dst[6])
        self.assertAlmostEqual(src[7], dst[7])
        self.assertAlmostEqual(src[8], dst[8])
        self.assertAlmostEqual(src[9], dst[9])
        self.assertAlmostEqual(src[10], dst[10])
        self.assertAlmostEqual(src[11], dst[11])
        self.assertAlmostEqual(src[12], dst[12])
        self.assertAlmostEqual(src[13], dst[13])
        self.assertAlmostEqual(src[14], dst[14])
        self.assertAlmostEqual(src[15], dst[15])

    def _test_abs_1(self, iset):
        source = """
p3 = abs(p1)
p4 = abs(2 * p1)
p5 = abs(p2)

p13 = abs(p11)
p14 = abs(2 * p11)
p15 = abs(p12)

p23 = abs(p21)
p24 = abs(2 * p21)
p25 = abs(p22)

p33 = abs(p31)
p34 = abs(2 * p31)
p35 = abs(p32)

        """

        args = [('p1', int32(-25)), ('p2', int32(10)),
                ('p3', int32()), ('p4', int32()), ('p5', int32()),
                ('p11', int64(-25)), ('p12', int64(10)),
                ('p13', int64()), ('p14', int64()), ('p15', int64()),
                ('p21', float64(-25)), ('p22', float64(10)),
                ('p23', float64()), ('p24', float64()), ('p25', float64()),
                ('p31', float32(-25)), ('p32', float32(10)),
                ('p33', float32()), ('p34', float32()), ('p35', float32())
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p3'), 25)
        self.assertEqual(kernel.get_value('p4'), 50)
        self.assertEqual(kernel.get_value('p5'), 10)

        self.assertEqual(kernel.get_value('p13'), 25)
        self.assertEqual(kernel.get_value('p14'), 50)
        self.assertEqual(kernel.get_value('p15'), 10)

        self.assertEqual(kernel.get_value('p23'), 25)
        self.assertEqual(kernel.get_value('p24'), 50)
        self.assertEqual(kernel.get_value('p25'), 10)

        self.assertEqual(kernel.get_value('p33'), 25)
        self.assertEqual(kernel.get_value('p34'), 50)
        self.assertEqual(kernel.get_value('p35'), 10)

    def test_abs_1(self):
        self._test_abs_1(ISet.AVX512)
        self._test_abs_1(ISet.AVX)
        self._test_abs_1(ISet.SSE)

    def _test_abs_2(self, iset):
        source = """
p3 = abs(p1)
p4 = abs(2 * p1)
p5 = abs(p2)

p13 = abs(p11)
p14 = abs(2 * p11)
p15 = abs(p12)

p23 = abs(p21)
p24 = abs(2 * p21)
p25 = abs(p22)

p33 = abs(p31)
p34 = abs(2 * p31)
p35 = abs(p32)

p43 = abs(p41)
p44 = abs(2 * p41)
p45 = abs(p42)

p53 = abs(p51)
p54 = abs(2 * p51)
p55 = abs(p52)

p63 = abs(p61)
p64 = abs(2 * p61)
p65 = abs(p62)

p73 = abs(p71)
p74 = abs(2 * p71)
p75 = abs(p72)

p83 = abs(p81)
p84 = abs(2 * p81)
p85 = abs(p82)

p93 = abs(p91)
p103 = abs(p101)
p113 = abs(p111)
p123 = abs(p121)
p133 = abs(p131)
        """
        args = [('p1', float64x2(-25, -10)), ('p2', float64x2(10, -5)),
                ('p3', float64x2()), ('p4', float64x2()), ('p5', float64x2()),
                ('p11', float64x3(-25, 10, -14)), ('p12', float64x3(10, -4, -7)),
                ('p13', float64x3()), ('p14', float64x3()), ('p15', float64x3()),
                ('p21', float64x4(-25, 10, -14, 6)), ('p22', float64x4(10, -4, -7, 4)),
                ('p23', float64x4()), ('p24', float64x4()), ('p25', float64x4()),
                ('p31', float64x8(-25, 10, -14, 6, -6, 8, -7, 8)),
                ('p32', float64x8(10, -4, -7, 4, 88, -4, -7, 3)),
                ('p33', float64x8()), ('p34', float64x8()), ('p35', float64x8()),

                ('p41', float32x2(-25, -10)), ('p42', float32x2(10, -5)),
                ('p43', float32x2()), ('p44', float32x2()), ('p45', float32x2()),
                ('p51', float32x3(-25, 10, -14)), ('p52', float32x3(10, -4, -7)),
                ('p53', float32x3()), ('p54', float32x3()), ('p55', float32x3()),
                ('p61', float32x4(-25, 10, -14, 6)), ('p62', float32x4(10, -4, -7, 4)),
                ('p63', float32x4()), ('p64', float32x4()), ('p65', float32x4()),
                ('p71', float32x8(-25, 10, -14, 6, -6, 8, -7, 8)),
                ('p72', float32x8(10, -4, -7, 4, 88, -4, -7, 3)),
                ('p73', float32x8()), ('p74', float32x8()), ('p75', float32x8()),
                ('p81', float32x16(-25, 10, -14, 6, -6, 8, -7, 8, -4, 8, -9, -7, 5, -6, -2, 4)),
                ('p82', float32x16(10, -4, -7, 4, 88, -4, -7, 3, 3, -5, -7, -5, 4, 7, -2, -7)),
                ('p83', float32x16()), ('p84', float32x16()), ('p85', float32x16()),

                ('p91', int32x2(-25, -10)), ('p92', int32x2(10, -5)),
                ('p93', int32x2()), ('p94', int32x2()), ('p95', int32x2()),
                ('p101', int32x3(-25, 10, -14)), ('p102', int32x3(10, -4, -7)),
                ('p103', int32x3()), ('p104', int32x3()), ('p105', int32x3()),
                ('p111', int32x4(-25, 10, -14, 6)), ('p112', int32x4(10, -4, -7, 4)),
                ('p113', int32x4()), ('p114', int32x4()), ('p115', int32x4()),
                ('p121', int32x8(-25, 10, -14, 6, -6, 8, -7, 8)),
                ('p122', int32x8(10, -4, -7, 4, 88, -4, -7, 3)),
                ('p123', int32x8()), ('p124', int32x8()), ('p125', int32x8()),
                ('p131', int32x16(-25, 10, -14, 6, -6, 8, -7, 8, -4, 8, -9, -7, 5, -6, -2, 4)),
                ('p132', int32x16(10, -4, -7, 4, 88, -4, -7, 3, 3, -5, -7, -5, 4, 7, -2, -7)),
                ('p133', int32x16()), ('p134', int32x16()), ('p135', int32x16()),
                ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_x2(kernel.get_value('p3'), float64x2(25, 10))
        self._check_x2(kernel.get_value('p4'), float64x2(50, 20))
        self._check_x2(kernel.get_value('p5'), float64x2(10, 5))

        self._check_x3(kernel.get_value('p13'), float64x3(25, 10, 14))
        self._check_x3(kernel.get_value('p14'), float64x3(50, 20, 28))
        self._check_x3(kernel.get_value('p15'), float64x3(10, 4, 7))

        self._check_x4(kernel.get_value('p23'), float64x4(25, 10, 14, 6))
        self._check_x4(kernel.get_value('p24'), float64x4(50, 20, 28, 12))
        self._check_x4(kernel.get_value('p25'), float64x4(10, 4, 7, 4))

        self._check_x8(kernel.get_value('p33'), float64x8(25, 10, 14, 6, 6, 8, 7, 8))
        self._check_x8(kernel.get_value('p34'), float64x8(50, 20, 28, 12, 12, 16, 14, 16))
        self._check_x8(kernel.get_value('p35'), float64x8(10, 4, 7, 4, 88, 4, 7, 3))

        self._check_x2(kernel.get_value('p43'), float32x2(25, 10))
        self._check_x2(kernel.get_value('p44'), float32x2(50, 20))
        self._check_x2(kernel.get_value('p45'), float32x2(10, 5))

        self._check_x3(kernel.get_value('p53'), float32x3(25, 10, 14))
        self._check_x3(kernel.get_value('p54'), float32x3(50, 20, 28))
        self._check_x3(kernel.get_value('p55'), float32x3(10, 4, 7))

        self._check_x4(kernel.get_value('p63'), float32x4(25, 10, 14, 6))
        self._check_x4(kernel.get_value('p64'), float32x4(50, 20, 28, 12))
        self._check_x4(kernel.get_value('p65'), float32x4(10, 4, 7, 4))

        self._check_x8(kernel.get_value('p73'), float32x8(25, 10, 14, 6, 6, 8, 7, 8))
        self._check_x8(kernel.get_value('p74'), float32x8(50, 20, 28, 12, 12, 16, 14, 16))
        self._check_x8(kernel.get_value('p75'), float32x8(10, 4, 7, 4, 88, 4, 7, 3))

        self._check_x16(kernel.get_value('p83'), float32x16(25, 10, 14, 6, 6, 8, 7, 8, 4, 8, 9, 7, 5, 6, 2, 4))
        self._check_x16(kernel.get_value('p84'), float32x16(50, 20, 28, 12, 12, 16, 14, 16, 8, 16, 18, 14, 10, 12, 4, 8))
        self._check_x16(kernel.get_value('p85'), float32x16(10, 4, 7, 4, 88, 4, 7, 3, 3, 5, 7, 5, 4, 7, 2, 7))

        self._check_x2(kernel.get_value('p93'), int32x2(25, 10))
        self._check_x3(kernel.get_value('p103'), int32x3(25, 10, 14))
        self._check_x4(kernel.get_value('p113'), int32x4(25, 10, 14, 6))
        self._check_x8(kernel.get_value('p123'), int32x8(25, 10, 14, 6, 6, 8, 7, 8))
        self._check_x16(kernel.get_value('p133'), int32x16(25, 10, 14, 6, 6, 8, 7, 8, 4, 8, 9, 7, 5, 6, 2, 4))

    def test_abs_2(self):
        self._test_abs_2(ISet.AVX512)
        self._test_abs_2(ISet.AVX2)
        self._test_abs_2(ISet.AVX)
        self._test_abs_2(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
