
import unittest
from math import frexp
from simdy import Kernel, float32, float64, int32, ISet


class BuiltinsFrexpTests(unittest.TestCase):

    def _test_frexp_f32_1(self, iset):
        source = """
a = float32(2.2)
p1, m1 = frexp(a)
p2, m2 = frexp(float32(0.0))
p3, m3 = frexp(float32(-0.034))
        """

        args = [('p1', float32()), ('m1', int32()), ('p2', float32()),
                ('m2', int32()), ('p3', float32()), ('m3', int32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p1, m1 = frexp(2.2)
        self.assertAlmostEqual(kernel.get_value('p1'), p1, places=6)
        self.assertEqual(kernel.get_value('m1'), m1)
        p2, m2 = frexp(0.0)
        self.assertAlmostEqual(kernel.get_value('p2'), p2, places=6)
        self.assertEqual(kernel.get_value('m2'), m2)
        p3, m3 = frexp(-0.034)
        self.assertAlmostEqual(kernel.get_value('p3'), p3, places=6)
        self.assertEqual(kernel.get_value('m3'), m3)

    def test_frexp_f32_1(self):
        self._test_frexp_f32_1(ISet.AVX)

    def test_frexp_f32_2(self):
        self._test_frexp_f32_1(ISet.SSE)

    def _test_frexp_f64_1(self, iset):
        source = """
a = 2.2
p1, m1 = frexp(a)
p2, m2 = frexp(0.0)
p3, m3 = frexp(-0.034)
        """
        args = [('p1', float64()), ('m1', int32()), ('p2', float64()),
                ('m2', int32()), ('p3', float64()), ('m3', int32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        p1, m1 = frexp(2.2)
        self.assertEqual(kernel.get_value('p1'), p1)
        self.assertEqual(kernel.get_value('m1'), m1)
        p2, m2 = frexp(0.0)
        self.assertEqual(kernel.get_value('p2'), p2)
        self.assertEqual(kernel.get_value('m2'), m2)
        p3, m3 = frexp(-0.034)
        self.assertEqual(kernel.get_value('p3'), p3)
        self.assertEqual(kernel.get_value('m3'), m3)

    def test_frexp_f64_1(self):
        self._test_frexp_f64_1(ISet.AVX)

    def test_frexp_f64_2(self):
        self._test_frexp_f64_1(ISet.SSE)

if __name__ == "__main__":
    unittest.main()
