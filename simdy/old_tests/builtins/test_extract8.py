import unittest
from simdy import Kernel, int32x8, int32x16, ISet, float32x8, float32x16


class Extract8Tests(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _test_extract8(self, iset):
        source = """
p1 = float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
p2 = float32x16(1)
r1 = extract8(p1, 0)
r2 = extract8(p1, 1)
r3 = extract8(p1 + p2, 0)
r4 = extract8(p1 + p2, 1)

p3 = int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
p4 = int32x16(1)
r5 = extract8(p3, 0)
r6 = extract8(p3, 1)
r7 = extract8(p3 + p4, 0)
r8 = extract8(p3 + p4, 1)
        """
        args = [('r1', float32x8()), ('r2', float32x8()), ('r3', float32x8()), ('r4', float32x8()),
                ('r5', int32x8()), ('r6', int32x8()), ('r7', int32x8()), ('r8', int32x8())
                ]
        k = Kernel(source, args=args, iset=iset)
        k.run()
        self._check(k.get_value('r1'), float32x8(1, 2, 3, 4, 5, 6, 7, 8), 8)
        self._check(k.get_value('r2'), float32x8(9, 10, 11, 12, 13, 14, 15, 16), 8)
        self._check(k.get_value('r3'), float32x8(2, 3, 4, 5, 6, 7, 8, 9), 8)
        self._check(k.get_value('r4'), float32x8(10, 11, 12, 13, 14, 15, 16, 17), 8)

        self._check(k.get_value('r5'), int32x8(1, 2, 3, 4, 5, 6, 7, 8), 8)
        self._check(k.get_value('r6'), int32x8(9, 10, 11, 12, 13, 14, 15, 16), 8)
        self._check(k.get_value('r7'), int32x8(2, 3, 4, 5, 6, 7, 8, 9), 8)
        self._check(k.get_value('r8'), int32x8(10, 11, 12, 13, 14, 15, 16, 17), 8)

    def test_extarct8(self):
        self._test_extract8(ISet.AVX512)
        self._test_extract8(ISet.AVX2)
        self._test_extract8(ISet.AVX)
        self._test_extract8(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
