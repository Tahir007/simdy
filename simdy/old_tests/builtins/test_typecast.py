
import unittest
from simdy import Kernel, int32, int64, float32, float64, ISet, float32x2,\
    float32x3, float32x4, float32x8, float32x16, float64x2, float64x3, float64x4,\
    float64x8, int32x2, int32x3, int32x4, int32x8, int32x16, int64x2, int64x3, int64x4, int64x8


class BuiltinsTypecastTests(unittest.TestCase):

    def _check_almost(self, src, dst, n):
        for i in range(n):
            self.assertAlmostEqual(src[i], dst[i])

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _test_typecast(self, iset):
        source = """
p1 = typecast(int32(0x3f800000), float32)
p2 = typecast(int32x2(0x3f800000), float32x2)
p3 = typecast(int32x3(0x3f800000), float32x3)
p4 = typecast(int32x4(0x3f800000), float32x4)
p5 = typecast(int32x8(0x3f800000), float32x8)
p6 = typecast(int32x16(0x3f800000), float32x16)

p7 = typecast(float32(1.0), int32)
p8 = typecast(float32x2(1.0), int32x2)
p9 = typecast(float32x3(1.0), int32x3)
p10 = typecast(float32x4(1.0), int32x4)
p11 = typecast(float32x8(1.0), int32x8)
p12 = typecast(float32x16(1.0), int32x16)

p13 = typecast(int64(0x3ff0000000000000), float64)
p14 = typecast(int64x2(0x3ff0000000000000), float64x2)
p15 = typecast(int64x3(0x3ff0000000000000), float64x3)
p16 = typecast(int64x4(0x3ff0000000000000), float64x4)
p17 = typecast(int64x8(0x3ff0000000000000), float64x8)

p18 = typecast(float64(1.0), int64)
p19 = typecast(float64x2(1.0), int64x2)
p20 = typecast(float64x3(1.0), int64x3)
p21 = typecast(float64x4(1.0), int64x4)
p22 = typecast(float64x8(1.0), int64x8)
        """
        args = [('p1', float32()), ('p2', float32x2()), ('p3', float32x3()),
                ('p4', float32x4()), ('p5', float32x8()), ('p6', float32x16()),
                ('p7', int32()), ('p8', int32x2()), ('p9', int32x3()),
                ('p10', int32x4()), ('p11', int32x8()), ('p12', int32x16()),
                ('p13', float64()), ('p14', float64x2()), ('p15', float64x3()),
                ('p16', float64x4()), ('p17', float64x8()),
                ('p18', int64()), ('p19', int64x2()), ('p20', int64x3()),
                ('p21', int64x4()), ('p22', int64x8())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 1.0)
        self._check_almost(kernel.get_value('p2'), float32x2(1.0), 2)
        self._check_almost(kernel.get_value('p3'), float32x3(1.0), 3)
        self._check_almost(kernel.get_value('p4'), float32x4(1.0), 4)
        self._check_almost(kernel.get_value('p5'), float32x8(1.0), 8)
        self._check_almost(kernel.get_value('p6'), float32x16(1.0), 16)

        self.assertEqual(kernel.get_value('p7'), 0x3f800000)
        self._check(kernel.get_value('p8'), int32x2(0x3f800000), 2)
        self._check(kernel.get_value('p9'), int32x3(0x3f800000), 3)
        self._check(kernel.get_value('p10'), int32x4(0x3f800000), 4)
        self._check(kernel.get_value('p11'), int32x8(0x3f800000), 8)
        self._check(kernel.get_value('p12'), int32x16(0x3f800000), 16)

        self.assertAlmostEqual(kernel.get_value('p13'), 1.0)
        self._check_almost(kernel.get_value('p14'), float64x2(1.0), 2)
        self._check_almost(kernel.get_value('p15'), float64x3(1.0), 3)
        self._check_almost(kernel.get_value('p16'), float64x4(1.0), 4)
        self._check_almost(kernel.get_value('p17'), float64x8(1.0), 8)

        self.assertEqual(kernel.get_value('p18'), 0x3ff0000000000000)
        self._check(kernel.get_value('p19'), int64x2(0x3ff0000000000000), 2)
        self._check(kernel.get_value('p20'), int64x3(0x3ff0000000000000), 3)
        self._check(kernel.get_value('p21'), int64x4(0x3ff0000000000000), 4)
        self._check(kernel.get_value('p22'), int64x8(0x3ff0000000000000), 8)

    def test_typecast_i32_1(self):
        self._test_typecast(ISet.AVX512)
        self._test_typecast(ISet.AVX2)
        self._test_typecast(ISet.AVX)
        self._test_typecast(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
