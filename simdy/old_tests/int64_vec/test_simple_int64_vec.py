
import unittest
from simdy import Kernel, int64x2, int64x3, int64x4, int64x8, ISet


class TestsSimpleInt64Vec(unittest.TestCase):

    def _check(self, src, dst, n):
        for i in range(n):
            self.assertEqual(src[i], dst[i])

    def _test_simple_int64_vec_1(self, iset):
        source = """
p3 = p1
p4 = p2
tmp1 = p1
p5 = tmp1

p8 = p6
p9 = p7
tmp2 = p6
p10 = tmp2

p13 = p11
p14 = p12
tmp3 = p11
p15 = tmp3

p18 = p16
p19 = p17
tmp4 = p16
p20 = tmp4
                """

        args = [
            ('p1', int64x2(1, 5)), ('p2', int64x2(2)), ('p3', int64x2()),
            ('p4', int64x2()), ('p5', int64x2()),
            ('p6', int64x3(1, 5, 9)), ('p7', int64x3(2)), ('p8', int64x3()),
            ('p9', int64x3()), ('p10', int64x3()),
            ('p11', int64x4(1, 5, 9, 18)), ('p12', int64x4(2)), ('p13', int64x4()),
            ('p14', int64x4()), ('p15', int64x4()),
            ('p16', int64x8(1, 5, 9, 18, 6, 15, 78, 125)), ('p17', int64x8(2)), ('p18', int64x8()),
            ('p19', int64x8()), ('p20', int64x8())
        ]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.set_value('p2', int64x2(3, 8))
        kernel.set_value('p7', int64x3(3, 8, 11))
        kernel.set_value('p12', int64x4(3, 8, 11, 18))
        kernel.set_value('p17', int64x8(3, 8, 11, 18, 9, 9, 8, 6))
        kernel.run()

        self._check(kernel.get_value('p3'), int64x2(1, 5), 2)
        self._check(kernel.get_value('p4'), int64x2(3, 8), 2)
        self._check(kernel.get_value('p5'), int64x2(1, 5), 2)
        self._check(kernel.get_value('p8'), int64x3(1, 5, 9), 3)
        self._check(kernel.get_value('p9'), int64x3(3, 8, 11), 3)
        self._check(kernel.get_value('p10'), int64x3(1, 5, 9), 3)
        self._check(kernel.get_value('p13'), int64x4(1, 5, 9, 18), 4)
        self._check(kernel.get_value('p14'), int64x4(3, 8, 11, 18), 4)
        self._check(kernel.get_value('p15'), int64x4(1, 5, 9, 18), 4)
        self._check(kernel.get_value('p18'), int64x8(1, 5, 9, 18, 6, 15, 78, 125), 8)
        self._check(kernel.get_value('p19'), int64x8(3, 8, 11, 18, 9, 9, 8, 6), 8)
        self._check(kernel.get_value('p20'), int64x8(1, 5, 9, 18, 6, 15, 78, 125), 8)

    def test_simple_int64_vec_1(self):
        self._test_simple_int64_vec_1(ISet.AVX512)
        self._test_simple_int64_vec_1(ISet.AVX2)
        self._test_simple_int64_vec_1(ISet.AVX)
        self._test_simple_int64_vec_1(ISet.SSE)

    def _test_simple_int64_vec_2(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
mask1 = p1 > p2
mask2 = p1 == p2
p8 = p1 & p3
p9 = p1 | p3
p10 = p1 ^ p3
mask3 = mask1 | mask2
        """
        args = [('p1', int64x2(2)), ('p2', int64x2(-16)), ('p3', int64x2(8)),
                ('p4', int64x2()), ('p5', int64x2()), ('p6', int64x2()), ('p7', int64x2(88)),
                ('p8', int64x2(88)), ('p9', int64x2(88)), ('p10', int64x2(88))]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), int64x2(-14), 2)
        self._check(kernel.get_value('p5'), int64x2(18), 2)
        self._check(kernel.get_value('p8'), int64x2(0), 2)
        self._check(kernel.get_value('p9'), int64x2(10), 2)
        self._check(kernel.get_value('p10'), int64x2(10), 2)

    def test_simple_int64_vec_2(self):
        self._test_simple_int64_vec_2(ISet.AVX512)
        self._test_simple_int64_vec_2(ISet.AVX)
        self._test_simple_int64_vec_2(ISet.SSE)

    def _test_simple_int64_vec_3(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
mask1 = p1 > p2
mask2 = p1 == p2
p8 = p1 & p3
p9 = p1 | p3
p10 = p1 ^ p3
mask3 = mask1 | mask2

p14 = p11 + p12
p15 = p11 - p12
mask11 = p11 > p12
mask12 = p11 == p12
p18 = p11 & p13
p19 = p11 | p13
p20 = p11 ^ p13
mask13 = mask11 ^ mask12

p24 = p21 + p22
p25 = p21 - p22
mask21 = p21 > p22
mask22 = p21 == p22
p28 = p21 & p23
p29 = p21 | p23
p30 = p21 ^ p23
mask23 = mask21 & mask22

        """
        args = [('p1', int64x3(2, 3, 4)), ('p2', int64x3(-16)), ('p3', int64x3(8)),
                ('p4', int64x3()), ('p5', int64x3()), ('p6', int64x3()), ('p7', int64x3(88)),
                ('p8', int64x3(88)), ('p9', int64x3(88)), ('p10', int64x3(88)),
                ('p11', int64x4(2, 3, 4, 5)), ('p12', int64x4(-16)), ('p13', int64x4(8)),
                ('p14', int64x4()), ('p15', int64x4()), ('p16', int64x4()), ('p17', int64x4(88)),
                ('p18', int64x4(88)), ('p19', int64x4(88)), ('p20', int64x4(88)),
                ('p21', int64x8(2, 3, 4, 5, 5, 4, 3, 2)), ('p22', int64x8(-16)), ('p23', int64x8(8)),
                ('p24', int64x8()), ('p25', int64x8()), ('p26', int64x8()), ('p27', int64x8(88)),
                ('p28', int64x8(88)), ('p29', int64x8(88)), ('p30', int64x8(88))
                ]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p4'), int64x3(-14, -13, -12), 3)
        self._check(kernel.get_value('p5'), int64x3(18, 19, 20), 3)
        self._check(kernel.get_value('p8'), int64x3(0), 3)
        self._check(kernel.get_value('p9'), int64x3(10, 11, 12), 3)
        self._check(kernel.get_value('p10'), int64x3(10, 11, 12), 3)

        self._check(kernel.get_value('p14'), int64x4(-14, -13, -12, -11), 4)
        self._check(kernel.get_value('p15'), int64x4(18, 19, 20, 21), 4)
        self._check(kernel.get_value('p18'), int64x4(0), 4)
        self._check(kernel.get_value('p19'), int64x4(10, 11, 12, 13), 4)
        self._check(kernel.get_value('p20'), int64x4(10, 11, 12, 13), 4)

        self._check(kernel.get_value('p24'), int64x8(-14, -13, -12, -11, -11, -12, -13, -14), 8)
        self._check(kernel.get_value('p25'), int64x8(18, 19, 20, 21, 21, 20, 19, 18), 8)
        self._check(kernel.get_value('p28'), int64x8(0), 8)
        self._check(kernel.get_value('p29'), int64x8(10, 11, 12, 13, 13, 12, 11, 10), 8)
        self._check(kernel.get_value('p30'), int64x8(10, 11, 12, 13, 13, 12, 11, 10), 8)

    def test_simple_int64_vec_3(self):
        self._test_simple_int64_vec_3(ISet.AVX512)
        self._test_simple_int64_vec_3(ISet.AVX2)
        self._test_simple_int64_vec_3(ISet.AVX)
        self._test_simple_int64_vec_3(ISet.SSE)

    def _test_simple_int64_vec4(self, iset):
        source = """
p2 = p1 << 2
p12 = p11 << 2
p22 = p21 << 2
p32 = p31 << 2
        """
        args = [('p1', int64x2(20)), ('p2', int64x2()),
                ('p11', int64x3(20)), ('p12', int64x3()),
                ('p21', int64x4(20)), ('p22', int64x4()),
                ('p31', int64x8(20, 20, 20, 20, 40, 40, 40, 80)), ('p32', int64x8())]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check(kernel.get_value('p2'), int64x2(80), 2)
        self._check(kernel.get_value('p12'), int64x3(80), 3)
        self._check(kernel.get_value('p22'), int64x4(80), 4)
        self._check(kernel.get_value('p32'), int64x8(80, 80, 80, 80, 160, 160, 160, 320), 8)

    def test_simple_int64_vec4(self):
        self._test_simple_int64_vec4(ISet.AVX512)
        self._test_simple_int64_vec4(ISet.AVX2)
        self._test_simple_int64_vec4(ISet.AVX)
        self._test_simple_int64_vec4(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
