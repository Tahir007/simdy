
import unittest
from simdy import float64, Kernel, ISet


class TestsExprF64(unittest.TestCase):

    def _test_expr_float64_1(self, iset, fma):
        source = """
tmp1 = p1 + p2 * p3
tmp2 = p1 * p2 - p3
tmp3 = (p1 + p2) * (p4 / p2)
tmp4 = 5 * p1 / 7
p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
p6 = 1 - tmp4 + tmp1 / tmp2 * tmp1 - 5 + p3
p7 = tmp3 + 1 - tmp2 + p5 * p1 / tmp2 * -4 + p3 - p2 + tmp3
p8 = p1 - tmp4 * 2 - p3 + tmp2 / tmp3 - tmp1 + p1 - p3 * tmp1 * tmp4
        """

        args = [('p1', float64(10)), ('p2', float64(-6)), ('p3', float64(2)), ('p4', float64(-20)),
                ('p5', float64()), ('p6', float64()), ('p7', float64(111)), ('p8', float64()),
                ('p9', float64())
                ]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        p1, p2, p3, p4 = kernel.get_value('p1'), kernel.get_value('p2'), kernel.get_value('p3'), kernel.get_value('p4')
        tmp1 = p1 + p2 * p3
        tmp2 = p1 * p2 - p3
        tmp3 = (p1 + p2) * (p4 / p2)
        tmp4 = 5 * p1 / 7
        p5 = tmp1 + p1 - 3.3 * tmp2 * p1 * p3 - p4
        p6 = 1 - tmp4 + tmp1 / tmp2 * tmp1 - 5 + p3
        p7 = tmp3 + 1 - tmp2 + p5 * p1 / tmp2 * -4 + p3 - p2 + tmp3
        p8 = p1 - tmp4 * 2 - p3 + tmp2 / tmp3 - tmp1 + p1 - p3 * tmp1 * tmp4
        self.assertEqual(kernel.get_value('p5'), p5)
        self.assertEqual(kernel.get_value('p6'), p6)
        self.assertEqual(kernel.get_value('p7'), p7)
        self.assertEqual(kernel.get_value('p8'), p8)

    def test_expr_float64_1(self):
        self._test_expr_float64_1(ISet.AVX512, fma=True)
        self._test_expr_float64_1(ISet.AVX512, fma=False)
        self._test_expr_float64_1(ISet.AVX2, fma=True)
        self._test_expr_float64_1(ISet.AVX2, fma=False)
        self._test_expr_float64_1(ISet.AVX, fma=False)
        self._test_expr_float64_1(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
