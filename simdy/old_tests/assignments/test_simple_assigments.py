
import unittest
from simdy import Kernel, int32, float64


class AssignTests1(unittest.TestCase):

    def test_simple_assign1(self):
        source = """
b = 33
p1 = 99
p2 = b
        """

        args = [('p1', int32()), ('p2', int32())]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 99)
        self.assertEqual(kernel.get_value('p2'), 33)

    def test_simple_assign2(self):
        source = """
p1, p2 = p2, p1
        """

        args = [('p1', int32(4)),
                ('p2', int32(9))]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 9)
        self.assertEqual(kernel.get_value('p2'), 4)

    def test_simple_assign3(self):
        source = """
p1 = 1.1
        """

        args = [('p1', float64(5))]
        kernel = Kernel(source, args=args)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 1.1)

if __name__ == "__main__":
    unittest.main()
