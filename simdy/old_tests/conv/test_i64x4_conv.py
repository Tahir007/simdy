
import unittest
from simdy import Kernel, int64x4, int32x4, int64x2, ISet


class ConvTestI64x4(unittest.TestCase):

    def _check_i64x4(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])

    def _test_conv_i64x4_1(self, iset):
        source = """
p1 = int64x4()
p2 = int64x4(2.2)
p3 = int64x4(4.4, 6, -7, -2)

tmp1 = int64(3)
tmp2 = int64(6)
tmp3 = int64(-4)
tmp4 = int64(-7)

p4 = int64x4(tmp1)
p5 = int64x4(2 * tmp2)

p6 = int64x4(tmp1, tmp2, tmp3, tmp4)
p7 = int64x4(2 * tmp1, 2 * tmp2, tmp3, tmp4)
p8 = int64x4(tmp1, tmp2, 2 * tmp3, tmp4)

p9 = int64x4(p2)
p10 = int64x4(p2 + p2)
p11 = int64x4(p20)
p12 = int64x4(p20 + p21)

p13 = int64x4(p22, p23)
p14 = int64x4(p22 + p23, p22)
        """

        args = [('p1', int64x4(10)), ('p2', int64x4()), ('p3', int64x4()),
                ('p4', int64x4()), ('p5', int64x4()), ('p6', int64x4()), ('p7', int64x4()),
                ('p8', int64x4()), ('p9', int64x4()), ('p10', int64x4()),
                ('p11', int64x4()), ('p12', int64x4()), ('p13', int64x4()), ('p14', int64x4()),
                ('p15', int64x4()), ('p16', int64x4()),
                ('p20', int32x4(5, 8, 4, -9)), ('p21', int32x4(3, -15, 8, 2)),
                ('p22', int64x2(-7, 3)), ('p23', int64x2(4, 8))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_i64x4(kernel.get_value('p1'), int64x4(0))
        self._check_i64x4(kernel.get_value('p2'), int64x4(2))
        self._check_i64x4(kernel.get_value('p3'), int64x4(4, 6, -7, -2))
        self._check_i64x4(kernel.get_value('p4'), int64x4(3))
        self._check_i64x4(kernel.get_value('p5'), int64x4(2 * 6))
        self._check_i64x4(kernel.get_value('p6'), int64x4(3, 6, -4, -7))
        self._check_i64x4(kernel.get_value('p7'), int64x4(2 * 3, 2 * 6, -4, -7))
        self._check_i64x4(kernel.get_value('p8'), int64x4(3, 6, -8, -7))
        self._check_i64x4(kernel.get_value('p9'), int64x4(2))
        self._check_i64x4(kernel.get_value('p10'), int64x4(4))
        self._check_i64x4(kernel.get_value('p11'), int64x4(5, 8, 4, -9))
        self._check_i64x4(kernel.get_value('p12'), int64x4(8, -7, 12, -7))
        self._check_i64x4(kernel.get_value('p13'), int64x4(-7, 3, 4, 8))
        self._check_i64x4(kernel.get_value('p14'), int64x4(-3, 11, -7, 3))

    def test_conv_i64x4_1(self):
        self._test_conv_i64x4_1(ISet.AVX512)
        self._test_conv_i64x4_1(ISet.AVX2)
        self._test_conv_i64x4_1(ISet.AVX)
        self._test_conv_i64x4_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
