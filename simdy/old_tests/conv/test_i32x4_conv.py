
import unittest
from simdy import Kernel, float64x4, int32x4, float32x4, int64x4, int32x2, int32x3, ISet


class ConvTestI32x4(unittest.TestCase):

    def _check_i32x4(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])

    def _test_conv_i32x4_1(self, iset):
        source = """
p1 = int32x4()
p2 = int32x4(2)
p3 = int32x4(4, 6, -3, 3)

tmp1 = int32(3)
tmp2 = int32(6)
tmp3 = int32(-4)
tmp4 = int32(-1)

p4 = int32x4(tmp1)
p5 = int32x4(tmp1 + tmp2)
p6 = int32x4(p3)
p7 = int32x4(p3 + p3)
p8 = int32x4(p20)
p9 = int32x4(2 * p20)
p10 = int32x4(p21)
p11 = int32x4(2 * p21)
p12 = int32x4(p22)
p13 = int32x4(p22 + p23)
p14 = int32x4(tmp1, 2 * tmp2, tmp3, tmp4)
p15 = int32x4(tmp1, tmp2, tmp3, tmp4)
p16 = int32x4(p24, p25)
p17 = int32x4(p24 + p24, p25)
p18 = int32x4(p26, tmp1)
p19 = int32x4(p26 + p26, tmp2)

        """

        args = [('p1', int32x4(10)), ('p2', int32x4()), ('p3', int32x4()),
                ('p4', int32x4()), ('p5', int32x4()), ('p6', int32x4()),
                ('p7', int32x4()), ('p8', int32x4()), ('p9', int32x4()),
                ('p10', int32x4()), ('p11', int32x4()), ('p12', int32x4()),
                ('p13', int32x4()), ('p14', int32x4()), ('p15', int32x4()),
                ('p16', int32x4()), ('p17', int32x4()), ('p18', int32x4()), ('p19', int32x4()),
                ('p20', float32x4(5, 8, -2, 4)), ('p21', float64x4(3, -15, 5, 7)),
                ('p22', int64x4(1, 2, 1, 3)), ('p23', int64x4(4, 1, -5, 6)),
                ('p24', int32x2(-7, 3)), ('p25', int32x2(4, 6)), ('p26', int32x3(-4, 2, 4))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check_i32x4(kernel.get_value('p1'), int32x4(0))
        self._check_i32x4(kernel.get_value('p2'), int32x4(2))
        self._check_i32x4(kernel.get_value('p3'), int32x4(4, 6, -3, 3))
        self._check_i32x4(kernel.get_value('p4'), int32x4(3))
        self._check_i32x4(kernel.get_value('p5'), int32x4(9))
        self._check_i32x4(kernel.get_value('p6'), int32x4(4, 6, -3, 3))
        self._check_i32x4(kernel.get_value('p7'), int32x4(8, 12, -6, 6))
        self._check_i32x4(kernel.get_value('p8'), int32x4(5, 8, -2, 4))
        self._check_i32x4(kernel.get_value('p9'), int32x4(10, 16, -4, 8))
        self._check_i32x4(kernel.get_value('p10'), int32x4(3, -15, 5, 7))
        self._check_i32x4(kernel.get_value('p11'), int32x4(6, -30, 10, 14))
        self._check_i32x4(kernel.get_value('p12'), int32x4(1, 2, 1, 3))
        self._check_i32x4(kernel.get_value('p13'), int32x4(5, 3, -4, 9))
        self._check_i32x4(kernel.get_value('p14'), int32x4(3, 12, -4, -1))
        self._check_i32x4(kernel.get_value('p15'), int32x4(3, 6, -4, -1))
        self._check_i32x4(kernel.get_value('p16'), int32x4(-7, 3, 4, 6))
        self._check_i32x4(kernel.get_value('p17'), int32x4(-14, 6, 4, 6))
        self._check_i32x4(kernel.get_value('p18'), int32x4(-4, 2, 4, 3))
        self._check_i32x4(kernel.get_value('p19'), int32x4(-8, 4, 8, 6))

    def test_conv_i32x4_1(self):
        self._test_conv_i32x4_1(ISet.AVX512)
        self._test_conv_i32x4_1(ISet.AVX2)
        self._test_conv_i32x4_1(ISet.AVX)
        self._test_conv_i32x4_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
