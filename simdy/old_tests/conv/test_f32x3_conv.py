
import unittest
from simdy import Kernel, float64x3, int32x3, float32x3, float32x2, ISet


class ConvTestF32x3(unittest.TestCase):

    def _check_f32x3(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0], places=6)
        self.assertAlmostEqual(src[1], dst[1], places=6)
        self.assertAlmostEqual(src[2], dst[2], places=6)

    def _test_conv_f32x3_1(self, iset):
        source = """
p1 = float32x3()
p2 = float32x3(2.2)
p3 = float32x3(4.4, 6, -7)
tmp1 = float32(3.4)
tmp2 = float32(6.6)
tmp3 = float32(-4.5)
p4 = float32x3(tmp1)
p5 = float32x3(2 * tmp2)
p6 = float32x3(tmp1, tmp2, tmp3)
p7 = float32x3(2 * tmp1, 2 * tmp2, tmp3)
p8 = float32x3(tmp1, tmp2, 2 * tmp3)
p9 = float32x3(p2)
p10 = float32x3(2 * p2)
p11 = float32x3(p20)
p12 = float32x3(p20 + p21)
p13 = float32x3(p22)
p14 = float32x3(2 * p22)
p15 = float32x3(p23, tmp1)
p16 = float32x3(2 * p23, tmp3)
        """

        args = [('p1', float32x3(10)), ('p2', float32x3()), ('p3', float32x3()),
                ('p4', float32x3()), ('p5', float32x3()), ('p6', float32x3()), ('p7', float32x3()),
                ('p8', float32x3()), ('p9', float32x3()), ('p10', float32x3()),
                ('p11', float32x3()), ('p12', float32x3()), ('p13', float32x3()), ('p14', float32x3()),
                ('p15', float32x3()), ('p16', float32x3()),
                ('p20', int32x3(5, 8, 4)), ('p21', int32x3(3, -15, 8)), ('p22', float64x3(1, 2, 3)),
                ('p23', float32x2(4, -9))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_f32x3(kernel.get_value('p1'), float32x3(0.0))
        self._check_f32x3(kernel.get_value('p2'), float32x3(2.2))
        self._check_f32x3(kernel.get_value('p3'), float32x3(4.4, 6, -7))
        self._check_f32x3(kernel.get_value('p4'), float32x3(3.4))
        self._check_f32x3(kernel.get_value('p5'), float32x3(2 * 6.6))
        self._check_f32x3(kernel.get_value('p6'), float32x3(3.4, 6.6, -4.5))
        self._check_f32x3(kernel.get_value('p7'), float32x3(2 * 3.4, 2 * 6.6, -4.5))
        self._check_f32x3(kernel.get_value('p8'), float32x3(3.4, 6.6, -9.0))
        self._check_f32x3(kernel.get_value('p9'), float32x3(2.2))
        self._check_f32x3(kernel.get_value('p10'), float32x3(4.4))
        self._check_f32x3(kernel.get_value('p11'), float32x3(5.0, 8.0, 4))
        self._check_f32x3(kernel.get_value('p12'), float32x3(8.0, -7.0, 12))
        self._check_f32x3(kernel.get_value('p13'), float32x3(1.0, 2.0, 3))
        self._check_f32x3(kernel.get_value('p14'), float32x3(2.0, 4.0, 6))
        self._check_f32x3(kernel.get_value('p15'), float32x3(4.0, -9.0, 3.4))
        self._check_f32x3(kernel.get_value('p16'), float32x3(8.0, -18.0, -4.5))

    def test_conv_f32x3_1(self):
        self._test_conv_f32x3_1(ISet.AVX512)
        self._test_conv_f32x3_1(ISet.AVX)
        self._test_conv_f32x3_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
