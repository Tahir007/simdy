
import unittest
from simdy import Kernel, int64x8, int32x8, int64x4, ISet


class ConvTestI64x8(unittest.TestCase):

    def _check_i64x8(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])
        self.assertEqual(src[4], dst[4])
        self.assertEqual(src[5], dst[5])
        self.assertEqual(src[6], dst[6])
        self.assertEqual(src[7], dst[7])

    def _test_conv_i64x8_1(self, iset):
        source = """
p1 = int64x8()
p2 = int64x8(2.2)
p3 = int64x8(4.4, 6, -7, -2, -4, 3, 4, -7)

tmp1 = int64(3)
tmp2 = int64(6)

p4 = int64x8(tmp1)
p5 = int64x8(2 * tmp2)
p6 = int64x8(p3)
p7 = int64x8(p3 + p3)

p8 = int64x8(p20)
p9 = int64x8(p20 + p21)

p10 = int64x8(p22, p23)
p11 = int64x8(p22 + p23, p22)
        """

        args = [('p1', int64x8(10)), ('p2', int64x8()), ('p3', int64x8()),
                ('p4', int64x8()), ('p5', int64x8()), ('p6', int64x8()), ('p7', int64x8()),
                ('p8', int64x8()), ('p9', int64x8()), ('p10', int64x8()),
                ('p11', int64x8()), ('p12', int64x8()), ('p13', int64x8()), ('p14', int64x8()),
                ('p15', int64x8()), ('p16', int64x8()),
                ('p20', int32x8(5, 8, 4, -9, 7, 4, -6, 4)), ('p21', int32x8(3, -15, 8, 2, -4, -9, 2, 3)),
                ('p22', int64x4(-7, 3, -5, 2)), ('p23', int64x4(4, 8, 7, -8))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_i64x8(kernel.get_value('p1'), int64x8(0))
        self._check_i64x8(kernel.get_value('p2'), int64x8(2))
        self._check_i64x8(kernel.get_value('p3'), int64x8(4, 6, -7, -2, -4, 3, 4, -7))
        self._check_i64x8(kernel.get_value('p4'), int64x8(3))
        self._check_i64x8(kernel.get_value('p5'), int64x8(2 * 6))
        self._check_i64x8(kernel.get_value('p6'), int64x8(4, 6, -7, -2, -4, 3, 4, -7))
        self._check_i64x8(kernel.get_value('p7'), int64x8(8, 12, -14, -4, -8, 6, 8, -14))
        self._check_i64x8(kernel.get_value('p8'), int64x8(5, 8, 4, -9, 7, 4, -6, 4))
        self._check_i64x8(kernel.get_value('p9'), int64x8(8, -7, 12, -7, 3, -5, -4, 7))
        self._check_i64x8(kernel.get_value('p10'), int64x8(-7, 3, -5, 2, 4, 8, 7, -8))
        self._check_i64x8(kernel.get_value('p11'), int64x8(-3, 11, 2, -6, -7, 3, -5, 2))

    def test_conv_i64x8_1(self):
        self._test_conv_i64x8_1(ISet.AVX512)
        self._test_conv_i64x8_1(ISet.AVX2)
        self._test_conv_i64x8_1(ISet.AVX)
        self._test_conv_i64x8_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
