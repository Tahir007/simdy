
import unittest
from simdy import Kernel, float64x8, int32x8, float32x8, float64x4, ISet


class ConvTestF64x8(unittest.TestCase):

    def _check_f64x8(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])
        self.assertEqual(src[4], dst[4])
        self.assertEqual(src[5], dst[5])
        self.assertEqual(src[6], dst[6])
        self.assertEqual(src[7], dst[7])

    def _test_conv_f64x8_1(self, iset):
        source = """
p1 = float64x8()
p2 = float64x8(2.2)
p3 = float64x8(4.4, 6, 2, 3, 7, 8, 9, -1)
tmp1 = 3.4
tmp2 = 6.6
p4 = float64x8(tmp1)
p5 = float64x8(2 * tmp2)
p6 = float64x8(p23, p24)
p7 = float64x8(2 * p23, 2 * p24)
p9 = float64x8(p2)
p10 = float64x8(2 * p2)
p11 = float64x8(p20)
p12 = float64x8(p20 + p21)
p13 = float64x8(p22)
p14 = float64x8(2 * p22)
        """

        args = [('p1', float64x8(10)), ('p2', float64x8()), ('p3', float64x8()),
                ('p4', float64x8()), ('p5', float64x8()), ('p6', float64x8()), ('p7', float64x8()),
                ('p8', float64x8()), ('p9', float64x8()), ('p10', float64x8()),
                ('p11', float64x8()), ('p12', float64x8()), ('p13', float64x8()), ('p14', float64x8()),
                ('p20', int32x8(5, 8, 4, 22, 2, 3, 1, 4)), ('p21', int32x8(3, -15, 8, -7, 1, 1, 8, 56)),
                ('p22', float32x8(1, 2, 3, 4, 5, 6, 7, 8)), ('p23', float64x4(2, 3, 5, -9)), ('p24', float64x4(1, 1, 4, 2))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_f64x8(kernel.get_value('p1'), float64x8(0.0))
        self._check_f64x8(kernel.get_value('p2'), float64x8(2.2))
        self._check_f64x8(kernel.get_value('p3'), float64x8(4.4, 6, 2, 3, 7, 8, 9, -1))
        self._check_f64x8(kernel.get_value('p4'), float64x8(3.4))
        self._check_f64x8(kernel.get_value('p5'), float64x8(2 * 6.6))
        self._check_f64x8(kernel.get_value('p6'), float64x8(2, 3, 5, -9, 1, 1, 4, 2))
        self._check_f64x8(kernel.get_value('p7'), float64x8(4, 6, 10, -18, 2, 2, 8, 4))
        self._check_f64x8(kernel.get_value('p9'), float64x8(2.2))
        self._check_f64x8(kernel.get_value('p10'), float64x8(4.4))
        self._check_f64x8(kernel.get_value('p11'), float64x8(5, 8, 4, 22, 2, 3, 1, 4))
        self._check_f64x8(kernel.get_value('p12'), float64x8(8, -7, 12, 15, 3, 4, 9, 60))
        self._check_f64x8(kernel.get_value('p13'), float64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_f64x8(kernel.get_value('p14'), float64x8(2, 4, 6, 8, 10, 12, 14, 16))

    def test_conv_f64x8_1(self):
        self._test_conv_f64x8_1(ISet.AVX512)
        self._test_conv_f64x8_1(ISet.AVX2)
        self._test_conv_f64x8_1(ISet.AVX)
        self._test_conv_f64x8_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
