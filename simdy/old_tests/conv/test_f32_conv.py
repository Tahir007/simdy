
import unittest
from simdy import Kernel, float32, ISet


class ConvTestF32(unittest.TestCase):

    def _test_conv_f32_1(self, iset):
        source = """
p1 = float32()
t = int64(1234567)
p2 = float32(t)
p3 = float32(5.6)
a = 4.4
p4 = float32(a)
gg = float32(6.6)
p5 = float32(gg)
b = 6
p6 = float32(b - 15)
p7 = float32(a * 2)
m = 34
p8 = float32(m)
        """

        args = [('p1', float32()), ('p2', float32()), ('p3', float32()), ('p4', float32()),
                ('p5', float32()), ('p6', float32()), ('p7', float32()), ('p8', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 0.0)
        self.assertEqual(kernel.get_value('p2'), 1234567.0)
        self.assertAlmostEqual(kernel.get_value('p3'), 5.6, places=6)
        self.assertAlmostEqual(kernel.get_value('p4'), 4.4, places=6)
        self.assertAlmostEqual(kernel.get_value('p5'), 6.6, places=6)
        self.assertEqual(kernel.get_value('p6'), -9.0)
        self.assertAlmostEqual(kernel.get_value('p7'), 8.8, places=6)
        self.assertEqual(kernel.get_value('p8'), 34.0)

    def test_conv_f32_1(self):
        self._test_conv_f32_1(ISet.AVX512)
        self._test_conv_f32_1(ISet.AVX)
        self._test_conv_f32_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
