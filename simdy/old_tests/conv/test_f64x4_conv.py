
import unittest
from simdy import Kernel, float64x4, int32x4, float32x4, float64x2, ISet


class ConvTestF64x4(unittest.TestCase):

    def _check_f64x4(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])

    def _test_conv_f64x4_1(self, iset):
        source = """
p1 = float64x4()
p2 = float64x4(2.2)
p3 = float64x4(4.4, 6, -7, 44)
tmp1 = 3.4
tmp2 = 6.6
tmp3 = -4.5
tmp4 = 1.2
p4 = float64x4(tmp1)
p5 = float64x4(2 * tmp2)
p6 = float64x4(tmp1, tmp2, tmp3, tmp4)
p7 = float64x4(2 * tmp1, 2 * tmp2, tmp3, tmp4)
p8 = float64x4(tmp1, tmp2, 2 * tmp3, 2 * tmp4)
p9 = float64x4(p2)
p10 = float64x4(2 * p2)
p11 = float64x4(p20)
p12 = float64x4(p20 + p21)
p13 = float64x4(p22)
p14 = float64x4(2 * p22)
p15 = float64x4(p23, p24)
p16 = float64x4(2 * p23, p24)
        """

        args = [('p1', float64x4(10)), ('p2', float64x4()), ('p3', float64x4()),
                ('p4', float64x4()), ('p5', float64x4()), ('p6', float64x4()), ('p7', float64x4()),
                ('p8', float64x4()), ('p9', float64x4()), ('p10', float64x4()),
                ('p11', float64x4()), ('p12', float64x4()), ('p13', float64x4()), ('p14', float64x4()),
                ('p15', float64x4()), ('p16', float64x4()),
                ('p20', int32x4(5, 8, 4, 22)), ('p21', int32x4(3, -15, 8, -7)), ('p22', float32x4(1, 2, 3, 4)),
                ('p23', float64x2(5, -3)), ('p24', float64x2(1, 2))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_f64x4(kernel.get_value('p1'), float64x4(0.0))
        self._check_f64x4(kernel.get_value('p2'), float64x4(2.2))
        self._check_f64x4(kernel.get_value('p3'), float64x4(4.4, 6, -7, 44))
        self._check_f64x4(kernel.get_value('p4'), float64x4(3.4))
        self._check_f64x4(kernel.get_value('p5'), float64x4(2 * 6.6))
        self._check_f64x4(kernel.get_value('p6'), float64x4(3.4, 6.6, -4.5, 1.2))
        self._check_f64x4(kernel.get_value('p7'), float64x4(2 * 3.4, 2 * 6.6, -4.5, 1.2))
        self._check_f64x4(kernel.get_value('p8'), float64x4(3.4, 6.6, -9.0, 2.4))
        self._check_f64x4(kernel.get_value('p9'), float64x4(2.2))
        self._check_f64x4(kernel.get_value('p10'), float64x4(4.4))
        self._check_f64x4(kernel.get_value('p11'), float64x4(5.0, 8.0, 4, 22))
        self._check_f64x4(kernel.get_value('p12'), float64x4(8.0, -7.0, 12, 15))
        self._check_f64x4(kernel.get_value('p13'), float64x4(1.0, 2.0, 3, 4))
        self._check_f64x4(kernel.get_value('p14'), float64x4(2.0, 4.0, 6, 8))
        self._check_f64x4(kernel.get_value('p15'), float64x4(5, -3, 1, 2))
        self._check_f64x4(kernel.get_value('p16'), float64x4(10.0, -6.0, 1, 2))

    def test_conv_f64x4_1(self):
        self._test_conv_f64x4_1(ISet.AVX512)
        self._test_conv_f64x4_1(ISet.AVX2)
        self._test_conv_f64x4_1(ISet.AVX)
        self._test_conv_f64x4_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
