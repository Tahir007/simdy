
import unittest
from simdy import Kernel, float64, ISet


class ConvTestF64(unittest.TestCase):

    def _test_conv_f64_1(self, iset):
        source = """
p1 = float64()
t = int64(123456789123)
p2 = float64(t)
p3 = float64(5.6)
a = 4.4
p4 = float64(a)
gg = float32(6.6)
p5 = float64(gg)
b = 6
p6 = float64(b - 15)
p7 = float64(a * 2)
        """

        args = [('p1', float64()), ('p2', float64()), ('p3', float64()), ('p4', float64()),
                ('p5', float64()), ('p6', float64()), ('p7', float64()), ('p8', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 0.0)
        self.assertEqual(kernel.get_value('p2'), 123456789123.0)
        self.assertEqual(kernel.get_value('p3'), 5.6)
        self.assertEqual(kernel.get_value('p4'), 4.4)
        self.assertAlmostEqual(kernel.get_value('p5'), 6.6, places=6)
        self.assertEqual(kernel.get_value('p6'), -9.0)
        self.assertEqual(kernel.get_value('p7'), 8.8)

    def test_conv_f64_1(self):
        self._test_conv_f64_1(ISet.AVX512)
        self._test_conv_f64_1(ISet.AVX)
        self._test_conv_f64_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
