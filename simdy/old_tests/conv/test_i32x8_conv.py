
import unittest
from simdy import Kernel, float64x8, int32x4, float32x8, int64x8, int32x8, ISet


class ConvTestI32x8(unittest.TestCase):

    def _check_i32x8(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])
        self.assertEqual(src[4], dst[4])
        self.assertEqual(src[5], dst[5])
        self.assertEqual(src[6], dst[6])
        self.assertEqual(src[7], dst[7])

    def _test_conv_i32x8_1(self, iset):
        source = """
p1 = int32x8()
p2 = int32x8(2)
p3 = int32x8(4, 6, -3, 3, 1, 2, -4, 3)

tmp1 = int32(3)
tmp2 = int32(6)

p4 = int32x8(p3)
p5 = int32x8(p3 + p3)
p6 = int32x8(tmp1)
p7 = int32x8(tmp1 + tmp2)
p8 = int32x8(p20)
p9 = int32x8(2 * p20)
p10 = int32x8(p21)
p11 = int32x8(2 * p21)
p12 = int32x8(p22)
p13 = int32x8(p22 + p23)
p14 = int32x8(p24, p25)
p15 = int32x8(p24 + p24, p25)
        """

        args = [('p1', int32x8(10)), ('p2', int32x8()), ('p3', int32x8()),
                ('p4', int32x8()), ('p5', int32x8()), ('p6', int32x8()),
                ('p7', int32x8()), ('p8', int32x8()), ('p9', int32x8()),
                ('p10', int32x8()), ('p11', int32x8()), ('p12', int32x8()),
                ('p13', int32x8()), ('p14', int32x8()), ('p15', int32x8()),
                ('p20', float32x8(5, 8, -2, 4, 1, 7, -4, 3)),
                ('p21', float64x8(3, -15, 5, 7, 2, 3, 1, 9)),
                ('p22', int64x8(1, 2, 1, 3, 2, 3, 1, 1)),
                ('p23', int64x8(4, 1, -5, 6, 3, 1, 9, -6)),
                ('p24', int32x4(-7, 3, 2, 3)), ('p25', int32x4(4, 6, 1, 2))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check_i32x8(kernel.get_value('p1'), int32x8(0))
        self._check_i32x8(kernel.get_value('p2'), int32x8(2))
        self._check_i32x8(kernel.get_value('p3'), int32x8(4, 6, -3, 3, 1, 2, -4, 3))
        self._check_i32x8(kernel.get_value('p4'), int32x8(4, 6, -3, 3, 1, 2, -4, 3))
        self._check_i32x8(kernel.get_value('p5'), int32x8(8, 12, -6, 6, 2, 4, -8, 6))
        self._check_i32x8(kernel.get_value('p6'), int32x8(3))
        self._check_i32x8(kernel.get_value('p7'), int32x8(9))
        self._check_i32x8(kernel.get_value('p8'), int32x8(5, 8, -2, 4, 1, 7, -4, 3))
        self._check_i32x8(kernel.get_value('p9'), int32x8(10, 16, -4, 8, 2, 14, -8, 6))
        self._check_i32x8(kernel.get_value('p10'), int32x8(3, -15, 5, 7, 2, 3, 1, 9))
        self._check_i32x8(kernel.get_value('p11'), int32x8(6, -30, 10, 14, 4, 6, 2, 18))
        self._check_i32x8(kernel.get_value('p12'), int32x8(1, 2, 1, 3, 2, 3, 1, 1))
        self._check_i32x8(kernel.get_value('p13'), int32x8(5, 3, -4, 9, 5, 4, 10, -5))
        self._check_i32x8(kernel.get_value('p14'), int32x8(-7, 3, 2, 3, 4, 6, 1, 2))
        self._check_i32x8(kernel.get_value('p15'), int32x8(-14, 6, 4, 6, 4, 6, 1, 2))

    def test_conv_i32x8_1(self):
        self._test_conv_i32x8_1(ISet.AVX512)
        self._test_conv_i32x8_1(ISet.AVX2)
        self._test_conv_i32x8_1(ISet.AVX)
        self._test_conv_i32x8_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
