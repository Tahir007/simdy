
import unittest
from simdy import Kernel, int64, ISet


class ConvTestI64(unittest.TestCase):

    def _test_conv_int64_1(self, iset):
        source = """
p1 = int64()
p2 = int64(5)
p3 = int64(5.6)
a = 4.4
p4 = int64(a)
p5 = int64(float32(6.6))
p6 = int64(6 - 15)
p7 = int64(a * 2)
        """

        args = [('p1', int64()), ('p2', int64()), ('p3', int64()), ('p4', int64()),
                ('p5', int64()), ('p6', int64()), ('p7', int64()), ('p8', int64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 0)
        self.assertEqual(kernel.get_value('p2'), 5)
        self.assertEqual(kernel.get_value('p3'), 5)
        self.assertEqual(kernel.get_value('p4'), 4)
        self.assertEqual(kernel.get_value('p5'), 6)
        self.assertEqual(kernel.get_value('p6'), -9)
        self.assertEqual(kernel.get_value('p7'), 8)

    def test_conv_int64_1(self):
        self._test_conv_int64_1(ISet.AVX512)
        self._test_conv_int64_1(ISet.AVX)
        self._test_conv_int64_1(ISet.SSE)

if __name__ == "__main__":
    unittest.main()
