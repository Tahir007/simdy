
import unittest
from simdy import Kernel, float64x2, int32x2, float32x2, ISet


class ConvTestF64x2(unittest.TestCase):

    def _check_f64x2(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])

    def _test_conv_f64x2_1(self, iset):
        source = """
p1 = float64x2()
p2 = float64x2(2.2)
p3 = float64x2(4.4, 6)
tmp1 = 3.4
tmp2 = 6.6
p4 = float64x2(tmp1)
p5 = float64x2(2 * tmp2)
p6 = float64x2(tmp1, tmp2)
p7 = float64x2(2 * tmp1, 2 * tmp2)
p8 = float64x2(4 * tmp1, tmp2)
p9 = float64x2(p2)
p10 = float64x2(2 * p2)
p11 = float64x2(p20)
p12 = float64x2(p20 + p21)
p13 = float64x2(p22)
p14 = float64x2(2 * p22)
        """

        args = [('p1', float64x2(10)), ('p2', float64x2()), ('p3', float64x2()),
                ('p4', float64x2()), ('p5', float64x2()), ('p6', float64x2()), ('p7', float64x2()),
                ('p8', float64x2()), ('p9', float64x2()), ('p10', float64x2()),
                ('p11', float64x2()), ('p12', float64x2()), ('p13', float64x2()), ('p14', float64x2()),
                ('p20', int32x2(5, 8)), ('p21', int32x2(3, -15)), ('p22', float32x2(1, 2))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_f64x2(kernel.get_value('p1'), float64x2(0.0))
        self._check_f64x2(kernel.get_value('p2'), float64x2(2.2))
        self._check_f64x2(kernel.get_value('p3'), float64x2(4.4, 6))
        self._check_f64x2(kernel.get_value('p4'), float64x2(3.4))
        self._check_f64x2(kernel.get_value('p5'), float64x2(2 * 6.6))
        self._check_f64x2(kernel.get_value('p6'), float64x2(3.4, 6.6))
        self._check_f64x2(kernel.get_value('p7'), float64x2(2 * 3.4, 2 * 6.6))
        self._check_f64x2(kernel.get_value('p8'), float64x2(4 * 3.4, 6.6))
        self._check_f64x2(kernel.get_value('p9'), float64x2(2.2))
        self._check_f64x2(kernel.get_value('p10'), float64x2(4.4))
        self._check_f64x2(kernel.get_value('p11'), float64x2(5.0, 8.0))
        self._check_f64x2(kernel.get_value('p12'), float64x2(8.0, -7.0))
        self._check_f64x2(kernel.get_value('p13'), float64x2(1.0, 2.0))
        self._check_f64x2(kernel.get_value('p14'), float64x2(2.0, 4.0))

    def test_conv_f64x2_1(self):
        self._test_conv_f64x2_1(ISet.AVX512)
        self._test_conv_f64x2_1(ISet.AVX)
        self._test_conv_f64x2_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
