
import unittest
from simdy import Kernel, int64x3, int32x3, ISet


class ConvTestI64x3(unittest.TestCase):

    def _check_i64x3(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])

    def _test_conv_i64x3_1(self, iset):
        source = """
p1 = int64x3()
p2 = int64x3(2.2)
p3 = int64x3(4.4, 6, -7)

tmp1 = int64(3)
tmp2 = int64(6)
tmp3 = int64(-4)

p4 = int64x3(tmp1)
p5 = int64x3(2 * tmp2)

p6 = int64x3(tmp1, tmp2, tmp3)
p7 = int64x3(2 * tmp1, 2 * tmp2, tmp3)
p8 = int64x3(tmp1, tmp2, 2 * tmp3)

p9 = int64x3(p2)
p10 = int64x3(p2 + p2)
p11 = int64x3(p20)
p12 = int64x3(p20 + p21)

#p15 = int64x3(p23, tmp1)
#p16 = int64x3(2 * p23, tmp3)
        """

        args = [('p1', int64x3(10)), ('p2', int64x3()), ('p3', int64x3()),
                ('p4', int64x3()), ('p5', int64x3()), ('p6', int64x3()), ('p7', int64x3()),
                ('p8', int64x3()), ('p9', int64x3()), ('p10', int64x3()),
                ('p11', int64x3()), ('p12', int64x3()), ('p13', int64x3()), ('p14', int64x3()),
                ('p15', int64x3()), ('p16', int64x3()),
                ('p20', int32x3(5, 8, 4)), ('p21', int32x3(3, -15, 8))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_i64x3(kernel.get_value('p1'), int64x3(0))
        self._check_i64x3(kernel.get_value('p2'), int64x3(2))
        self._check_i64x3(kernel.get_value('p3'), int64x3(4, 6, -7))
        self._check_i64x3(kernel.get_value('p4'), int64x3(3))
        self._check_i64x3(kernel.get_value('p5'), int64x3(2 * 6))
        self._check_i64x3(kernel.get_value('p6'), int64x3(3, 6, -4))
        self._check_i64x3(kernel.get_value('p7'), int64x3(2 * 3, 2 * 6, -4))
        self._check_i64x3(kernel.get_value('p8'), int64x3(3, 6, -8))
        self._check_i64x3(kernel.get_value('p9'), int64x3(2))
        self._check_i64x3(kernel.get_value('p10'), int64x3(4))
        self._check_i64x3(kernel.get_value('p11'), int64x3(5, 8, 4))
        self._check_i64x3(kernel.get_value('p12'), int64x3(8, -7, 12))
        # self._check_f64x3(kernel.get_value('p13'), float64x3(1.0, 2.0, 3))
        # self._check_f64x3(kernel.get_value('p14'), float64x3(2.0, 4.0, 6))
        # self._check_f64x3(kernel.get_value('p15'), float64x3(4.0, -9.0, 3.4))
        # self._check_f64x3(kernel.get_value('p16'), float64x3(8.0, -18.0, -4.5))

    def test_conv_i64x3_1(self):
        self._test_conv_i64x3_1(ISet.AVX512)
        self._test_conv_i64x3_1(ISet.AVX2)
        self._test_conv_i64x3_1(ISet.AVX)
        self._test_conv_i64x3_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
