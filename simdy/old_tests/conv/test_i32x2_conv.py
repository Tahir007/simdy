
import unittest
from simdy import Kernel, float64x2, int32x2, float32x2, int64x2, ISet


class ConvTestI32x2(unittest.TestCase):

    def _check_i32x2(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])

    def _test_conv_i32x2_1(self, iset):
        source = """
p1 = int32x2()
p2 = int32x2(2)
p3 = int32x2(4, 6)
tmp1 = int32(3)
tmp2 = int32(6)

p4 = int32x2(p22)
p5 = int32x2(p22 - p23)
p6 = int32x2(tmp1)
p7 = int32x2(2 * tmp2)
p8 = int32x2(p3)
p9 = int32x2(p3 + p3)
p10 = int32x2(p20)
p11 = int32x2(2 * p20)
p12 = int32x2(p21)
p13 = int32x2(2 * p21)
p14 = int32x2(tmp1, tmp2)
p15 = int32x2(2 * tmp1, tmp2)

        """

        args = [('p1', int32x2(10)), ('p2', int32x2()), ('p3', int32x2()),
                ('p4', int32x2()), ('p5', int32x2()), ('p6', int32x2()),
                ('p7', int32x2()), ('p8', int32x2()), ('p9', int32x2()),
                ('p10', int32x2()), ('p11', int32x2()), ('p12', int32x2()),
                ('p13', int32x2()), ('p14', int32x2()), ('p15', int32x2()),
                ('p20', float32x2(5, 8)), ('p21', float64x2(3, -15)), ('p22', int64x2(1, 2)), ('p23', int64x2(4, 1))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check_i32x2(kernel.get_value('p1'), int32x2(0))
        self._check_i32x2(kernel.get_value('p2'), int32x2(2))
        self._check_i32x2(kernel.get_value('p3'), int32x2(4, 6))
        self._check_i32x2(kernel.get_value('p4'), int32x2(1, 2))
        self._check_i32x2(kernel.get_value('p5'), int32x2(-3, 1))
        self._check_i32x2(kernel.get_value('p6'), int32x2(3, 3))
        self._check_i32x2(kernel.get_value('p7'), int32x2(12, 12))
        self._check_i32x2(kernel.get_value('p8'), int32x2(4, 6))
        self._check_i32x2(kernel.get_value('p9'), int32x2(8, 12))
        self._check_i32x2(kernel.get_value('p10'), int32x2(5, 8))
        self._check_i32x2(kernel.get_value('p11'), int32x2(10, 16))
        self._check_i32x2(kernel.get_value('p12'), int32x2(3, -15))
        self._check_i32x2(kernel.get_value('p13'), int32x2(6, -30))
        self._check_i32x2(kernel.get_value('p14'), int32x2(3, 6))
        self._check_i32x2(kernel.get_value('p15'), int32x2(6, 6))

    def test_conv_i32x2_1(self):
        self._test_conv_i32x2_1(ISet.AVX512)
        self._test_conv_i32x2_1(ISet.AVX)
        self._test_conv_i32x2_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
