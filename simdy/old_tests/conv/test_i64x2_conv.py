
import unittest
from simdy import Kernel, int32x2, int64x2, ISet


class ConvTestI64x2(unittest.TestCase):

    def _check_i64x2(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])

    def _test_conv_i64x2_1(self, iset):
        source = """
p1 = int64x2()
p2 = int64x2(2)
p3 = int64x2(4.4, 6)
tmp1 = int64(77)
tmp2 = int64(-45)
p4 = int64x2(tmp1)
p5 = int64x2(2 * tmp1)
p6 = int64x2(tmp1, tmp2)
p7 = int64x2(2 * tmp1, 2 * tmp2)
p8 = int64x2(p2)
p9 = int64x2(p2 + p2)
p10 = int64x2(p20)
p11 = int64x2(p20 + p21)
        """

        args = [('p1', int64x2(10)), ('p2', int64x2()), ('p3', int64x2()),
                ('p4', int64x2()), ('p5', int64x2()), ('p6', int64x2()), ('p7', int64x2()),
                ('p8', int64x2()), ('p9', int64x2()), ('p10', int64x2()),
                ('p11', int64x2()), ('p12', int64x2()), ('p13', int64x2()), ('p14', int64x2()),
                ('p20', int32x2(5, -8)), ('p21', int32x2(2, 3))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_i64x2(kernel.get_value('p1'), int64x2(0))
        self._check_i64x2(kernel.get_value('p2'), int64x2(2))
        self._check_i64x2(kernel.get_value('p3'), int64x2(4, 6))
        self._check_i64x2(kernel.get_value('p4'), int64x2(77))
        self._check_i64x2(kernel.get_value('p5'), int64x2(154))
        self._check_i64x2(kernel.get_value('p6'), int64x2(77, -45))
        self._check_i64x2(kernel.get_value('p7'), int64x2(154, -90))
        self._check_i64x2(kernel.get_value('p8'), int64x2(2))
        self._check_i64x2(kernel.get_value('p9'), int64x2(4))
        self._check_i64x2(kernel.get_value('p10'), int64x2(5, -8))
        self._check_i64x2(kernel.get_value('p11'), int64x2(7, -5))

    def test_conv_i64x2_1(self):
        self._test_conv_i64x2_1(ISet.AVX512)
        self._test_conv_i64x2_1(ISet.AVX)
        self._test_conv_i64x2_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
