
import unittest
from simdy import Kernel, int32x8, float32x16, int32x16, ISet


class ConvTestI32x16(unittest.TestCase):

    def _check_i32x16(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])
        self.assertEqual(src[3], dst[3])
        self.assertEqual(src[4], dst[4])
        self.assertEqual(src[5], dst[5])
        self.assertEqual(src[6], dst[6])
        self.assertEqual(src[7], dst[7])
        self.assertEqual(src[8], dst[8])
        self.assertEqual(src[9], dst[9])
        self.assertEqual(src[10], dst[10])
        self.assertEqual(src[11], dst[11])
        self.assertEqual(src[12], dst[12])
        self.assertEqual(src[13], dst[13])
        self.assertEqual(src[14], dst[14])
        self.assertEqual(src[15], dst[15])

    def _test_conv_i32x16_1(self, iset):
        source = """
p1 = int32x16()
p2 = int32x16(2)
p3 = int32x16(4, 6, -3, 3, 1, 2, -4, 3, 1, 2, 3, 4, -5, 6, 7, 8)

tmp1 = int32(3)
tmp2 = int32(6)

p4 = int32x16(p3)
p5 = int32x16(p3 + p3)
p6 = int32x16(tmp1)
p7 = int32x16(tmp1 + tmp2)
p8 = int32x16(p20)
p9 = int32x16(2 * p20)
p10 = int32x16(p21, p22)
p11 = int32x16(p21 + p22, p22)
        """

        args = [('p1', int32x16(10)), ('p2', int32x16()), ('p3', int32x16()),
                ('p4', int32x16()), ('p5', int32x16()), ('p6', int32x16()),
                ('p7', int32x16()), ('p8', int32x16()), ('p9', int32x16()),
                ('p10', int32x16()), ('p11', int32x16()), ('p12', int32x16()),
                ('p13', int32x16()), ('p14', int32x16()), ('p15', int32x16()),
                ('p20', float32x16(5, 8, -2, 4, 1, 7, -4, 3, 3, 3, 4, 4, 2, 2, 1, 5)),
                ('p21', int32x8(-7, 3, 2, 3, 1, 1, 2, 2)),
                ('p22', int32x8(4, 6, 1, 2, 2, 2, 5, 5))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check_i32x16(kernel.get_value('p1'), int32x16(0))
        self._check_i32x16(kernel.get_value('p2'), int32x16(2))
        self._check_i32x16(kernel.get_value('p3'), int32x16(4, 6, -3, 3, 1, 2, -4, 3, 1, 2, 3, 4, -5, 6, 7, 8))
        self._check_i32x16(kernel.get_value('p4'), int32x16(4, 6, -3, 3, 1, 2, -4, 3, 1, 2, 3, 4, -5, 6, 7, 8))
        self._check_i32x16(kernel.get_value('p5'), int32x16(8, 12, -6, 6, 2, 4, -8, 6, 2, 4, 6, 8, -10, 12, 14, 16))
        self._check_i32x16(kernel.get_value('p6'), int32x16(3))
        self._check_i32x16(kernel.get_value('p7'), int32x16(9))
        self._check_i32x16(kernel.get_value('p8'), int32x16(5, 8, -2, 4, 1, 7, -4, 3, 3, 3, 4, 4, 2, 2, 1, 5))
        self._check_i32x16(kernel.get_value('p9'), int32x16(10, 16, -4, 8, 2, 14, -8, 6, 6, 6, 8, 8, 4, 4, 2, 10))
        self._check_i32x16(kernel.get_value('p10'), int32x16(-7, 3, 2, 3, 1, 1, 2, 2, 4, 6, 1, 2, 2, 2, 5, 5))
        self._check_i32x16(kernel.get_value('p11'), int32x16(-3, 9, 3, 5, 3, 3, 7, 7, 4, 6, 1, 2, 2, 2, 5, 5))

    def test_conv_i32x16_1(self):
        self._test_conv_i32x16_1(ISet.AVX512)
        self._test_conv_i32x16_1(ISet.AVX2)
        self._test_conv_i32x16_1(ISet.AVX)
        self._test_conv_i32x16_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
