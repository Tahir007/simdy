
import unittest
from simdy import Kernel, int32x16, float32x8, float32x16, ISet


class ConvTestF32x16(unittest.TestCase):

    def _check_f32x16(self, src, dst):
        self.assertAlmostEqual(src[0], dst[0], places=6)
        self.assertAlmostEqual(src[1], dst[1], places=6)
        self.assertAlmostEqual(src[2], dst[2], places=6)
        self.assertAlmostEqual(src[3], dst[3], places=6)
        self.assertAlmostEqual(src[4], dst[4], places=6)
        self.assertAlmostEqual(src[5], dst[5], places=6)
        self.assertAlmostEqual(src[6], dst[6], places=6)
        self.assertAlmostEqual(src[7], dst[7], places=6)
        self.assertAlmostEqual(src[8], dst[8], places=6)
        self.assertAlmostEqual(src[9], dst[9], places=6)
        self.assertAlmostEqual(src[10], dst[10], places=6)
        self.assertAlmostEqual(src[11], dst[11], places=6)
        self.assertAlmostEqual(src[12], dst[12], places=6)
        self.assertAlmostEqual(src[13], dst[13], places=6)
        self.assertAlmostEqual(src[14], dst[14], places=6)
        self.assertAlmostEqual(src[15], dst[15], places=6)

    def _test_conv_f32x16_1(self, iset):
        source = """
p1 = float32x16()
p2 = float32x16(2.2)
p3 = float32x16(4.4, 6, 2, 3, 7, 8, 9, -1, 1, 2, 3, 4, 5, 6, 7, 8)
tmp1 = float32(3.4)
tmp2 = float32(6.6)
p4 = float32x16(tmp1)
p5 = float32x16(2 * tmp2)
p6 = float32x16(p23, p24)
p7 = float32x16(2 * p23, 2 * p24)
p9 = float32x16(p2)
p10 = float32x16(2 * p2)
p11 = float32x16(p20)
p12 = float32x16(p20 + p21)
        """

        args = [('p1', float32x16(10)), ('p2', float32x16()), ('p3', float32x16()),
                ('p4', float32x16()), ('p5', float32x16()), ('p6', float32x16()), ('p7', float32x16()),
                ('p8', float32x16()), ('p9', float32x16()), ('p10', float32x16()),
                ('p11', float32x16()), ('p12', float32x16()), ('p13', float32x16()), ('p14', float32x16()),
                ('p20', int32x16(5, 8, 4, 22, 2, 3, 1, 4, 3, 3, 2, 2, 6, 6, 7, 7)),
                ('p21', int32x16(3, -15, 8, -7, 1, 1, 8, 56, 1, 2, 3, 4, 5, 6, 7, 8)),
                ('p23', float32x8(2, 3, 5, -9, 2, 3, 4, 5)), ('p24', float32x8(1, 1, 4, 2, 1, 2, 3, 4))]

        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self._check_f32x16(kernel.get_value('p1'), float32x16(0.0))
        self._check_f32x16(kernel.get_value('p2'), float32x16(2.2))
        self._check_f32x16(kernel.get_value('p3'), float32x16(4.4, 6, 2, 3, 7, 8, 9, -1, 1, 2, 3, 4, 5, 6, 7, 8))
        self._check_f32x16(kernel.get_value('p4'), float32x16(3.4))
        self._check_f32x16(kernel.get_value('p5'), float32x16(2 * 6.6))
        self._check_f32x16(kernel.get_value('p6'), float32x16(2, 3, 5, -9, 2, 3, 4, 5, 1, 1, 4, 2, 1, 2, 3, 4))
        self._check_f32x16(kernel.get_value('p7'), float32x16(4, 6, 10, -18, 4, 6, 8, 10, 2, 2, 8, 4, 2, 4, 6, 8))
        self._check_f32x16(kernel.get_value('p9'), float32x16(2.2))
        self._check_f32x16(kernel.get_value('p10'), float32x16(4.4))
        self._check_f32x16(kernel.get_value('p11'), float32x16(5, 8, 4, 22, 2, 3, 1, 4, 3, 3, 2, 2, 6, 6, 7, 7))
        self._check_f32x16(kernel.get_value('p12'), float32x16(8, -7, 12, 15, 3, 4, 9, 60, 4, 5, 5, 6, 11, 12, 14, 15))

    def test_conv_f32x16_1(self):
        self._test_conv_f32x16_1(ISet.AVX512)
        self._test_conv_f32x16_1(ISet.AVX2)
        self._test_conv_f32x16_1(ISet.AVX)
        self._test_conv_f32x16_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
