
import unittest
from simdy import Kernel, float64x3, int32x3, float32x3, int64x3, int32x2, ISet


class ConvTestI32x3(unittest.TestCase):

    def _check_i32x3(self, src, dst):
        self.assertEqual(src[0], dst[0])
        self.assertEqual(src[1], dst[1])
        self.assertEqual(src[2], dst[2])

    def _test_conv_i32x3_1(self, iset):
        source = """
p1 = int32x3()
p2 = int32x3(2)
p3 = int32x3(4, 6, -3)
tmp1 = int32(3)
tmp2 = int32(6)
tmp3 = int32(-4)

p4 = int32x3(tmp1)
p5 = int32x3(tmp1 + tmp2)
p6 = int32x3(p3)
p7 = int32x3(p3 + p3)
p8 = int32x3(p20)
p9 = int32x3(2 * p20)
p10 = int32x3(p21)
p11 = int32x3(2 * p21)
p12 = int32x3(p22)
p13 = int32x3(p22 + p23)
p14 = int32x3(tmp1, 2 * tmp2, tmp3)
p15 = int32x3(tmp1, tmp2, tmp3)
p16 = int32x3(p24, tmp1)
p17 = int32x3(p24 + p24, tmp2)
        """

        args = [('p1', int32x3(10)), ('p2', int32x3()), ('p3', int32x3()),
                ('p4', int32x3()), ('p5', int32x3()), ('p6', int32x3()),
                ('p7', int32x3()), ('p8', int32x3()), ('p9', int32x3()),
                ('p10', int32x3()), ('p11', int32x3()), ('p12', int32x3()),
                ('p13', int32x3()), ('p14', int32x3()), ('p15', int32x3()),
                ('p16', int32x3()), ('p17', int32x3()), ('p18', int32x3()),
                ('p20', float32x3(5, 8, -2)), ('p21', float64x3(3, -15, 5)),
                ('p22', int64x3(1, -2, -1)), ('p23', int64x3(4, 1, 5)),
                ('p24', int32x2(-7, 8))]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()

        self._check_i32x3(kernel.get_value('p1'), int32x3(0))
        self._check_i32x3(kernel.get_value('p2'), int32x3(2))
        self._check_i32x3(kernel.get_value('p3'), int32x3(4, 6, -3))
        self._check_i32x3(kernel.get_value('p4'), int32x3(3))
        self._check_i32x3(kernel.get_value('p5'), int32x3(9))
        self._check_i32x3(kernel.get_value('p6'), int32x3(4, 6, -3))
        self._check_i32x3(kernel.get_value('p7'), int32x3(8, 12, -6))
        self._check_i32x3(kernel.get_value('p8'), int32x3(5, 8, -2))
        self._check_i32x3(kernel.get_value('p9'), int32x3(10, 16, -4))
        self._check_i32x3(kernel.get_value('p10'), int32x3(3, -15, 5))
        self._check_i32x3(kernel.get_value('p11'), int32x3(6, -30, 10))
        self._check_i32x3(kernel.get_value('p12'), int32x3(1, -2, -1))
        self._check_i32x3(kernel.get_value('p13'), int32x3(5, -1, 4))
        self._check_i32x3(kernel.get_value('p14'), int32x3(3, 12, -4))
        self._check_i32x3(kernel.get_value('p15'), int32x3(3, 6, -4))
        self._check_i32x3(kernel.get_value('p16'), int32x3(-7, 8, 3))
        self._check_i32x3(kernel.get_value('p17'), int32x3(-14, 16, 6))

    def test_conv_i32x3_1(self):
        self._test_conv_i32x3_1(ISet.AVX512)
        self._test_conv_i32x3_1(ISet.AVX2)
        self._test_conv_i32x3_1(ISet.AVX)
        self._test_conv_i32x3_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
