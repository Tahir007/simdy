
import unittest
from simdy import float32, Kernel, ISet


class TestsSimpleFloat64(unittest.TestCase):

    def _test_simple_float32_1(self, iset):
        source = """
p3 = p1
p4 = p2
tmp2 = p1
p7 = tmp2
        """

        args = [('p1', float32(10)), ('p2', float32()), ('p3', float32()), ('p4', float32()),
                ('p5', float32()), ('p6', float32()), ('p7', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.set_value('p2', -69)
        kernel.run()
        self.assertEqual(kernel.get_value('p3'), 10)
        self.assertEqual(kernel.get_value('p4'), -69)
        self.assertEqual(kernel.get_value('p7'), 10)

    def test_simple_float32_1(self):
        self._test_simple_float32_1(ISet.AVX512)
        self._test_simple_float32_1(ISet.AVX)
        self._test_simple_float32_1(ISet.SSE)

    def _test_simple_float32_2(self, iset):
        source = """
p4 = p1 + p2
p5 = p1 - p2
tmp = p1 * p2
p6 = tmp
p7 = p3 / p1

p8 = 2 + p1
p9 = p1 * 3
p10 = -3.0 - p2
p11 = p1 / 4
        """
        args = [('p1', float32(2)), ('p2', float32(-16)), ('p3', float32(87)),
                ('p4', float32()), ('p5', float32()), ('p6', float32()), ('p7', float32()),
                ('p8', float32()), ('p9', float32()), ('p10', float32()), ('p11', float32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), -14.0)
        self.assertEqual(kernel.get_value('p5'), 18)
        self.assertEqual(kernel.get_value('p6'), -32)
        self.assertEqual(kernel.get_value('p7'), 43.5)
        self.assertEqual(kernel.get_value('p8'), 4)
        self.assertEqual(kernel.get_value('p9'), 6)
        self.assertEqual(kernel.get_value('p10'), 13)
        self.assertEqual(kernel.get_value('p11'), 0.5)

    def test_simple_float32_2(self):
        self._test_simple_float32_2(ISet.AVX512)
        self._test_simple_float32_2(ISet.AVX)
        self._test_simple_float32_2(ISet.SSE)

    def _test_simple_float32_3(self, iset, fma):
        source = """
p4 = p1 * p2 + p3
p5 = p1 * p2 - p3
p6 = p3 + p1 * p2
p7 = p3 - p1 * p2
        """
        args = [('p1', float32(2)), ('p2', float32(3)), ('p3', float32(4)),
                ('p4', float32()), ('p5', float32()), ('p6', float32()), ('p7', float32()),
                ('p8', float32()), ('p9', float32()), ('p10', float32()), ('p11', float32())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), 10)
        self.assertEqual(kernel.get_value('p5'), 2)
        self.assertEqual(kernel.get_value('p6'), 10)
        self.assertEqual(kernel.get_value('p7'), -2)

    def test_simple_float32_3(self):
        self._test_simple_float32_3(ISet.AVX512, fma=False)
        self._test_simple_float32_3(ISet.AVX512, fma=True)
        self._test_simple_float32_3(ISet.AVX2, fma=False)
        self._test_simple_float32_3(ISet.AVX2, fma=True)
        self._test_simple_float32_3(ISet.AVX, fma=False)
        self._test_simple_float32_3(ISet.SSE, fma=False)

    def _test_simple_float32_4(self, iset, fma):
        source = """
p4 = 3 * p2 + p3
p5 = p1 * p2 + 5
p6 = 10 + p1 * 4
p7 = p3 - 9 * p2
p8 = 9 - 9 * p2
p9 = -9 + p1 * p3
        """
        args = [('p1', float32(2)), ('p2', float32(3)), ('p3', float32(4)),
                ('p4', float32()), ('p5', float32()), ('p6', float32()), ('p7', float32()),
                ('p8', float32()), ('p9', float32()), ('p10', float32())]
        kernel = Kernel(source, args=args, iset=iset, fma=fma)
        kernel.run()
        self.assertEqual(kernel.get_value('p4'), 13)
        self.assertEqual(kernel.get_value('p5'), 11)
        self.assertEqual(kernel.get_value('p6'), 18)
        self.assertEqual(kernel.get_value('p7'), -23)
        self.assertEqual(kernel.get_value('p8'), -18)
        self.assertEqual(kernel.get_value('p9'), -1)

    def test_simple_float32_4(self):
        self._test_simple_float32_4(ISet.AVX512, fma=True)
        self._test_simple_float32_4(ISet.AVX512, fma=False)
        self._test_simple_float32_4(ISet.AVX2, fma=True)
        self._test_simple_float32_4(ISet.AVX2, fma=False)
        self._test_simple_float32_4(ISet.AVX, fma=False)
        self._test_simple_float32_4(ISet.SSE, fma=False)


if __name__ == "__main__":
    unittest.main()
