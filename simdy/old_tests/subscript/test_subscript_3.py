
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, float32, float64, int64x2, int64x3, int64x4, float64x3, float64x4,\
    float64x8, float32x2, float32x3, array, struct, register_user_type, ISet, float32x4


class TestSubscript3(unittest.TestCase):

    def _test_subscript_1(self, iset):
        source = """
val1 = float32x4(3, 4, 1, -3)
val1[2] = val1[1]
val1[3] = float32(5)
p1 = val1[2]
p2 = val1[3]

val2 = float32x8(1, 2, 3, 4, 5, 6, 7, 8)
val2[1] = val2[3]
val2[5] = val2[7]
p3 = val2[1]
p4 = val2[5]

val3 = float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
val3[2] = val3[3]
val3[4] = val3[6]
val3[9] = val3[11]
val3[12] = val3[14]
p5 = val3[2]
p6 = val3[4]
p7 = val3[9]
p8 = val3[12]

val4 = int32x4(3, 4, 1, -3)
val4[2] = val4[1]
val4[3] = int32(5)
p9 = val4[2]
p10 = val4[3]

val5 = int32x8(1, 2, 3, 4, 5, 6, 7, 8)
val5[1] = val5[3]
val5[5] = val5[7]
p11 = val5[1]
p12 = val5[7]

val6 = int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
val6[2] = val6[3]
val6[4] = val6[6]
val6[9] = val6[11]
val6[12] = val6[14]
p13 = val6[2]
p14 = val6[4]
p15 = val6[9]
p16 = val6[12]
        """
        args = [('p1', float32()), ('p2', float32()), ('p3', float32()),
                ('p4', float32()), ('p5', float32()), ('p6', float32()), ('p7', float32()),
                ('p8', float32()), ('p9', int32()), ('p10', int32()), ('p11', int32()),
                ('p12', int32()), ('p13', int32()), ('p14', int32()), ('p15', int32()), ('p16', int32())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 4)
        self.assertAlmostEqual(kernel.get_value('p2'), 5)
        self.assertAlmostEqual(kernel.get_value('p3'), 4)
        self.assertAlmostEqual(kernel.get_value('p4'), 8)
        self.assertAlmostEqual(kernel.get_value('p5'), 4)
        self.assertAlmostEqual(kernel.get_value('p6'), 7)
        self.assertAlmostEqual(kernel.get_value('p7'), 12)
        self.assertAlmostEqual(kernel.get_value('p8'), 15)

        self.assertEqual(kernel.get_value('p9'), 4)
        self.assertEqual(kernel.get_value('p10'), 5)
        self.assertEqual(kernel.get_value('p11'), 4)
        self.assertEqual(kernel.get_value('p12'), 8)
        self.assertEqual(kernel.get_value('p13'), 4)
        self.assertEqual(kernel.get_value('p14'), 7)
        self.assertEqual(kernel.get_value('p15'), 12)
        self.assertEqual(kernel.get_value('p16'), 15)

    def test_subscript_1(self):
        self._test_subscript_1(ISet.AVX512)
        self._test_subscript_1(ISet.AVX2)
        self._test_subscript_1(ISet.AVX)
        self._test_subscript_1(ISet.SSE)

    def _test_subscript_2(self, iset):
        source = """
val1 = float64x2(3, 4)
val1[0] = val1[1]
val1[1] = float64(5)
p1 = val1[0]
p2 = val1[1]

val2 = float64x4(1, 2, 3, 4)
val2[1] = val2[0]
val2[2] = val2[3]
p3 = val2[1]
p4 = val2[2]

val3 = float64x8(1, 2, 3, 4, 5, 6, 7, 8)
val3[0] = val3[3]
val3[4] = val3[7]
val3[2] = val3[6]
val3[5] = val3[1]
p5 = val3[0]
p6 = val3[4]
p7 = val3[2]
p8 = val3[5]

val4 = int64x2(3, 4)
val4[0] = val4[1]
val4[1] = int64(5)
p9 = val4[0]
p10 = val4[1]

val5 = int64x4(1, 2, 3, 4)
val5[1] = val5[0]
val5[2] = val5[3]
p11 = val5[1]
p12 = val5[2]

val6 = int64x8(1, 2, 3, 4, 5, 6, 7, 8)
val6[0] = val6[3]
val6[4] = val6[7]
val6[2] = val6[6]
val6[5] = val6[1]
p13 = val6[0]
p14 = val6[4]
p15 = val6[2]
p16 = val6[5]


        """
        args = [('p1', float64()), ('p2', float64()), ('p3', float64()),
                ('p4', float64()), ('p5', float64()), ('p6', float64()), ('p7', float64()),
                ('p8', float64()), ('p9', int64()), ('p10', int64()), ('p11', int64()),
                ('p12', int64()), ('p13', int64()), ('p14', int64()), ('p15', int64()), ('p16', int64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 4)
        self.assertAlmostEqual(kernel.get_value('p2'), 5)
        self.assertAlmostEqual(kernel.get_value('p3'), 1)
        self.assertAlmostEqual(kernel.get_value('p4'), 4)
        self.assertAlmostEqual(kernel.get_value('p5'), 4)
        self.assertAlmostEqual(kernel.get_value('p6'), 8)
        self.assertAlmostEqual(kernel.get_value('p7'), 7)
        self.assertAlmostEqual(kernel.get_value('p8'), 2)

        self.assertEqual(kernel.get_value('p9'), 4)
        self.assertEqual(kernel.get_value('p10'), 5)
        self.assertEqual(kernel.get_value('p11'), 1)
        self.assertEqual(kernel.get_value('p12'), 4)
        self.assertEqual(kernel.get_value('p13'), 4)
        self.assertEqual(kernel.get_value('p14'), 8)
        self.assertEqual(kernel.get_value('p15'), 7)
        self.assertEqual(kernel.get_value('p16'), 2)

    def test_subscript_2(self):
        self._test_subscript_2(ISet.AVX512)
        self._test_subscript_2(ISet.AVX2)
        self._test_subscript_2(ISet.AVX)
        self._test_subscript_2(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
