
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, float32, float64, int64x2, int64x3, int64x4, int64x8, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, array, struct, register_user_type, ISet


class StackAStruct1(struct):
    __slots__ = ['p1', 'p2', 'p3', 'p4', 'p5', 'p6']

    def __init__(self):
        self.p1 = int32()
        self.p2 = float64()
        self.p3 = int32x8()
        self.p4 = float32x3()
        self.p5 = int64x4()
        self.p6 = float64x3()


register_user_type(StackAStruct1, factory=lambda: StackAStruct1())


class TestSubscript4(unittest.TestCase):

    def _check_x3(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])

    def _check_x4(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])

    def _check_x8(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])

    def _test_subscript_1(self, iset):
        source = """
arr_i32 = array(int32, 5)
n = 1
arr_i32[2] = 3
arr_i32[n] = -55
p1 = arr_i32[2]
p2 = arr_i32[n]

arr_f64 = array(float64, 10)
index = 9
while index >= 0:
    arr_f64[index] = float64(index)
    index = index - 1

p3 = arr_f64[5]

arr_f64x3 = array(float64x3, 10)
index = 9
while index >= 0:
    arr_f64x3[index] = float64x3(float64(index), float64(index + 1), float64(index + 2))
    index = index - 1

p4 = arr_f64x3[8]

arr_f32x8 = array(float32x8, 8)
index = 7
while index >= 0:
    val1 = float32x4(float32(index), float32(index + 1), float32(index + 2), float32(index + 3))
    val2 = float32x4(float32(index + 4), float32(index + 5), float32(index + 6), float32(index + 7))
    arr_f32x8[index] = float32x8(val1, val2)
    index = index - 1

p5 = arr_f32x8[5]


strc_arr = array(StackAStruct1, 16)
index = 7
while index >= 0:
    strc = strc_arr[index]
    strc.p1 = index
    strc.p2 = float64(index + 1)
    strc.p4 = float32x3(float32(index), float32(index + 1), float32(index + 2))
    index = index - 1


def test_arr_i32x4(arr_i32x4):
    index = 9
    while index >= 0:
        arr_i32x4[index] = int32x4(index, index + 1, index + 2, index + 3)
        index = index - 1

arr_i32x4 = array(int32x4, 10)
test_arr_i32x4(arr_i32x4)
p6 = arr_i32x4[2]

        """
        args = [('p1', int32()), ('p2', int32()), ('p3', float64()), ('p4', float64x3()),
                ('p5', float32x8()), ('p6', int32x4())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(kernel.get_value('p1'), 3)
        self.assertEqual(kernel.get_value('p2'), -55)
        self.assertEqual(kernel.get_value('p3'), 5)
        self._check_x3(kernel.get_value('p4'), float64x3(8, 9, 10))
        self._check_x8(kernel.get_value('p5'), float32x8(5, 6, 7, 8, 9, 10, 11, 12))
        self._check_x4(kernel.get_value('p6'), int32x4(2, 3, 4, 5))

    def test_subscript_1(self):
        self._test_subscript_1(ISet.AVX512)
        self._test_subscript_1(ISet.AVX2)
        self._test_subscript_1(ISet.AVX)
        self._test_subscript_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
