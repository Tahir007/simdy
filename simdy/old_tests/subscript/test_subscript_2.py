
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, float32, float64, int64x2, int64x3, int64x4, float64x3, float64x4,\
    float64x8, float32x2, float32x3, array, struct, register_user_type, ISet, array_float64


class TestArrStruct1(struct):
    __slots__ = ['p1', 'p2', 'arr']

    def __init__(self):
        self.p1 = float64(3)
        self.p2 = float64(4)
        self.arr = array_float64()
        self.arr.extend([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0])


class TestArrStruct2(struct):
    __slots__ = ['p1', 'p2']

    def __init__(self, p1, p2):
        self.p1 = float64(p1)
        self.p2 = float64(p2)


register_user_type(TestArrStruct2, factory=lambda: TestArrStruct2(0, 0))


class array_TestArrStruct2(array):
    def __init__(self, size=None, reserve=None):
        super(array_TestArrStruct2, self).__init__(lambda: TestArrStruct2(0, 0), size=size, reserve=reserve)


class TestSubscript2(unittest.TestCase):

    def _test_subscript_1(self, iset):
        source = """

p1 = arr_f64[2] + 4.0
p2 = 6.0 + arr_f64[1]
p3 = arr_f64[4] + arr_f64[6]

n1 = 8.0
p4 = arr_f64[3] + n1
p5 = n1 + arr_f64[6]
p6 = float64(4) + arr_f64[3]
p7 = arr_f64[5] + float64(5)

p8 = (arr_f64[5] + arr_f64[1]) + arr_f64[1]
p9 = arr_f64[2] - (arr_f64[1] + arr_f64[2])

p10 = arr_f64[2] + struc1.p1
p11 = struc1.p2  - arr_f64[7]

p_arr = arr_f64
p_arr[3] = p_arr[7]
p12 = p_arr[3]

struc1.arr[1] = struc1.arr[5]
p13 = struc1.arr[1]

p_arr = struc1.arr
p_arr[4] = 3.5

p_struc = struc1
p_struc.arr[2] = p_struc.arr[8]
p_arr = p_struc.arr
p_arr[9] = 2.2
        """

        arr_f64 = array_float64()
        arr_f64.extend([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0])
        struc1 = TestArrStruct1()
        args = [('arr_f64', arr_f64), ('p1', float64()), ('p2', float64()), ('p3', float64()),
                ('p4', float64()), ('p5', float64()), ('p6', float64()), ('p7', float64()),
                ('p8', float64()), ('p9', float64()), ('p10', float64()), ('p11', float64()),
                ('struc1', struc1), ('p12', float64()), ('p13', float64()), ('p14', float64())]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 6)
        self.assertAlmostEqual(kernel.get_value('p2'), 7)
        self.assertAlmostEqual(kernel.get_value('p3'), 10)
        self.assertAlmostEqual(kernel.get_value('p4'), 11)
        self.assertAlmostEqual(kernel.get_value('p5'), 14)
        self.assertAlmostEqual(kernel.get_value('p6'), 7)
        self.assertAlmostEqual(kernel.get_value('p7'), 10)
        self.assertAlmostEqual(kernel.get_value('p8'), 7)
        self.assertAlmostEqual(kernel.get_value('p9'), -1)
        self.assertAlmostEqual(kernel.get_value('p10'), 5)
        self.assertAlmostEqual(kernel.get_value('p11'), -3)
        self.assertAlmostEqual(kernel.get_value('p12'), 7)
        self.assertAlmostEqual(kernel.get_value('p13'), 5)
        self.assertAlmostEqual(struc1.arr[4], 3.5)
        self.assertAlmostEqual(struc1.arr[2], 8)
        self.assertAlmostEqual(struc1.arr[9], 2.2)

    def test_subscript_1(self):
        self._test_subscript_1(ISet.AVX512)
        self._test_subscript_1(ISet.AVX2)
        self._test_subscript_1(ISet.AVX)
        self._test_subscript_1(ISet.SSE)

    def _test_subscript_2(self, iset):
        source = """
def add_elements(arr):
    return arr[1] + arr[3]

def add_elements2(arr: array_float64):
    return arr[2] + arr[4]

p1 = add_elements(arr_f64)
p2 = add_elements2(arr_f64)
p3 = add_elements(struc1.arr)
p4 = add_elements2(struc1.arr)
ptr_1 = arr_s[1]
p5 = ptr_1.p1
ptr_1.p2 = 25.0
        """

        arr_f64 = array_float64()
        arr_f64.extend([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0])
        struc1 = TestArrStruct1()
        arr_s = array_TestArrStruct2()
        arr_s.append(TestArrStruct2(3, 3))
        arr_s.append(TestArrStruct2(1, 5))
        args = [('arr_f64', arr_f64), ('p1', float64()), ('p2', float64()), ('p3', float64()),
                ('p4', float64()), ('p5', float64()), ('p6', float64()), ('p7', float64()),
                ('p8', float64()), ('p9', float64()), ('p10', float64()), ('p11', float64()),
                ('struc1', struc1), ('p12', float64()), ('p13', float64()), ('p14', float64()),
                ('arr_s', arr_s)]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertAlmostEqual(kernel.get_value('p1'), 4)
        self.assertAlmostEqual(kernel.get_value('p2'), 6)
        self.assertAlmostEqual(kernel.get_value('p3'), 4)
        self.assertAlmostEqual(kernel.get_value('p4'), 6)
        self.assertAlmostEqual(kernel.get_value('p5'), 1)
        self.assertAlmostEqual(arr_s[1].p2, 25)

    def test_subscript_2(self):
        self._test_subscript_2(ISet.AVX512)
        self._test_subscript_2(ISet.AVX2)
        self._test_subscript_2(ISet.AVX)
        self._test_subscript_2(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
