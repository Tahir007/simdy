
import unittest
from simdy import Kernel, int32, int32x2, int32x3, int32x4, int32x8, int32x16,\
    int64, float32, float64, int64x2, int64x3, int64x4, int64x8, float64x2, float64x3, float64x4,\
    float64x8, float32x2, float32x3, float32x4, float32x8, float32x16, ISet, array_int32,\
    array_float32, array_float32x2, array_float32x3, array_float32x4, array_float32x8, array_float32x16,\
    array_int64, array_float64, array_float64x2, array_float64x3, array_float64x4, array_float64x8,\
    array_int32x2, array_int32x3, array_int32x4, array_int32x8, array_int32x16, array_int64x2,\
    array_int64x3, array_int64x4, array_int64x8


class TestSubscript1(unittest.TestCase):

    def _check_x2(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])

    def _check_x3(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])

    def _check_x4(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])

    def _check_x8(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])

    def _check_x16(self, src1, src2):
        self.assertAlmostEqual(src1[0], src2[0])
        self.assertAlmostEqual(src1[1], src2[1])
        self.assertAlmostEqual(src1[2], src2[2])
        self.assertAlmostEqual(src1[3], src2[3])
        self.assertAlmostEqual(src1[4], src2[4])
        self.assertAlmostEqual(src1[5], src2[5])
        self.assertAlmostEqual(src1[6], src2[6])
        self.assertAlmostEqual(src1[7], src2[7])
        self.assertAlmostEqual(src1[8], src2[8])
        self.assertAlmostEqual(src1[9], src2[9])
        self.assertAlmostEqual(src1[10], src2[10])
        self.assertAlmostEqual(src1[11], src2[11])
        self.assertAlmostEqual(src1[12], src2[12])
        self.assertAlmostEqual(src1[13], src2[13])
        self.assertAlmostEqual(src1[14], src2[14])
        self.assertAlmostEqual(src1[15], src2[15])

    def _test_subscript_1(self, iset):
        source = """
n, g = 2, 5
arr_i32[3] = arr_i32[1]
arr_i32[n] = arr_i32[g]

arr_i64[3] = arr_i64[1]
arr_i64[n] = arr_i64[g]

arr_f32[3] = arr_f32[1]
arr_f32[n] = arr_f32[g]

arr_f64[3] = arr_f64[1]
arr_f64[n] = arr_f64[g]

arr_f32x2[3] = arr_f32x2[1]
arr_f32x2[n] = arr_f32x2[g]

arr_f32x3[3] = arr_f32x3[1]
arr_f32x3[n] = arr_f32x3[g]

arr_f32x4[3] = arr_f32x4[1]
arr_f32x4[n] = arr_f32x4[g]

arr_f32x8[3] = arr_f32x8[1]
arr_f32x8[n] = arr_f32x8[g]

arr_f32x16[3] = arr_f32x16[1]
arr_f32x16[n] = arr_f32x16[g]

arr_f64x2[3] = arr_f64x2[1]
arr_f64x2[n] = arr_f64x2[g]

arr_f64x3[3] = arr_f64x3[1]
arr_f64x3[n] = arr_f64x3[g]

arr_f64x4[3] = arr_f64x4[1]
arr_f64x4[n] = arr_f64x4[g]

arr_f64x8[3] = arr_f64x8[1]
arr_f64x8[n] = arr_f64x8[g]

arr_i32x2[3] = arr_i32x2[1]
arr_i32x2[n] = arr_i32x2[g]

arr_i32x3[3] = arr_i32x3[1]
arr_i32x3[n] = arr_i32x3[g]

arr_i32x4[3] = arr_i32x4[1]
arr_i32x4[n] = arr_i32x4[g]

arr_i32x8[3] = arr_i32x8[1]
arr_i32x8[n] = arr_i32x8[g]

arr_i32x16[3] = arr_i32x16[1]
arr_i32x16[n] = arr_i32x16[g]

arr_i64x2[3] = arr_i64x2[1]
arr_i64x2[n] = arr_i64x2[g]

arr_i64x3[3] = arr_i64x3[1]
arr_i64x3[n] = arr_i64x3[g]

arr_i64x4[3] = arr_i64x4[1]
arr_i64x4[n] = arr_i64x4[g]

arr_i64x8[3] = arr_i64x8[1]
arr_i64x8[n] = arr_i64x8[g]

        """
        arr_i32 = array_int32()
        arr_i32.extend([0, 1, 2, 3, 4, 5, 6, 7])
        arr_i64 = array_int64()
        arr_i64.extend([0, 1, 2, 3, 4, 5, 6, 7])
        arr_f32 = array_float32()
        arr_f32.extend([float32(i) for i in range(7)])
        arr_f64 = array_float64()
        arr_f64.extend([float64(i) for i in range(7)])
        arr_f32x2 = array_float32x2()
        arr_f32x2.extend([float32x2(i, i + 1) for i in range(7)])
        arr_f32x3 = array_float32x3()
        arr_f32x3.extend([float32x3(i, i + 1, i + 2) for i in range(7)])
        arr_f32x4 = array_float32x4()
        arr_f32x4.extend([float32x4(i, i + 1, i + 2, i + 3) for i in range(7)])
        arr_f32x8 = array_float32x8()
        arr_f32x8.extend([float32x8(i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7) for i in range(7)])
        arr_f32x16 = array_float32x16()
        arr_f32x16.extend([float32x16(i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7,
                           i + 8, i + 9, i + 10, i + 11, i + 12, i + 13, i + 14, i + 15) for i in range(7)])

        arr_f64x2 = array_float64x2()
        arr_f64x2.extend([float64x2(i, i + 1) for i in range(7)])
        arr_f64x3 = array_float64x3()
        arr_f64x3.extend([float64x3(i, i + 1, i + 2) for i in range(7)])
        arr_f64x4 = array_float64x4()
        arr_f64x4.extend([float64x4(i, i + 1, i + 2, i + 3) for i in range(7)])
        arr_f64x8 = array_float64x8()
        arr_f64x8.extend([float64x8(i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7) for i in range(7)])

        arr_i32x2 = array_int32x2()
        arr_i32x2.extend([int32x2(i, i + 1) for i in range(7)])
        arr_i32x3 = array_int32x3()
        arr_i32x3.extend([int32x3(i, i + 1, i + 2) for i in range(7)])
        arr_i32x4 = array_int32x4()
        arr_i32x4.extend([int32x4(i, i + 1, i + 2, i + 3) for i in range(7)])
        arr_i32x8 = array_int32x8()
        arr_i32x8.extend([int32x8(i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7) for i in range(7)])
        arr_i32x16 = array_int32x16()
        arr_i32x16.extend([int32x16(i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7,
                           i + 8, i + 9, i + 10, i + 11, i + 12, i + 13, i + 14, i + 15) for i in range(7)])

        arr_i64x2 = array_int64x2()
        arr_i64x2.extend([int64x2(i, i + 1) for i in range(7)])
        arr_i64x3 = array_int64x3()
        arr_i64x3.extend([int64x3(i, i + 1, i + 2) for i in range(7)])
        arr_i64x4 = array_int64x4()
        arr_i64x4.extend([int64x4(i, i + 1, i + 2, i + 3) for i in range(7)])
        arr_i64x8 = array_int64x8()
        arr_i64x8.extend([int64x8(i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7) for i in range(7)])

        args = [('arr_i32', arr_i32), ('arr_i64', arr_i64), ('arr_f32', arr_f32), ('arr_f64', arr_f64),
                ('arr_f32x2', arr_f32x2), ('arr_f32x3', arr_f32x3), ('arr_f32x4', arr_f32x4),
                ('arr_f32x8', arr_f32x8), ('arr_f32x16', arr_f32x16),
                ('arr_f64x2', arr_f64x2), ('arr_f64x3', arr_f64x3),
                ('arr_f64x4', arr_f64x4), ('arr_f64x8', arr_f64x8),
                ('arr_i32x2', arr_i32x2), ('arr_i32x3', arr_i32x3), ('arr_i32x4', arr_i32x4),
                ('arr_i32x8', arr_i32x8), ('arr_i32x16', arr_i32x16),
                ('arr_i64x2', arr_i64x2), ('arr_i64x3', arr_i64x3),
                ('arr_i64x4', arr_i64x4), ('arr_i64x8', arr_i64x8)]
        kernel = Kernel(source, args=args, iset=iset)
        kernel.run()
        self.assertEqual(arr_i32[3], 1)
        self.assertEqual(arr_i32[2], 5)
        self.assertEqual(arr_i64[3], 1)
        self.assertEqual(arr_i64[2], 5)
        self.assertEqual(arr_f32[3], 1)
        self.assertEqual(arr_f32[2], 5)
        self.assertEqual(arr_f64[3], 1)
        self.assertEqual(arr_f64[2], 5)
        self._check_x2(arr_f32x2[3], float32x2(1, 2))
        self._check_x2(arr_f32x2[2], float32x2(5, 6))
        self._check_x3(arr_f32x3[3], float32x3(1, 2, 3))
        self._check_x3(arr_f32x3[2], float32x3(5, 6, 7))
        self._check_x4(arr_f32x4[3], float32x4(1, 2, 3, 4))
        self._check_x4(arr_f32x4[2], float32x4(5, 6, 7, 8))
        self._check_x8(arr_f32x8[3], float32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(arr_f32x8[2], float32x8(5, 6, 7, 8, 9, 10, 11, 12))
        self._check_x16(arr_f32x16[3], float32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        self._check_x16(arr_f32x16[2], float32x16(5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))

        self._check_x2(arr_f64x2[3], float64x2(1, 2))
        self._check_x2(arr_f64x2[2], float64x2(5, 6))
        self._check_x3(arr_f64x3[3], float64x3(1, 2, 3))
        self._check_x3(arr_f64x3[2], float64x3(5, 6, 7))
        self._check_x4(arr_f64x4[3], float64x4(1, 2, 3, 4))
        self._check_x4(arr_f64x4[2], float64x4(5, 6, 7, 8))
        self._check_x8(arr_f64x8[3], float64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(arr_f64x8[2], float64x8(5, 6, 7, 8, 9, 10, 11, 12))

        self._check_x2(arr_i32x2[3], int32x2(1, 2))
        self._check_x2(arr_i32x2[2], int32x2(5, 6))
        self._check_x3(arr_i32x3[3], int32x3(1, 2, 3))
        self._check_x3(arr_i32x3[2], int32x3(5, 6, 7))
        self._check_x4(arr_i32x4[3], int32x4(1, 2, 3, 4))
        self._check_x4(arr_i32x4[2], int32x4(5, 6, 7, 8))
        self._check_x8(arr_i32x8[3], int32x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(arr_i32x8[2], int32x8(5, 6, 7, 8, 9, 10, 11, 12))
        self._check_x16(arr_i32x16[3], int32x16(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        self._check_x16(arr_i32x16[2], int32x16(5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))

        self._check_x2(arr_i64x2[3], int64x2(1, 2))
        self._check_x2(arr_i64x2[2], int64x2(5, 6))
        self._check_x3(arr_i64x3[3], int64x3(1, 2, 3))
        self._check_x3(arr_i64x3[2], int64x3(5, 6, 7))
        self._check_x4(arr_i64x4[3], int64x4(1, 2, 3, 4))
        self._check_x4(arr_i64x4[2], int64x4(5, 6, 7, 8))
        self._check_x8(arr_i64x8[3], int64x8(1, 2, 3, 4, 5, 6, 7, 8))
        self._check_x8(arr_i64x8[2], int64x8(5, 6, 7, 8, 9, 10, 11, 12))

    def test_subscript_1(self):
        self._test_subscript_1(ISet.AVX512)
        self._test_subscript_1(ISet.AVX2)
        self._test_subscript_1(ISet.AVX)
        self._test_subscript_1(ISet.SSE)


if __name__ == "__main__":
    unittest.main()
