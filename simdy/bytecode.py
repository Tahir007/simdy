from enum import Enum
import inspect
import sys

from .types import return_type_codes, create_asm_desc_type, return_type_names, return_type_code_name
from .asm_cmds import copy_const_cmd, copy_cmd, arith_cmd, load_to_temp_cmd, convert_to_temp_cmd, compare_cmd


class VarInfo(Enum):
    LOCAL = 1
    BUILTIN = 2
    FUNCTION = 3
    CLASS = 4
    KERNEL = 5


def load_const(stack, inst, function, asm):
    # Version supported 3.5, 3.6, 3.7, 3.8
    val = function.__code__.co_consts[inst.arg]
    stack.append((val, VarInfo.LOCAL))


def load_fast(stack, inst, function, asm):
    # Version supported 3.5, 3.6, 3.7, 3.8
    name = function.__code__.co_varnames[inst.arg]
    stack.append((name, VarInfo.LOCAL))


def load_global(stack, inst, function, asm):
    # Version supported 3.5, 3.6, 3.7, 3.8
    names = function.__code__.co_names
    val = names[inst.arg]

    supported_builtins = ['int', 'bool', 'float']

    if val in function.__globals__:
        var = function.__globals__[val]
        if isinstance(var, (type(None), int, float, bool)):
            stack.append((var, VarInfo.LOCAL))
        elif hasattr(var, '_function') and inspect.isfunction(var._function):
            stack.append((var, VarInfo.KERNEL))
        elif inspect.isfunction(var):
            stack.append((var, VarInfo.FUNCTION))
        elif inspect.isclass(var):
            stack.append((var, VarInfo.CLASS))
        else:
            raise ValueError("Unknown global variable ", val)
    elif inspect.ismodule(function.__globals__['__builtins__']) and val in dir(function.__globals__['__builtins__']):
        if val not in supported_builtins:
            raise ValueError("Built-in %s function is not supported!" % val)
        stack.append((val, VarInfo.BUILTIN))
    elif val in function.__globals__['__builtins__']:
        if val not in supported_builtins:
            raise ValueError("Built-in %s function is not supported!" % val)
        stack.append((val, VarInfo.BUILTIN))
    else:
        raise ValueError("Unknown global variable ", val)


def create_descriptor(value, asm):
    if isinstance(value, (type(None), int, float, bool)):
        src_desc = create_asm_desc_type('undefined', type(value), value=value, is_const=True)
    elif isinstance(value, str) and asm.has_variable(value):
        src_desc = create_asm_desc_type(value, asm.get_data_type(value))
    else:
        # TODO on stack can be name of function, etc...
        raise ValueError("Unknown variable!",  value)
    return src_desc


def store_fast(stack, inst, function, asm):
    # Version supported 3.5, 3.6, 3.7, 3.8
    name = function.__code__.co_varnames[inst.arg]
    val, info = stack.pop()
    if info != VarInfo.LOCAL:
        raise ValueError("Store fast only LOCAL!", val, info)

    src_desc = create_descriptor(val, asm)
    dst_desc = create_asm_desc_type(name, src_desc.data_type())
    copy_cmd(dst_desc, src_desc, asm)
    # TODO must be improved, multiple store can happen
    asm.free_temp_variables()


def return_value(stack, inst, function, asm):
    val, info = stack.pop()
    if info != VarInfo.LOCAL:
        raise ValueError("Return only LOCAL!", val, info)

    var_desc = create_descriptor(val, asm)
    ret_code = return_type_codes[var_desc.data_type()]
    copy_const_cmd(return_type_code_name, ret_code, asm)

    if val is not None:
        name = return_type_names[ret_code]
        pdesc = create_asm_desc_type(name, var_desc.data_type(), value=var_desc.asm_value())
        if not asm.has_param(name):
            asm.add_param(name, pdesc.asm_type_name(), var_desc.data_type(), value=pdesc.asm_value())
        copy_cmd(pdesc, var_desc, asm)
    asm.free_temp_variables()


def binary_add(stack, inst, function, asm):
    tos, info_tos = stack.pop()
    tos1, info_tos1 = stack.pop()
    if info_tos != VarInfo.LOCAL:
        raise ValueError("Binary operation only LOCAL!", tos, info_tos)
    if info_tos1 != VarInfo.LOCAL:
        raise ValueError("Binary operation only LOCAL!", tos1, info_tos1)

    # TODO: optimization if store instruction is next
    ldsc = create_descriptor(tos1, asm)
    rdsc = create_descriptor(tos, asm)
    op = '+'
    reverse = False
    supported = ldsc.is_arith_supported(op, rdsc.data_type(), reverse=False)
    if not supported:
        if rdsc.is_arith_supported(op, ldsc.data_type(), reverse=True):
            reverse = True
        else:
            raise ValueError('Operator %s not supported for ' % op, ldsc.data_type(), rdsc.data_type())

    if ldsc.data_type() is not rdsc.data_type():
        # NOTE reverse must be taken into acount which operand must be converted!
        raise NotImplementedError('TODO: conversion needed', op, ldsc.data_type(), rdsc.data_type())

    name = arith_cmd(ldsc, rdsc, op, asm, reverse)
    stack.append((name, VarInfo.LOCAL))


def construct(stack, cls, arguments, asm):
    desc = create_asm_desc_type('tmp', cls)
    if len(arguments) == 0:
        arg_desc = create_descriptor(desc.create_default_value(), asm)
        name = load_to_temp_cmd(arg_desc, asm)
    elif len(arguments) == 1:
        arg, arg_info = arguments[0]
        if arg_info != VarInfo.LOCAL:
            raise ValueError("Constructors only LOCAL support.", arg, arg_info)
        arg_desc = create_descriptor(arg, asm)
        if arg_desc.data_type() is not desc.data_type():
            if arg_desc.is_const and desc.can_accept_const(arg_desc.asm_value()):
                value = desc.data_type()(arg_desc.asm_value())
                arg_desc = create_asm_desc_type(arg_desc.name, desc.data_type(), value=value, is_const=True)
                name = load_to_temp_cmd(arg_desc, asm)
            elif desc.can_convert(arg_desc.data_type()):
                name = convert_to_temp_cmd(desc, arg_desc, asm)
            else:
                raise ValueError("Construct: conversion of 1 argument is needed.", desc.data_type(), arg_desc.data_type())
        else:
            name = load_to_temp_cmd(arg_desc, asm)
    else:
        raise ValueError("Todo construct! - more arguments", cls, desc, arguments)
    stack.append((name, VarInfo.LOCAL))


def call_function(stack, inst, function, asm):
    # 3.5 stack contains positional and kw arguments
    # 3.6, 3.7, 3.8 stack contains only positional arguments
    if sys.version_info.minor == 5:
        n_kw = inst.arg >> 8
        n_pos = inst.arg & 0xFF
    else:
        n_kw = 0
        n_pos = inst.arg

    if n_kw != 0:
        raise ValueError("Only positionals arguments are supported.", inst, n_kw)

    arguments = []
    for i in range(n_pos):
        arg, arg_info = stack.pop()
        arguments.insert(0, (arg, arg_info))

    var, var_info = stack.pop()
    if var_info == VarInfo.BUILTIN:
        if var in ('int', 'bool', 'float'):
            built_in_constructors = {'int': int, 'bool': bool, 'float': float}
            construct(stack, built_in_constructors[var], arguments, asm)
        else:
            raise ValueError("NOTE: builtin %s function is not implemented" % var)
    elif var_info == VarInfo.CLASS:
        construct(stack, var, arguments, asm)
    elif var_info == VarInfo.FUNCTION:
        raise ValueError("TODO: call function")
    elif var_info == VarInfo.KERNEL:
        raise ValueError("TODO: call kernel")
    else:
        raise ValueError("Unsupported variable type! ", var, var_info)


def compare_op(stack, inst, function, asm):
    tos, info_tos = stack.pop()
    tos1, info_tos1 = stack.pop()
    if info_tos != VarInfo.LOCAL:
        raise ValueError("Compare operation only LOCAL are supported!", tos, info_tos)
    if info_tos1 != VarInfo.LOCAL:
        raise ValueError("Compare operation only LOCAL are supported", tos1, info_tos1)
    op = inst.argrepr
    ldsc = create_descriptor(tos1, asm)
    rdsc = create_descriptor(tos, asm)

    if not ldsc.is_compare_supported(op, rdsc.data_type()):
        raise ValueError("Compare for operand %s is not supported" % op, ldsc.data_type(), rdsc.data_type())

    must_convert, dst_data_type = ldsc.implicit_conversion(op, rdsc.data_type())
    if must_convert:
        dst_desc = create_asm_desc_type('temp', dst_data_type)
        lname = convert_to_temp_cmd(dst_desc, ldsc, asm)
        ldsc = create_descriptor(lname, asm)

    if ldsc.data_type() is not rdsc.data_type():
        rname = convert_to_temp_cmd(ldsc, rdsc, asm)
        rdsc = create_descriptor(rname, asm)

    if ldsc.data_type() is not rdsc.data_type():
        raise ValueError('Compare can be only perform on same types.', op, ldsc.data_type(), rdsc.data_type())
    name = compare_cmd(ldsc, rdsc, op, asm)
    stack.append((name, VarInfo.LOCAL))


bytecode_callables = {
    100: load_const,
    83: return_value,
    125: store_fast,
    124: load_fast,
    116: load_global,
    23: binary_add,
    131: call_function,
    107: compare_op,
}
