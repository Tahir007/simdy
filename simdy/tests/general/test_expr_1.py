from simdy import kernel


@kernel
def expr_1():
    a = 3
    b = a
    return b


def test_expr_1():
    ret_val = expr_1()
    assert ret_val == 3

