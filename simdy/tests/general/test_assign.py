from simdy import kernel


@kernel
def simple_assign_1():
    a = 2
    return a


def test_assign1():
    ret_val = simple_assign_1()
    assert ret_val == 2

