from simdy import kernel


@kernel
def simple_true_compare_1():
    a = 9 > 6
    return a


def test_simple_true_compare_1():
    res = simple_true_compare_1()
    assert res is True and isinstance(res, bool)


@kernel
def simple_false_compare_1():
    a = -9 > 6
    return a


def test_simple_false_compare_1():
    res = simple_false_compare_1()
    assert res is False and isinstance(res, bool)


@kernel
def simple_false_compare_2():
    a = 66
    b = 11
    return b > a


def test_simple_false_compare_2():
    res = simple_false_compare_2()
    assert res is False and isinstance(res, bool)


@kernel
def simple_false_float_cmp():
    a = 66.66
    b = 11.77
    return b > a


def test_simple_false_float_cmp():
    res = simple_false_float_cmp()
    assert res is False and isinstance(res, bool)

