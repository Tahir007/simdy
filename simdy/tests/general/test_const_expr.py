from simdy import kernel


@kernel
def const_expr1():
    return 3 + 2 * 4 // 3


def test_const_expr1():
    ret_val = const_expr1()
    assert ret_val == 5

# TODO division example: 5 / 2

