from simdy import kernel


@kernel
def empty_fun():
    pass


def test_empty_fun():
    ret_val = empty_fun()
    assert ret_val is None


@kernel
def fun1():
    return 3


def test_fun1():
    ret_val = fun1()
    assert ret_val == 3
