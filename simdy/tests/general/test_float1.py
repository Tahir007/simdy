from simdy import kernel


@kernel
def default_float():
    return float()


def test_default_float():
    result = default_float()
    assert result == 0.0


@kernel
def float_const():
    return float(3.2)


def test_float_const():
    result = float_const()
    assert result == 3.2


@kernel
def float_const2():
    return 3.7


def test_float_const2():
    result = float_const2()
    assert result == 3.7


@kernel
def float_name():
    x = 3.9
    return x


def test_float_name():
    result = float_name()
    assert result == 3.9


