
from simdy import kernel


@kernel
def default_bool():
    return bool()


def test_default_bool():
    result = default_bool()
    assert result is False


@kernel
def true_bool():
    return bool(True)


def test_true_bool():
    result = true_bool()
    assert result is True


@kernel
def convert_const_bool():
    return bool(88)


def test_convert_const_bool():
    result = convert_const_bool()
    assert result is True


@kernel
def convert_const_false_bool():
    return bool(0)


def test_convert_const_bool():
    result = convert_const_false_bool()
    assert result is False


@kernel
def convert_name_bool():
    a = 44
    return bool(a)


def test_convert_name_bool():
    result = convert_name_bool()
    assert result is True
