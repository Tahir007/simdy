from simdy import kernel


@kernel
def ret_param_0(a: int):
    return a


def test_param_0():
    ret_val = ret_param_0(3)
    assert ret_val == 3


@kernel
def ret_param_1(a: int):
    b = a
    return b


def test_param_1():
    ret_val = ret_param_1(5)
    assert ret_val == 5


@kernel
def ret_param_2(a):
    return a


def test_param_2():
    ret_val = ret_param_2(6)
    assert ret_val == 6
