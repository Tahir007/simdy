
from simdy import kernel



@kernel
def arith_1(a: int, b: int):
    return a + b


def test_arith_1():
    ret_val = arith_1(5, 6)
    assert ret_val == 11


@kernel
def arith_2(a: int):
    b = 8
    c = 5 + a + b + 9
    return c


def test_arith_2():
    ret_val = arith_2(5)
    assert ret_val == 27

