from simdy import kernel, int64


@kernel
def float_const_int_conv():
    return float(3)


def test_float_const_int_conv():
    ret_val = float_const_int_conv()
    assert ret_val == 3.0 and isinstance(ret_val, float)


@kernel
def float_const_bool_conv():
    return float(True)


def test_float_const_bool_conv():
    ret_val = float_const_bool_conv()
    assert ret_val == 1.0 and isinstance(ret_val, float)


@kernel
def float_name_int_conv():
    a = 44
    return float(a)


def test_float_name_int_conv():
    ret_val = float_name_int_conv()
    assert ret_val == 44.0 and isinstance(ret_val, float)


@kernel
def float_name_bool_conv():
    a = True
    return float(a)


def test_float_name_bool_conv():
    ret_val = float_name_bool_conv()
    assert ret_val == 1.0 and isinstance(ret_val, float)


@kernel
def float_name_int64_conv():
    a = int64(9000000000)
    return float(a)


def test_float_name_int64_conv():
    ret_val = float_name_int64_conv()
    assert ret_val == 9000000000.0 and isinstance(ret_val, float)


@kernel
def int_const_bool_conv():
    return int(True)


def test_int_const_bool_conv():
    ret_val = int_const_bool_conv()
    assert ret_val == 1 and isinstance(ret_val, int)


@kernel
def int_const_float_conv():
    return int(3.6)


def test_int_const_float_conv():
    ret_val = int_const_float_conv()
    assert ret_val == 3 and isinstance(ret_val, int)


@kernel
def int_name_bool_conv():
    a = True
    return int(a)


def test_int_name_bool_conv():
    ret_val = int_name_bool_conv()
    assert ret_val == 1 and isinstance(ret_val, int)


@kernel
def int_name_float_conv():
    a = 3.8
    return int(a)


def test_int_name_float_conv():
    ret_val = int_name_float_conv()
    assert ret_val == 3 and isinstance(ret_val, int)


@kernel
def int_name_int64_conv():
    a = int64(5)
    return int(a)


def test_int_name_int64_conv():
    ret_val = int_name_int64_conv()
    assert ret_val == 5 and isinstance(ret_val, int)


@kernel
def bool_const_int_conv():
    return bool(33)


def test_bool_const_int_conv():
    ret_val = bool_const_int_conv()
    assert ret_val is True and isinstance(ret_val, bool)


@kernel
def bool_name_int_conv():
    a = 55
    return bool(a)


def test_bool_name_int_conv():
    ret_val = bool_name_int_conv()
    assert ret_val is True and isinstance(ret_val, bool)


@kernel
def bool_name_float_conv():
    a = 1.59
    return bool(a)


def test_bool_name_float_conv():
    ret_val = bool_name_float_conv()
    assert ret_val is True and isinstance(ret_val, bool)


@kernel
def bool_name_int64_conv():
    a = int64(33)
    return bool(a)


def test_bool_name_int64_conv():
    ret_val = bool_name_int64_conv()
    assert ret_val is True and isinstance(ret_val, bool)


@kernel
def int64_const_bool_conv():
    return int64(True)


def test_int64_const_bool_conv():
    ret_val = int64_const_bool_conv()
    assert ret_val == 1 and isinstance(ret_val, int64)


@kernel
def int64_const_float_conv():
    return int64(3.9)


def test_int64_const_float_conv():
    ret_val = int64_const_float_conv()
    assert ret_val == 3 and isinstance(ret_val, int64)


@kernel
def int64_name_float_conv():
    a = 7.7
    return int64(a)


def test_int64_name_float_conv():
    ret_val = int64_name_float_conv()
    assert ret_val == 7 and isinstance(ret_val, int64)


@kernel
def int64_name_bool_conv():
    a = True
    return int64(a)


def test_int64_name_bool_conv():
    ret_val = int64_name_bool_conv()
    assert ret_val == 1 and isinstance(ret_val, int64)
