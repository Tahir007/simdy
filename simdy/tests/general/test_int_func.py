from simdy import kernel, int64


@kernel
def empty_int_func():
    return int()


def test_empty_int_func():
    ret_val = empty_int_func()
    assert ret_val == 0


@kernel
def const_int_func():
    return int(6)


def test_const_int_func():
    ret_val = const_int_func()
    assert ret_val == 6


@kernel
def name_int_func():
    a = 88
    return int(a)


def test_name_int_func():
    ret_val = name_int_func()
    assert ret_val == 88


@kernel
def param_int_func(a):
    return int(a)


def test_param_int_func():
    ret_val = param_int_func(33)
    assert ret_val == 33


@kernel
def complex_int_func():
    return int(int(int(3)))


def test_complex_int_func():
    ret_val = complex_int_func()
    assert ret_val == 3


@kernel
def empty_int64_func():
    return int64()


def test_empty_int64_func():
    ret_val = empty_int64_func()
    assert ret_val == 0


@kernel
def const_int64_func():
    return int64(9)


def test_const_int64_func():
    ret_val = const_int64_func()
    assert ret_val == 9


@kernel
def name_int64_func():
    a = int64(9)
    return int64(a)


def test_name_int64_func():
    ret_val = name_int64_func()
    assert ret_val == 9


@kernel
def convert_int64_func():
    a = int(52)
    return int64(a)


def test_convert_int64_func():
    ret_val = convert_int64_func()
    assert ret_val == 52
