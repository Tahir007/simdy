from dis import Bytecode, dis
from collections import deque

from .asm import AsmBuilder
from .types import simdy_types, return_type_code_name
from .bytecode import bytecode_callables


def _create_asm_params(pnames, ptypes, asm):
    pdescs = []
    for pname, ptype in zip(pnames, ptypes):
        if ptype not in simdy_types:
            raise ValueError("Not supported param type ", pname, ptype)
        pdesc = simdy_types[ptype](pname)
        pdescs.append(pdesc)
        if pdesc.is_asm_array():
            asm.add_param_array(pname,
                                pdesc.asm_type_name(),
                                length=pdesc.asm_arr_length(),
                                data_type=pdesc.data_type(),
                                values=pdesc.asm_arr_values())
        else:
            asm.add_param(pname,
                          pdesc.asm_type_name(),
                          pdesc.data_type(),
                          value=pdesc.asm_value())
    return pdescs


def create_assembly_seq(pnames, ptypes, function):

    stack = deque()
    asm = AsmBuilder()
    pdescs = _create_asm_params(pnames, ptypes, asm)
    # dis(function)
    asm.add_param(return_type_code_name, 'int32', int, value=-1)
    for inst in Bytecode(function):
        # TODO raise error or warning is some opcodes are not processed
        if inst.opcode in bytecode_callables:
            bytecode_callables[inst.opcode](stack, inst, function, asm)
        else:
            raise NotImplementedError('Bytecode Instruction not implemented', inst)
    asm.end()
    # print(asm.asm())
    return asm.get_instruction_seq()
