from .data_types import int64
from .types import register_simdy_type
from .asm import RegType, RegSize


class BaseDescType:
    d_type = None

    def __init__(self, name, value=None, is_const=False):
        self._name = name
        self._is_const = is_const
        if value is None:
            value = self.create_default_value()
        if not isinstance(value, self.data_type()):
            value = self.data_type()(value)
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def is_const(self):
        return self._is_const

    @property
    def asm_name(self):
        return self._asm_name

    def create_default_value(self):
        raise NotImplementedError()

    def asm_value(self):
        return self._value

    def data_type(self):
        return self.d_type

    @classmethod
    def asm_type_name(cls):
        return NotImplementedError()

    @classmethod
    def asm_type_size_name(cls):
        return NotImplementedError()

    @classmethod
    def stack_size(cls):
        return NotImplementedError()

    @classmethod
    def mov_cmd(cls):
        return NotImplementedError()

    @classmethod
    def op_cmd(cls, operator):
        return NotImplementedError()

    @classmethod
    def storage_reg_info(cls):
        return NotImplementedError()

    def is_arith_supported(self, op, data_type, reverse=False):
        return False

    def is_compare_supported(self, op, data_type):
        return False

    def cmove_cmd(self, op, satisfied):
        return NotImplementedError()

    def implicit_conversion(self, op, data_type):
        return False, None

    def can_accept_const(self, const):
        return False

    def can_convert(self, data_type):
        return False

    def convert_cmd(self, data_type):
        return NotImplementedError()

    def is_asm_array(self):
        return False

    def asm_arr_length(self):
        return NotImplementedError()

    def asm_arr_values(self):
        return NotImplementedError()


class Int32(BaseDescType):
    d_type = int

    def create_default_value(self):
        return int()

    @classmethod
    def asm_type_name(cls):
        return 'int32'

    @classmethod
    def asm_type_size_name(cls):
        return 'dword'

    @classmethod
    def stack_size(cls):
        return 4

    @classmethod
    def move_cmd(cls):
        return 'mov'

    @classmethod
    def op_cmd(cls, operator):
        ops = {'+': 'add', '-': 'sub', '<': 'cmp', '>': 'cmp', '==': 'cmp', '<=': 'cmp', '>=': 'cmp', '!=': 'cmp'}
        return ops[operator]

    @classmethod
    def storage_reg_info(cls):
        return RegType.GENERAL, RegSize.BIT32

    def is_arith_supported(self, op, data_type, reverse=False):
        if op == '+' and data_type is self.data_type():
            return True
        return False

    def is_compare_supported(self, op, data_type):
        if op in ('>', '<', '==', '<=', '>=', '!=') and data_type in (int, bool, float, int64):
            return True
        return False

    def cmove_cmd(self, op, satisfied):
        if satisfied:
            cons = {'==': 'cmove', '<': 'cmovl', '>': 'cmovg',
                    '<=': 'cmovle', '>=': 'cmovge', '!=': 'cmovne'}
        else:
            cons = {'==': 'cmovne', '<': 'cmovge', '>': 'cmovle',
                    '<=': 'cmovg', '>=': 'cmovl', '!=': 'cmove'}
        return cons[op]

    def implicit_conversion(self, op, data_type):
        if op == '/':
            return True, float
        if data_type in (float, int64):
            return True, data_type
        return False, None

    def can_accept_const(self, const):
        return isinstance(const, (bool, float, int64))

    def can_convert(self, data_type):
        return data_type in (bool, float, int64)

    def convert_cmd(self, data_type):
        if data_type is bool:
            return 'mov'
        elif data_type is float:
            return 'cvttsd2si'
        elif data_type is int64:
            def conv_cmd(dst_reg, src_reg, tmp_reg, asm):
                asm.conv_int64_to_int32(dst_reg, src_reg, tmp_reg)
            return conv_cmd
        raise ValueError("Only bool, int64, float can be converted to int", data_type)


register_simdy_type(int, Int32)


class NoneDesc(Int32):
    d_type = type(None)

    def create_default_value(self):
        return None


register_simdy_type(type(None), NoneDesc)


class BoolDesc(Int32):
    d_type = bool

    def create_default_value(self):
        return bool()

    def asm_value(self):
        return int(self._value)

    def can_accept_const(self, const):
        return isinstance(const, (int, float, int64))

    def can_convert(self, data_type):
        return data_type in (int, float, int64)

    def convert_cmd(self, data_type):
        if data_type is int:
            def conv_cmd(dst_reg, src_reg, tmp_reg, asm):
                asm.conv_int32_to_bool(dst_reg, src_reg, tmp_reg)
            return conv_cmd
        elif data_type is float:
            def conv_cmd(dst_reg, src_reg, tmp_reg, asm):
                asm.conv_float_to_bool(dst_reg, src_reg, tmp_reg)
            return conv_cmd
        elif data_type is int64:
            def conv_cmd(dst_reg, src_reg, tmp_reg, asm):
                asm.conv_int64_to_bool(dst_reg, src_reg, tmp_reg)
            return conv_cmd
        raise ValueError("Only int, float can be converted to bool", data_type)


register_simdy_type(bool, BoolDesc)


class Int64(BaseDescType):
    d_type = int64

    def create_default_value(self):
        return int64()

    @classmethod
    def asm_type_name(cls):
        return 'int64'

    @classmethod
    def asm_type_size_name(cls):
        return 'qword'

    @classmethod
    def stack_size(cls):
        return 8

    @classmethod
    def move_cmd(cls):
        return 'mov'

    @classmethod
    def storage_reg_info(cls):
        return RegType.GENERAL, RegSize.BIT64

    @classmethod
    def op_cmd(cls, operator):
        ops = {'+': 'add', '-': 'sub'}
        return ops[operator]

    def is_arith_supported(self, op, data_type, reverse=False):
        if op == '+' and data_type is self.data_type():
            return True
        return False

    def can_accept_const(self, const):
        return isinstance(const, (bool, int, float))

    def can_convert(self, data_type):
        return data_type in (bool, int, float)

    def convert_cmd(self, data_type):
        if data_type is bool:
            return 'movsx'
        elif data_type is int:
            return 'movsx'
        elif data_type is float:
            return 'cvttsd2si'
        raise ValueError("Only int can be converted to int64", data_type)


register_simdy_type(int64, Int64)


class Float(BaseDescType):
    d_type = float

    def create_default_value(self):
        return float()

    @classmethod
    def asm_type_name(cls):
        return 'double'

    @classmethod
    def asm_type_size_name(cls):
        return 'qword'

    @classmethod
    def stack_size(cls):
        return 8

    @classmethod
    def move_cmd(cls):
        return 'movsd'

    @classmethod
    def op_cmd(cls, operator):
        ops = {'+': 'addsd', '-': 'subsd', '<': 'ucomisd', '>': 'ucomisd',
               '==': 'ucomisd', '<=': 'ucomisd', '>=': 'ucomisd', '!=': 'ucomisd'}
        return ops[operator]

    def can_accept_const(self, const):
        return isinstance(const, (bool, int, int64))

    def can_convert(self, data_type):
        return data_type in (int, bool, int64)

    def convert_cmd(self, data_type):
        if data_type in (int, bool, int64):
            return 'cvtsi2sd'
        raise ValueError("Only int, int64, bool can be converted to float", data_type)

    @classmethod
    def storage_reg_info(cls):
        return RegType.SSE, RegSize.BIT128

    def is_arith_supported(self, op, data_type, reverse=False):
        if op == '+' and data_type is self.data_type():
            return True
        return False

    def is_compare_supported(self, op, data_type):
        if op in ('>', '<', '==', '<=', '>=', '!=') and data_type in (bool, int, float, int64):
            return True
        return False

    def cmove_cmd(self, op, satisfied):
        if satisfied:
            cons = {'==': 'cmove', '<': 'cmovb', '>': 'cmova',
                    '<=': 'cmovbe', '>=': 'cmovae', '!=': 'cmovne'}
        else:
            cons = {'==': 'cmovne', '<': 'cmovae', '>': 'cmovbe',
                    '<=': 'cmova', '>=': 'cmovb', '!=': 'cmove'}
        return cons[op]


register_simdy_type(float, Float)
