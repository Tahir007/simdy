import struct

from .data_types import int64
from .asm import RegType, RegSize
from .types import create_asm_desc_type


def float_to_int(value):
    res = struct.pack('d', value)
    asm_value = struct.unpack('q', res)[0]
    return asm_value


def copy_const_cmd(name, value, asm):
    if isinstance(value, bool):
        asm_value = int(value)
    elif isinstance(value, int):
        asm_value = value
    elif isinstance(value, float):
        asm_value = float_to_int(value)
    else:
        raise NotImplementedError("TODO: constant currently not supported!", name, value)
    pdesc = create_asm_desc_type(name, type(value), value)
    if not asm.has_local_variable(name) and asm.has_param(name) and pdesc.data_type() is asm.get_data_type(name):
        if isinstance(value, (float, int64)):
            reg = asm.register([(RegType.GENERAL, RegSize.BIT64)])[0]
            asm.add_inst_2_op('mov', dst_reg=reg, src_value=asm_value)
            asm.add_inst_2_op('mov', dst_size_name=pdesc.asm_type_size_name(), dst_name=pdesc.name, src_reg=reg)
        else:
            asm.add_inst_2_op(pdesc.move_cmd(), dst_size_name=pdesc.asm_type_size_name(),
                              dst_name=pdesc.name, src_value=asm_value)
    else:
        stack_offset = asm.add_local(pdesc.name, pdesc.asm_type_name(), pdesc.data_type(), pdesc.stack_size())
        if isinstance(value, (float, int64)):
            reg = asm.register([(RegType.GENERAL, RegSize.BIT64)])[0]
            asm.add_inst_2_op('mov', dst_reg=reg, src_value=asm_value)
            asm.add_inst_2_op('mov', dst_size_name=pdesc.asm_type_size_name(),
                              dst_reg=asm.stack_reg, dst_offset=stack_offset, src_reg=reg)
        else:
            asm.add_inst_2_op(pdesc.move_cmd(), dst_size_name=pdesc.asm_type_size_name(),
                              dst_reg=asm.stack_reg, dst_offset=stack_offset, src_value=asm_value)


def copy_var_cmd(dst_desc, src_desc, asm):

    if dst_desc.asm_type_name() != src_desc.asm_type_name():
        raise ValueError("Copy operation is only allowed for same types!",
                         dst_desc, dst_desc.data_type(), src_desc, src_desc.data_type())

    reg_type, reg_size = src_desc.storage_reg_info()
    reg = asm.register([(reg_type, reg_size)])[0]

    if asm.has_local_variable(src_desc.name):
        stack_offset = asm.get_local(src_desc.name, src_desc.asm_type_name(), src_desc.data_type())
        asm.add_inst_2_op(src_desc.move_cmd(), dst_reg=reg, src_size_name=src_desc.asm_type_size_name(),
                          src_reg=asm.stack_reg, src_offset=stack_offset)
    elif asm.has_param(src_desc.name):
        asm.add_inst_2_op(src_desc.move_cmd(), dst_reg=reg,
                          src_size_name=src_desc.asm_type_size_name(), src_name=src_desc.name)
    else:
        raise NotImplementedError("Source variable %s doesn't exist!" % src_desc.name)

    if not asm.has_local_variable(dst_desc.name) and asm.has_param(dst_desc.name) and dst_desc.data_type() is src_desc.data_type():
        asm.add_inst_2_op(dst_desc.move_cmd(), dst_size_name=dst_desc.asm_type_size_name(),
                          dst_name=dst_desc.name, src_reg=reg)
    else:
        stack_offset = asm.add_local(dst_desc.name, dst_desc.asm_type_name(), dst_desc.data_type(), dst_desc.stack_size())
        asm.add_inst_2_op(dst_desc.move_cmd(), dst_reg=asm.stack_reg, dst_offset=stack_offset,
                          dst_size_name=dst_desc.asm_type_size_name(), src_reg=reg)


def copy_cmd(dst_desc, src_desc, asm):
    if src_desc.is_const:
        copy_const_cmd(dst_desc.name, src_desc.asm_value(), asm)
    else:
        copy_var_cmd(dst_desc, src_desc, asm)


def load_cmd(dsc, asm, reg=None):
    if reg is None:
        reg_type, reg_size = dsc.storage_reg_info()
        reg = asm.register([(reg_type, reg_size)])[0]
    if dsc.is_const:
        if isinstance(dsc.asm_value(), bool):
            raise ValueError("TODO: support for bool missing")
        elif isinstance(dsc.asm_value(), int):
            pass
        elif isinstance(dsc.asm_value(), float):
            raise ValueError("TODO: support for float missing")
        else:
            raise ValueError("TODO: support for constant missing.", dsc.asm_value())
        asm.add_inst_2_op(dsc.move_cmd(), dst_reg=reg, src_value=dsc.asm_value())
    elif asm.has_local_variable(dsc.name):
        stack_offset = asm.get_local(dsc.name, dsc.asm_type_name(), dsc.data_type())
        asm.add_inst_2_op(dsc.move_cmd(), dst_reg=reg, src_size_name=dsc.asm_type_size_name(),
                          src_reg=asm.stack_reg, src_offset=stack_offset)
    elif asm.has_param(dsc.name):
        asm.add_inst_2_op(dsc.move_cmd(), dst_reg=reg,
                          src_size_name=dsc.asm_type_size_name(), src_name=dsc.name)
    else:
        raise ValueError("Cannot load %s variable" % dsc.name)
    return reg


def load_to_temp_cmd(desc, asm):
    name, stack_offset = asm.add_temp_var(desc.asm_type_name(), desc.data_type(), desc.stack_size())
    if desc.is_const:
        copy_const_cmd(name, desc.asm_value(), asm)
    else:
        reg1 = load_cmd(desc, asm)
        asm.add_inst_2_op(desc.move_cmd(), dst_reg=asm.stack_reg, dst_offset=stack_offset,
                          dst_size_name=desc.asm_type_size_name(), src_reg=reg1)
    return name


def convert_to_temp_cmd(dst_desc, src_desc, asm):
    arg_name = load_to_temp_cmd(src_desc, asm)  # TODO investigate, maybe it can be optimized!, maybe just for constants and parameters needed!
    conv_cmd = dst_desc.convert_cmd(src_desc.data_type())
    reg_type, reg_size = dst_desc.storage_reg_info()
    stack_offset = asm.get_local(arg_name, src_desc.asm_type_name(), src_desc.data_type())
    if callable(conv_cmd):
        src_reg_type, src_reg_size = src_desc.storage_reg_info()
        reg, src_reg, tmp_reg = asm.register([(reg_type, reg_size), (src_reg_type, src_reg_size), (src_reg_type, src_reg_size)])
        load_cmd(src_desc, asm, src_reg)
        conv_cmd(reg, src_reg, tmp_reg, asm)
    else:
        reg = asm.register([(reg_type, reg_size)])[0]
        asm.add_inst_2_op(conv_cmd, dst_reg=reg,
                          src_size_name=src_desc.asm_type_size_name(), src_reg=asm.stack_reg, src_offset=stack_offset)
    name, stack_offset = asm.add_temp_var(dst_desc.asm_type_name(), dst_desc.data_type(), dst_desc.stack_size())
    asm.add_inst_2_op(dst_desc.move_cmd(), dst_reg=asm.stack_reg, dst_offset=stack_offset,
                      dst_size_name=dst_desc.asm_type_size_name(), src_reg=reg)
    return name


def arith_cmd(ldsc, rdsc, operator, asm, reverse):
    if ldsc.asm_type_name() != rdsc.asm_type_name():
        raise ValueError("Operation %s is only allowed for same types!" % operator,
                         ldsc, ldsc.data_type(), rdsc, rdsc.data_type())

    reg1 = load_cmd(ldsc, asm)
    if rdsc.is_const:
        # TODO float constants
        asm.add_inst_2_op(ldsc.op_cmd(operator), dst_reg=reg1, src_value=rdsc.asm_value())
    elif asm.has_local_variable(rdsc.name):
        stack_offset = asm.get_local(rdsc.name, rdsc.asm_type_name(), rdsc.data_type())
        asm.add_inst_2_op(ldsc.op_cmd(operator), dst_reg=reg1,
                          src_size_name=ldsc.asm_type_size_name(), src_reg=asm.stack_reg, src_offset=stack_offset)
    elif asm.has_param(rdsc.name):
        asm.add_inst_2_op(ldsc.op_cmd(operator), dst_reg=reg1,
                          src_size_name=ldsc.asm_type_size_name(), src_name=rdsc.name)
    else:
        raise ValueError("Cannot load %s variable" % rdsc.name)
    name, stack_offset = asm.add_temp_var(ldsc.asm_type_name(), ldsc.data_type(), ldsc.stack_size())
    asm.add_inst_2_op(ldsc.move_cmd(), dst_reg=asm.stack_reg, dst_offset=stack_offset,
                      dst_size_name=ldsc.asm_type_size_name(), src_reg=reg1)
    return name


def compare_cmd(ldsc, rdsc, operator, asm):
    if ldsc.asm_type_name() != rdsc.asm_type_name():
        raise ValueError("Operation %s is only allowed for same types!" % operator,
                         ldsc, ldsc.data_type(), rdsc, rdsc.data_type())

    reg_type, reg_size = ldsc.storage_reg_info()
    reg1, reg2, reg3 = asm.register([(reg_type, reg_size), (RegType.GENERAL, RegSize.BIT32), (RegType.GENERAL, RegSize.BIT32)])
    load_cmd(ldsc, asm, reg1)
    asm.add_inst_2_op('mov', dst_reg=reg3, src_value=1)
    asm.add_inst_2_op('xor', dst_reg=reg2, src_reg=reg2)
    if rdsc.is_const:
        # TODO float constants
        asm.add_inst_2_op(ldsc.op_cmd(operator), dst_reg=reg1, src_value=rdsc.asm_value())
    elif asm.has_local_variable(rdsc.name):
        stack_offset = asm.get_local(rdsc.name, rdsc.asm_type_name(), rdsc.data_type())
        asm.add_inst_2_op(ldsc.op_cmd(operator), dst_reg=reg1,
                          src_size_name=ldsc.asm_type_size_name(), src_reg=asm.stack_reg, src_offset=stack_offset)
    elif asm.has_param(rdsc.name):
        asm.add_inst_2_op(ldsc.op_cmd(operator), dst_reg=reg1,
                          src_size_name=ldsc.asm_type_size_name(), src_name=rdsc.name)
    else:
        raise ValueError("Cannot load %s variable" % rdsc.name)
    asm.add_inst_2_op(ldsc.cmove_cmd(operator, True), dst_reg=reg2, src_reg=reg3)

    res_dsc = create_asm_desc_type('temp', bool)
    name, stack_offset = asm.add_temp_var(res_dsc.asm_type_name(), res_dsc.data_type(), res_dsc.stack_size())
    asm.add_inst_2_op(res_dsc.move_cmd(), dst_reg=asm.stack_reg, dst_offset=stack_offset,
                      dst_size_name=res_dsc.asm_type_size_name(), src_reg=reg2)
    return name
