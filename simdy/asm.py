from enum import Enum
from collections import defaultdict

from tdasm.holders import Directive, DataMembers, DataMember,\
    ArrayMember, ConstOperand, NameOperand, Instruction, RegOperand, MemOperand

VAR_SUFFIX = '_simdy'


class RegType(Enum):
    GENERAL = 1
    SSE = 2


class RegSize(Enum):
    BIT32 = 1
    BIT64 = 2
    BIT128 = 3
    BIT256 = 4


class AsmBuilder:
    """
    1. Stack setup
    2. Code
    3. Stack cleanup
    """
    def __init__(self):
        self._asm_insts = []
        self._inside_code = True
        self._data_types = frozenset(['int32', 'int64', 'double'])
        self._params = {}
        self._locals = {}
        self._free_locals = defaultdict(list)
        self._temp_vars = {}
        self._free_temp_vars = defaultdict(list)
        self._temp_var_num = 0
        self._stack_reg = 'rsp'

        self._stack_setup()

    @property
    def stack_reg(self):
        return self._stack_reg

    def add_data_directive(self):
        self._asm_insts.append(Directive('DATA'))
        self._inside_code = False

    def add_code_directive(self):
        self._asm_insts.append(Directive('CODE'))
        self._inside_code = True

    def _check_type(self, name, typ, value=None):
        if typ not in self._data_types:
            raise ValueError("Unsupported data type ", name, typ, value)

    def _add_directive(self, is_code):
        if self._inside_code and is_code is False:
            self.add_data_directive()
        elif not self._inside_code and is_code is True:
            self.add_code_directive()

    def has_param(self, name):
        return name in self._params

    def has_variable(self, name):
        return name in self._params or name in self._locals or name in self._temp_vars

    def has_local_variable(self, name):
        return name in self._locals or name in self._temp_vars

    def get_data_type(self, name):
        if name in self._locals:
            stack_offset, asm_typ_name, data_type = self._locals[name]
            return data_type
        if name in self._temp_vars:
            stack_offset, asm_typ_name, data_type = self._temp_vars[name]
            return data_type
        if name in self._params:
            value = self._params[name]
            # NOTE difference is because of param and param array, try to improve this
            if len(value) == 5:
                return value[3]
            else:
                return value[2]
        raise ValueError("Variable %s does't exist" % name)

    def add_param(self, name, asm_type, data_type, value=None):
        self._check_type(name, asm_type, value=value)
        self._add_directive(is_code=False)
        if name in self._params:
            raise ValueError("Parameter %s already exist" % name)
        self._params[name] = (name, asm_type, data_type, value)
        self._asm_insts.append(DataMembers([DataMember(name + VAR_SUFFIX, asm_type, value)]))

    def add_local(self, name, asm_type, data_type, stack_size):
        self._check_type(name, asm_type)
        if name in self._locals:
            stack_offset, l_asm_type, l_data_type = self._locals[name]
            if l_asm_type == asm_type:
                return stack_offset
            self._free_locals[l_data_type].append((stack_offset, l_asm_type, name + '_free'))
            del self._locals[name]
        elif name in self._temp_vars:
            stack_offset, l_asm_type, l_data_type = self._temp_vars[name]
            if l_asm_type == asm_type:
                return stack_offset

        # NOTE We first look in free variables for storage
        if data_type in self._free_locals and len(self._free_locals[data_type]) > 0:
            stack_offset, l_asm_type, l_name = self._free_locals[data_type].pop()
            self._locals[name] = (stack_offset, asm_type, data_type)
            return stack_offset

        # NOTE: New free variable is created
        stack_offset = self._stack_size
        self._locals[name] = (stack_offset, asm_type, data_type)
        self._stack_size += stack_size
        return stack_offset

    def get_local(self, name, asm_type, data_type):
        if name in self._locals:
            stack_offset, l_asm_type, l_data_type = self._locals[name]
        elif name in self._temp_vars:
            stack_offset, l_asm_type, l_data_type = self._temp_vars[name]
        else:
            raise ValueError("Variable %s doesn't exist" % name, asm_type, data_type)
        if l_asm_type != asm_type:
            raise ValueError("Variable %s has wrong type" % name, asm_type, data_type)
        return stack_offset

    def add_temp_var(self, asm_type, data_type, stack_size):
        if asm_type not in self._data_types:
            raise ValueError("Unsupported data type ", asm_type, data_type)

        if data_type in self._free_temp_vars and len(self._free_temp_vars[data_type]) > 0:
            stack_offset, asm_type, name = self._free_temp_vars[data_type].pop()
            self._temp_vars[name] = (stack_offset, asm_type, data_type)
            return name, stack_offset

        # Note: new temp variable is created
        stack_offset = self._stack_size
        name = 'temp_%i' % self._temp_var_num
        self._temp_var_num += 1
        self._temp_vars[name] = (stack_offset, asm_type, data_type)
        self._stack_size += stack_size
        return name, stack_offset

    def free_temp_variables(self):
        for name, (stack_offset, asm_type, data_type) in self._temp_vars.items():
            self._free_temp_vars[data_type].append((stack_offset, asm_type, name))
        self._temp_vars.clear()

    def add_param_array(self, name, asm_type, length, data_type, values=None):
        if asm_type not in self._data_types:
            raise ValueError("Unsupported data type ", name, asm_type, length, data_type, values)
        if name in self._params:
            raise ValueError("Parameter %s already exist" % name)
        self._add_directive(is_code=False)
        self._params[name] = (name, asm_type, length, data_type, values)
        self._asm_insts.append(DataMembers([ArrayMember(name + VAR_SUFFIX, asm_type, length, values)]))

    def add_inst_2_op(self, name, dst_size_name=None, dst_reg=None, dst_name=None, dst_offset=None,
                      src_size_name=None, src_reg=None, src_name=None, src_offset=None, src_value=None):
        if dst_name is not None:
            dst_name = dst_name + VAR_SUFFIX
        if src_name is not None:
            src_name = src_name + VAR_SUFFIX
        op1 = self._op(dst_size_name, dst_reg, dst_name, dst_offset)
        op2 = self._op(src_size_name, src_reg, src_name, src_offset, src_value)
        self._add_instruction(Instruction(name, op1, op2))

    def _op(self, type_size_name=None, reg=None, name=None, offset=None, value=None):
        if name is not None and type_size_name is not None:
            op = NameOperand(name, type_size_name, displacement=offset)
        elif reg is not None and type_size_name is not None:
            op = MemOperand(reg, type_size_name, displacement=offset)
        elif reg is not None:
            op = RegOperand(reg)
        elif value is not None and isinstance(value, int):
            op = ConstOperand(value)
        else:
            raise ValueError("Operand cannot be created", type_size_name, reg, name, offset, value)
        return op

    def _add_instruction(self, inst):
        self._add_directive(is_code=True)
        self._asm_insts.append(inst)

    def _stack_setup(self):
        # dynamic stack alignment on 64-byte boundary
        self._add_instruction(Instruction('push', RegOperand('r13')))
        self._add_instruction(Instruction('mov', RegOperand('r13'), RegOperand('rsp')))
        self._add_instruction(Instruction('and', RegOperand('rsp'), ConstOperand(-64)))
        self._add_instruction(None)  # code = 'sub rsp, %i \n' % stack_size
        self._stack_size_inst = len(self._asm_insts) - 1
        self._stack_size = 0

    def _stack_cleanup(self):
        #code = self.return_label() + ':\n'
        self._asm_insts[self._stack_size_inst] = Instruction('sub', RegOperand('rsp'), ConstOperand(self._stack_size))
        self._add_instruction(Instruction('mov', RegOperand('rsp'), RegOperand('r13')))
        self._add_instruction(Instruction('pop', RegOperand('r13')))

    def end(self):
        self._stack_cleanup()

    def get_instruction_seq(self):
        return self._asm_insts

    def asm(self):
        text = ''.join(i.to_str() for i in self._asm_insts)
        return text

    def register(self, regs_info):
        regs32 = ['edi', 'esi', 'edx', 'ecx', 'ebx', 'eax']
        regs64 = ['rdi', 'rsi', 'rdx', 'rcx', 'rbx', 'rax']
        xmm = ['xmm4', 'xmm3', 'xmm2', 'xmm1', 'xmm0']
        ymm = ['ymm4', 'ymm3', 'ymm2', 'ymm1', 'ymm0']
        regs = []
        for reg_type, reg_size in regs_info:
            if reg_type == RegType.GENERAL and reg_size == RegSize.BIT32:
                regs.append(regs32.pop())
            elif reg_type == RegType.GENERAL and reg_size == RegSize.BIT64:
                regs.append(regs64.pop())
            elif reg_type == RegType.SSE and reg_size == RegSize.BIT128:
                regs.append(xmm.pop())
            elif reg_type == RegType.SSE and reg_size == RegSize.BIT256:
                regs.append(ymm.pop())
        return regs

    def conv_int32_to_bool(self, dst_reg, src_reg, tmp_reg):
        self.add_inst_2_op('xor', dst_reg=tmp_reg, src_reg=tmp_reg)
        self.add_inst_2_op('mov', dst_reg=dst_reg, src_value=1)
        self.add_inst_2_op('cmp', dst_reg=src_reg, src_value=0)
        self.add_inst_2_op('cmove', dst_reg=dst_reg, src_reg=tmp_reg)

    def conv_int64_to_bool(self, dst_reg, src_reg, tmp_reg):
        regs_64 = {'rax': 'eax', 'rbx': 'ebx', 'rcx': 'ecx', 'rdx': 'edx'}
        tmp_reg = regs_64[tmp_reg]
        self.add_inst_2_op('xor', dst_reg=tmp_reg, src_reg=tmp_reg)
        self.add_inst_2_op('mov', dst_reg=dst_reg, src_value=1)
        self.add_inst_2_op('cmp', dst_reg=src_reg, src_value=0)
        self.add_inst_2_op('cmove', dst_reg=dst_reg, src_reg=tmp_reg)

    def conv_int64_to_int32(self, dst_reg, src_reg, tmp_reg):
        regs_64 = {'rax': 'eax', 'rbx': 'ebx', 'rcx': 'ecx', 'rdx': 'edx'}
        reg32 = regs_64[src_reg]
        self.add_inst_2_op('mov', dst_reg=dst_reg, src_reg=reg32)

    def conv_float_to_bool(self, dst_reg, src_reg, tmp_reg):
        reg15d = 'r15d'
        self.add_inst_2_op('xorpd', dst_reg=tmp_reg, src_reg=tmp_reg)
        self.add_inst_2_op('xor', dst_reg=reg15d, src_reg=reg15d)
        self.add_inst_2_op('mov', dst_reg=dst_reg, src_value=1)
        self.add_inst_2_op('ucomisd', dst_reg=src_reg, src_reg=tmp_reg)
        self.add_inst_2_op('cmove', dst_reg=dst_reg, src_reg=reg15d)
