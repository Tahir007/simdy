from tdasm import TdasmJit

from .generate_asm_seq import create_assembly_seq
from .types import return_type_getter_names, return_type_code_getter_name, return_type_codes, return_data_type
from .asm import VAR_SUFFIX


class CallableKernel:
    def __init__(self, function):
        self._function = function
        self._code = function.__code__
        self._pnames = self._code.co_varnames[:self._code.co_argcount]
        self._asm_params_names = [name + VAR_SUFFIX for name in self._pnames]
        self._runable_kernels = {}

        ptypes = self._get_types()
        if len(ptypes) == self._code.co_argcount:
            self._create_jit(ptypes)

    def _create_jit(self, ptypes):
        seq = create_assembly_seq(self._pnames, ptypes, self._function)
        jit = TdasmJit(seq)
        self._runable_kernels[ptypes] = jit
        return jit

    def _get_types(self):
        ptypes = []
        for name in self._pnames:
            if name in self._function.__annotations__:
                ptype = self._function.__annotations__[name]
                ptypes.append(ptype)
        return tuple(ptypes)

    def _get_jit(self, ptypes):
        if ptypes in self._runable_kernels:
            jit = self._runable_kernels[ptypes]
        else:
            jit = self._create_jit(ptypes)
        return jit

    def __call__(self, *args, **kwargs):
        if self._code.co_argcount != len(args):
            # TODO - Improve error message
            raise TypeError("%s() Wrong number of arguments!" % self._function.__name__)
        ptypes = tuple(type(arg) for arg in args)
        jit = self._get_jit(ptypes)
        for name, val in zip(self._asm_params_names, args):
            jit.set_param(name, val)

        jit.run()
        ret_val_type_code = jit.get_param(return_type_code_getter_name)
        if ret_val_type_code == return_type_codes[type(None)]:
            return None
        asm_value = jit.get_param(return_type_getter_names[ret_val_type_code])
        py_value = return_data_type[ret_val_type_code](asm_value)
        return py_value


def kernel(*args, **kwargs):
    if len(args) == 1 and len(kwargs) == 0 and callable(args[0]):
        # called as @decorator
        function = args[0]
        return CallableKernel(function)
    else:
        raise NotImplementedError("With arguments")
