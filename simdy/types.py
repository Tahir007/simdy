
from .asm import VAR_SUFFIX


simdy_types = {}
return_type_codes = {}
return_type_names = {}
return_type_getter_names = {}
asm_typ_to_desc_type = {}
return_type_code_name = 'return_type_code'
return_type_code_getter_name = 'return_type_code' + VAR_SUFFIX
return_data_type = {}


def register_simdy_type(data_type, asm_desc_type):
    simdy_types[data_type] = asm_desc_type
    asm_typ_to_desc_type[asm_desc_type.asm_type_name()] = asm_desc_type

    code = max(return_type_codes.values(), default=0) + 1
    return_type_codes[data_type] = code
    return_data_type[code] = data_type
    data_type_name = 'NoneType' if data_type is None else data_type.__name__
    return_type_names[code] = 'return_val_name_' + data_type_name
    return_type_getter_names[code] = 'return_val_name_' + data_type_name + VAR_SUFFIX


def create_asm_desc_type(name, data_type, value=None, is_const=False):
    if data_type not in simdy_types:
        raise ValueError("Not supported data type ", name, data_type, value)
    return simdy_types[data_type](name, value, is_const)
